<?php

namespace Site\Bundle\BackendBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class UsersControllerTest extends WebTestCase
{
    public function testIndex()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/admin/users');
    }

    public function testAdd()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/admin/users/add');
    }

    public function testEdit()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/admin/users/edit/{id}');
    }

    public function testDelete()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/admin/users/delete/{id}');
    }

    public function testPermissions()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/admin/users/permssions/{id}');
    }

    public function testView()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/admin/users/view/{id}');
    }

}
