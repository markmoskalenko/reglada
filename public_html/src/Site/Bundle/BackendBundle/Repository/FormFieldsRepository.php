<?php

namespace Site\Bundle\BackendBundle\Repository;

use Doctrine\ORM\EntityRepository;


class FormFieldsRepository extends EntityRepository
{   

	public function getFields($keys){
        $em = $this->getEntityManager();
        
        $qb = $em->createQueryBuilder();
        $qb->select('b')
            ->from('SiteBackendBundle:FormFields','b')
            ->where('b.id IN ('.implode(',', $keys).') ')
            ->orderBy('b.pos', 'ASC');
            
        $fields = $qb->getQuery()->getResult();
        
        return $fields;
	}

    public function getFieldsArray($keys){
        $em = $this->getEntityManager();
        
        $qb = $em->createQueryBuilder();
        $qb->select('b')
            ->from('SiteBackendBundle:FormFields','b')
            ->where('b.id IN ('.implode(',', $keys).') ')
            ->orderBy('b.pos', 'ASC');
            
        $fields = $qb->getQuery()->getArrayResult();
        
        return $fields;
    }
}