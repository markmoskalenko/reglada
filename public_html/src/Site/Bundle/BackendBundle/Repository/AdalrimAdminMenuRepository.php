<?php

namespace Site\Bundle\BackendBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * AdalrimAdminMenuRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class AdalrimAdminMenuRepository extends EntityRepository
{   
    public function getMenu($permission = array())
    {
		$menu = array();
		$allowed = implode(',',array_keys($permission));
		$em = $this->getEntityManager();
		$connection = $em->getConnection();
		if ( empty($permission) )
			$query = "SELECT * FROM adalrim_admin_menu where status=1  and delta=0";
		else
			$query = "SELECT * FROM adalrim_admin_menu where status=1 and delta=0 and id in ($allowed)";
		$statement = $connection->prepare($query);
		$statement->execute();
		$results = $statement->fetchAll();
		$permissions = array();
		foreach ($results as $key => $value) {
			$menu[$value['action']]['sub'] = array();
			$menu[$value['action']]['title'] = $value['title'];
			$menu[$value['action']]['id'] = $value['id'];
			if ( !empty($permission) )
				$permissions[$value['id']] = array($value['read'],$value['create'],$value['edit'],$value['delete']);
			else
				$menu[$value['action']]['perms'] = array($value['read'],$value['create'],$value['edit'],$value['delete']);

			    $connection = $em->getConnection();
			    if ( empty($permission) )
			    	$query = "SELECT * FROM adalrim_admin_menu where status=1 and parent = ".$value['id'];
			    else
					$query = "SELECT * FROM adalrim_admin_menu where status=1 and parent = ".$value['id']." and id in ($allowed)";
	    		$statement = $connection->prepare($query);
	    		$statement->execute();
	    		$subResults = $statement->fetchAll();
	    		foreach ($subResults as $k => $v) {
	    			$menu[$value['action']]['sub'][$v['action']] = array();
	    			$menu[$value['action']]['sub'][$v['action']]['title'] = $v['title'];
	    			$menu[$value['action']]['sub'][$v['action']]['id'] = $v['id'];
	    			$menu[$value['action']]['sub'][$v['action']]['sub'] = array();
					if ( !empty($permission) )
	    				$permissions[$v['id']] = array($v['read'],$v['create'],$v['edit'],$v['delete']);
					else
						$menu[$value['action']]['sub'][$v['action']]['perms'] = array($v['read'],$v['create'],$v['edit'],$v['delete']);

	    			    $connection = $em->getConnection();
			    		if ( empty($permission) )
							$query = "SELECT * FROM adalrim_admin_menu where status=1 and parent = ".$v['id'];
						else
							$query = "SELECT * FROM adalrim_admin_menu where status=1 and parent = ".$v['id']." and id in ($allowed)";
			    		$statement = $connection->prepare($query);
			    		$statement->execute();
			    		$subSubResults = $statement->fetchAll();
			    		foreach ($subSubResults as $x => $y) {
			    			$menu[$value['action']]['sub'][$v['action']]['sub'][$y['action']] = array();
			    			$menu[$value['action']]['sub'][$v['action']]['sub'][$y['action']]['title'] = $y['title'];
			    			$menu[$value['action']]['sub'][$v['action']]['sub'][$y['action']]['id'] = $y['id'];
							if ( !empty($permission) )
			    				$permissions[$y['id']] = array($y['read'],$y['create'],$y['edit'],$y['delete']);
							else
								$menu[$value['action']]['sub'][$v['action']]['sub'][$y['action']]['perms'] = array($y['read'],$y['create'],$y['edit'],$y['delete']);

			    		}

	    		}
		}
		return array('menu'=>$menu,'perms'=>$permissions);
    }

    public function getMenuSideBar($permission = array())
    {
		$menu = array();
		$allowed = implode(',',array_keys($permission));
		$em = $this->getEntityManager();
		$connection = $em->getConnection();
		if ( empty($permission) )
			$query = "SELECT * FROM adalrim_admin_menu where status=1 and delta=0 and showed=1";
		else
			$query = "SELECT * FROM adalrim_admin_menu where status=1 and delta=0  and showed=1 and id in ($allowed)";
		$statement = $connection->prepare($query);
		$statement->execute();
		$results = $statement->fetchAll();
		$permissions = array();
		foreach ($results as $key => $value) {
			$menu[$value['action']]['sub'] = array();
			$menu[$value['action']]['title'] = $value['title'];
			$menu[$value['action']]['id'] = $value['id'];
			if ( !empty($permission) )
				$permissions[$value['id']] = array($value['read'],$value['create'],$value['edit'],$value['delete']);
			else
				$menu[$value['action']]['perms'] = array($value['read'],$value['create'],$value['edit'],$value['delete']);

			    $connection = $em->getConnection();
			    if ( empty($permission) )
			    	$query = "SELECT * FROM adalrim_admin_menu where status=1  and showed=1 and parent = ".$value['id'];
			    else
					$query = "SELECT * FROM adalrim_admin_menu where status=1  and showed=1 and parent = ".$value['id']." and id in ($allowed)";
	    		$statement = $connection->prepare($query);
	    		$statement->execute();
	    		$subResults = $statement->fetchAll();
	    		foreach ($subResults as $k => $v) {
	    			$menu[$value['action']]['sub'][$v['action']] = array();
	    			$menu[$value['action']]['sub'][$v['action']]['title'] = $v['title'];
	    			$menu[$value['action']]['sub'][$v['action']]['id'] = $v['id'];
	    			$menu[$value['action']]['sub'][$v['action']]['sub'] = array();
					if ( !empty($permission) )
	    				$permissions[$v['id']] = array($v['read'],$v['create'],$v['edit'],$v['delete']);
					else
						$menu[$value['action']]['sub'][$v['action']]['perms'] = array($v['read'],$v['create'],$v['edit'],$v['delete']);

	    			    $connection = $em->getConnection();
			    		if ( empty($permission) )
							$query = "SELECT * FROM adalrim_admin_menu where status=1 and showed=1 and parent = ".$v['id'];
						else
							$query = "SELECT * FROM adalrim_admin_menu where status=1 and showed=1 and parent = ".$v['id']." and id in ($allowed)";
			    		$statement = $connection->prepare($query);
			    		$statement->execute();
			    		$subSubResults = $statement->fetchAll();
			    		foreach ($subSubResults as $x => $y) {
			    			$menu[$value['action']]['sub'][$v['action']]['sub'][$y['action']] = array();
			    			$menu[$value['action']]['sub'][$v['action']]['sub'][$y['action']]['title'] = $y['title'];
			    			$menu[$value['action']]['sub'][$v['action']]['sub'][$y['action']]['id'] = $y['id'];
							if ( !empty($permission) )
			    				$permissions[$y['id']] = array($y['read'],$y['create'],$y['edit'],$y['delete']);
							else
								$menu[$value['action']]['sub'][$v['action']]['sub'][$y['action']]['perms'] = array($y['read'],$y['create'],$y['edit'],$y['delete']);

			    		}
			    		if(count($subSubResults) == 0 && $v['target'] != 1){
			    			unset($menu[$value['action']]['sub'][$v['action']]);
			    		}

	    		}
	    		if(count($subResults) == 0 && $value['target'] != 1){
	    			unset($menu[$value['action']]);
	    		}
		}
		return array('menu'=>$menu,'perms'=>$permissions);
    }

    public function getPermissions($role){
		$em = $this->getEntityManager();
		$em = $this->getEntityManager();
		$connection = $em->getConnection();
     	$query = "SELECT * FROM user_roles where rolekey = '".$role."'";
		$statement = $connection->prepare($query);
		$statement->execute();
		$results = $statement->fetchAll();
		$result = unserialize($results[0]['perms']);
		return $result;
    }
}