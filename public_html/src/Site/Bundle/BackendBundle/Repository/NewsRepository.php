<?php

namespace Site\Bundle\BackendBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Site\Bundle\BackendBundle\Utils\Utils;
/**
 * CssSchemesRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class NewsRepository extends EntityRepository
{   

	public function getNews($region, $unsettop = false){
		$regionNews = array();
		$top = array();
		$settings = Utils::settings('news');
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('i')->from('SiteBackendBundle:News', 'i')
                    ->where('i.status = :status')->setParameter('status', 1)
                    ->orderBy('i.date','DESC');
        $result = $qb->getQuery()->getResult();
        if( count($result) > 0 ){
        	
			foreach ($result as $key => $item) {
				if( in_array($region, $item->getRegions())){
					$regionNews[$item->getId()] = $item;
					if( $item->getTop() == 1 ){
						$top[$item->getId()] = $item;
					}
				}
			}
			if( count($top) == 0 && count($regionNews) > 0 ){
				$top[] = $result[0];
			}
			if( count($top) >= 1 ){
				foreach ($top as $key => $value) {
					$top = $value;
					break;
				}
			}
			$i = 0;
			$limit = $settings['items_per_page_news_one'] + 1;
			if ( $unsettop == true ){
				unset($regionNews[$top->getId()]);
				$i = 1;
				$limit = $settings['items_per_page_news_all'] - 1;
			}
			$newarray = array();
			foreach ($regionNews as $key => $value) {
				$i++;
				$newarray[$value->getId()] = $value;
				if( $i > $limit )
					break;
				
			}
			$newarray['top'] = $top;
		}
		return $newarray;
	}

	public function getNewsByPage($page, $region){
		$page = (int) $page;
		$regionNews = array();
		$top = array();
		$settings = Utils::settings('news');
		$start = $page-1;
		$limit = $settings['items_per_page_news_previous'];
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('i')->from('SiteBackendBundle:News', 'i')
                    ->where('i.status = :status')->setParameter('status', 1)
                    ->orderBy('i.date','DESC')
                    ->setFirstResult($limit*$start)
                    ->setMaxResults($limit);
        $result = $qb->getQuery()->getArrayResult();
		return $result;
	}

	public function countActive($region){
		$regionNews = array();
		$settings = Utils::settings('news');
		$limit = $settings['items_per_page_news_previous'];
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('i')->from('SiteBackendBundle:News', 'i')
                    ->where('i.status = :status')->setParameter('status', 1)
                    ->orderBy('i.date','DESC');
        $result = $qb->getQuery()->getArrayResult();
		return count($result);
	}
	
}