<?php

namespace Site\Bundle\BackendBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * RegionsRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class RegionsRepository extends EntityRepository
{   

	public function getRegionUsers($id){
        $qb = $this->getEntityManager()->createQueryBuilder();
        
        $qb->select('ru.regionId','u.email','u.roles','u.fio','u.username','u.id','u.enabled')
                    ->from('SiteBackendBundle:RegionsUsers', 'ru')
                    ->leftJoin('SiteBackendBundle:Regions','r','WITH','ru.regionId = r.id')
                    ->leftJoin('SiteBackendBundle:User','u','WITH','ru.userId = u.id')
                    ->where('ru.regionId = :id')->setParameter('id',$id)
                    ->orderBy('u.id','ASC');
		$result = $qb->getQuery()->getArrayResult();
		foreach ($result as $key => $value) {
			//$result[$key]['roles'] = unserialize($value['roles']);
		}
		return $result;
	}

}