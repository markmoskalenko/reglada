<?php

namespace Site\Bundle\BackendBundle\Handler;

use Symfony\Component\Security\Core\Event\AuthenticationFailureEvent;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Site\Bundle\BackendBundle\Utils\Utils;

class AuthenticationSuccessHandler
{
    public function onAuthenticationFailure( AuthenticationFailureEvent $event )
    {
        // executes on failed login
    }
    
    public function onAuthenticationSuccess( InteractiveLoginEvent $event )
    {
        $kernel = $GLOBALS['kernel'];
        $container = $kernel->getContainer();
        
        $roles = $container->get('security.context')->getToken()->getUser()->getRoles();
        
        $em = $container->get('doctrine.orm.entity_manager');
        
        $qb = $em->createQueryBuilder();
        $qb->select('r')
            ->from('Site\Bundle\BackendBundle\Entity\UserRoles', 'r')
            ->where($qb->expr()->in('r.rolekey', $roles));
        
        $user_roles = $qb->getQuery()->getResult();
        
        $perms = array();
        $permissions = array();//d.v
        $permissionsTemp = array();//d.v
        foreach($user_roles as $ur)
        {
            $permissionsTemp =  Utils::userSelectedRolePermissions($ur->getId(),true);//d.v
            $permissions = array_merge($permissions,$permissionsTemp);//d.v
            foreach(unserialize($ur->getPerms()) as $p)
            {
                $perms[] = $p;
            }
        }
        
        $container->get('session')->set('user_perms',serialize($perms));
        $container->get('session')->set('admin.permissions', $permissions);//d.v
        $referer = $container->get('request')->headers->get('referer');
        return new RedirectResponse('/admin');
        //return new RedirectResponse($referer);
    }
}

/*use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\Security\Http\Authentication\DefaultAuthenticationSuccessHandler;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;

class AuthenticationSuccessHandler extends DefaultAuthenticationSuccessHandler
{
    public function __construct(SecurityContext $securityContext, $session)
    {
        parent::__construct( $httpUtils, $options );
        
        $this->securityContext = $securityContext;
        $this->em = $em;
        $this->session = $session;
    }
    
    
    public function onAuthenticationSuccess(Request $request, TokenInterface $token)
    {
        $qb = $this->em->createQueryBuilder()
            ->select('r')
            ->from('Site\Bundle\BackendBundle\Entity\UserRoles', 'r');
        $all_roles = $qb->getQuery()->getResult();
        $user_roles = $token->getUser()->getRoles();
        
        $perms = array();
        foreach($all_roles as $ar)
        {
            if(in_array($ar->getRolekey(), $user_roles))
            {
                $tmperms = unserialize($ar->getPerms());
                if(is_array($tmperms) && count($tmperms) > 0)
                {
                    foreach($tmperms as $k => $v)
                    {
                        if((isset($perms[$k]) && ($perms[$k]*1) < ($v*1)) || !isset($perms[$k]))
                            $perms[$k] = $v;
                    }
                }
            }
        }
        
        $this->session->set('user_perms',serialize($perms));
                 $referer = $request->headers->get('referer');
        return new RedirectResponse($referer);       
        //return parent::onAuthenticationSuccess($request, $token);
    }
}*/