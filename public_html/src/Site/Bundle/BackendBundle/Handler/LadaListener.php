<?php

namespace Site\Bundle\BackendBundle\Handler;

use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Site\Bundle\BackendBundle\Utils\Utils;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\HttpKernel\HttpKernel;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;

class LadaListener
{
    protected $container;
    protected $context;

    public function __construct(SecurityContext $context, ContainerInterface $container,$router) // this is @service_container
    {
        $this->container = $container;
        $this->context = $context;
        $this->router = $router;
    }

    public function onKernelRequest(GetResponseEvent $event)
    {
        $kernel    = $event->getKernel();
        $request   = $event->getRequest();
        $container = $this->container;
        $session   = $request->getSession();
        
        if($session && $session->get('user_perms')){
            $user_perms = unserialize($session->get('user_perms'));
            $adminPermissions = $session->get('admin.permissions');//d.v
        }
        else{
            // $url = $this->router->generate('index');
            // $event->setResponse(new RedirectResponse($url));
            $user_perms = array();
            $adminPermissions = array();
            //d.v
        }
        /* start d.v correction */
        if ( 0 == Utils::checkPermissions() ){
                $url = $this->router->generate('admin_index');
                $event->setResponse(new RedirectResponse($url));
        }
        if ( false == Utils::ifRegionsExists() ){
                $event->setResponse(new RedirectResponse('http://reglada.ru'));
        }
        /* end d.v correction */

        // if(!$container->get('security.context')->isGranted('ROLE_ADMIN') && $request->getPathInfo() != '/login_check')
        // {
        //     $all_perms = Utils::userRolesPermissions();
            
        //     $router = $this->container->get("router");
        //     $route = $router->match($request->getPathInfo());
        //     $routeName = $route['_route'];
            
        //     $allow = array();
            
        //     $all_control = array();
        //     foreach($all_perms as $perm)
        //     {
        //         foreach($perm['routes'] as $k => $v)
        //         {
        //             $all_control[] = $k;
        //         }
        //     }
            
            
        //     // if(in_array($routeName, $all_control) && !in_array($routeName, $user_perms))
        //     // {
        //     //     die('DONT ACCESS');
        //     // }
        // }
    }

    public function onKernelResponse(FilterResponseEvent $event)
    {
        $response  = $event->getResponse();
        $request   = $event->getRequest();
        $kernel    = $event->getKernel();
        $container = $this->container;


        /*switch ($request->query->get('option')) {
            case 2:
                $response->setContent('Blah');
                break;

            case 3:
                $response->headers->setCookie(new Cookie('test', 1));
                break;
        }*/
    }
}