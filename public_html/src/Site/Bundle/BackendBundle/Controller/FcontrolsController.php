<?php

namespace Site\Bundle\BackendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Site\Bundle\BackendBundle\Form\BaseAutoSliderSlideForm;
use Site\Bundle\BackendBundle\Entity\BaseAutoSliderSlides;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class FcontrolsController extends Controller
{
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $qb = $em->createQueryBuilder()
            ->select(array('i.id','i.name as name','i.groupId','g.name as grname','i.pos','i.pages','i.identifier'))
            ->from('Site\Bundle\BackendBundle\Entity\Info', 'i')
            ->leftJoin('Site\Bundle\BackendBundle\Entity\InfoGroups', 'g','WITH', 'i.groupId = g.id')
            ->orderBy('i.pos','ASC');
        $features = $qb->getQuery()->getArrayResult();
        $featuress = array();
        foreach ($features as $key => $value) {
            $features[$key]['pages'] = unserialize($value['pages']);
            $value['pages'] = unserialize($value['pages']);
            if( !isset($featuress[$value['groupId']])){
                $featuress[$value['groupId']] = array();
                $featuress[$value['groupId']]['name'] = $value['grname'];
                $featuress[$value['groupId']]['features'] = array();
            }
            $featuress[$value['groupId']]['features'][$key] = $value;
        }
        //$features = $em->getRepository('SiteBackendBundle:Info')->findAll();
       
        return $this->render('SiteBackendBundle:Fcontrols:index.html.twig', array(
                'features' => $features,
                'featuress' => $featuress,
            ));
    }

    public function ajaxChangevalueAction(Request $request){
        $em = $this->getDoctrine()->getManager();
        $post = $request->request->all();
        $id = $post['id'];
        $feature = $em->getRepository('SiteBackendBundle:Info')->find($id);
        if( $feature ){
            $feature->setPages(serialize($post['pages']));
            $feature->setIdentifier($post['identifier']);
        }
        else{
            return new response(0);
        }
        $em->persist($feature);
        $em->flush();
        return new response(1);
    }

    public function ajaxChangegroupAction(Request $request){
        $em = $this->getDoctrine()->getManager();
        $post = $request->request->all();
        $id = $post['id'];
        $feature = $em->getRepository('SiteBackendBundle:Info')->find($id);
        if( $feature ){
            $feature->setGroupId($post['groupId']);
        }
        else{
            return new response(0);
        }
        $em->persist($feature);
        $em->flush();
        return new response(1);
    }

    public function ajaxChangeposAction(Request $request){
        $em = $this->getDoctrine()->getManager();
        $post = $request->request->all();
        $features = $post['pos'];
        foreach ($features as $key => $value) {
            $feature = $em->getRepository('SiteBackendBundle:Info')->find($key);
            if( $feature ){
                $feature->setPos($value);
            }
            $em->persist($feature);
            $em->flush();
        }

        return new response(1);
    }
}
