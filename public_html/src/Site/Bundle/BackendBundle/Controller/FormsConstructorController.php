<?php

namespace Site\Bundle\BackendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Site\Bundle\BackendBundle\Form\SliderSlideForm;
use Site\Bundle\BackendBundle\Entity\FormFields;
use Site\Bundle\BackendBundle\Entity\FormsConstructor;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class FormsConstructorController extends Controller
{
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        
        $qb = $em->createQueryBuilder();
        $qb->select('s')
            ->from('SiteBackendBundle:FormsConstructor','s');
        $forms = $qb->getQuery()->getResult();
        
        return $this->render('SiteBackendBundle:FormsConstructor:index.html.twig', array(
                'forms' => $forms,
            ));   
    }

    public function addAction(){
        $em = $this->getDoctrine()->getManager();
        $qb = $em->createQueryBuilder();
        $qb->select('f')
            ->from('SiteBackendBundle:FormFields','f')->orderBy('f.pos','ASC');
        $fields = $qb->getQuery()->getResult();
        return $this->render('SiteBackendBundle:FormsConstructor:add.html.twig', array(
                'fields' => $fields,
            ));  
    }

    public function ajaxAddAction(Request $request){
        $em = $this->getDoctrine()->getManager();
        $post = $request->request->all();
        if($post['options'])
            $post['options'] = serialize($post['options']);
        $form = new FormsConstructor();
        foreach ($post as $key => $value) {
            $func = "set".ucfirst($key);
            $form->{$func}($value);
        }
        $em->persist($form);
        $em->flush();
        return new Response('true');
    }

    public function ajaxEditAction(Request $request){
        $em = $this->getDoctrine()->getManager();
        $post = $request->request->all();
        if(isset($post['options']))
            $post['options'] = serialize($post['options']);
        else
            $post['options'] = null;
        $form = $em->getRepository('SiteBackendBundle:FormsConstructor')->find($post['id']);
        unset($post['id']);
        foreach ($post as $key => $value) {
            $func = "set".ucfirst($key);
            $form->{$func}($value);
        }
        $em->persist($form);
        $em->flush();
        return new Response('true');
    }

    public function editAction($id){
        $em = $this->getDoctrine()->getManager();
        $qb = $em->createQueryBuilder();
        $qb->select('f')
            ->from('SiteBackendBundle:FormFields','f')->orderBy('f.pos','ASC');;
        $fields = $qb->getQuery()->getResult();

        $form = $em->getRepository('SiteBackendBundle:FormsConstructor')->find($id);
        return $this->render('SiteBackendBundle:FormsConstructor:edit.html.twig', array('fields'=>$fields,'form'=>$form));  
    }

    public function deleteAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $form = $em->getRepository('SiteBackendBundle:FormsConstructor')->find($id);
        if ($post = $request->request->all())
        {
            if( isset($post['delete']) ){
                $em->remove($form);
                $em->flush();
            }
            return $this->redirect($this->generateUrl('admin_formsconstructor_index'));

        }
        return $this->render('SiteBackendBundle:FormsConstructor:delete.html.twig', array(
                'form' => $form,
            ));     
    }
}
