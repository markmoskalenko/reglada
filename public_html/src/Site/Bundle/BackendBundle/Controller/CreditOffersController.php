<?php

namespace Site\Bundle\BackendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Site\Bundle\BackendBundle\Utils\Utils;
use Site\Bundle\BackendBundle\Entity\UserRoles;
use Site\Bundle\BackendBundle\Entity\CreditOffers;
use Site\Bundle\BackendBundle\Form\RolesForm;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class CreditOffersController extends Controller
{
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $rsite = Utils::getRegionsUser();
        $filter = array();
        if ( $rsite == false ){
            return $this->render('SiteBackendBundle:Errors:error.html.twig', array(
                    // ...
                )); 
        }
        else{
            $qb = $em->createQueryBuilder();
            $qb->select('c')
                ->from('SiteBackendBundle:CreditOffers','c');
            $offers = $qb->getQuery()->getArrayResult();
            return $this->render('SiteBackendBundle:CreditOffers:index.html.twig', array(
                    'offers' => $offers,
                ));      
        }
        

    }

    public function addAction(){
        $em = $this->getDoctrine()->getManager();
        $regions = $em->getRepository('SiteBackendBundle:Regions')->findAll();
        $cars = Utils::minimalCarsPrices();
        return $this->render('SiteBackendBundle:CreditOffers:add.html.twig', array(
                'cars' => $cars,
                'regions' => $regions,
            ));    
    }

    public function editAction($id,Request $request){
        $em = $this->getDoctrine()->getManager();
        $regions = $em->getRepository('SiteBackendBundle:Regions')->findAll();
        $qb = $em->createQueryBuilder();
        $qb->select('c')
            ->from('SiteBackendBundle:CreditOffers','c')->where('c.id = '.$id);
        $offers = $qb->getQuery()->getArrayResult();
        $offers[0]['tax'] = unserialize($offers[0]['tax']);
        $offers[0]['auto'] = unserialize($offers[0]['auto']);
        $cars = Utils::minimalCarsPrices();
        return $this->render('SiteBackendBundle:CreditOffers:edit.html.twig', array(
                'offer'=>$offers[0],
                'cars' => $cars,
                'regions' => $regions,
            ));    
    }

    public function ajaxAddAction(Request $request){
        $em = $this->getDoctrine()->getManager();
        $post = $request->request->all();
        $post['auto'] = $post['cars'];
        unset($post['cars']);
        $offer = new CreditOffers();
        foreach ($post as $key => $value) {
            $func = "set".ucfirst($key);
            $offer->{$func}($value);
        }
        $offer->setDateCreated(time());
        $offer->setDateUpdated(time());
        $em->persist($offer);
        $em->flush();
        return new Response('true');
    }

    public function deleteAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $offer = $em->getRepository('SiteBackendBundle:CreditOffers')->find($id);
        if ($post = $request->request->all())
        {
            if( isset($post['delete']) ){
                $em->remove($offer);
                $em->flush();
            }
            return $this->redirect($this->generateUrl('admin_creditoffers_index'));

        }
        return $this->render('SiteBackendBundle:CreditOffers:delete.html.twig', array(
                'offer' => $offer,
            ));     
    }

    public function ajaxEditAction(Request $request){
        $em = $this->getDoctrine()->getManager();
        $post = $request->request->all();
        $offer = $em->getRepository('SiteBackendBundle:CreditOffers')->find($post['id']);
        $post['auto'] = $post['cars'];
        unset($post['cars']);
        unset($post['id']);
        foreach ($post as $key => $value) {
            $func = "set".ucfirst($key);
            $offer->{$func}($value);
        }
        $offer->setDateCreated(time());
        $offer->setDateUpdated(time());
        $em->persist($offer);
        $em->flush();
        return new Response('true');
    }
}
