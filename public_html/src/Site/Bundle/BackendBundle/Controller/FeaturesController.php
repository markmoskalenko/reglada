<?php

namespace Site\Bundle\BackendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Site\Bundle\BackendBundle\Form\FeaturesGroupsForm;
use Site\Bundle\BackendBundle\Entity\InfoGroups;
use Site\Bundle\BackendBundle\Form\FeaturesForm;
use Site\Bundle\BackendBundle\Entity\Info;
use Site\Bundle\BackendBundle\Form\FeaturesValuesForm;
use Site\Bundle\BackendBundle\Entity\InfoValue;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class FeaturesController extends Controller
{
    public function featuresgroupsAction($type, $id = false, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        
        $qb = $em->createQueryBuilder();
        
        $qb->select('ig')
            ->from('SiteBackendBundle:InfoGroups', 'ig')
            ->where("ig.gtype = '$type'");
            
        $groups = $qb->getQuery()->getResult();
        
        $qb = $em->createQueryBuilder();
                
        $qb->select('f')
            ->from('SiteBackendBundle:Info', 'f')
            ->where("f.infoType = '$type'");
            
        if($id !== false)
        {
            $qb->andWhere('f.groupId = '.$id);
        }
        $features = $qb->getQuery()->getResult();
        return $this->render('SiteBackendBundle:Features:featuresgroups.html.twig', array(
                'groups' => $groups,
                'features' => $features,
                'type' => $type
            ));
    }
        

    public function addfeatureAction($type, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $info = new Info();
        
        
        $qb = $em->createQueryBuilder();
        
        $qb->select('ig')
            ->from('SiteBackendBundle:InfoGroups', 'ig')
            ->where("ig.gtype = '$type'");
            
        $groups = $qb->getQuery()->getResult();
        
        $gp = array(0 => 'Без группы');
        foreach($groups as $group)
        {
            $gp[$group->getId()] = $group->getName();
        }
        
        
		$form   = $this->createForm(new FeaturesForm($gp), $info);
        
        if ($post = $request->request->all())
        {
            $form->submit($request);
            
            $info->setInfoType($type);
            
            $em->persist($info);
            $em->flush();
            
            return $this->redirect($this->generateUrl('admin_featuresgroups_editfeature', array('id' => $info->getId(), 'type' => $type)));
        }
           
        return $this->render('SiteBackendBundle:Features:addfeature.html.twig', array(
            'form'   => $form->createView(),
            'type' => $type
            ));
    }

    public function addgroupAction($type, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $group = new InfoGroups();
        
		$form   = $this->createForm(new FeaturesGroupsForm(), $group);
        
        if ($post = $request->request->all())
        {
            $form->submit($request);
            
            $group->setGtype($type);
            
            $em->persist($group);
            $em->flush();
            
            return $this->redirect($this->generateUrl('admin_featuresgroups_editgroup', array('id' => $group->getId(), 'type' => $type)));
        }
           
        return $this->render('SiteBackendBundle:Features:addgroup.html.twig', array(
            'form'   => $form->createView(),
            'type' => $type
            ));    
    }

    public function editfeatureAction($id, $type, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $info = $em->getRepository('SiteBackendBundle:Info')->find($id);
        
        
        $qb = $em->createQueryBuilder();
        
        $qb->select('ig')
            ->from('SiteBackendBundle:InfoGroups', 'ig')
            ->where("ig.gtype = '$type'");
            
        $groups = $qb->getQuery()->getResult();
        
        $gp = array(0 => 'Без группы');
        foreach($groups as $group)
        {
            $gp[$group->getId()] = $group->getName();
        }
        
        
		$form   = $this->createForm(new FeaturesForm($gp), $info);
        
        if ($post = $request->request->all())
        {
            $form->submit($request);
            
            $info->setInfoType($type);
            
            $em->persist($info);
            $em->flush();
            
            return $this->redirect($this->generateUrl('admin_featuresgroups_editfeature', array('id' => $info->getId(), 'type' => $type)));
        }
           
        return $this->render('SiteBackendBundle:Features:editfeature.html.twig', array(
            'form'   => $form->createView(),
            'type' => $type
            ));
            
    }

    public function editgroupAction($id, $type, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $group = $em->getRepository('SiteBackendBundle:InfoGroups')->find($id);
        
		$form   = $this->createForm(new FeaturesGroupsForm(), $group);
        
        if ($post = $request->request->all())
        {
            $form->submit($request);
            
            $group->setGtype($type);
            
            $em->persist($group);
            $em->flush();
            
            return $this->redirect($this->generateUrl('admin_featuresgroups_editgroup', array('id' => $group->getId(), 'type' => $type)));
        }
        
        return $this->render('SiteBackendBundle:Features:editgroup.html.twig', array(
            'form'   => $form->createView(),
            'type' => $type
            ));
    }

    public function deletefeatureAction($id, $type)
    {
        $em = $this->getDoctrine()->getManager();
        $info = $em->getRepository('SiteBackendBundle:Info')->find($id);
        
        $to_del = $em->getRepository('SiteBackendBundle:InfoValue')->findBy(array('infoId' => $id));
        foreach($to_del as $del)
        {
            $em->remove($del);
            $em->flush();
        }
        $to_del = $em->getRepository('SiteBackendBundle:CarsInfo')->findBy(array('infoId' => $id));
        foreach($to_del as $del)
        {
            $em->remove($del);
            $em->flush();
        }
        
        $em->remove($info);
        $em->flush();
        
        return $this->redirect($this->generateUrl('admin_featuresgroups', array('type' => $type)));
    }

    public function deletegroupAction($id)
    {
        return $this->render('SiteBackendBundle:Features:deletegroup.html.twig', array(
                // ...
            ));    }

    public function editfeaturevalueAction($featureid, $id, $type, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $infovalue = $em->getRepository('SiteBackendBundle:InfoValue')->find($id);
        
		$form   = $this->createForm(new FeaturesValuesForm(), $infovalue);
        
        if ($post = $request->request->all())
        {
            $form->submit($request);
            
            $infovalue->setInfoId($featureid);
            
            $em->persist($infovalue);
            $em->flush();
            
            return $this->redirect($this->generateUrl('admin_featuresgroups_editfeaturevalue', array('featureid' => $featureid, 'id' => $infovalue->getId())));
        }
           
        return $this->render('SiteBackendBundle:Features:editfeaturevalue.html.twig', array(
            'form'   => $form->createView(),
            'type' => $type
            ));
    }

    public function addfeaturevalueAction($featureid, $type, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $infovalue = new InfoValue();
        
		$form   = $this->createForm(new FeaturesValuesForm(), $infovalue);
        
        if ($post = $request->request->all())
        {
            $form->submit($request);
            
            $infovalue->setInfoId($featureid);
            
            $em->persist($infovalue);
            $em->flush();
            
            return $this->redirect($this->generateUrl('admin_featuresgroups_editfeaturevalue', array('featureid' => $featureid, 'id' => $infovalue->getId(), 'type' => $type)));
        }
           
        return $this->render('SiteBackendBundle:Features:addfeaturevalue.html.twig', array(
            'form'   => $form->createView(),
            'type' => $type
            ));
    }

    public function deletefeaturevalueAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        
        $to_del = $em->getRepository('SiteBackendBundle:CarsInfo')->findBy(array('valueId' => $id));
        foreach($to_del as $del)
        {
            $em->remove($del);
            $em->flush();
        }
        
        $model = $em->getRepository('SiteBackendBundle:InfoValue')->find($id);
        
        $iid = $model->getInfoId();
        
        $em->remove($model);
        $em->flush();
        
        return $this->redirect($this->generateUrl('admin_featuresgroups_viewvalues', array('id' => $iid)));
    }

    public function viewvaluesAction($id, $type, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        
        $values = $em->getRepository('SiteBackendBundle:InfoValue')->findByInfoId($id);
        
        return $this->render('SiteBackendBundle:Features:viewvalues.html.twig', array(
                'values' => $values,
                'type' => $type
            ));    }

}
