<?php

namespace Site\Bundle\BackendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Site\Bundle\BackendBundle\Form\BaseAutoGalleryForm;
use Site\Bundle\BackendBundle\Entity\BaseAutoGallery;

class BaseAutoGalleryController extends Controller
{
    public function indexAction($bodyid)
    {
        $em = $this->getDoctrine()->getManager();
        
        $gallery = $em->getRepository('SiteBackendBundle:BaseAutoGallery')->findBy(array('carId' => $bodyid));
        
        return $this->render('SiteBackendBundle:BaseAutoGallery:index.html.twig', array(
                'gallery' => $gallery,
                'bodyid' => $bodyid
            ));
    }

    public function addAction($bodyid, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $gallery = new BaseAutoGallery();
        
		$form = $this->createForm(new BaseAutoGalleryForm(), $gallery);
        
        if($post = $request->request->all())
        {
            $form->submit($request);
            $gallery->setCarId($bodyid);
            
            $em->persist($gallery);
            $em->flush();
            return $this->redirect($this->generateUrl('admin_auto_gallery_edit', array('bodyid' => $bodyid, 'id' => $gallery->getId())));
        }
        
        
        return $this->render('SiteBackendBundle:BaseAutoGallery:add.html.twig', array(
                'form'   => $form->createView(),
                'bodyid' => $bodyid
            ));
    }

    public function editAction($bodyid, $id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        
        $gallery = $em->getRepository('SiteBackendBundle:BaseAutoGallery')->find($id);
        
		$form = $this->createForm(new BaseAutoGalleryForm(), $gallery);
        
        if($post = $request->request->all())
        {
            $form->submit($request);
            $gallery->setCarId($bodyid);
            
            $em->persist($gallery);
            $em->flush();
            return $this->redirect($this->generateUrl('admin_auto_gallery_edit', array('bodyid' => $bodyid, 'id' => $gallery->getId())));
        }
        
        
        return $this->render('SiteBackendBundle:BaseAutoGallery:edit.html.twig', array(
                'form'   => $form->createView(),
                'bodyid' => $bodyid
            ));
    }

    public function deleteAction($bodyid, $id)
    {
        $em = $this->getDoctrine()->getManager();
        
        $gallery = $em->getRepository('SiteBackendBundle:BaseAutoGallery')->find($id);
        $em->remove($gallery);
        $em->flush();
        
        return $this->redirect($this->generateUrl('admin_auto_gallery_index', array('bodyid' => $bodyid)));
    }
}
