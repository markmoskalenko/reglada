<?php

namespace Site\Bundle\BackendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Site\Bundle\BackendBundle\Form\FrontDillerCentreInfoForm;
use Site\Bundle\BackendBundle\Form\FrontDillerSpecialForm;
use Site\Bundle\BackendBundle\Entity\DillerCentreSpecial;
use Site\Bundle\BackendBundle\Entity\DillerAutoTestDrive;
use Site\Bundle\BackendBundle\Entity\DillerCentreColors;
use Site\Bundle\BackendBundle\Entity\DillerCentreComplectations;

class CabinetDealerController extends Controller
{
    public function indexAction($dealerid = 0)
    {
        $em = $this->getDoctrine()->getManager();
        
        $user = $this->getUser();
        
        $dealer_ids = $user->getDillerCentreIds();
        
        if($dealerid == 0)
        {
            $qb = $em->createQueryBuilder();
            $qb->select('d')
                ->from('Site\Bundle\BackendBundle\Entity\DillerCentre', 'd')
                ->where($qb->expr()->in('d.id', $dealer_ids));
            $all_dealers = $qb->getQuery()->getResult();
            
            return $this->redirect($this->generateUrl('cabinet_dealer_index', array('dealerid' => $all_dealers[0]->getId())));
        }
        
        $dealer = $em->getRepository('SiteBackendBundle:DillerCentre')->find($dealerid);
        
        $count_requests = $this->getCountRequests($dealerid);
        
        
        return $this->render('SiteBackendBundle:CabinetDealer:index.html.twig', array(
                'dealer' => $dealer,
                'dealerid' => $dealerid,
                'count_requests' => $count_requests,
            ));
    }

    public function deallersListAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        
        $user = $this->getUser();
        
        $dealer_ids = $user->getDillerCentreIds();
        $qb = $em->createQueryBuilder();
        $qb->select('d')
            ->from('Site\Bundle\BackendBundle\Entity\DillerCentre', 'd')
            ->where($qb->expr()->in('d.id', $dealer_ids));
        $all_dealers = $qb->getQuery()->getResult();
        
        $dealers = array();
        foreach($all_dealers as $ad)
        {
            $dealers[$ad->getId()] = $ad;
        }
        
        $dealerid = $request->query->get('dealerid');
        return $this->render('SiteBackendBundle:CabinetDealer:deallersList.html.twig', array(
                'all_dealers' => $dealers,
                'dealerid' => $dealerid
            ));
    }

    public function infoAction($dealerid, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        
        $dealer = $em->getRepository('SiteBackendBundle:DillerCentre')->find($dealerid);
        
        
		$form = $this->createForm(new FrontDillerCentreInfoForm($dealer->getPhone(), $dealer->getTextBlocks()), $dealer);
        
        if ($post = $request->request->all())
        {
            $form->submit($request);
            
            
            if($form['pricelist']->getData())
            {
                if(is_file('diller_pricelist/'.$dealer->getPricelist()))
                {
                    unlink('diller_pricelist/'.$dealer->getPricelist());
                }
                $file = $form['pricelist']->getData();
                $extension = $file->guessExtension();
                $filename = date('Ymd_His').'_'.$dealerid.'.'.$extension;
                $file->move('diller_pricelist', $filename);
                $dealer->setPricelist($filename);
            }
            
            
            $phones = serialize(explode(', ', $post['diller']['phone']));
            
            $blocks = array();
            foreach($post['diller'] as $k => $v)
            {
                if(substr($k, 0, 5) == 'block')
                {
                    $blocks[$k] = $post['diller'][$k];
                }
            }
            $dealer->setPhone($phones);
            $dealer->setTextBlocks(serialize($blocks));
            
            $dealerservices = array();
            if(isset($post['services']))
            {
                foreach($post['services'] as $k => $v)
                {
                    $dealerservices[] = $k;
                }
            }
            $dealer->setServices(serialize($dealerservices));
            
            $em->persist($dealer);
            $em->flush();
            
            return $this->redirect($this->generateUrl('cabinet_dealer_info', array('dealerid' => $dealerid)));
        }
        return $this->render('SiteBackendBundle:CabinetDealer:info.html.twig', array(
                'dealerid' => $dealerid,
                'dealer' => $dealer,
                'text_blocks' => unserialize($dealer->getTextBlocks()),
                'services' => unserialize($dealer->getServices()),
                'form'   => $form->createView(),
            ));    
    }

    public function specialAction($dealerid, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        
        
        if ($post = $request->request->all())
        {
            if(isset($post['spec_delete']) && count($post['special']) > 0)
            {
                $qb = $em->createQueryBuilder();
                    
                $to_del = $qb->select('sp')
                    ->from('SiteBackendBundle:DillerCentreSpecial', 'sp')
                    ->where($qb->expr()->in('sp.id', $post['special']))
                    ->andWhere("sp.dillerCentreId = '$dealerid'")
                    ->getQuery()
                    ->getResult();
                foreach($to_del as $del)
                {
                    if(is_file('dillerspecial/'.$del->getImage()))
                    {
                        unlink('dillerspecial/'.$del->getImage());
                    }
                    $em->remove($del);
                    $em->flush();
                }
            }
            return $this->redirect($this->generateUrl('cabinet_dealer_special', array('dealerid' => $dealerid)));
        }
        
        $specials = $em->getRepository('SiteBackendBundle:DillerCentreSpecial')->findByDillerCentreId($dealerid);
        
        return $this->render('SiteBackendBundle:CabinetDealer:special.html.twig', array(
                'dealerid' => $dealerid,
                'specials' => $specials
            ));
    }
    
    public function toArchiveSpecialAction($dealerid, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $special = $em->getRepository('SiteBackendBundle:DillerCentreSpecial')->find($id);
        if($special->getIsArchive() == 0)
        {
            $special->setIsArchive(1);
        }
        else
        {
            $special->setIsArchive(0);
        }
        $em->persist($special);
        $em->flush();
        
        return $this->redirect($this->generateUrl('cabinet_dealer_special', array('dealerid' => $dealerid)));
    }

    public function addSpecialAction($dealerid, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        
        $special = new DillerCentreSpecial();
		$form = $this->createForm(new FrontDillerSpecialForm(), $special);
        
        $qb = $em->createQueryBuilder();
        $qb->select('count(sp.id)');
        $qb->from('SiteBackendBundle:DillerCentreSpecial','sp');
        $qb->where('sp.isArchive = 0 AND sp.dillerCentreId = '.$dealerid);
        $count_sp = $qb->getQuery()->getSingleScalarResult();
        
        $qb = $em->createQueryBuilder();
        $qb->select('sp.id, sp.title, sp.dillerCentreId');
        $qb->from('SiteBackendBundle:DillerCentreSpecial','sp');
        $qb->where('sp.isArchive = 0 AND sp.dillerCentreId = '.$dealerid);
        $specials = $qb->getQuery()->getResult();
        
        if ($post = $request->request->all())
        {
            
            if(isset($post['special_replace']))
            {
                $special2 = $em->getRepository('SiteBackendBundle:DillerCentreSpecial')->findOneBy(array('id' => $post['special_replace'], 'dillerCentreId' => $dealerid));
                
                $special2->setIsArchive(1);
                $em->persist($special2);
                $em->flush();
            }
            
            $form->submit($request);
            $special->setDillerCentreId($dealerid);
            $special->setIsArchive(0);
            
            $filename = '';
            if($form['image']->getData())
            {
                $file = $form['image']->getData();
                $extension = $file->guessExtension();
                $filename = date('Ymd_His').'_'.$dealerid.'.'.$extension;
                $file->move('dillerspecial', $filename);
            }
            
            $special->setImage($filename);
            
            $em->persist($special);
            $em->flush();
            
            
            return $this->redirect($this->generateUrl('cabinet_dealer_edit_special', array('dealerid' => $dealerid, 'id' => $special->getId())));
        }
        
        return $this->render('SiteBackendBundle:CabinetDealer:addSpecial.html.twig', array(
                'dealerid' => $dealerid,
                'form'   => $form->createView(),
                'count_sp'   => $count_sp,
                'specials'   => $specials,
            ));
    }

    public function editSpecialAction($dealerid, $id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        
        $special = $em->getRepository('SiteBackendBundle:DillerCentreSpecial')->find($id);
		$form = $this->createForm(new FrontDillerSpecialForm(), $special);
        
        $qb = $em->createQueryBuilder();
        $qb->select('count(sp.id)');
        $qb->from('SiteBackendBundle:DillerCentreSpecial','sp');
        $qb->where('sp.isArchive = 0 AND sp.dillerCentreId = '.$dealerid);
        $count_sp = $qb->getQuery()->getSingleScalarResult();
        
        $qb = $em->createQueryBuilder();
        $qb->select('sp.id, sp.title, sp.dillerCentreId');
        $qb->from('SiteBackendBundle:DillerCentreSpecial','sp');
        $qb->where('sp.isArchive = 0 AND sp.dillerCentreId = '.$dealerid);
        $specials = $qb->getQuery()->getResult();
        
        if ($post = $request->request->all())
        {
            if(isset($post['special_replace']))
            {
                $special2 = $em->getRepository('SiteBackendBundle:DillerCentreSpecial')->findOneBy(array('id' => $post['special_replace'], 'dillerCentreId' => $dealerid));
                
                $special2->setIsArchive(1);
                $em->persist($special2);
                $em->flush();
            }
            
            $form->submit($request);
            $special->setDillerCentreId($dealerid);
            $special->setIsArchive(0);
            
            $filename = '';
            if($form['image']->getData())
            {
                if(is_file('dillerspecial/'.$special->getImage()))
                {
                    unlink('dillerspecial/'.$special->getImage());
                }
                $file = $form['image']->getData();
                $extension = $file->guessExtension();
                $filename = date('Ymd_His').'_'.$dealerid.'.'.$extension;
                $file->move('dillerspecial', $filename);
                $special->setImage($filename);
            }
            
            
            $em->persist($special);
            $em->flush();
            
            return $this->redirect($this->generateUrl('cabinet_dealer_edit_special', array('dealerid' => $dealerid, 'id' => $special->getId())));
        }
        return $this->render('SiteBackendBundle:CabinetDealer:editSpecial.html.twig', array(
                'dealerid' => $dealerid,
                'special' => $special,
                'form'   => $form->createView(),
                'count_sp'   => $count_sp,
                'specials'   => $specials,
            ));    }

    public function autoTestDriveAction($dealerid, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        
        $qb = $em->createQueryBuilder();
        $qb->select('ba.name, babt.name AS body_name, babt.id AS body_id')
            ->from('SiteBackendBundle:BaseAuto','ba')
            ->leftJoin('SiteBackendBundle:BaseAutoBodyTypes',   'babt', 'WITH','babt.carId = ba.id')
            ->orderBy('ba.pos', 'ASC');
        $bodies = $qb->getQuery()->getResult();
            
        foreach($bodies as $k => $v)
        {
            $qb = $em->createQueryBuilder();
            $qb->select('iv.id, iv.name')
                ->from('SiteBackendBundle:InfoValue', 'iv')
                ->leftJoin('SiteBackendBundle:CarsInfo', 'ci','WITH','ci.valueId = iv.id')
                ->leftJoin('SiteBackendBundle:Cars', 'c','WITH','c.id = ci.carModelId')
                ->where('iv.infoId = 11 AND c.bodyId = '.$v['body_id'])
                ->orderBy('c.pos', 'ASC');
            $kpp = $qb->getQuery()->getResult();
            $bodies[$k]['kpp'] = array();
            
            foreach($kpp as $kp)
            {
                $bodies[$k]['kpp'][$kp['id']] = $kp['name'];
            }
        }
        
        $exists_data = $em->getRepository('SiteBackendBundle:DillerAutoTestDrive')->findByDillerId($dealerid);
        
        $exists = array();
        foreach($exists_data as $ex)
        {
            $exists[$ex->getCarId()] = $ex;
        }
        
        if ($post = $request->request->all())
        {
            $not_del = array();
            if(isset($post['car']))
            {
                foreach($post['car'] as $k_car => $v_car)
                {
                    $ent = new DillerAutoTestDrive();
                    $ent->setDillerId($dealerid);
                    $ent->setCarId($k_car);
                    $ent->setDillerId($dealerid);
                    if(isset($post['mt'][$k_car]))
                        $ent->setMt(1);
                    else
                        $ent->setMt(0);
                    if(isset($post['at'][$k_car]))
                        $ent->setAt(1);
                    else
                        $ent->setAt(0);
                    if(isset($post['rb'][$k_car]))
                        $ent->setRb(1);
                    else
                        $ent->setRb(0);
                    
                    $em->persist($ent);
                    $em->flush();
                    $not_del[] = $ent->getId();
                }
            }
            if(count($not_del) > 0)
            {
                $qb = $em->createQueryBuilder();
                
                $to_del = $qb->select('d')
                    ->from('SiteBackendBundle:DillerAutoTestDrive', 'd')
                    ->where($qb->expr()->notIn('d.id', $not_del))
                    ->andWhere("d.dillerId = '$dealerid'")
                    ->getQuery()
                    ->getResult();
            }
            else
            {
                $to_del = $em->getRepository('SiteBackendBundle:CarsInfo')->findBy(array('carModelId' => $model_id));
            }
            foreach($to_del as $del)
            {
                $em->remove($del);
                $em->flush();
            }
            return $this->redirect($this->generateUrl('cabinet_dealer_auto_test_drive', array('dealerid' => $dealerid)));
        }
        
        return $this->render('SiteBackendBundle:CabinetDealer:autoTestDrive.html.twig', array(
                'dealerid' => $dealerid,
                'bodies' => $bodies,
                'exists' => $exists
            ));    }

    public function bidAction($dealerid)
    {
        return $this->render('SiteBackendBundle:CabinetDealer:bid.html.twig', array(
                'dealerid' => $dealerid
            ));    }

    public function viewBidAction($dealerid)
    {
        return $this->render('SiteBackendBundle:CabinetDealer:viewBid.html.twig', array(
                'dealerid' => $dealerid
            ));    }

    public function documentsAction($dealerid)
    {
        $em = $this->getDoctrine()->getManager();
        
        $qb = $em->createQueryBuilder();
        $qb->select('d')
            ->from('SiteBackendBundle:DillerCentreDocuments','d')
            ->where("d.type != 'template' AND d.type != 'template_card' AND d.dillerId = ".$dealerid)
            ->orderBy('d.dateAdd', 'DESC');
            
        $documents = $qb->getQuery()->getResult();
        
        $bill_count = 0;
        $close_count = 0;
        $report_count = 0;
        
        foreach($documents as $document)
        {
            if($document->getType() == 'bill')
                $bill_count++;
            if($document->getType() == 'close')
                $close_count++;
            if($document->getType() == 'report')
                $report_count++;
        }
        
        return $this->render('SiteBackendBundle:CabinetDealer:documents.html.twig', array(
                'dealerid' => $dealerid,
                'documents' => $documents,
                'bill_count' => $bill_count,
                'close_count' => $close_count,
                'report_count' => $report_count
            ));
    }

    public function documentsTemplatesAction($dealerid)
    {        
        $em = $this->getDoctrine()->getManager();
        
        $qb = $em->createQueryBuilder();
        $qb->select('d')
            ->from('SiteBackendBundle:DillerCentreDocuments','d')
            ->where("(d.type = 'template' OR d.type = 'template_card') AND d.dillerId = ".$dealerid)
            ->orderBy('d.dateAdd', 'DESC');
            
        $documents = $qb->getQuery()->getResult();
        
        
        return $this->render('SiteBackendBundle:CabinetDealer:documentsTemplates.html.twig', array(
                'dealerid' => $dealerid,
                'documents' => $documents,
            ));
    }


    public function complectationsAction($dealerid, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        
        $qb = $em->createQueryBuilder();
        $qb->select('ba.id AS auto_id, ba.name, babt.name AS body_name, babt.id AS body_id, iv.id AS value_id, iv.name AS value_name, iv2.id AS value_id2, iv2.name AS value_name2')
            ->from('SiteBackendBundle:BaseAuto','ba')
            ->leftJoin('SiteBackendBundle:BaseAutoBodyTypes',   'babt', 'WITH','babt.carId = ba.id')
            ->leftJoin('SiteBackendBundle:Cars',   'c', 'WITH','c.bodyId = babt.id')
            ->leftJoin('SiteBackendBundle:CarsInfo',   'ci', 'WITH','ci.carModelId = c.id')
            ->leftJoin('SiteBackendBundle:CarsInfo',   'ci2', 'WITH','ci2.carModelId = c.id')
            ->leftJoin('SiteBackendBundle:InfoValue',   'iv', 'WITH','ci.valueId = iv.id')
            ->leftJoin('SiteBackendBundle:InfoValue',   'iv2', 'WITH','ci2.valueId = iv2.id')
            ->where('iv.infoId = 31 AND iv2.infoId = 30')
            ->orderBy('ba.pos', 'ASC');
        $bodies_data = $qb->getQuery()->getResult();
        
        
        $qb = $em->createQueryBuilder();
        $qb->select('c.id, c.name, bc.bodyTypeId')
            ->from('SiteBackendBundle:BodyTypesColors','bc')
            ->leftJoin('SiteBackendBundle:Colors',   'c', 'WITH','bc.colorId = c.id');
        $colors = $qb->getQuery()->getResult();
        
        
        $colors_arr = array();
        foreach($colors as $color)
        {
            $colors_arr[$color['id']] = array('name' => $color['name'], 'body' => $color['bodyTypeId']);
            //print_r($color->getAuto());
        }
        
        
        $bodies = array();
        foreach($bodies_data as $bd)
        {
            if(!array_key_exists($bd['auto_id'], $bodies))
                $bodies[$bd['auto_id']] = array('name' => $bd['name'], 'values' => array());
            if(!array_key_exists($bd['body_id'], $bodies[$bd['auto_id']]['values']))
                $bodies[$bd['auto_id']]['values'][$bd['body_id']] = array('name' => $bd['body_name'], 'complectations' => array());
            if(!array_key_exists($bd['value_id2'], $bodies[$bd['auto_id']]['values'][$bd['body_id']]['complectations']))
                $bodies[$bd['auto_id']]['values'][$bd['body_id']]['complectations'][$bd['value_id2']] = array('name' => $bd['value_name2'], 'values' => array());
            if(!array_key_exists($bd['value_id'], $bodies[$bd['auto_id']]['values'][$bd['body_id']]['complectations'][$bd['value_id2']]['values']))
                $bodies[$bd['auto_id']]['values'][$bd['body_id']]['complectations'][$bd['value_id2']]['values'][$bd['value_id']] = $bd['value_name'];
        }
        
        $exist_colors = $em->getRepository('SiteBackendBundle:DillerCentreColors')->findBy(array('dillerId' => $dealerid));
        
        $color_data = array();
        foreach($exist_colors as $ec)
        {
            if(!array_key_exists($ec->getBodyId(), $color_data))
                $color_data[$ec->getBodyId()] = array();
            if(!array_key_exists($ec->getColorId(), $color_data[$ec->getBodyId()]))
                $color_data[$ec->getBodyId()][$ec->getColorId()] = 1;
        }
        
        
        $exist_compl = $em->getRepository('SiteBackendBundle:DillerCentreComplectations')->findBy(array('dillerId' => $dealerid));
        
        $compl_data = array();
        foreach($exist_compl as $ec)
        {
            if(!array_key_exists($ec->getBodyId(), $compl_data))
                $compl_data[$ec->getBodyId()] = array();
            if(!array_key_exists($ec->getVariantId(), $compl_data[$ec->getBodyId()]))
                $compl_data[$ec->getBodyId()][$ec->getVariantId()] = array();
            if(!array_key_exists($ec->getComplectationId(), $compl_data[$ec->getBodyId()][$ec->getVariantId()]))
                $compl_data[$ec->getBodyId()][$ec->getVariantId()][$ec->getComplectationId()] = 1;
        }
        if ($post = $request->request->all())
        {
            $to_del = array();
            
            if(isset($post['color']))
            {
                $not_del = array();
                foreach($post['color'] as $k_body => $v_body)
                {
                    foreach($v_body as $k_color => $v_color)
                    {
                        $color = $em->getRepository('SiteBackendBundle:DillerCentreColors')->findBy(array('dillerId' => $dealerid, 'bodyId' => $k_body, 'colorId' => $k_color));
                        if(!$color)
                        {
                            $color = new DillerCentreColors();
                            $color->setDillerId($dealerid);
                            $color->setBodyId($k_body);
                            $color->setColorId($k_color);
                            $em->persist($color);
                            $em->flush();
                            $not_del[] = $color->getId();
                        }
                        else
                        {
                            $not_del[] = $color[0]->getId(); 
                        }
                    }
                }
                if(count($not_del) > 0)
                {
                    $qb = $em->createQueryBuilder();
                    
                    $to_del = $qb->select('c')
                        ->from('SiteBackendBundle:DillerCentreColors', 'c')
                        ->where($qb->expr()->notIn('c.id', $not_del))
                        ->andWhere("c.dillerId = '$dealerid'")
                        ->getQuery()
                        ->getResult();
                }
            }
            else
            {
                $to_del = $em->getRepository('SiteBackendBundle:DillerCentreColors')->findBy(array('dillerId' => $dealerid));
            }
            foreach($to_del as $del)
            {
                $em->remove($del);
                $em->flush();
            }
            
            
            if(isset($post['compl']))
            {
                $not_del = array();
                foreach($post['compl'] as $k_body => $v_body)
                {
                    foreach($v_body as $k_isp => $v_isp)
                    {
                        foreach($v_isp as $k_compl => $v_compl)
                        {
                            $compl = $em->getRepository('SiteBackendBundle:DillerCentreComplectations')->findBy(array('dillerId' => $dealerid,
                                                                                                                      'bodyId' => $k_body,
                                                                                                                      'variantId' => $k_isp,
                                                                                                                      'complectationId' => $k_compl));
                            if(!$compl)
                            {
                                $compl = new DillerCentreComplectations();
                                $compl->setDillerId($dealerid);
                                $compl->setBodyId($k_body);
                                $compl->setComplectationId($k_compl);
                                $compl->setVariantId($k_isp);
                                $em->persist($compl);
                                $em->flush();
                                $not_del[] = $compl->getId();
                            }
                            else
                            {
                                $not_del[] = $compl[0]->getId(); 
                            }
                        }
                    }
                }
                if(count($not_del) > 0)
                {
                    $qb = $em->createQueryBuilder();
                    
                    $to_del = $qb->select('c')
                        ->from('SiteBackendBundle:DillerCentreComplectations', 'c')
                        ->where($qb->expr()->notIn('c.id', $not_del))
                        ->andWhere("c.dillerId = '$dealerid'")
                        ->getQuery()
                        ->getResult();
                }
            }
            else
            {
                $to_del = $em->getRepository('SiteBackendBundle:DillerCentreComplectations')->findBy(array('dillerId' => $dealerid));
            }
            foreach($to_del as $del)
            {
                $em->remove($del);
                $em->flush();
            }
            return $this->redirect($this->generateUrl('cabinet_dealer_complectations', array('dealerid' => $dealerid)));
        }
        
        return $this->render('SiteBackendBundle:CabinetDealer:complectations.html.twig', array(
                'dealerid' => $dealerid,
                'bodies' => $bodies,
                'colors' => $colors_arr,
                'color_data' => $color_data,
                'compl_data' => $compl_data,
            ));
    }
    
    public function requestsAction($dealerid, $type, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        
        $qb = $em->createQueryBuilder();
                    
        $items = $qb->select('r')
            ->from('SiteBackendBundle:Requests', 'r')
            ->where("r.dc = '$dealerid' AND r.type='$type'")
            ->orderBy('r.requestDate', 'DESC')
            ->getQuery()
            ->getResult();
        
        $data = array();
        foreach($items as $item)
        {
            $data[] = array('date_add' => $item->getRequestDate(),
                            'data' => unserialize($item->getRequestData()));
        }
        
        $count_requests = $this->getCountRequests($dealerid);
        
        return $this->render('SiteBackendBundle:CabinetDealer:requests.html.twig', array(
                'dealerid' => $dealerid,
                'items' => $data,
                'type' => $type,
                'count_requests' => $count_requests,
                ));
    }
    
    public function getCountRequests($dealerid)
    {
        $em = $this->getDoctrine()->getManager();
        
        $qb = $em->createQueryBuilder();
        
        
        $arr = array('callback' => 0,
                     'service' => 0,
                     'credit' => 0,
                     'testdrive' => 0,
                     'insurance' => 0,
                     'tradeinquery' => 0,
                     'car-request' => 0,
                     'car-buy-request' => 0,
                     'mail' => 0,
                     'cupon' => 0,
                     'contactbase' => 0);
        
        $items = $qb->select('r.readEd, r.type')
            ->from('SiteBackendBundle:Requests', 'r')
            ->where("r.dc = '$dealerid'")
            ->orderBy('r.requestDate', 'DESC')
            ->getQuery()
            ->getResult();
        
        foreach($items as $item)
        {
            if($item['readEd'] == 0)
            {
                $arr[$item['type']]++;
            }
        }
        return $arr;
    }
}
