<?php

namespace Site\Bundle\BackendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Site\Bundle\BackendBundle\Form\ModelForm;
use Site\Bundle\BackendBundle\Entity\Cars;
use Site\Bundle\BackendBundle\Entity\CarsInfo;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class AutoController extends Controller
{
    public function modelsAction($type, $bodyid, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        
        $models = $em->getRepository('SiteBackendBundle:Cars')->findBy(array('bodyId' => $bodyid));
        
        
        return $this->render('SiteBackendBundle:Auto:models.html.twig', array(
                'models' => $models,
                'type' => $type,
                'bodyid' => $bodyid
            ));
    }

    public function addAction($type, $bodyid, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $model = new Cars();
        
        $qb = $em->createQueryBuilder()
            ->select(array('iv.id AS ivid', 'iv.name AS ivname', 'i.name AS iname', 'i.id AS iid', 'ig.name AS igname', 'ig.id AS igid', 'i.seltype'))
            ->from('Site\Bundle\BackendBundle\Entity\Info', 'i')
            ->leftJoin('SiteBackendBundle:InfoValue','iv', 'WITH', 'i.id = iv.infoId')
            ->leftJoin('SiteBackendBundle:InfoGroups','ig', 'WITH', 'ig.id = i.groupId');
        $info_values = $qb->getQuery()->getResult();
        
        $info_data = array();
        $inf_arr = array();
        foreach($info_values as $iv)
        {
            if(!array_key_exists($iv['igid'], $info_data))
            {
                $info_data[$iv['igid']] = array('label' => $iv['igname'], 'features' => array());
            }
            if(!array_key_exists($iv['iid'], $info_data[$iv['igid']]['features']))
            {
                $inf_arr[$iv['iid']] = $iv['seltype'];
                $info_data[$iv['igid']]['features'][$iv['iid']] = array('label' => $iv['iname'], 'type' => $iv['seltype'], 'values' => array());
            }
            
            if($iv['ivid'] != '')
                $info_data[$iv['igid']]['features'][$iv['iid']]['values'][] = array('label' => $iv['ivname'], 'value' => $iv['ivid']);
        }
        
        
        $body = $em->getRepository('SiteBackendBundle:BaseAutoBodyTypes')->find($bodyid);
        $car_bodies = $em->getRepository('SiteBackendBundle:BaseAutoBodyTypes')->findByCarId($body->getCarId());
        
		$form = $this->createForm(new ModelForm($car_bodies, $bodyid), $model);
        
        if ($post = $request->request->all())
        {
            $form->submit($request);
            
            $model->setBodyId($bodyid);
            
            $em->persist($model);
            $em->flush();
            
            $model_id = $model->getId();
            
            if(!isset($post['info']))
                $post['info'] = array();
                
            $not_del = array();
            foreach($post['info'] as $k => $v)
            {
                if($v != 0)
                {
                    $cars_data[$k] = new CarsInfo();
                    $cars_data[$k]->setCarModelId($model_id);
                    $cars_data[$k]->setInfoId($k);
                    $cars_data[$k]->setValueId($v);
                    
                    $em->persist($cars_data[$k]);
                    $em->flush();
                    $not_del[] = $cars_data[$k]->getId();
                }
            }
            
            if(count($not_del) > 0)
            {
                $qb = $em->createQueryBuilder();
                
                $to_del = $qb->select('ci')
                    ->from('SiteBackendBundle:CarsInfo', 'ci')
                    ->where($qb->expr()->notIn('ci.id', $not_del))
                    ->andWhere("ci.carModelId = '$model_id'")
                    ->getQuery()
                    ->getResult();
            }
            else
            {
                $to_del = $em->getRepository('SiteBackendBundle:CarsInfo')->findBy(array('carModelId' => $model_id));
            }
            foreach($to_del as $del)
            {
                $em->remove($del);
                $em->flush();
            }
            
            return $this->redirect($this->generateUrl('admin_models_edit', array('id' => $model->getId(), 'type' => $type)));
        }
        
        return $this->render('SiteBackendBundle:Auto:add.html.twig', array(
            'form'   => $form->createView(),
            'info_data' => $info_data,
            'type' => $type
            ));
    }
    
    public function copyAction($id, $type, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        
        /*$model = $em->getRepository('SiteBackendBundle:Cars')->find($id);
        
        $new_model = new Cars();
        $new_model->setEquipmentId($model->getEquipmentId());
        $new_model->setItemType($model->getItemType());
        $new_model->setName($model->getName());
        $new_model->setPos($model->getPos());
        
        $em->persist($new_model);
        $em->flush();
        
        $car_info = $em->getRepository('SiteBackendBundle:CarsInfo')->findBy(array('carModelId' => $id));
        
        foreach($car_info as $ci)
        {
            $carinfo = new CarsInfo();
            $carinfo->setCarModelId($new_model->getId());
            $carinfo->setInfoId($ci->getInfoId());
            $carinfo->setValueId($ci->getValueId());
            $em->persist($carinfo);
            $em->flush();
        }
        
        return $this->redirect($this->generateUrl('admin_models_edit', array('id' => $new_model->getId(), 'type' => $type)));*/
    }

    public function editAction($id, $type, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        
        $model = $em->getRepository('SiteBackendBundle:Cars')->find($id);
        
        
        
        
        $qb = $em->createQueryBuilder()
            ->select(array('iv.id AS ivid', 'iv.name AS ivname', 'i.name AS iname', 'i.id AS iid', 'ig.name AS igname', 'ig.id AS igid', 'i.seltype'))
            ->from('Site\Bundle\BackendBundle\Entity\Info', 'i')
            ->leftJoin('SiteBackendBundle:InfoValue','iv', 'WITH', 'i.id = iv.infoId')
            ->leftJoin('SiteBackendBundle:InfoGroups','ig', 'WITH', 'ig.id = i.groupId');
        $info_values = $qb->getQuery()->getResult();
        
        $info_data = array();
        
        $inf_arr = array();
        
        foreach($info_values as $iv)
        {
            if(!array_key_exists($iv['igid'], $info_data))
            {
                $info_data[$iv['igid']] = array('label' => $iv['igname'], 'features' => array());
            }
            if(!array_key_exists($iv['iid'], $info_data[$iv['igid']]['features']))
            {
                $inf_arr[$iv['iid']] = $iv['seltype'];
                $info_data[$iv['igid']]['features'][$iv['iid']] = array('label' => $iv['iname'], 'type' => $iv['seltype'], 'values' => array());
            }
            if($iv['ivid'] != '')
                $info_data[$iv['igid']]['features'][$iv['iid']]['values'][] = array('label' => $iv['ivname'], 'value' => $iv['ivid']);
        }
        
        
        $car_info = $em->getRepository('SiteBackendBundle:CarsInfo')->findBy(array('carModelId' => $id));
        $cars_data = array();
        foreach($car_info as $ci)
        {
            $cars_data[$ci->getInfoId()][$ci->getValueId()]['id'] = $ci->getId();
            $cars_data[$ci->getInfoId()][$ci->getValueId()]['infoId'] = $ci->getInfoId();
            $cars_data[$ci->getInfoId()][$ci->getValueId()]['valueId'] = $ci->getValueId();
            $cars_data[$ci->getInfoId()][$ci->getValueId()]['carModelId'] = $ci->getCarModelId();
        }
            
        $body = $em->getRepository('SiteBackendBundle:BaseAutoBodyTypes')->find($model->getBodyId());
        $car_bodies = $em->getRepository('SiteBackendBundle:BaseAutoBodyTypes')->findByCarId($body->getCarId());
        
		$form = $this->createForm(new ModelForm($car_bodies), $model);
        
        if ($post = $request->request->all())
        {
            $form->submit($request);
            
            $em->persist($model);
            $em->flush();
            
            $model_id = $model->getId();
            
                
            
            
            if(!isset($post['info']))
                $post['info'] = array();
                
            $not_del = array();
            foreach($post['info'] as $k => $v)
            {
                if($v != '0')
                {
                    if(!is_array($v))
                    {
                        if(array_key_exists($k, $cars_data))
                        {
                            if($inf_arr[$k] == 'yesno')
                            {
                                $vpost = $v;
                                $v = key($cars_data[$k]);
                                $cars_data[$k][$v]->setValueId($vpost);
                            }
                            else
                            {
                                $vpost = $v;
                                $v = key($cars_data[$k]);
                                $cars_data[$k][$v]->setValueId($vpost);
                            }
                        }
                        else
                        {
                            $cars_data[$k][$v] = new CarsInfo();
                            $cars_data[$k][$v]->setCarModelId($model_id);
                            $cars_data[$k][$v]->setInfoId($k);
                            $cars_data[$k][$v]->setValueId($v);
                        }
                        $em->persist($cars_data[$k][$v]);
                        $em->flush();
                        $not_del[] = $cars_data[$k][$v]->getId();
                    }
                    else
                    {
                        foreach($v as $vl)
                        {
                            if(array_key_exists($k, $cars_data) && isset($cars_data[$k][$vl]))
                            {
                                $cars_data[$k][$vl]->setValueId($vl);
                            }
                            else
                            {
                                $cars_data[$k][$vl] = new CarsInfo();
                                $cars_data[$k][$vl]->setCarModelId($model_id);
                                $cars_data[$k][$vl]->setInfoId($k);
                                $cars_data[$k][$vl]->setValueId($vl);
                            }
                            $em->persist($cars_data[$k][$vl]);
                            $em->flush();
                            $not_del[] = $cars_data[$k][$vl]->getId();
                        }
                    }
                }
            }
            
            if(count($not_del) > 0)
            {
                $qb = $em->createQueryBuilder();
                
                $to_del = $qb->select('ci')
                    ->from('SiteBackendBundle:CarsInfo', 'ci')
                    ->where($qb->expr()->notIn('ci.id', $not_del))
                    ->andWhere("ci.carModelId = '$model_id'")
                    ->getQuery()
                    ->getResult();
            }
            else
            {
                $to_del = $em->getRepository('SiteBackendBundle:CarsInfo')->findBy(array('carModelId' => $model_id));
            }
            foreach($to_del as $del)
            {
                $em->remove($del);
                $em->flush();
            }
            
            return $this->redirect($this->generateUrl('admin_models_edit', array('id' => $model->getId(), 'type' => $type, 'bodyid' => $model->getBodyId())));
        }
        
        return $this->render('SiteBackendBundle:Auto:edit.html.twig', array(
            'form'   => $form->createView(),
            'info_data' => $info_data,
            'cars_data' => $cars_data,
            'type' => $type
            ));
    }

    public function deleteAction($id, $type, $bodyid)
    {
        $em = $this->getDoctrine()->getManager();
        
        $to_del = $em->getRepository('SiteBackendBundle:CarsInfo')->findBy(array('carModelId' => $id));
        foreach($to_del as $del)
        {
            $em->remove($del);
            $em->flush();
        }
        
        $model = $em->getRepository('SiteBackendBundle:Cars')->find($id);
        
        $em->remove($model);
        $em->flush();
        
        return $this->redirect($this->generateUrl('admin_models_view', array('type' => $type, 'bodyid' => $bodyid)));
    }
}
