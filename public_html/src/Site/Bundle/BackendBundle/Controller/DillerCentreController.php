<?php

namespace Site\Bundle\BackendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Site\Bundle\BackendBundle\Utils\Utils;
use Site\Bundle\BackendBundle\Entity\UserRoles;
use Site\Bundle\BackendBundle\Entity\DillerCentre;
use Site\Bundle\BackendBundle\Form\RolesForm;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DillerCentreController extends Controller
{
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $rsite = Utils::getRegionsUser();
        $filter = array();
        if ( $rsite == false ){
            return $this->render('SiteBackendBundle:Errors:error.html.twig', array(
                    // ...
                )); 
        }
        else{
            if ( is_array($rsite) ){ $filter['regions'] = 0; } else { $filter['regions'] = 1; }
            $dcs = $em->getRepository('SiteBackendBundle:DillerCentre')->getDcs($rsite);
            $regions = $em->getRepository('SiteBackendBundle:Regions')->findAll();
            return $this->render('SiteBackendBundle:DillerCentre:index.html.twig', array(
                    'dcs' => $dcs,
                    'regs' => $regions,
                    'filter' => $filter,
                ));      
        }
        

    }

    public function editAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $dc = $em->getRepository('SiteBackendBundle:DillerCentre')->getDcsOne($id);
        $regions = $em->getRepository('SiteBackendBundle:Regions')->findAll();
        $cars = Utils::minimalCarsPrices();
        return $this->render('SiteBackendBundle:DillerCentre:edit.html.twig', array(
                'dc' => $dc,
                'regions' => $regions,
                'cars' => $cars,
            ));    
    }

    public function deleteAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $dc = $em->getRepository('SiteBackendBundle:DillerCentre')->find($id);
        if ($post = $request->request->all())
        {
            if( isset($post['delete']) ){
                $em->remove($dc);
                $em->flush();
            }
            return $this->redirect($this->generateUrl('admin_dillercentres_index'));

        }
        return $this->render('SiteBackendBundle:DillerCentre:delete.html.twig', array(
                'dc' => $dc,
            ));     
    }

    public function ajaxEditAction(Request $request){
        $em = $this->getDoctrine()->getManager();
        $post = $request->request->all();
        $dc = $em->getRepository('SiteBackendBundle:DillerCentre')->find($post['id']);
        $post['email'] = serialize($post['email']);
        $post['phone'] = serialize($post['phone']);
        $post['cars'] = serialize($post['cars']);
        $post['coordinates'] = $post['cw'].'::'.$post['ch'];
        unset($post['id']);
        unset($post['cw']);
        unset($post['ch']);
        foreach ($post as $key => $value) {
            $func = "set".ucfirst($key);
            $dc->{$func}($value);
        }
        $em->persist($dc);
        $em->flush();
        return new Response('true');
    }

    public function ajaxAddAction(Request $request){
        $em = $this->getDoctrine()->getManager();
        $post = $request->request->all();
        $post['email'] = serialize($post['email']);
        $post['phone'] = serialize($post['phone']);
        $post['cars'] = serialize($post['cars']);
        $post['coordinates'] = $post['cw'].'::'.$post['ch'];
        unset($post['cw']);
        unset($post['ch']);
        $dc = new DillerCentre();
        foreach ($post as $key => $value) {
            $func = "set".ucfirst($key);
            $dc->{$func}($value);
        }
        $em->persist($dc);
        $em->flush();
        return new Response('true');
    }
    public function viewAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $dc = $em->getRepository('SiteBackendBundle:DillerCentre')->getDcsOne($id);
        return $this->render('SiteBackendBundle:DillerCentre:view.html.twig', array(
                'dc' => $dc,
            ));    
    }

    public function addAction()
    {
        $em = $this->getDoctrine()->getManager();
        $dcs = $em->getRepository('SiteBackendBundle:DillerCentre')->getDcs();
        $regions = $em->getRepository('SiteBackendBundle:Regions')->findAll();
        $cars = Utils::minimalCarsPrices();
        return $this->render('SiteBackendBundle:DillerCentre:add.html.twig', array(
                'dcs' => $dcs,
                'regions' => $regions,
                'cars' => $cars,
            ));    
    }

}
