<?php

namespace Site\Bundle\BackendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Site\Bundle\BackendBundle\Entity\DillerCentreDocuments;
use Site\Bundle\BackendBundle\Form\DillerCentreDocumentsForm;

class DillerCentreDocumentsController extends Controller
{
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        
        $documents = $em->getRepository('SiteBackendBundle:DillerCentreDocuments')->findAll();
        
        return $this->render('SiteBackendBundle:DillerCentreDocuments:index.html.twig', array(
                'documents' => $documents,
            ));
    }

    public function addAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $document = new DillerCentreDocuments();
        
        
        $qb = $em->createQueryBuilder();
        $qb->select('dc.id, dc.name')
            ->from('SiteBackendBundle:DillerCentre','dc')
            ->orderBy('dc.id', 'ASC');
        $dillers_data = $qb->getQuery()->getResult();
        
        $dillers = array();
        foreach($dillers_data as $dd)
        {
            $dillers[$dd['id']] = $dd['name'];
        }
        
		$form = $this->createForm(new DillerCentreDocumentsForm($dillers), $document);
        
        
        if ($post = $request->request->all())
        {
            $form->submit($request);
            
            $filename = '';
            if($form['link']->getData())
            {
                $file = $form['link']->getData();
                $extension = $file->guessExtension();
                $filename = date('Ymd_His').'_'.$post['doc']['dillerId'].'_'.$post['doc']['type'].'.'.$extension;
                $file->move('dillerdocs', $filename);
            }
            
            $document->setLink($filename);
            
            $em->persist($document);
            $em->flush();
            
            return $this->redirect($this->generateUrl('admin_dillerdocs_edit', array('id' => $document->getId())));
        }
        
        return $this->render('SiteBackendBundle:DillerCentreDocuments:add.html.twig', array(
            'form'   => $form->createView(),
            ));
    }

    public function editAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        
        $document = $em->getRepository('SiteBackendBundle:DillerCentreDocuments')->find($id);
        
        
        $qb = $em->createQueryBuilder();
        $qb->select('dc.id, dc.name')
            ->from('SiteBackendBundle:DillerCentre','dc')
            ->orderBy('dc.id', 'ASC');
        $dillers_data = $qb->getQuery()->getResult();
        
        $dillers = array();
        foreach($dillers_data as $dd)
        {
            $dillers[$dd['id']] = $dd['name'];
        }
        
		$form = $this->createForm(new DillerCentreDocumentsForm($dillers), $document);
        
        
        if ($post = $request->request->all())
        {
            $form->submit($request);
            
            $filename = '';
            if($form['link']->getData())
            {
                if(is_file('dillerdocs/'.$document->getLink()))
                {
                    unlink('dillerdocs/'.$document->getLink());
                }
                $file = $form['link']->getData();
                $extension = $file->guessExtension();
                $filename = date('Ymd_His').'_'.$post['doc']['dillerId'].'_'.$post['doc']['type'].'.'.$extension;
                $file->move('dillerdocs', $filename);
                
                $document->setLink($filename);
            }
            
            
            $em->persist($document);
            $em->flush();
            
            return $this->redirect($this->generateUrl('admin_dillerdocs_edit', array('id' => $document->getId())));
        }
        
        return $this->render('SiteBackendBundle:DillerCentreDocuments:edit.html.twig', array(
            'form'   => $form->createView(),
            'document' => $document
            ));
    }

    public function deleteAction()
    {
        $em = $this->getDoctrine()->getManager();
        
        $document = $em->getRepository('SiteBackendBundle:DillerCentreDocuments')->find($id);
        
        if(is_file('dillerdocs/'.$document->getLink()))
        {
            unlink('dillerdocs/'.$document->getLink());
        }
        
        $em->remove($document);
        $em->flush();
        
        return $this->redirect($this->generateUrl('admin_models', array('type' => $type)));
    }

    public function checkAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        
        $date_from = date('Y-m-d', strtotime("-1 month", time()));
        $date_to = date('Y-m-d', time());
        
        if($request->request->get('date_from'))
            $date_from = $request->request->get('date_from');
        if($request->request->get('date_to'))
            $date_to = $request->request->get('date_to');
        
        
        $qb = $em->createQueryBuilder()
            ->select(array('dc.id AS diller_id',
                           'dc.name AS diller_name',
                           'r.name AS region_name',))
            ->from('SiteBackendBundle:DillerCentre', 'dc')
            ->leftJoin('SiteBackendBundle:Regions',   'r', 'WITH','r.id = dc.regionId')
            ->orderBy('dc.id', 'ASC');
            
        $diller_centres = $qb->getQuery()->getResult();
        
        $dillers = array();
        foreach($diller_centres as $dc)
        {
            $dillers[$dc['diller_id']] = $dc;
            $dillers[$dc['diller_id']]['dates'] = array();
        }
        
        
        $qb = $em->createQueryBuilder()
            ->select(array(
                'dc.id AS diller_id',
                'dc.name AS diller_name',
                'dcd.type AS document_type',
                'dcd.dateAdd AS document_date_add',
                'r.name AS region_name',
            ))
            ->from('SiteBackendBundle:DillerCentre', 'dc')
            ->leftJoin('SiteBackendBundle:DillerCentreDocuments',   'dcd', 'WITH','dc.id = dcd.dillerId')
            ->leftJoin('SiteBackendBundle:Regions',   'r', 'WITH','r.id = dc.regionId')
            //->where("dcd.dateAdd >= '$date_from' AND dcd.dateAdd <= '$date_to'")
            ->where("dcd.dateAdd BETWEEN '$date_from' AND '$date_to'")
            ->orderBy('dc.id', 'ASC');
            
        $data = $qb->getQuery()->getResult();
        
        $dates = array();
        foreach($data as $d)
        {
            if(!array_key_exists($d['document_date_add']->format('Y-m'), $dates))
            {
                $dates[$d['document_date_add']->format('Y-m')] = array();
            }
        }
        
        foreach($dillers as $k => $v)
        {
            $dillers[$k]['dates'] = $dates;
        }
        
        
        $docs = array();
        foreach($data as $d)
        {
            //if(!array_key_exists('dates', $dillers[$d['diller_id']]))
            //{
                //$dillers[$d['diller_id']]['dates'] = $dates;
            //}
            if(!in_array($d['document_type'], $dillers[$d['diller_id']]['dates'][$d['document_date_add']->format('Y-m')]))
            {
                $dillers[$d['diller_id']]['dates'][$d['document_date_add']->format('Y-m')][] = $d['document_type'];
            }
        }
        
        //print_r($dates);
        //die;
        
        return $this->render('SiteBackendBundle:DillerCentreDocuments:check.html.twig', array(
                'documents' => $dillers,
                'dates' => $dates,
                'date_from' => $date_from,
                'date_to' => $date_to,
            ));
    }

}
