<?php

namespace Site\Bundle\BackendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Site\Bundle\BackendBundle\Form\BaseAutoDocumentsForm;
use Site\Bundle\BackendBundle\Entity\BaseAutoDocuments;
use Site\Bundle\BackendBundle\Entity\BaseAutoDocumentsAssigned;

class DocumentsController extends Controller
{
    public function indexAction($bodyid)
    {
        $em = $this->getDoctrine()->getManager();
        
        $documents = $em->getRepository('SiteBackendBundle:BaseAutoDocuments')->findBy(array('carId' => $bodyid));
        
        return $this->render('SiteBackendBundle:Documents:index.html.twig', array(
                'documents' => $documents,
                'bodyid' => $bodyid
            ));
    }

    public function addAction($bodyid, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $document = new BaseAutoDocuments();
        
		$form = $this->createForm(new BaseAutoDocumentsForm(), $document);
        
        if($post = $request->request->all())
        {
            $form->submit($request);
            
            $document->setDateUpload(new \DateTime());
            $document->setCarId($bodyid);
            
            $em->persist($document);
            $em->flush();
            
            return $this->redirect($this->generateUrl('admin_documents_edit', array('bodyid' => $bodyid, 'id' => $document->getId())));
        }
        
        return $this->render('SiteBackendBundle:Documents:add.html.twig', array(
            'form'   => $form->createView(),
            'bodyid' => $bodyid,
            ));
    }

    public function editAction($bodyid, $id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        
        $document = $em->getRepository('SiteBackendBundle:BaseAutoDocuments')->find($id);
        
		$form = $this->createForm(new BaseAutoDocumentsForm(), $document);
        
        
        if($post = $request->request->all())
        {
            $form->submit($request);
            
            $em->persist($document);
            $em->flush();
            
            return $this->redirect($this->generateUrl('admin_documents_edit', array('id' => $document->getId(), 'bodyid' => $bodyid)));
        }
        
        return $this->render('SiteBackendBundle:Documents:edit.html.twig', array(
            'form'   => $form->createView(),
            'bodyid' => $bodyid,
            ));
    }

    public function deleteAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        
        $model = $em->getRepository('SiteBackendBundle:BaseAutoDocuments')->find($id);
        
        $em->remove($model);
        $em->flush();
        
        return $this->redirect($this->generateUrl('admin_documents'));
    }

}
