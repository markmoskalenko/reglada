<?php

namespace Site\Bundle\BackendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Site\Bundle\BackendBundle\Utils\Utils;
use Site\Bundle\BackendBundle\Entity\UserRoles;
use Site\Bundle\BackendBundle\Entity\Colors;
use Site\Bundle\BackendBundle\Form\RolesForm;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ColorsController extends Controller
{
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $rsite = Utils::getRegionsUser();
        $filter = array();
        if ( $rsite == false ){
            return $this->render('SiteBackendBundle:Errors:error.html.twig', array(
                    // ...
                )); 
        }
        else{
            $cars = Utils::minimalCarsPrices();
            $colors = $em->getRepository('SiteBackendBundle:Colors')->findAll();
            return $this->render('SiteBackendBundle:Colors:index.html.twig', array(
                    'colors' => $colors,
                    'cars' => $cars,
                ));      
        }
        

    }

    public function addAction(){
        $em = $this->getDoctrine()->getManager();
        $cars = Utils::minimalCarsPrices();
        return $this->render('SiteBackendBundle:Colors:add.html.twig', array(
                'cars' => $cars,
            ));    
    }

    public function ajaxAddAction(Request $request){
        $em = $this->getDoctrine()->getManager();
        $post = $request->request->all();
        $post['auto'] = serialize($post['cars']);
        unset($post['cars']);
        $color = new Colors();
        foreach ($post as $key => $value) {
            $func = "set".ucfirst($key);
            $color->{$func}($value);
        }
        $em->persist($color);
        $em->flush();
        return new Response('true');
    }

    public function editAction($id,Request $request){
        $em = $this->getDoctrine()->getManager();
        $color = $em->getRepository('SiteBackendBundle:Colors')->find($id);
        $cars = Utils::minimalCarsPrices();
        return $this->render('SiteBackendBundle:Colors:edit.html.twig', array(
                'color'=>$color,
                'cars' => $cars,
            ));    
    }



    public function deleteAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $color = $em->getRepository('SiteBackendBundle:Colors')->find($id);
        if ($post = $request->request->all())
        {
            if( isset($post['delete']) ){
                $em->remove($color);
                $em->flush();
            }
            return $this->redirect($this->generateUrl('admin_auto_colors'));

        }
        return $this->render('SiteBackendBundle:Colors:delete.html.twig', array(
                'color' => $color,
            ));     
    }

    public function ajaxEditAction(Request $request){
        $em = $this->getDoctrine()->getManager();
        $post = $request->request->all();
        $color = $em->getRepository('SiteBackendBundle:Colors')->find($post['id']);
        $post['auto'] = serialize($post['cars']);
        unset($post['cars']);
        unset($post['id']);
        foreach ($post as $key => $value) {
            $func = "set".ucfirst($key);
            $color->{$func}($value);
        }
        $em->persist($color);
        $em->flush();
        return new Response('true');
    }
}
