<?php

namespace Site\Bundle\BackendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Site\Bundle\BackendBundle\Utils\Utils;
use Site\Bundle\BackendBundle\Entity\UserRoles;
use Site\Bundle\BackendBundle\Form\RolesForm;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class RolesController extends Controller
{
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        
        $roles = $em->createQueryBuilder()
                    ->select('r.id','r.name', 'r.rolekey','r.scheme')
                    ->from('Site\Bundle\BackendBundle\Entity\UserRoles', 'r')
                    ->getQuery()->getResult();

        return $this->render('SiteBackendBundle:Roles:index.html.twig', array(
                'roles' => $roles,
            ));    }

    public function editAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $menuAll = $em->getRepository('SiteBackendBundle:AdalrimAdminMenu')->getMenu();
        $role = $em->getRepository('SiteBackendBundle:UserRoles')->find($id);
        $schemes = $em->getRepository('SiteBackendBundle:CssSchemes')->findAll();
        $enabled = Utils::userSelectedRolePermissions($id);
        $form  = $this->createForm(new RolesForm(true), $role);     
        return $this->render('SiteBackendBundle:Roles:edit.html.twig', array(
            'enabled' => $enabled,
            'form'   => $form->createView(),
            'allmenu'=>$menuAll['menu'],
            'id' => $id,
            'schemeUser' => $role->getScheme(),
            'schemes' => $schemes,
            'role'=> $role,
            ));    
    }

    public function viewAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $menuAll = $em->getRepository('SiteBackendBundle:AdalrimAdminMenu')->getMenu();
        $role = $em->getRepository('SiteBackendBundle:UserRoles')->find($id);
        $scheme = $em->getRepository('SiteBackendBundle:CssSchemes')->find($role->getScheme());
        $enabled = Utils::userSelectedRolePermissions($id);
        $form  = $this->createForm(new RolesForm(true), $role);
        return $this->render('SiteBackendBundle:Roles:view.html.twig', array(
            'enabled' => $enabled,
            'form'   => $form->createView(),
            'allmenu'=>$menuAll['menu'],
            'id' => $id,
            'role' => $role,
            'scheme' => $scheme,
            ));    
    }
    public function ajaxEditAction(Request $request){
        $em = $this->getDoctrine()->getManager();
        $post = $request->request->all();
        $role = $em->getRepository('SiteBackendBundle:UserRoles')->find($post['id']);
        $post['perms'] = serialize($post['perms']);
        unset($post['id']);
        foreach ($post as $key => $value) {
            $func = "set".ucfirst($key);
            $role->{$func}($value);
        }
        $em->persist($role);
        $em->flush();
        return new Response('true');
    }

    public function ajaxAddAction(Request $request){
        $em = $this->getDoctrine()->getManager();
        $post = $request->request->all();
        $post['perms'] = serialize($post['perms']);

        $rolekey = $em->getRepository('SiteBackendBundle:UserRoles')->findBy(array('rolekey'=>$post['rolekey']));
        if( count($rolekey) > 0 ){
            return new Response(1);
        }
        $role = new UserRoles();
        foreach ($post as $key => $value) {
            $func = "set".ucfirst($key);
            $role->{$func}($value);
        }
        $em->persist($role);
        $em->flush();
        return new Response(0);
    }

    public function addAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
		$role = new UserRoles();
		$form   = $this->createForm(new RolesForm(), $role);
        $menuAll = $em->getRepository('SiteBackendBundle:AdalrimAdminMenu')->getMenu();
        $schemes = $em->getRepository('SiteBackendBundle:CssSchemes')->findAll();
        return $this->render('SiteBackendBundle:Roles:add.html.twig', array(
            'enabled' => array(),
            'form'   => $form->createView(),
            'allmenu'=>$menuAll['menu'],
            'schemes' => $schemes,
            ));    }

}
