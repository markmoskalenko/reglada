<?php

namespace Site\Bundle\BackendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Site\Bundle\BackendBundle\Form\SliderSlideForm;
use Site\Bundle\BackendBundle\Entity\FormFields;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class FormsController extends Controller
{
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        
        $qb = $em->createQueryBuilder();
        $qb->select('s')
            ->from('SiteBackendBundle:FormFields','s')
            ->orderBy('s.pos', 'ASC');
        $fields = $qb->getQuery()->getResult();
        
        return $this->render('SiteBackendBundle:Forms:index.html.twig', array(
                'fields' => $fields
            ));   
    }

    public function addAction(){
        return $this->render('SiteBackendBundle:Forms:add.html.twig', array());  
    }

    public function ajaxAddAction(Request $request){
        $em = $this->getDoctrine()->getManager();
        $post = $request->request->all();
        if($post['options'])
            $post['options'] = serialize($post['options']);
        $field = new FormFields();
        foreach ($post as $key => $value) {
            $func = "set".ucfirst($key);
            $field->{$func}($value);
        }
        $field->setPos(99);
        $em->persist($field);
        $em->flush();
        return new Response('true');
    }

    public function ajaxPosAction(Request $request){
        $em = $this->getDoctrine()->getManager();
        $post = $request->request->all();
        foreach ($post as $key => $value) {
            $field = $em->getRepository('SiteBackendBundle:FormFields')->find($key);
            $field->setPos($value);
        }
        $em->persist($field);
        $em->flush();
        return new Response('true');
    }

    public function ajaxEditAction(Request $request){
        $em = $this->getDoctrine()->getManager();
        $post = $request->request->all();
        if($post['options'])
            $post['options'] = serialize($post['options']);
        $field = $em->getRepository('SiteBackendBundle:FormFields')->find($post['id']);
        unset($post['id']);
        foreach ($post as $key => $value) {
            $func = "set".ucfirst($key);
            $field->{$func}($value);
        }
        $field->setPos(0);
        $em->persist($field);
        $em->flush();
        return new Response('true');
    }

    public function editAction($id){
        $em = $this->getDoctrine()->getManager();
        $field = $em->getRepository('SiteBackendBundle:FormFields')->find($id);
        return $this->render('SiteBackendBundle:Forms:edit.html.twig', array('field'=>$field));  
    }

    public function deleteAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $field = $em->getRepository('SiteBackendBundle:FormFields')->find($id);
        if ($post = $request->request->all())
        {
            if( isset($post['delete']) ){
                $em->remove($field);
                $em->flush();
            }
            return $this->redirect($this->generateUrl('admin_formsfields_index'));

        }
        return $this->render('SiteBackendBundle:Forms:delete.html.twig', array(
                'field' => $field,
            ));     
    }
}
