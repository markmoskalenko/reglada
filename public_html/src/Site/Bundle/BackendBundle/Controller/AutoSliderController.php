<?php

namespace Site\Bundle\BackendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Site\Bundle\BackendBundle\Form\BaseAutoSliderSlideForm;
use Site\Bundle\BackendBundle\Entity\BaseAutoSliderSlides;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class AutoSliderController extends Controller
{
    public function indexAction($id, $slider, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        
        $qb = $em->createQueryBuilder();
        $qb->select('s')
            ->from('SiteBackendBundle:BaseAutoSliderSlides','s')
            ->where('s.carId = '.$id.' AND s.sliderPos = '.$slider)
            ->orderBy('s.pos', 'ASC');
        $slides = $qb->getQuery()->getResult();
        
        return $this->render('SiteBackendBundle:AutoSlider:index.html.twig', array(
                'slides' => $slides,
                'car_id' => $id,
                'slider_num' => $slider
            ));
    }

    public function addAction($id, $slider, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $slide = new BaseAutoSliderSlides();
        
        $form = $this->createForm(new BaseAutoSliderSlideForm($slider), $slide);
        
        if ($post = $request->request->all())
        {
            $form->submit($request);
            $slide->setCarId($id);
            $slide->setSliderPos($slider);
            $em->persist($slide);
            $em->flush();
            
            return $this->redirect($this->generateUrl('admin_cars_slider_edit', array('id' => $id, 'slider' => $slider, 'sliderid' => $slide->getId())));
        }
        
        return $this->render('SiteBackendBundle:AutoSlider:addslide.html.twig', array(
                'form'   => $form->createView(),
                'id'   => $id,
                'slider'   => $slider,
            ));
    }

    public function editAction($id, $slider, $sliderid, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $slide = $em->getRepository('SiteBackendBundle:BaseAutoSliderSlides')->find($sliderid);
        $href = $this->getRequest()->headers->get('referer');
        $form = $this->createForm(new BaseAutoSliderSlideForm($slider), $slide);
        
        if ($post = $request->request->all())
        {
            $form->submit($request);
            
            $slide->setCarId($id);
            $slide->setSliderPos($slider);
            
            $em->persist($slide);
            $em->flush();
            
            return $this->redirect($this->generateUrl('admin_cars_slider_edit', array('id' => $id, 'slider' => $slider, 'sliderid' => $slide->getId())));
        }
        
        return $this->render('SiteBackendBundle:AutoSlider:editslide.html.twig', array(
                'form'   => $form->createView(),
                'id'   => $id,
                'slider'   => $slider,
                'previous' => $href,
            ));
    }

    public function deleteAction($id, $slider, $sliderid, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $model = $em->getRepository('SiteBackendBundle:BaseAutoSliderSlides')->find($sliderid);
        
        $em->remove($model);
        $em->flush();
        
        return $this->redirect($this->generateUrl('admin_models', array('id' => $id, 'slider' => $slider)));
    }

}
