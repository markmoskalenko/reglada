<?php

namespace Site\Bundle\BackendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Site\Bundle\BackendBundle\Utils\Utils;
use Site\Bundle\BackendBundle\Entity\UserRoles;
use Site\Bundle\BackendBundle\Entity\FeatureGroups;
use Site\Bundle\BackendBundle\Entity\FeatureValues;
use Site\Bundle\BackendBundle\Entity\AutoFeatures;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class MainFeaturesControlController extends Controller
{
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $structure = $em->getRepository('SiteBackendBundle:FeatureComplectations')->grabFeaturesStructure();      
        return $this->render('SiteBackendBundle:MainFeaturesControl:index.html.twig', array(
        'structure' => $structure,
        ));     

    }

    public function editAction($type, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $data = array();
        $data['groups'] = $em->getRepository('SiteBackendBundle:FeatureGroups')->findAll();
        if( $type == 'group' ){
            return $this->render('SiteBackendBundle:MainFeaturesControl:edit.html.twig', $data);    
        }
        if( $type == 'feature' ){
            $data['feature'] = $em->getRepository('SiteBackendBundle:AutoFeatures')->find($id);
            $data['values'] = $em->getRepository('SiteBackendBundle:FeatureValues')->findBy(array('featureId'=>$id));
            return $this->render('SiteBackendBundle:MainFeaturesControl:edit_feature.html.twig', $data);    
        }
    }

    public function viewAction($type, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $data = array();
        $data['block'] = $type;
        if( $type == 'group' ){
            
        }
        if( $type == 'feature' ){
            $data['feature'] = $em->getRepository('SiteBackendBundle:AutoFeatures')->find($id);
            $data['values'] = $em->getRepository('SiteBackendBundle:FeatureValues')->findBy(array('featureId'=>$id));
        }
        return $this->render('SiteBackendBundle:MainFeaturesControl:view.html.twig', $data);     

    }

    public function addAction($type)
    {
        $em = $this->getDoctrine()->getManager();
        $data = array();
        $data['block'] = $type;
        if( $type == 'group' ){
            
        }
        if( $type == 'feature' ){
            $data['groups'] = $em->getRepository('SiteBackendBundle:FeatureGroups')->findAll();
        }  
        return $this->render('SiteBackendBundle:MainFeaturesControl:add.html.twig', $data);     

    }

    public function deleteAction($type, $id = null,Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        if( $type == 'group' ){
            $structure = $em->getRepository('SiteBackendBundle:FeatureComplectations')->grabFeaturesStructure();      
            return $this->render('SiteBackendBundle:MainFeaturesControl:delete.html.twig', array(
            'structure' => $structure,
            ));    
        }
        if( $type == 'feature' ){
            $feature = $em->getRepository('SiteBackendBundle:AutoFeatures')->find($id);
            if ($post = $request->request->all())
            {
                if( isset($post['delete']) ){
                    $values = $em->getRepository('SiteBackendBundle:FeatureValues')->findBy(array('featureId'=>$id));
                    foreach ($values as $key => $value) {
                        $em->remove($value);
                        $em->flush();
                    }
                    $em->remove($feature);
                    $em->flush();
                }
                return $this->redirect($this->generateUrl('admin_afeatures_index'));

            }
            return $this->render('SiteBackendBundle:MainFeaturesControl:delete_feature.html.twig', array(
            'feature' => $feature,
            )); 
        }
 

    }
    public function ajaxEditAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $post = $request->request->all();
        if( $post['type'] == 'group' ){
            unset($post['type']);
            foreach ($post as $key => $value) {
                $group = $em->getRepository('SiteBackendBundle:FeatureGroups')->find($key);  
                $group->setName($value['name']);
                $group->setStatus($value['status']);
                $em->persist($group);
                $em->flush();
            }

            return new response('true');
        }
        if( $post['type'] == 'feature' ){
            $fid = $post['id'];
            unset($post['type']);
            unset($post['id']);
            $feature = $em->getRepository('SiteBackendBundle:AutoFeatures')->find($fid);
            $feature->setType($post['ftype']);
            $feature->setStatus($post['status']);
            $feature->setGroupId($post['groupId']);
            $feature->setName($post['name']);
            $em->persist($feature);
            $em->flush();

            $values = $em->getRepository('SiteBackendBundle:FeatureValues')->findBy(array('featureId'=>$fid));
            foreach ($values as $key => $value) {
                $em->remove($value);
                $em->flush();
            }

            if( $post['ftype'] == 'multiple' && isset($post['values']) && !empty($post['values']) ){
                foreach ($post['values'] as $k => $v) {
                    $newvalue = new FeatureValues();
                    $newvalue->setFeatureId($fid);
                    $newvalue->setValue($v['value']);
                    $newvalue->setDescription($v['description']);
                    $em->persist($newvalue);
                    $em->flush();
                }
            }
            return new response('true');
        }
        return new response('false');
    }
    public function ajaxDeleteAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $post = $request->request->all();
        if( $post['type'] == 'group' ){
            unset($post['type']);
            foreach ($post as $key => $value) {
                $group = $em->getRepository('SiteBackendBundle:FeatureGroups')->find($value['id']);  
                if( $value['code'] == 1 ){
                    $features = $em->getRepository('SiteBackendBundle:AutoFeatures')->findBy(array('groupId'=>$value['id']));
                    $fremove = array();
                    foreach ($features as $key => $feature) {
                        $fremove[] = $feature->getId();
                        $em->remove($feature);
                        $em->flush();
                    }
                    foreach ($fremove as $key => $value) {
                        $values = $em->getRepository('SiteBackendBundle:FeatureValues')->findBy(array('featureId'=>$value));
                        foreach ($values as $k => $v) {
                            $em->remove($v);
                            $em->flush();
                        }
                    }
                }
                else{
                    $features = $em->getRepository('SiteBackendBundle:AutoFeatures')->findBy(array('groupId'=>$value['id']));
                    foreach ($features as $key => $feature) {
                        $feature->setGroupId(null);
                        $em->persist($feature);
                        $em->flush();
                    }
                }
                $em->remove($group);
                $em->flush();
            }

            return new response('true');
        }
        if( $post['type'] == 'feature' ){

        }
        return $this->render('SiteBackendBundle:MainFeaturesControl:add.html.twig', array(
        ));    
    }
    public function ajaxAddAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $post = $request->request->all();
        if( $post['type'] == 'group' ){
            unset($post['type']);
            
            foreach ($post as $key => $value) {
                $group = new FeatureGroups();
                $group->setName($value['name']);
                $group->setStatus($value['status']);
                $em->persist($group);
                $em->flush();
            }

            return new response('true');
        }
        if( $post['type'] == 'feature' ){
            unset($post['type']);
            
            foreach ($post as $key => $value) {
                $feature = new AutoFeatures();
                $feature->setName($value['name']);
                $feature->setStatus($value['status']);
                $feature->setType($value['type']);
                $feature->setGroupId($value['groupId']);
                $em->persist($feature);
                $em->flush();
            }
            return new response('true');
        }
  

    }
}
