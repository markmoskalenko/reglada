<?php

namespace Site\Bundle\BackendBundle\Controller;

use Site\Bundle\BackendBundle\Utils\Utils;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class HomeController extends Controller
{
    public function indexAction()
    {
    	$em = $this->getDoctrine()->getManager();
    	$user = $this->getUser();
    	if ( $user ){
        	$regions = $em->getRepository('SiteBackendBundle:Regions')->find(1);
        	return $this->render('SiteBackendBundle:Home:index.html.twig', 
        		array(
            	));
    	}
		else{
			return $this->redirect($this->generateUrl('login'));
		}
    }

    public function getBreadcrumbsAction(Request $request){
        return Utils::generateBreadcrumbs();
    }
    public function getMenuAction(Request $request){
    	return Utils::generateAdminMenu();
    }
    public function getSchemeAction(Request $request){
    	return Utils::generateScheme();
    }
    public function getCabinetNameAction(Request $request){
    	return Utils::generateNameCabinet();
    }
}
