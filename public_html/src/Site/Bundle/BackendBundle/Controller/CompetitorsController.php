<?php

namespace Site\Bundle\BackendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Site\Bundle\BackendBundle\Form\BaseAutoCompetitorsAutoForm;
use Site\Bundle\BackendBundle\Form\BaseAutoCompetitorsInfoForm;
use Site\Bundle\BackendBundle\Entity\BaseAutoCompetitorsAuto;
use Site\Bundle\BackendBundle\Entity\BaseAutoCompetitorsInfo;
use Site\Bundle\BackendBundle\Entity\BaseAutoCompetitorsAutoAssigned;
use Site\Bundle\BackendBundle\Entity\BaseAutoCompetitorsAutoInfo;

class CompetitorsController extends Controller
{
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        
        $qb = $em->createQueryBuilder();
        $qb->select('b')
            ->from('SiteBackendBundle:BaseAutoCompetitorsAuto','b')
            ->orderBy('b.pos', 'ASC');
        $competitors = $qb->getQuery()->getResult();
        
        return $this->render('SiteBackendBundle:Competitors:index.html.twig', array(
                'competitors' => $competitors
            ));    
    }
    
    public function viewAction($bodyid, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        
        $qb = $em->createQueryBuilder();
        $qb->select('b')
            ->from('SiteBackendBundle:BaseAutoCompetitorsAuto','b')
            ->orderBy('b.pos', 'ASC');
        $competitors = $qb->getQuery()->getResult();
        
        $competitors_data_auto = $em->getRepository('SiteBackendBundle:BaseAutoCompetitorsAutoAssigned')->findBy(array('carId' => $bodyid));
        $competitors_auto = array();
        foreach($competitors_data_auto as $cda)
        {
            $competitors_auto[$cda->getCompetitorId()] = $cda;
        }
        
        return $this->render('SiteBackendBundle:Competitors:view.html.twig', array(
                'competitors' => $competitors, 
                'competitors_auto' => $competitors_auto, 
                'bodyid' => $bodyid, 
            ));    
    }
    
    public function assignAction($bodyid, $competitorid, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        
        $competitor = $em->getRepository('SiteBackendBundle:BaseAutoCompetitorsAutoAssigned')->findOneBy(array('carId' => $bodyid, 'competitorId' => $competitorid));
        
        if($competitor)
        {
            $em->remove($competitor);
            $em->flush();
        }
        else
        {
            $competitor = new BaseAutoCompetitorsAutoAssigned();
            $competitor->setCarId($bodyid);
            $competitor->setCompetitorId($competitorid);
            $competitor->setIsMain(0);
            $em->persist($competitor);
            $em->flush();
        }
        
        return $this->redirect($this->generateUrl('admin_competitors_view', array('bodyid' => $bodyid)));
    }
    
    public function tomainAction($bodyid, $competitorid, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        
        $competitor = $em->getRepository('SiteBackendBundle:BaseAutoCompetitorsAutoAssigned')->findOneBy(array('carId' => $bodyid, 'competitorId' => $competitorid));
        
        if($competitor)
        {
            if($competitor->getIsMain() == 1)
                $competitor->setIsMain(0);
            else
                $competitor->setIsMain(1);
                
            $em->persist($competitor);
            $em->flush();
        }
        
        
        return $this->redirect($this->generateUrl('admin_competitors_view', array('bodyid' => $bodyid)));
    }

    public function addAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $competitor = new BaseAutoCompetitorsAuto();
        
        $qb = $em->createQueryBuilder()
            ->select('i')
            ->from('Site\Bundle\BackendBundle\Entity\BaseAutoCompetitorsInfo', 'i')
            ->orderBy('i.pos', 'ASC');
        $info_values = $qb->getQuery()->getResult();
        
        $form = $this->createForm(new BaseAutoCompetitorsAutoForm(), $competitor);
        
        if ($post = $request->request->all())
        {
            $form->submit($request);
            $em->persist($competitor);
            $em->flush();
            
            
            if(!isset($post['info']))
                $post['info'] = array();
                
            foreach($post['info'] as $k => $v)
            {
                if($v != '')
                {
                    $inf = new BaseAutoCompetitorsAutoInfo();
                    $inf->setAutoId($competitor->getId());
                    $inf->setInfoId($k);
                    $inf->setValue($v);
                    $em->persist($inf);
                    $em->flush();
                }
            }
            
            
            return $this->redirect($this->generateUrl('admin_competitors_edit', array('id' => $competitor->getId())));
        }
        
        return $this->render('SiteBackendBundle:Competitors:add.html.twig', array(
                'form'   => $form->createView(),
                'info_values'   => $info_values,
            ));
    }

    public function editAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $competitor = $em->getRepository('SiteBackendBundle:BaseAutoCompetitorsAuto')->find($id);
        
        $qb = $em->createQueryBuilder()
            ->select('i')
            ->from('Site\Bundle\BackendBundle\Entity\BaseAutoCompetitorsInfo', 'i')
            ->orderBy('i.pos', 'ASC');
        $info_values = $qb->getQuery()->getResult();
        
        
        $qb = $em->createQueryBuilder()
            ->select('i')
            ->from('Site\Bundle\BackendBundle\Entity\BaseAutoCompetitorsAutoInfo', 'i');
        $info_values_auto = $qb->getQuery()->getResult();
        
        $info_auto = array();
        foreach($info_values_auto as $iva)
        {
            $info_auto[$iva->getInfoId()] = $iva;
        }
        
        $form = $this->createForm(new BaseAutoCompetitorsAutoForm(), $competitor);
        
        if ($post = $request->request->all())
        {
            $form->submit($request);
            $em->persist($competitor);
            $em->flush();
            
            
            if(!isset($post['info']))
                $post['info'] = array();
                
            $to_del_arr = array();
            foreach($post['info'] as $k => $v)
            {
                if($v !== '')
                {
                    if(array_key_exists($k, $info_auto))
                    {
                        $info_auto[$k]->setValue($v);
                        $em->persist($info_auto[$k]);
                        $em->flush();
                    }
                    else
                    {
                        $info_auto[$k] = new BaseAutoCompetitorsAutoInfo();
                        $info_auto[$k]->setAutoId($competitor->getId());
                        $info_auto[$k]->setInfoId($k);
                        $info_auto[$k]->setValue($v);
                        $em->persist($info_auto[$k]);
                        $em->flush();
                    }
                }
                else
                {
                    $to_del_arr[] = $k;
                }
            }
            if(count($to_del_arr) > 0)
            {
                $qb = $em->createQueryBuilder();
                
                $to_del = $qb->select('ci')
                    ->from('SiteBackendBundle:BaseAutoCompetitorsAutoInfo', 'ci')
                    ->where($qb->expr()->in('ci.infoId', $to_del_arr))
                    ->andWhere("ci.autoId = '$id'")
                    ->getQuery()
                    ->getResult();
                    
                foreach($to_del as $del)
                {
                    $em->remove($del);
                    $em->flush();
                }
            }
            
            
            return $this->redirect($this->generateUrl('admin_competitors_edit', array('id' => $competitor->getId())));
        }
        
        return $this->render('SiteBackendBundle:Competitors:edit.html.twig', array(
                'form'   => $form->createView(),
                'info_values'   => $info_values,
                'info_auto'   => $info_auto,
            ));
    }

    public function deleteAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        
        $info = $em->getRepository('SiteBackendBundle:BaseAutoCompetitorsAutoInfo')->findBy(array('autoId' => $id));
        
        foreach($info as $inf)
        {
            $em->remove($inf);
            $em->flush();
        }
        
        $competitorsAssigned = $em->getRepository('SiteBackendBundle:BaseAutoCompetitorsAutoAssigned')->findOneBy(array('competitorId' => $id));
        foreach ($competitorsAssigned as $k => $v) {
            $em->remove($v);
            $em->flush();
        }

        $competitor = $em->getRepository('SiteBackendBundle:BaseAutoCompetitorsAuto')->find($id);
        $em->remove($competitor);
        $em->flush();
        
        return $this->redirect($this->generateUrl('admin_competitors'));
    }

    public function addInfoAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $info = new BaseAutoCompetitorsInfo();
        
        $form = $this->createForm(new BaseAutoCompetitorsInfoForm(), $info);
        
        if ($post = $request->request->all())
        {
            $form->submit($request);
            $em->persist($info);
            $em->flush();
            
            return $this->redirect($this->generateUrl('admin_competitors_info_edit', array('id' => $info->getId())));
        }
        
        return $this->render('SiteBackendBundle:Competitors:addInfo.html.twig', array(
                'form'   => $form->createView()
            )); 
    }

    public function editInfoAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $info = $em->getRepository('SiteBackendBundle:BaseAutoCompetitorsInfo')->find($id);
        
        $form = $this->createForm(new BaseAutoCompetitorsInfoForm(), $info);
        
        if ($post = $request->request->all())
        {
            $form->submit($request);
            $em->persist($info);
            $em->flush();
            
            return $this->redirect($this->generateUrl('admin_competitors_info_edit', array('id' => $info->getId())));
        }
        
        return $this->render('SiteBackendBundle:Competitors:editInfo.html.twig', array(
                'form'   => $form->createView()
            )); 
    }

    public function deleteInfoAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        
        $info_values = $em->getRepository('SiteBackendBundle:BaseAutoCompetitorsAutoInfo')->findBy(array('infoId' => $id));
        
        foreach($info_values as $inf)
        {
            $em->remove($inf);
            $em->flush();
        }
        

        $info = $em->getRepository('SiteBackendBundle:BaseAutoCompetitorsInfo')->find($id);
        $em->remove($info);
        $em->flush();
        
        return $this->redirect($this->generateUrl('admin_competitors_info')); 
    }

    public function infoAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        
        $qb = $em->createQueryBuilder();
        $qb->select('i')
            ->from('SiteBackendBundle:BaseAutoCompetitorsInfo','i')
            ->orderBy('i.pos', 'ASC');
        $info = $qb->getQuery()->getResult();
        
        return $this->render('SiteBackendBundle:Competitors:info.html.twig', array(
                'info' => $info
            ));    
    }

}
