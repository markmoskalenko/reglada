<?php

namespace Site\Bundle\BackendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Site\Bundle\BackendBundle\Form\SliderSlideForm;
use Site\Bundle\BackendBundle\Entity\SliderSlides;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class SliderController extends Controller
{
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        
        $qb = $em->createQueryBuilder();
        $qb->select('s')
            ->from('SiteBackendBundle:SliderSlides','s')
            ->orderBy('s.pos', 'ASC');
        $slides = $qb->getQuery()->getResult();
        
        return $this->render('SiteBackendBundle:Slider:index.html.twig', array(
                'slides' => $slides
            ));    }

    public function addslideAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $slide = new SliderSlides();
        
        $form = $this->createForm(new SliderSlideForm(), $slide);
        
        if ($post = $request->request->all())
        {
            $form->submit($request);
            $em->persist($slide);
            $em->flush();
            
            return $this->redirect($this->generateUrl('admin_pages_main_slider_editslide', array('id' => $slide->getId())));
        }
        
        return $this->render('SiteBackendBundle:Slider:addslide.html.twig', array(
                'form'   => $form->createView(),
            ));
    }

    public function editslideAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $slide = $em->getRepository('SiteBackendBundle:SliderSlides')->find($id);
        
        $form = $this->createForm(new SliderSlideForm(), $slide);
        
        if ($post = $request->request->all())
        {
            $form->submit($request);
            $em->persist($slide);
            $em->flush();
            
            return $this->redirect($this->generateUrl('admin_pages_main_slider_editslide', array('id' => $slide->getId())));
        }
        
        return $this->render('SiteBackendBundle:Slider:editslide.html.twig', array(
                'form'   => $form->createView(),
            ));
    }

    public function deleteslideAction($id)
    {
        return $this->render('SiteBackendBundle:Slider:deleteslide.html.twig', array(
                // ...
            ));    }

}
