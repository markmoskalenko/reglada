<?php

namespace Site\Bundle\BackendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Site\Bundle\BackendBundle\Utils\Utils;
use Site\Bundle\BackendBundle\Entity\UserRoles;
use Site\Bundle\BackendBundle\Entity\News;
use Site\Bundle\BackendBundle\Entity\NewsGroups;
use Site\Bundle\BackendBundle\Form\RolesForm;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class NewsController extends Controller
{
    public function indexItemsAction()
    {
        $em = $this->getDoctrine()->getManager();
        $rsite = Utils::getRegionsUser();
        $filter = array();
        if ( $rsite == false ){
            return $this->render('SiteBackendBundle:Errors:error.html.twig', array(
                    // ...
                )); 
        }
        else{
            $news = array();
            $news = $em->getRepository('SiteBackendBundle:News')->findAll();
            $groups = $em->getRepository('SiteBackendBundle:NewsGroups')->findAll();   
            return $this->render('SiteBackendBundle:News:indexItems.html.twig', array(
                    'news' => $news,
                    'groups' => $groups,
                ));      
        }
        

    }
    public function indexGroupsAction()
    {
        $em = $this->getDoctrine()->getManager();
        $rsite = Utils::getRegionsUser();
        $filter = array();
        if ( $rsite == false ){
            return $this->render('SiteBackendBundle:Errors:error.html.twig', array(
                    // ...
                )); 
        }
        else{
            $groups = array();
            $groups = $em->getRepository('SiteBackendBundle:NewsGroups')->findAll();
            return $this->render('SiteBackendBundle:News:indexGroups.html.twig', array(
                    'groups' => $groups,
                ));      
        }
        

    }
    public function indexSettingsAction()
    {
        $em = $this->getDoctrine()->getManager();
        $rsite = Utils::getRegionsUser();
        $filter = array();
        if ( $rsite == false ){
            return $this->render('SiteBackendBundle:Errors:error.html.twig', array(
                    // ...
                )); 
        }
        else{
            $modes = array();
            $settings = $em->getRepository('SiteBackendBundle:NewsSettings')->findAll();
            foreach ($settings as $key => $value) {
                $modes[$value->getId()]['key'] = $value->getKey();
                $modes[$value->getId()]['name'] = $value->getName();
                if( $value->getKey() == 'tags_news' )
                    $modes[$value->getId()]['value'] = unserialize($value->getValue());
                else
                    $modes[$value->getId()]['value'] = $value->getValue();
            }
            return $this->render('SiteBackendBundle:News:indexSettings.html.twig', array(
                    'settings' => $modes,
                ));      
        }
        

    }

    public function editSettingsAction()
    {
        $em = $this->getDoctrine()->getManager();
        $rsite = Utils::getRegionsUser();
        $filter = array();
        if ( $rsite == false ){
            return $this->render('SiteBackendBundle:Errors:error.html.twig', array(
                    // ...
                )); 
        }
        else{
            $modes = array();
            $settings = $em->getRepository('SiteBackendBundle:NewsSettings')->findAll();
            foreach ($settings as $key => $value) {
                $modes[$value->getId()]['key'] = $value->getKey();
                $modes[$value->getId()]['name'] = $value->getName();
                if( $value->getKey() == 'tags_news' )
                    $modes[$value->getId()]['value'] = unserialize($value->getValue());
                else
                    $modes[$value->getId()]['value'] = $value->getValue();
            }
            return $this->render('SiteBackendBundle:News:editSettings.html.twig', array(
                    'settings' => $modes,
                ));      
        }
        

    }

    public function editItemAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $regions = $em->getRepository('SiteBackendBundle:Regions')->findAll();
        $groups = $em->getRepository('SiteBackendBundle:NewsGroups')->findAll();  
        $item = $em->getRepository('SiteBackendBundle:News')->find($id);   
        $tags = Utils::settings('news');   
        return $this->render('SiteBackendBundle:News:editItem.html.twig', array(
                'groups' => $groups,
                'regions' => $regions,
                'item' => $item,
                'tags' => unserialize($tags['tags_news']),
            ));    
    }

    public function editGroupAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $group = $em->getRepository('SiteBackendBundle:NewsGroups')->find($id);  
        $items = $em->getRepository('SiteBackendBundle:News')->findAll();
        $news = array();
        foreach ($items as $key => $item) {
            if( in_array($id, $item->getGroupId())){
                $news[] = $item;
            }
        }
        return $this->render('SiteBackendBundle:News:editGroup.html.twig', array(
                'group' => $group,
                'news' => $news,
            ));    
    }

    public function deleteItemAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $item = $em->getRepository('SiteBackendBundle:News')->find($id);
        if ($post = $request->request->all())
        {
            if( isset($post['delete']) ){
                $em->remove($item);
                $em->flush();
            }
            return $this->redirect($this->generateUrl('admin_newsitems_index'));

        }
        return $this->render('SiteBackendBundle:News:deleteItem.html.twig', array(
                'item' => $item,
            ));     
    }

    public function deleteGroupAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $group = $em->getRepository('SiteBackendBundle:NewsGroups')->find($id);
        if ($post = $request->request->all())
        {
            if( isset($post['delete']) ){
                $em->remove($group);
                $em->flush();
            }

            $news = $em->getRepository('SiteBackendBundle:News')->findAll();
            foreach ($news as $key => $value) {
                if( in_array($id, $value->getGroupId())){
                    $updated = $value->getGroupId();
                    foreach ($updated as $k => $v) {
                        if ( $v == $id )
                            $unset = $k;
                    }
                    unset($updated[$unset]);
                    $item = $em->getRepository('SiteBackendBundle:News')->find($value->getId());
                    $item->setGroupId(serialize($updated));
                    $em->persist($item);
                    $em->flush();
                }
            }
            return $this->redirect($this->generateUrl('admin_newsgroups_index'));

        }
        return $this->render('SiteBackendBundle:News:deleteGroup.html.twig', array(
                'group' => $group,
            ));     
    }
    public function ajaxEditSettingsAction(Request $request){
        $em = $this->getDoctrine()->getManager();
        $post = $request->request->all();
        foreach ($post as $key => $value) {
            $mode = $em->getRepository('SiteBackendBundle:NewsSettings')->findBy(array('key'=>$key));
            if( $key == 'tags_news' ){ $value = serialize($value); }
            $mode[0]->setValue($value);
            $em->persist($mode[0]);
            $em->flush();
        }
        return new Response('true');
    }

    public function ajaxEditGroupAction(Request $request){
        $em = $this->getDoctrine()->getManager();
        $post = $request->request->all();
        $group = $em->getRepository('SiteBackendBundle:NewsGroups')->find($post['id']);
        unset($post['id']);
        $data = $post;
        foreach ($data as $key => $value) {
            $func = "set".ucfirst($key);
            $group->{$func}($value);
        }
        $em->persist($group);
        $em->flush();
        return new Response('true');
    }

    public function ajaxEditItemAction(Request $request){
        $em = $this->getDoctrine()->getManager();
        $post = $request->request->all();
        $news = $em->getRepository('SiteBackendBundle:News')->find($post['id']);
        unset($post['id']);
        $post['groupId'] = serialize($post['groupId']);
        $post['regions'] = serialize($post['regions']);
        $post['tags'] = serialize($post['tags']);
        $data = $post;
        $data['date'] = time();
        foreach ($data as $key => $value) {
            $func = "set".ucfirst($key);
            $news->{$func}($value);
        }
        $em->persist($news);
        $em->flush();
        return new Response('true');
    }

    public function ajaxAddItemAction(Request $request){
        $em = $this->getDoctrine()->getManager();
        $post = $request->request->all();
        $post['groupId'] = serialize($post['groupId']);
        $post['regions'] = serialize($post['regions']);
        $data = $post;
        $data['date'] = time();
        $news = new News();
        foreach ($data as $key => $value) {
            $func = "set".ucfirst($key);
            $news->{$func}($value);
        }
        $em->persist($news);
        $em->flush();
        return new Response('true');
    }

    public function ajaxAddGroupAction(Request $request){
        $em = $this->getDoctrine()->getManager();
        $post = $request->request->all();
        foreach ($post as $key => $value) {
            $group = new NewsGroups();
            $group->setName($value['name']);
            $group->setStatus($value['status']);
            $em->persist($group);
            $em->flush();
        }
        return new Response('true');
    }

    public function viewGroupAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $group = $em->getRepository('SiteBackendBundle:NewsGroups')->find($id);
        $items = $em->getRepository('SiteBackendBundle:News')->findAll();
        $news = array();
        foreach ($items as $key => $item) {
            if( in_array($id, $item->getGroupId())){
                $news[] = $item;
            }
        }
        return $this->render('SiteBackendBundle:News:viewGroup.html.twig', array(
                'group' => $group,
                'news' => $news,
            ));    
    }
    public function addGroupAction()
    {
        $em = $this->getDoctrine()->getManager();
         return $this->render('SiteBackendBundle:News:addGroup.html.twig', array(
            ));    
    }
    public function addItemAction()
    {
        $em = $this->getDoctrine()->getManager();
        $regions = $em->getRepository('SiteBackendBundle:Regions')->findAll();
        $groups = $em->getRepository('SiteBackendBundle:NewsGroups')->findAll();   
        return $this->render('SiteBackendBundle:News:addItem.html.twig', array(
                'groups' => $groups,
                'regions' => $regions,
            ));    
    }
}
