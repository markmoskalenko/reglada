<?php

namespace Site\Bundle\BackendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Site\Bundle\BackendBundle\Form\ModelForm;
use Site\Bundle\BackendBundle\Entity\Cars;
use Site\Bundle\BackendBundle\Entity\CarsInfo;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class EngineController extends Controller
{
    public function modelsAction()
    {
        $em = $this->getDoctrine()->getManager();
        
        $models = $em->getRepository('SiteBackendBundle:Cars')->findBy(array('itemType' => 'engine'));
        
        
        return $this->render('SiteBackendBundle:Engine:models.html.twig', array(
                'models' => $models
            ));
    }

    public function addAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $model = new Cars();
        
        $qb = $em->createQueryBuilder()
            ->select(array('iv.id AS ivid', 'iv.name AS ivname', 'i.name AS iname', 'i.id AS iid', 'ig.name AS igname', 'ig.id AS igid'))
            ->from('Site\Bundle\BackendBundle\Entity\InfoValue', 'iv')
            ->leftJoin('SiteBackendBundle:Info','i', 'WITH', 'i.id = iv.infoId')
            ->leftJoin('SiteBackendBundle:InfoGroups','ig', 'WITH', 'ig.id = i.groupId')
            ->where("i.infoType = 'engine'");
        $info_values = $qb->getQuery()->getResult();
        
        $info_data = array();
        foreach($info_values as $iv)
        {
            if(!array_key_exists($iv['igid'], $info_data))
            {
                $info_data[$iv['igid']] = array('label' => $iv['igname'], 'features' => array());
            }
            if(!array_key_exists($iv['iid'], $info_data[$iv['igid']]['features']))
            {
                $info_data[$iv['igid']]['features'][$iv['iid']] = array('label' => $iv['iname'], 'values' => array());
            }
            $info_data[$iv['igid']]['features'][$iv['iid']]['values'][] = array('label' => $iv['ivname'], 'value' => $iv['ivid']);
        }
        
		$form = $this->createForm(new ModelForm(), $model);
        
        if ($post = $request->request->all())
        {
            $form->submit($request);
            
            $model->setItemType('engine');
            
            $em->persist($model);
            $em->flush();
            
            
            $model_id = $model->getId();
            
            if(!isset($post['info']))
                $post['info'] = array();
                
            $not_del = array();
            foreach($post['info'] as $k => $v)
            {
                if($v != 0)
                {
                    
                    $cars_data[$k] = new CarsInfo();
                    $cars_data[$k]->setCarModelId($model_id);
                    $cars_data[$k]->setInfoId($k);
                    $cars_data[$k]->setValueId($v);
                    
                    $em->persist($cars_data[$k]);
                    $em->flush();
                    $not_del[] = $cars_data[$k]->getId();
                }
            }
            
            if(count($not_del) > 0)
            {
                $qb = $em->createQueryBuilder();
                
                $to_del = $qb->select('ci')
                    ->from('SiteBackendBundle:CarsInfo', 'ci')
                    ->where($qb->expr()->notIn('ci.id', $not_del))
                    ->andWhere("ci.carModelId = '$model_id'")
                    ->getQuery()
                    ->getResult();
            }
            else
            {
                $to_del = $em->getRepository('SiteBackendBundle:CarsInfo')->findBy(array('carModelId' => $model_id));
            }
            foreach($to_del as $del)
            {
                $em->remove($del);
                $em->flush();
            }
            
            return $this->redirect($this->generateUrl('admin_engines_edit', array('id' => $model->getId())));
        }
        
        return $this->render('SiteBackendBundle:Engine:add.html.twig', array(
            'form'   => $form->createView(),
            'info_data' => $info_data
            ));
    }

    public function editAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        
        $model = $em->getRepository('SiteBackendBundle:Cars')->find($id);
        
        $qb = $em->createQueryBuilder()
            ->select(array('iv.id AS ivid', 'iv.name AS ivname', 'i.name AS iname', 'i.id AS iid', 'ig.name AS igname', 'ig.id AS igid'))
            ->from('Site\Bundle\BackendBundle\Entity\InfoValue', 'iv')
            ->leftJoin('SiteBackendBundle:Info','i', 'WITH', 'i.id = iv.infoId')
            ->leftJoin('SiteBackendBundle:InfoGroups','ig', 'WITH', 'ig.id = i.groupId')
            ->where("i.infoType = 'engine'");
        $info_values = $qb->getQuery()->getResult();
        
        $info_data = array();
        foreach($info_values as $iv)
        {
            if(!array_key_exists($iv['igid'], $info_data))
            {
                $info_data[$iv['igid']] = array('label' => $iv['igname'], 'features' => array());
            }
            if(!array_key_exists($iv['iid'], $info_data[$iv['igid']]['features']))
            {
                $info_data[$iv['igid']]['features'][$iv['iid']] = array('label' => $iv['iname'], 'values' => array());
            }
            $info_data[$iv['igid']]['features'][$iv['iid']]['values'][] = array('label' => $iv['ivname'], 'value' => $iv['ivid']);
        }
        
        $car_info = $em->getRepository('SiteBackendBundle:CarsInfo')->findBy(array('carModelId' => $id));
        $cars_data = array();
        foreach($car_info as $ci)
        {
            $cars_data[$ci->getInfoId()] = $ci;
        }
            
		$form = $this->createForm(new ModelForm(), $model);
        
        if ($post = $request->request->all())
        {
            $form->submit($request);
            $model->setItemType('engine');
            
            $em->persist($model);
            $em->flush();
            
            $model_id = $model->getId();
            
                
            
            
            if(!isset($post['info']))
                $post['info'] = array();
                
            $not_del = array();
            foreach($post['info'] as $k => $v)
            {
                if($v != 0)
                {
                    if(array_key_exists($k, $cars_data))
                    {
                        $cars_data[$k]->setValueId($v);
                    }
                    else
                    {
                        $cars_data[$k] = new CarsInfo();
                        $cars_data[$k]->setCarModelId($model_id);
                        $cars_data[$k]->setInfoId($k);
                        $cars_data[$k]->setValueId($v);
                    }
                    $em->persist($cars_data[$k]);
                    $em->flush();
                    $not_del[] = $cars_data[$k]->getId();
                }
            }
            
            if(count($not_del) > 0)
            {
                $qb = $em->createQueryBuilder();
                
                $to_del = $qb->select('ci')
                    ->from('SiteBackendBundle:CarsInfo', 'ci')
                    ->where($qb->expr()->notIn('ci.id', $not_del))
                    ->getQuery()
                    ->getResult();
            }
            else
            {
                $to_del = $em->getRepository('SiteBackendBundle:CarsInfo')->findBy(array('carModelId' => $model_id));
            }
            foreach($to_del as $del)
            {
                $em->remove($del);
                $em->flush();
            }
            
            return $this->redirect($this->generateUrl('admin_engines_edit', array('id' => $model->getId())));
        }
        
        return $this->render('SiteBackendBundle:Engine:edit.html.twig', array(
            'form'   => $form->createView(),
            'info_data' => $info_data,
            'cars_data' => $cars_data
            ));
    }

    public function deleteAction($id)
    {
        return $this->render('SiteBackendBundle:Engine:delete.html.twig', array(
                // ...
            ));    }

}
