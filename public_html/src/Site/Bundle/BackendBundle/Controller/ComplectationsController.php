<?php

namespace Site\Bundle\BackendBundle\Controller;

use Site\Bundle\BackendBundle\Utils\Utils;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Site\Bundle\BackendBundle\Entity\AutoComplectations;
use Site\Bundle\BackendBundle\Entity\FeatureComplectations;

class ComplectationsController extends Controller
{
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $structure = $em->getRepository('SiteBackendBundle:FeatureComplectations')->grabFeaturesStructureFull(true); 
        $complectations = $em->getRepository('SiteBackendBundle:AutoComplectations')->findAll(); 
        return $this->render('SiteBackendBundle:Complectations:index.html.twig', array(
        'complectations' => $complectations,
        ));     

    }

    public function addAction($type, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $structure = $em->getRepository('SiteBackendBundle:FeatureComplectations')->grabFeaturesStructureFull(true);      
        return $this->render('SiteBackendBundle:Complectations:add.html.twig', array(
        'structure' => $structure,
        'type' => $type,
        ));     

    }
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $type="dynamic";
        $structure = $em->getRepository('SiteBackendBundle:FeatureComplectations')->grabFeaturesStructureFull(true);
        $complectation = $em->getRepository('SiteBackendBundle:AutoComplectations')->find($id);   
        $features = $em->getRepository('SiteBackendBundle:FeatureComplectations')->findBy(array('complectationId'=>$id)); 
        $cfeatures = array();
        foreach ($features as $key => $value) {
                $cfeatures[$value->getFeatureId()] = $value->getFeatureValue();
            }    
        return $this->render('SiteBackendBundle:Complectations:edit.html.twig', array(
        'structure' => $structure,
        'type' => $type,
        'complectation' => $complectation,
        'cfeatures' => $cfeatures,
        ));     

    }
    public function viewAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $type="dynamic";
        $structure = $em->getRepository('SiteBackendBundle:FeatureComplectations')->grabFeaturesStructureFull(true);
        $complectation = $em->getRepository('SiteBackendBundle:AutoComplectations')->find($id);   
        $features = $em->getRepository('SiteBackendBundle:FeatureComplectations')->findBy(array('complectationId'=>$id)); 
        $cfeatures = array();
        foreach ($features as $key => $value) {
                $cfeatures[$value->getFeatureId()] = $value->getFeatureValue();
            }    
        return $this->render('SiteBackendBundle:Complectations:view.html.twig', array(
        'structure' => $structure,
        'type' => $type,
        'complectation' => $complectation,
        'cfeatures' => $cfeatures,
        ));     

    }

    public function ajaxEditAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $post = $request->request->all();
        if( $post['type'] == 'dynamic' ){
            unset($post['type']);
            $cid = $post['id'];
            $complectation = $em->getRepository('SiteBackendBundle:AutoComplectations')->find($cid);  
            $complectation->setCode($post['code']);
            $complectation->setName($post['name']);
            $complectation->setStatus($post['status']);
            $em->persist($complectation);
            $em->flush();

            unset($post['code']);
            unset($post['name']);
            unset($post['status']);
            unset($post['id']);
            $features = $em->getRepository('SiteBackendBundle:FeatureComplectations')->findBy(array('complectationId'=>$cid)); 
            foreach ($features as $key => $feature) {
                $em->remove($feature);
                $em->flush();
            }
            foreach ($post as $fid => $value) {
                $fcomplectation = new FeatureComplectations();
                $fcomplectation->setComplectationId($cid);
                $fcomplectation->setFeatureId($fid);
                $fcomplectation->setFeatureValue(null);
                if ( $value['status'] == 1 )
                    $fcomplectation->setFeatureValue($value['value-code']);
                $em->persist($fcomplectation);
                $em->flush();
            }
        }
        return new Response('true');

    }

    public function ajaxAddAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $post = $request->request->all();
        if( $post['type'] == 'dynamic' ){
            unset($post['type']);
            $complectation = new AutoComplectations();
            $complectation->setCode($post['code']);
            $complectation->setName($post['name']);
            $complectation->setStatus($post['status']);
            $em->persist($complectation);
            $em->flush();

            unset($post['code']);
            unset($post['name']);
            unset($post['status']);
            
            $cid = $complectation->getId();
            foreach ($post as $fid => $value) {
                $fcomplectation = new FeatureComplectations();
                $fcomplectation->setComplectationId($cid);
                $fcomplectation->setFeatureId($fid);
                $fcomplectation->setFeatureValue(null);
                if ( $value['status'] == 1 )
                    $fcomplectation->setFeatureValue($value['value-code']);
                $em->persist($fcomplectation);
                $em->flush();
            }
        }
        return new Response('true')  ;

    }
}
