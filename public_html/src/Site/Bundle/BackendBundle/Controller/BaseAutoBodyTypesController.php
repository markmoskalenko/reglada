<?php

namespace Site\Bundle\BackendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Site\Bundle\BackendBundle\Entity\BaseAutoBodyTypes;
use Site\Bundle\BackendBundle\Entity\BodyTypesColors;
use Site\Bundle\BackendBundle\Form\BaseAutoBodyTypesForm;

class BaseAutoBodyTypesController extends Controller
{
    public function indexAction($autoid)
    {
        $em = $this->getDoctrine()->getManager();
        
        $bodies = $em->getRepository('SiteBackendBundle:BaseAutoBodyTypes')->findBy(array('carId' => $autoid));
        
        return $this->render('SiteBackendBundle:BaseAutoBodyTypes:index.html.twig', array(
                'bodies' => $bodies,
                'autoid' => $autoid
            ));
    }

    public function addAction($autoid, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $bodytype = new BaseAutoBodyTypes();
        
        $cars = $em->getRepository('SiteBackendBundle:BaseAuto')->findAll();
        
		$form   = $this->createForm(new BaseAutoBodyTypesForm($cars, $autoid), $bodytype);
        
        $colors = $em->getRepository('SiteBackendBundle:Colors')->findAll();
        
        
        if ($post = $request->request->all())
        {
            $form->submit($request);
            
            $bodytype->setCarId($autoid);
            
            $em->persist($bodytype);
            $em->flush();
            
            
            if(isset($post['color']))
            {
                foreach($post['color'] as $k => $v)
                {
                    $nc = new BodyTypesColors();
                    $nc->setBodyTypeId($bodytype->getId());
                    $nc->setColorId($k);
                    
                    $em->persist($nc);
                    $em->flush();
                }
            }
            
            return $this->redirect($this->generateUrl('admin_auto_body_types_edit', array('id' => $bodytype->getId(), 'autoid' => $autoid)));
        }
        
        return $this->render('SiteBackendBundle:BaseAutoBodyTypes:add.html.twig', array(
                'form'   => $form->createView(),
                'autoid' => $autoid,
                'colors' => $colors,
            ));
    }

    public function editAction($autoid, $id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $bodytype = $em->getRepository('SiteBackendBundle:BaseAutoBodyTypes')->find($id);
        
        $cars = $em->getRepository('SiteBackendBundle:BaseAuto')->findAll();
        
        
        $colors = $em->getRepository('SiteBackendBundle:Colors')->findAll();
        
		$form   = $this->createForm(new BaseAutoBodyTypesForm($cars), $bodytype);
        
        
        $colors_data = $em->getRepository('SiteBackendBundle:BodyTypesColors')->findByBodyTypeId($id);
        $colors_data_assigned = array();
        foreach($colors_data as $cd)
        {
            $colors_data_assigned[$cd->getColorId()] = $cd;
        }
        
        
        if ($post = $request->request->all())
        {
            $form->submit($request);
            
            $bodytype->setCarId($autoid);
            
            $em->persist($bodytype);
            $em->flush();
            
            
            $not_del = array();
            if(isset($post['color']))
            {
                foreach($post['color'] as $k => $v)
                {
                    if(isset($colors_data_assigned[$k]))
                    {
                        $not_del[] = $colors_data_assigned[$k]->getId();
                    }
                    else
                    {
                        $colors_data_assigned[$k] = new BodyTypesColors();
                        $colors_data_assigned[$k]->setBodyTypeId($id);
                        $colors_data_assigned[$k]->setColorId($k);
                        
                        $em->persist($colors_data_assigned[$k]);
                        $em->flush();
                        $not_del[] = $colors_data_assigned[$k]->getId();
                    }
                }
            }
                
            if(count($not_del) > 0)
            {
                $qb = $em->createQueryBuilder();
                
                $to_del = $qb->select('ci')
                    ->from('SiteBackendBundle:BodyTypesColors', 'ci')
                    ->where($qb->expr()->notIn('ci.id', $not_del))
                    ->andWhere("ci.bodyTypeId = '$id'")
                    ->getQuery()
                    ->getResult();
            }
            else
            {
                $to_del = $em->getRepository('SiteBackendBundle:BodyTypesColors')->findBy(array('bodyTypeId' => $id));
            }
            foreach($to_del as $del)
            {
                $em->remove($del);
                $em->flush();
            }
        
            
            return $this->redirect($this->generateUrl('admin_auto_body_types_edit', array('id' => $bodytype->getId(), 'autoid' => $autoid)));
        }
        
        return $this->render('SiteBackendBundle:BaseAutoBodyTypes:edit.html.twig', array(
                'form'   => $form->createView(),
                'autoid' => $autoid,
                'colors' => $colors,
                'colors_data_assigned' => $colors_data_assigned
            ));
    }

    public function deleteAction($autoid, $id)
    {
        $em = $this->getDoctrine()->getManager();
        
        $bodytype = $em->getRepository('SiteBackendBundle:BaseAutoBodyTypes')->find($id);
        
        $models = $em->getRepository('SiteBackendBundle:Cars')->findBy(array('bodyId' => $id));
        
        foreach($models as $model)
        {
            $to_del = $em->getRepository('SiteBackendBundle:CarsInfo')->findBy(array('carModelId' => $model->getId()));
            foreach($to_del as $del)
            {
                $em->remove($del);
                $em->flush();
            }
            $to_del = $em->getRepository('SiteBackendBundle:BaseAutoReviewsAsigned')->findBy(array('carId' => $model->getId()));
            foreach($to_del as $del)
            {
                $em->remove($del);
                $em->flush();
            }
            
            $em->remove($model);
            $em->flush();
        }
        
        
        $em->remove($bodytype);
        $em->flush();
        
        return $this->redirect($this->generateUrl('admin_auto_body_types_index', array('autoid' => $autoid)));
    }

}
