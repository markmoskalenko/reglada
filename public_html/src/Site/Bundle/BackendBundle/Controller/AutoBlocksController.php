<?php

namespace Site\Bundle\BackendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Site\Bundle\BackendBundle\Form\BaseAutoBlocksForm;
use Site\Bundle\BackendBundle\Entity\BaseAutoCarsBlocks;

class AutoBlocksController extends Controller
{
    public function indexAction($id, $block_type, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        
        $qb = $em->createQueryBuilder();
        $qb->select('b')
            ->from('SiteBackendBundle:BaseAutoCarsBlocks','b')
            ->where('b.carId = '.$id.' AND b.blockType = '.$block_type)
            ->orderBy('b.pos', 'ASC');
        $blocks = $qb->getQuery()->getResult();
        return $this->render('SiteBackendBundle:AutoBlocks:index.html.twig', array(
                'blocks' => $blocks,
                'car_id' => $id,
                'block_type' => $block_type,
            ));
    }

    public function addAction($id, $block_type, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $block = new BaseAutoCarsBlocks();
        
        $form = $this->createForm(new BaseAutoBlocksForm(), $block);
        
        if ($post = $request->request->all())
        {
            $form->submit($request);
            $block->setCarId($id);
            $block->setBlockType($block_type);
            $em->persist($block);
            $em->flush();
            
            return $this->redirect($this->generateUrl('admin_cars_blocks_edit', array('id' => $id, 'block_type' => $block_type, 'blockid' => $block->getId())));
        }
        
        return $this->render('SiteBackendBundle:AutoBlocks:add.html.twig', array(
                'form'   => $form->createView(),
                'id'   => $id,
                'block'   => $block,
            ));
    }

    public function editAction($id, $block_type, $blockid, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $block = $em->getRepository('SiteBackendBundle:BaseAutoCarsBlocks')->find($blockid);
        
        $form = $this->createForm(new BaseAutoBlocksForm(), $block);
        $href = $this->getRequest()->headers->get('referer');
        if ($post = $request->request->all())
        {
            $form->submit($request);
            $block->setCarId($id);
            $block->setBlockType($block_type);
            $em->persist($block);
            $em->flush();
            
            return $this->redirect($this->generateUrl('admin_cars_blocks_edit', array('id' => $id, 'block_type' => $block_type, 'blockid' => $block->getId())));
        }
        
        return $this->render('SiteBackendBundle:AutoBlocks:edit.html.twig', array(
                'form'   => $form->createView(),
                'id'   => $id,
                'block'   => $block,
                'previous' => $href,
            )); 
    }

    public function deleteAction()
    {
        return $this->render('SiteBackendBundle:AutoBlocks:delete.html.twig', array(
                // ...
            ));    }

}
