<?php

namespace Site\Bundle\BackendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Site\Bundle\BackendBundle\Entity\Discount;
use Site\Bundle\BackendBundle\Utils\Utils;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DiscountController extends Controller
{
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $rsite = Utils::getRegionsUser();
        $filter = array();
        if ( $rsite == false ){
            return $this->render('SiteBackendBundle:Errors:error.html.twig', array(
                    // ...
                )); 
        }
        else{
            $qb = $em->createQueryBuilder();
            $qb->select('c')
                ->from('SiteBackendBundle:Discount','c');
            $discounts = $qb->getQuery()->getArrayResult();
            return $this->render('SiteBackendBundle:Discount:index.html.twig', array(
                    'discounts' => $discounts,
                ));      
        }
    }

    public function ajaxAddAction(Request $request){
        $em = $this->getDoctrine()->getManager();
        $post = $request->request->all();
        $post['auto'] = $post['cars'];
        unset($post['periodMax']);
        unset($post['periodMin']);
        unset($post['firstpayMax']);
        unset($post['firstpayMin']);
        unset($post['cars']);
        $discount = new Discount();
        foreach ($post as $key => $value) {
            $func = "set".ucfirst($key);
            $discount->{$func}($value);
        }
        $em->persist($discount);
        $em->flush();
        return new Response(1);
    }

    public function ajaxEditAction(Request $request){
        $em = $this->getDoctrine()->getManager();
        $post = $request->request->all();
        $discount = $em->getRepository('SiteBackendBundle:Discount')->find($post['id']);   
        $post['auto'] = $post['cars'];
        unset($post['periodMax']);
        unset($post['periodMin']);
        unset($post['firstpayMax']);
        unset($post['firstpayMin']);
        unset($post['cars']);
        unset($post['id']);
        foreach ($post as $key => $value) {
            $func = "set".ucfirst($key);
            $discount->{$func}($value);
        }
        $em->persist($discount);
        $em->flush();
        return new Response(1);
    }

    public function addAction()
    {
        $em = $this->getDoctrine()->getManager();
        $rsite = Utils::getRegionsUser();
        $filter = array();
        if ( $rsite == false ){
            return $this->render('SiteBackendBundle:Errors:error.html.twig', array(
                    // ...
                )); 
        }
        else{
            $regions = $em->getRepository('SiteBackendBundle:Regions')->findAll();
            $cars = Utils::minimalCarsPrices();
            return $this->render('SiteBackendBundle:Discount:add.html.twig', array(
                    'cars' => $cars,
                    'regions' => $regions,
                ));      
        }
    }

    public function editAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $regions = $em->getRepository('SiteBackendBundle:Regions')->findAll();

        $cars = Utils::minimalCarsPrices();
        $item = $em->getRepository('SiteBackendBundle:Discount')->find($id);  
        $data = array();
        $data['name'] = $item->getName();
        $data['id'] = $item->getId();
        $data['status'] = $item->getName();
        $data['persent'] = $item->getPersent();
        $data['summ'] = $item->getSumm();
        $data['firstpay'] = $item->getFirstpay();
        $data['period'] = $item->getPeriod();
        $data['auto'] = $item->getAuto();
        $data['regions'] = $item->getRegions();

        return $this->render('SiteBackendBundle:Discount:edit.html.twig', array(
                'regions' => $regions,
                'cars' => $cars,
                'item' => $data,
            ));     
    }

    public function deleteAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $item = $em->getRepository('SiteBackendBundle:Discount')->find($id);
        if ($post = $request->request->all())
        {
            if( isset($post['delete']) ){
                $em->remove($item);
                $em->flush();
            }
            return $this->redirect($this->generateUrl('admin_basediscount_index'));

        }
        return $this->render('SiteBackendBundle:Shares:delete.html.twig', array(
                'item' => $item,
            ));     
    }

}
