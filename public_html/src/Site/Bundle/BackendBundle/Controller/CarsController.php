<?php

namespace Site\Bundle\BackendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Site\Bundle\BackendBundle\Form\CarsForm;
use Site\Bundle\BackendBundle\Entity\BaseAuto;
use Site\Bundle\BackendBundle\Entity\BaseAutoCompetitorsAutoAssigned;

class CarsController extends Controller
{
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        
        $autos = $em->getRepository('SiteBackendBundle:BaseAuto')->findAll();
        
        return $this->render('SiteBackendBundle:Cars:index.html.twig', array(
                'autos' => $autos,
            ));    
    }

    public function addAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $model = new BaseAuto();
        
		$form = $this->createForm(new CarsForm(), $model);
        
        
        if ($post = $request->request->all())
        {
            $form->submit($request);
            
            $em->persist($model);
            $em->flush();
            
            
            return $this->redirect($this->generateUrl('admin_cars_edit', array('id' => $model->getId())));
        }
        
        return $this->render('SiteBackendBundle:Cars:add.html.twig', array(
            'form'   => $form->createView(),
            ));
    }

    public function editAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        
        $model = $em->getRepository('SiteBackendBundle:BaseAuto')->find($id);
        
		$form = $this->createForm(new CarsForm(), $model);
        
        
        if ($post = $request->request->all())
        {
            $form->submit($request);
            
            $em->persist($model);
            $em->flush();
            
            return $this->redirect($this->generateUrl('admin_cars_edit', array('id' => $model->getId())));
        }
        
        return $this->render('SiteBackendBundle:Cars:edit.html.twig', array(
            'form'   => $form->createView()
            ));
    }

    public function deleteAction($id, Request $request)
    {
        return $this->render('SiteBackendBundle:Cars:delete.html.twig', array(
                // ...
            ));
    }

}
