<?php

namespace Site\Bundle\BackendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Site\Bundle\BackendBundle\Utils\Utils;
use Site\Bundle\BackendBundle\Entity\User;
use Site\Bundle\BackendBundle\Entity\RegionsUsers;
use Site\Bundle\BackendBundle\Form\UsersForm;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class UsersController extends Controller
{
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $rsite = Utils::getRegionsUser();
        if ( $rsite == false ){
            return $this->render('SiteBackendBundle:Errors:error.html.twig', array(
                    // ...
                )); 
        }
        else{
            $users = $em->getRepository('SiteBackendBundle:User')->getUsers($rsite);
            return $this->render('SiteBackendBundle:Users:index.html.twig', array(
                    'users' => $users,
                ));    
        }
    }

    public function addAction()
    {
        return $this->render('SiteBackendBundle:Users:add.html.twig', array(
                // ...
            ));    }

    public function viewAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('SiteBackendBundle:User')->find($id);
        
        $all_roles = $em->createQueryBuilder()
                    ->select('r.id','r.name', 'r.rolekey')
                    ->from('Site\Bundle\BackendBundle\Entity\UserRoles', 'r')
                    ->getQuery()->getResult();

        $regionsSites = $em->createQueryBuilder()
                    ->select('r')
                    ->from('Site\Bundle\BackendBundle\Entity\Regions', 'r')
                    ->getQuery()->getResult();

        $form = $this->createForm(new UsersForm(), $user);
        return $this->render('SiteBackendBundle:Users:view.html.twig', array(
                'form' => $form->createView(),
                'all_roles' => $all_roles,
                'user_roles' => $user->getRoles(),
                'regionsSites' => $regionsSites,
                'u' =>''.'1'
            ));    
        }


    public function editAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('SiteBackendBundle:User')->find($id);
        $cur = null;
        $regionSite = $em->createQueryBuilder()
                    ->select('ru.regionId')
                    ->from('SiteBackendBundle:RegionsUsers', 'ru')
                    ->where('ru.userId = :id')->setParameter('id',$id);
                    $regionSite = $regionSite->getQuery()->getArrayResult();

        if (count($regionSite) > 0){
           $cur = $regionSite[0]['regionId'];
        }

        $all_roles = $em->createQueryBuilder()
                    ->select('r.id','r.name', 'r.rolekey')
                    ->from('Site\Bundle\BackendBundle\Entity\UserRoles', 'r')
                    ->getQuery()->getResult();

        $regionsSites = $em->createQueryBuilder()
                    ->select('r')
                    ->from('Site\Bundle\BackendBundle\Entity\Regions', 'r')
                    ->getQuery()->getResult();
     
        
        $diller_centres = $em->getRepository('SiteBackendBundle:DillerCentre')->findAll();

		$form = $this->createForm(new UsersForm(), $user);
       
        if ($post = $request->request->all())
        {
            $form->submit($request);
            $roles = array('ROLE_USER');
            if(isset($post['role']))
                $roles[] = $post['role'];
            if( isset($post['site']) && $post['site'] != 'none'){
                $ruser = $em->getRepository('SiteBackendBundle:RegionsUsers')->findBy(array('userId'=>$id));
                if ( count($ruser) == 0 ){
                    $regionsUsers = new RegionsUsers();
                    $regionsUsers->setUserId($id);
                    $regionsUsers->setRegionId($post['site']);
                    $em->persist($regionsUsers);
                    $em->flush();
                }
                else{
                    $ruser =  $ruser[0];
                    $ruser->setRegionId($post['site']);
                    $em->persist($ruser);
                    $em->flush();
                }              
            }
            else{
                $ruser = $em->getRepository('SiteBackendBundle:RegionsUsers')->findBy(array('userId'=>$id));
                if ( count($ruser) == 0 ){
                        $em->remove($ruser[0]);
                        $em->flush();
                }
            }
            $user->setRoles($roles);
            
            if(isset($post['diller_centre']))
            {
                $user->setDillerCentreIds($post['diller_centre']);
            }
            
            $em->persist($user);
            $em->flush();
            
            return $this->redirect($this->generateUrl('admin_users_edit', array(
                'id' => $user->getId(),
                )));
        }
        
        return $this->render('SiteBackendBundle:Users:edit.html.twig', array(
                'form' => $form->createView(),
                'all_roles' => $all_roles,
                'user_roles' => $user->getRoles(),
                'regionsSites' => $regionsSites,
                'current_region_site' => $cur,
                'diller_centres' => $diller_centres,
                'user' => $user
            ));    }

    public function deleteAction($id)
    {
        return $this->render('SiteBackendBundle:Users:delete.html.twig', array(
                // ...
            ));    }

    public function permissionsAction($id)
    {
        return $this->render('SiteBackendBundle:Users:permissions.html.twig', array(
                // ...
            ));    }


}
