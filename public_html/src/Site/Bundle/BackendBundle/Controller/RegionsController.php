<?php

namespace Site\Bundle\BackendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Site\Bundle\BackendBundle\Utils\Utils;
use Site\Bundle\BackendBundle\Entity\UserRoles;
use Site\Bundle\BackendBundle\Entity\Regions;
use Site\Bundle\BackendBundle\Entity\BlockSliderSettings;
use Site\Bundle\BackendBundle\Form\RolesForm;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class RegionsController extends Controller
{
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        // $sliders = array();
        // $sliders = serialize($sliders);
        // $s = new BlockSliderSettings();
        // $s->setType('custom');
        // $s->setSliders($sliders);
        $rsite = Utils::getRegionsUser();
        if( $rsite == false ){
            return $this->render('SiteBackendBundle:Errors:error.html.twig', array(
                    // ...
                )); 
        }
        else{
        $regions = $em->createQueryBuilder()
                ->select('r')
                ->from('Site\Bundle\BackendBundle\Entity\Regions', 'r');
                    if ( is_array($rsite) ){
                        $regions->where('r.id = :id')->setParameter('id',$rsite['id']);
                    }
                $regions->orderBy('r.name','ASC');
                $regions = $regions->getQuery()->getResult();

        return $this->render('SiteBackendBundle:Regions:index.html.twig', array(
                'regions' => $regions,
            ));    
        }
    }

    public function deleteAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $region = $em->getRepository('SiteBackendBundle:Regions')->find($id);
        if ($post = $request->request->all())
        {
            if( isset($post['delete']) ){
                $regions = $em->getRepository('SiteBackendBundle:RegionsUsers')->findBy(array('regionId'=>$id));
                foreach ($regions as $key => $value) {
                    $em->remove($value);
                    $em->flush();
                }
                $em->remove($region);
                $em->flush();

            }
            return $this->redirect($this->generateUrl('admin_regionssites_index'));

        }
        return $this->render('SiteBackendBundle:Regions:delete.html.twig', array(
                'region' => $region,
            ));    
    }

    public function editAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $roles = $em->getRepository('SiteBackendBundle:UserRoles')->findAll();
        $uroles = array();
        foreach ($roles as $key => $value) {
            $uroles[$value->getRolekey()] = $value->getName();
        }

        $region = $em->getRepository('SiteBackendBundle:Regions')->find($id);
        $gtime = gmdate("h:i A", time());
        $diff = $region->getTimeZone();
        $ltime = gmdate("h:i A", time()+$diff*3600);

        $users = array();
        $users = $em->getRepository('SiteBackendBundle:Regions')->getRegionUsers($id);
        return $this->render('SiteBackendBundle:Regions:edit.html.twig', array(
                'region' => $region,
                'id' => $id,
                'gtime' => $gtime,
                'ltime' => $ltime,
                'users' => $users,
                'uroles' =>$uroles,
            ));    
    }

    public function addAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $roles = $em->getRepository('SiteBackendBundle:UserRoles')->findAll();
        $uroles = array();
        foreach ($roles as $key => $value) {
            $uroles[$value->getRolekey()] = $value->getName();
        }
        $gtime = gmdate("h:i A", time());
        return $this->render('SiteBackendBundle:Regions:add.html.twig', array(
                'gtime' => $gtime,
                'uroles' =>$uroles,
            ));    
    }

    public function ajaxEditAction(Request $request){
        $em = $this->getDoctrine()->getManager();
        $post = $request->request->all();
        $reg = $em->getRepository('SiteBackendBundle:Regions')->findBy(array('alias'=>$post['alias']));
            if(count($reg) > 0){
                if($reg[0]->getId() != $post['id'] ){
                    return new Response('false');
                }
            }
        $role = $em->getRepository('SiteBackendBundle:Regions')->find($post['id']);
        unset($post['id']);
        if ( isset($post['users']) ){
            $users = $post['users'];
            unset($post['users']);
            foreach ($users as $key => $value) {
                $user = $em->getRepository('SiteBackendBundle:RegionsUsers')->findBy(array('userId'=>$value));
                $em->remove($user[0]);
                $em->flush();
            }
        }
        foreach ($post as $key => $value) {
            $func = "set".ucfirst($key);
            $role->{$func}($value);
        }
        $em->persist($role);
        $em->flush();
        return new Response('true');
    }

    public function ajaxAddAction(Request $request){
        $em = $this->getDoctrine()->getManager();
        $post = $request->request->all();

        $reg = $em->getRepository('SiteBackendBundle:Regions')->findBy(array('alias'=>$post['alias']));
            if(count($reg) > 0){
                return new Response('false');
            }
        $region = New Regions();
        foreach ($post as $key => $value) {
            $func = "set".ucfirst($key);
            $region->{$func}($value);
        }
        $em->persist($region);
        $em->flush();
        return new Response('true');
    }

    public function viewAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $roles = $em->getRepository('SiteBackendBundle:UserRoles')->findAll();
        $uroles = array();
        foreach ($roles as $key => $value) {
            $uroles[$value->getRolekey()] = $value->getName();
        }

        $region = $em->getRepository('SiteBackendBundle:Regions')->find($id);
        $gtime = gmdate("h:i A", time());
        $diff = $region->getTimeZone();
        $ltime = gmdate("h:i A", time()+$diff*3600);

        $users = array();
        $users = $em->getRepository('SiteBackendBundle:Regions')->getRegionUsers($id);
        return $this->render('SiteBackendBundle:Regions:view.html.twig', array(
                'region' => $region,
                'id' => $id,
                'gtime' => $gtime,
                'ltime' => $ltime,
                'users' => $users,
                'uroles' =>$uroles,
            ));    
    }

}
