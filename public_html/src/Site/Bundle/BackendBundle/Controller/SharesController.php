<?php

namespace Site\Bundle\BackendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Site\Bundle\BackendBundle\Utils\Utils;
use Site\Bundle\BackendBundle\Entity\UserRoles;
use Site\Bundle\BackendBundle\Entity\Shares;
use Site\Bundle\BackendBundle\Form\RolesForm;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class SharesController extends Controller
{
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $rsite = Utils::getRegionsUser();
        $filter = array();
        if ( $rsite == false ){
            return $this->render('SiteBackendBundle:Errors:error.html.twig', array(
                    // ...
                )); 
        }
        else{
            $shares = array();
            $shares = $em->getRepository('SiteBackendBundle:Shares')->findAll();
            return $this->render('SiteBackendBundle:Shares:index.html.twig', array(
                    'shares' => $shares,
                ));      
        }
        

    }

    public function viewAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $regions = $em->getRepository('SiteBackendBundle:Regions')->findAll();
        $item = $em->getRepository('SiteBackendBundle:Shares')->find($id);   
        $cars = $em->getRepository('SiteBackendBundle:BaseAuto')->findAll();
        $dc = null;
        if( $item->getDc() != null )
            $dc = $em->getRepository('SiteBackendBundle:DillerCentre')->find($item->getDc());
        return $this->render('SiteBackendBundle:Shares:view.html.twig', array(
                'regions' => $regions,
                'item' => $item,
                'cars' => $cars,
                'dc' => $dc,
            ));    
    }

    public function editAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $regions = $em->getRepository('SiteBackendBundle:Regions')->findAll();
        $cars = $em->getRepository('SiteBackendBundle:BaseAuto')->findAll();
        $item = $em->getRepository('SiteBackendBundle:Shares')->find($id);   
        return $this->render('SiteBackendBundle:Shares:edit.html.twig', array(
                'regions' => $regions,
                'item' => $item,
                'cars' => $cars,
            ));    
    }

    public function deleteAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $item = $em->getRepository('SiteBackendBundle:Shares')->find($id);
        if ($post = $request->request->all())
        {
            if( isset($post['delete']) ){
                $em->remove($item);
                $em->flush();
            }
            return $this->redirect($this->generateUrl('admin_baseshares_index'));

        }
        return $this->render('SiteBackendBundle:Shares:delete.html.twig', array(
                'item' => $item,
            ));     
    }


    public function ajaxEditAction(Request $request){
        $em = $this->getDoctrine()->getManager();
        $post = $request->request->all();
        $share = $em->getRepository('SiteBackendBundle:Shares')->find($post['id']);
        unset($post['id']);
        if( isset($post['regions']) )
            $post['regions'] = serialize($post['regions']);
        else
            $post['regions'] = serialize(array());
        if( isset($post['cars']) )
            $post['cars'] = serialize($post['cars']);
        else
            $post['cars'] = serialize(array());
        $data = $post;
        foreach ($data as $key => $value) {
            $func = "set".ucfirst($key);
            $share->{$func}($value);
        }
        $em->persist($share);
        $em->flush();
        return new Response(1);
    }

    public function ajaxAddAction(Request $request){
        $em = $this->getDoctrine()->getManager();
        $post = $request->request->all();
        if( isset($post['regions']) )
            $post['regions'] = serialize($post['regions']);
        else
            $post['regions'] = serialize(array());
        if( isset($post['cars']) )
            $post['cars'] = serialize($post['cars']);
        else
            $post['cars'] = serialize(array());
        $data = $post;
        $data['author'] = $this->getUser()->getId();
        $data['regionParent'] = Utils::getRegionsUser();
        $share = new Shares();
        foreach ($data as $key => $value) {
            $func = "set".ucfirst($key);
            $share->{$func}($value);
        }
        $em->persist($share);
        $em->flush();
        return new Response(1);
    }

    public function addAction()
    {
        $em = $this->getDoctrine()->getManager();
        $regions = $em->getRepository('SiteBackendBundle:Regions')->findAll();
        $cars = $em->getRepository('SiteBackendBundle:BaseAuto')->findAll();
        return $this->render('SiteBackendBundle:Shares:add.html.twig', array(
                'regions' => $regions,
                'cars' => $cars,
            ));    
    }
}
