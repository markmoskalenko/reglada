<?php

namespace Site\Bundle\BackendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Site\Bundle\BackendBundle\Form\BaseAutoReviewsForm;
use Site\Bundle\BackendBundle\Entity\BaseAutoReviews;
use Site\Bundle\BackendBundle\Entity\BaseAutoReviewsAsigned;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ReviewsController extends Controller
{
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        
        $reviews = $em->getRepository('SiteBackendBundle:BaseAutoReviews')->findAll();
        
        return $this->render('SiteBackendBundle:Reviews:index.html.twig', array(
                'reviews' => $reviews,
            ));
    }

    public function addAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $review = new BaseAutoReviews();
        
		$form = $this->createForm(new BaseAutoReviewsForm(), $review);
        
        
        
        $qb = $em->createQueryBuilder()
            ->select('bab.id AS model_id, bab.name AS model_name, ba.id AS base_auto_id, ba.name AS base_auto_name')
            ->from('SiteBackendBundle:BaseAutoBodyTypes', 'bab')
            ->leftJoin('SiteBackendBundle:BaseAuto','ba', 'WITH', 'ba.id = bab.carId');
        $cars = $qb->getQuery()->getResult();
        
        $cars_data = array();
        foreach($cars as $c)
        {
            if(!array_key_exists($c['model_id'], $cars_data))
            {
                $cars_data[$c['model_id']] = $c['model_name'].' '.$c['base_auto_name'];
            }
        }
        
        if($post = $request->request->all())
        {
            $form->submit($request);
            
            $review->setDateAdd(new \DateTime());
            $review->setDateUpdate(new \DateTime());
            
            $em->persist($review);
            $em->flush();
            
            if(!isset($post['car']))
                $post['car'] = array();
                
            $cars_data = array();
            foreach($post['car'] as $k => $v)
            {
                $new_rev = new BaseAutoReviewsAsigned();
                $new_rev->setCarId($k);
                $new_rev->setReviewId($review->getId());
                
                $em->persist($new_rev);
                $em->flush();
            }
            
            
            return $this->redirect($this->generateUrl('admin_reviews_edit', array('id' => $review->getId())));
        }
        
        return $this->render('SiteBackendBundle:Reviews:add.html.twig', array(
            'form'   => $form->createView(),
            'cars_data' => $cars_data
            ));
    }

    public function editAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        
        $review = $em->getRepository('SiteBackendBundle:BaseAutoReviews')->find($id);
		$form = $this->createForm(new BaseAutoReviewsForm(), $review);
        
        
        $qb = $em->createQueryBuilder()
            ->select('bab.id AS model_id, bab.name AS model_name, ba.id AS base_auto_id, ba.name AS base_auto_name')
            ->from('SiteBackendBundle:BaseAutoBodyTypes', 'bab')
            ->leftJoin('SiteBackendBundle:BaseAuto','ba', 'WITH', 'ba.id = bab.carId');
        $cars = $qb->getQuery()->getResult();
        
        $cars_data = array();
        foreach($cars as $c)
        {
            if(!array_key_exists($c['model_id'], $cars_data))
            {
                $cars_data[$c['model_id']] = $c['model_name'].' '.$c['base_auto_name'];
            }
        }
        
        $qb = $em->createQueryBuilder()
            ->select('ra')
            ->from('SiteBackendBundle:BaseAutoReviewsAsigned', 'ra')
            ->where("ra.reviewId = '".$review->getId()."'");
        $ass_reviews_data = $qb->getQuery()->getResult();
        
        $ass_reviews = array();
        foreach($ass_reviews_data as $ard)
        {
            $ass_reviews[$ard->getCarId()] = $ard;
        }
        
        
        if($post = $request->request->all())
        {
            $form->submit($request);
            
            $review->setDateAdd(new \DateTime());
            $review->setDateUpdate(new \DateTime());
            
            $em->persist($review);
            $em->flush();
            
            if(!isset($post['car']))
                $post['car'] = array();
                
            $not_del = array();
            foreach($post['car'] as $k => $v)
            {
                
                if(array_key_exists($k, $ass_reviews))
                {
                    $em->persist($ass_reviews[$k]);
                    $em->flush();
                }
                else
                {
                    $ass_reviews[$k] = new BaseAutoReviewsAsigned();
                    $ass_reviews[$k]->setCarId($k);
                    $ass_reviews[$k]->setReviewId($review->getId());
                    
                    $em->persist($ass_reviews[$k]);
                    $em->flush();
                }
                $not_del[] = $ass_reviews[$k]->getId();
            }
            
            if(count($not_del) > 0)
            {
                $qb = $em->createQueryBuilder();
                
                $to_del = $qb->select('ci')
                    ->from('SiteBackendBundle:BaseAutoReviewsAsigned', 'ci')
                    ->where($qb->expr()->notIn('ci.id', $not_del))
                    ->andWhere("ci.reviewId = '".$review->getId()."'")
                    ->getQuery()
                    ->getResult();
            }
            else
            {
                $to_del = $em->getRepository('SiteBackendBundle:BaseAutoReviewsAsigned')->findBy(array('reviewId' => $review->getId()));
            }
            foreach($to_del as $del)
            {
                $em->remove($del);
                $em->flush();
            }
            
            return $this->redirect($this->generateUrl('admin_reviews_edit', array('id' => $review->getId())));
        }
        
        return $this->render('SiteBackendBundle:Reviews:edit.html.twig', array(
            'form'   => $form->createView(),
            'cars_data' => $cars_data,
            'ass_reviews' => $ass_reviews
            ));
    }

    public function deleteAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        
        $to_del = $em->getRepository('SiteBackendBundle:BaseAutoReviewsAsigned')->findBy(array('reviewId' => $id));
        
        foreach($to_del as $del)
        {
            $em->remove($del);
            $em->flush();
        }
        $review = $em->getRepository('SiteBackendBundle:BaseAutoReviews')->find($id);
        
        $em->remove($review);
        $em->flush();
        
        return $this->redirect($this->generateUrl('admin_reviews_index'));
    }

}
