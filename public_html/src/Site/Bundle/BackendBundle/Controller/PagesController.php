<?php

namespace Site\Bundle\BackendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Site\Bundle\BackendBundle\Utils\Utils;
use Site\Bundle\BackendBundle\Entity\UserRoles;
use Site\Bundle\BackendBundle\Entity\Pages;
use Site\Bundle\BackendBundle\Form\RolesForm;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class PagesController extends Controller
{
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $rsite = Utils::getRegionsUser();
        $filter = array();
        if ( $rsite == false ){
            return $this->render('SiteBackendBundle:Errors:error.html.twig', array(
                    // ...
                )); 
        }
        else{
            $pages = $em->getRepository('SiteBackendBundle:Pages')->findAll();
            return $this->render('SiteBackendBundle:Pages:index.html.twig', array(
                    'pages' => $pages,
                ));      
        }
        

    }

    public function editAction($id, $default, Request $request){
        $em = $this->getDoctrine()->getManager();
        $regions = $em->getRepository('SiteBackendBundle:Regions')->findAll();
        $page = $em->getRepository('SiteBackendBundle:Pages')->find($id);
        if( $default == 1 ){
            $options = unserialize($page->getDefaultOptions());
        }
        return $this->render('SiteBackendBundle:Pages:edit.html.twig', array(
                'page'=> $page,
                'default' => $default,
                'regions' => $regions,
                'options' => $options,
            ));    
    }

    public function ajaxEditAction(Request $request){
        $em = $this->getDoctrine()->getManager();
        $post = $request->request->all();
        $page = $em->getRepository('SiteBackendBundle:Pages')->find($post['id']);
        if( isset($post['defaultOptions']))
            $post['defaultOptions'] = serialize($post['defaultOptions']);
        unset($post['id']);
        unset($post['default']);
        foreach ($post as $key => $value) {
            $func = "set".ucfirst($key);
            $page->{$func}($value);
        }
        $em->persist($page);
        $em->flush();
        return new Response('true');
    }
}
