<?php

namespace Site\Bundle\BackendBundle\Utils;

use Site\Bundle\BackendBundle\Entity\Requests;
use Site\Bundle\BackendBundle\Entity\MassMail;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;

class Utils
{

    public static function checkPermissions(){
        $kernel = $GLOBALS['kernel'];
        $container = $kernel->getContainer();
        $url = $_SERVER['REQUEST_URI'];
        $session = $kernel->getContainer()->get('session');
        $adminPermissions = $session->get('admin.permissions');
        if( preg_match('/admin/', $url) ){
            $route = explode('admin', $url);
            $key = $route[1];
            if( $key && $key != null && $key != '' ){
                $keys = array_filter(explode('/', $key));
                $xkeys = array();
                foreach ($keys as $i => $j) {
                    $xkeys[] = $j;
                }
                if( isset($adminPermissions[$xkeys[0]]) ){
                    $session->set('admin.current-permissions.create',$adminPermissions[$xkeys[0]]['c']);
                    $session->set('admin.current-permissions.edit',$adminPermissions[$xkeys[0]]['e']);
                    $session->set('admin.current-permissions.delete',$adminPermissions[$xkeys[0]]['d']);
                    $t = array('r','c','e','d');
                    $a = array('view','add','edit','delete');

                    if ( isset($xkeys[1]) ){

                        if (in_array($xkeys[1], $a)){
                            $needle = array_search($xkeys[1], $a);
                            if ( $adminPermissions[$xkeys[0]][$t[$needle]] == 1 ){
                                return true;
                            }
                            else{
                                return false;
                            }

                        }
                    }                  
                    return true;
                }
                else{
                    return 0;
                }
            }
            return true;
        }
        else{
            //front pages
            return true;
        }
    }

    public static function getRegionAlias(){
        $kernel = $GLOBALS['kernel'];
        $em = $kernel->getContainer()->get('doctrine.orm.entity_manager');
        $region = $em->getRepository('SiteBackendBundle:Regions')->find(1);
        $url = $_SERVER['HTTP_HOST'];
        $sub = explode('.', $url);
        if( $sub[0] == 'reglada')
            return $region->getAlias();
        else
            return $sub[0];
    }

    public static function getRegion(){
        $kernel = $GLOBALS['kernel'];
        $em = $kernel->getContainer()->get('doctrine.orm.entity_manager');
        $region = $em->getRepository('SiteBackendBundle:Regions')->find(1);
        $url = $_SERVER['HTTP_HOST'];
        $sub = explode('.', $url);
        if( $sub[0] == 'reglada')
            return $region;
        else{
            $region = $em->getRepository('SiteBackendBundle:Regions')->findBy(array('alias'=>$sub[0]));
            return  $region[0];
        }
            
    }
    public static function getNewsForMain(){
        $kernel = $GLOBALS['kernel'];
        $em = $kernel->getContainer()->get('doctrine.orm.entity_manager');
        $news = $em->getRepository('SiteBackendBundle:News')->findBy(array('top'=>1));
        if(count($news) == 0){
			$ut = new Utils;
            return $ut->getthisnews($em, Utils::getRegion(), 0);
        }
        return $news[0];
    }

    private function getthisnews($em, $region, $i){
            $qb = $em->createQueryBuilder();
            $qb->select('i.name, i.date, i.teaser, i.picture, i.regions')->from('SiteBackendBundle:News', 'i')
                        ->andWhere('i.status = :status')->setParameter('status', 1)->orderBy('i.date', 'DESC')->setFirstResult($i)->setMaxResults(1);
            $result = $qb->getQuery()->getResult();
            if (count($result) > 0)
            {
                if (in_array($region->getId(), unserialize($result[0]['regions'])))
                    return $result[0];
                else
                    return Utils::getthisnews($em, $region->getId(), $i++);
            }
    }

    public static function ifRegionsExists(){
        $kernel = $GLOBALS['kernel'];
        $em = $kernel->getContainer()->get('doctrine.orm.entity_manager');
        $region = $em->getRepository('SiteBackendBundle:Regions')->find(1);
        $url = $_SERVER['HTTP_HOST'];
        $sub = explode('.', $url);
        if( $sub[0] == 'reglada')
            return true;
        else{
            $region = $em->getRepository('SiteBackendBundle:Regions')->findBy(array('alias'=>$sub[0]));
            if ($region){
                return true;
            }
            else{
                return false;
            }
        }
            
    }

    public static function generateBreadcrumbs(){
        $kernel = $GLOBALS['kernel'];
        $em = $kernel->getContainer()->get('doctrine.orm.entity_manager');
        $container = $kernel->getContainer();
        $url = $_SERVER['REQUEST_URI'];
        $session = $kernel->getContainer()->get('session');
        $adminPermissions = $session->get('admin.permissions');
        $links = array();
        if( preg_match('/admin/', $url) ){
            $route = explode('admin', $url);
            $key = $route[1];
            if( $key && $key != null && $key != '' ){
                $keys = explode('/', $key);
                $connection = $em->getConnection();
                $query = "SELECT * FROM adalrim_admin_menu where action='".$keys[1]."'";
                $statement = $connection->prepare($query);
                $statement->execute();
                $title = $statement->fetchAll();
                if( isset($keys[2]) ){
                    $actions = array('view'=>'просмотр','add'=>'создание','edit'=>'редактирование','delete'=>'удаление');
                    if( isset($actions[$keys[2]]))
                        $links['javascript:void(0)'] = $actions[$keys[2]];
                }
                $links['/admin/'.$title[0]['action']] =  $title[0]['title'];
                if( $title[0]['parent'] != 0 ){
                    Utils::getLoopInner($title, $em, $links);
                }
  
        }
            $dataforbreadcrumbs = array();
            $dataforbreadcrumbs['links'] = array_reverse($links);
            return $kernel->getContainer()->get('templating')->renderResponse('SiteBackendBundle:Home:breadcrumbs.html.twig', $dataforbreadcrumbs); 
        }
        else{
            //front pages
            return true;
        }
    }

    public static function getLoopInner($array, $em, &$links){
        $connection = $em->getConnection();
        $query = "SELECT * FROM adalrim_admin_menu where id='".$array[0]['parent']."'";
        $statement = $connection->prepare($query);
        $statement->execute();
        $title = $statement->fetchAll();
        $k = count($links) + 1;
        if( $title[0]['target'] == 1 ){
            $k = '/admin/'.$title[0]['action'];
        }
        $links[$k] =  $title[0]['title'];
        if( $title[0]['parent'] != 0 ){
            Utils::getLoopInner($title, $em, $links);
        }
    }

    public static function  generateFormGeneral($alias, $code){
        $kernel = $GLOBALS['kernel'];
        $em = $kernel->getContainer()->get('doctrine.orm.entity_manager');
        $data = array();
        $form = $em->getRepository('SiteBackendBundle:FormsConstructor')->findBy(array('alias'=>$alias));
        if( count($form) > 0 ){
            $options = $form[0]->getOptions();
        }
        $data['fields'] = array();
        $data['fields'] = $em->getRepository('SiteBackendBundle:FormFields')->getFields(array_keys($options));
        foreach ($data['fields'] as $k => $v) {
            if( $options[$v->getId()] != $code )
                unset($data['fields'][$k]);
        }
        $data['allFields'] = Utils::getFieldsAttributes($alias);
        $data['formtype'] = str_replace('form-', '', $alias);
        return $kernel->getContainer()->get('templating')->renderResponse('SiteFrontendBundle:Home:form.html.twig', $data); 
    }

    public static function getFieldsAttributes($type){
        $kernel = $GLOBALS['kernel'];
        $em = $kernel->getContainer()->get('doctrine.orm.entity_manager');
        $form = $em->getRepository('SiteBackendBundle:FormsConstructor')->findBy(array('alias'=>$type));
        $options = array();
        if( count($form) > 0 ){
            $options = $form[0]->getOptions();
        }
        $data['fields'] = array();
        $data['fields'] = $em->getRepository('SiteBackendBundle:FormFields')->getFieldsArray(array_keys($options));
        return $data['fields'];
    }

    public static function generateScheme(){
        $kernel = $GLOBALS['kernel'];
        $em = $kernel->getContainer()->get('doctrine.orm.entity_manager');
        $user = $kernel->getContainer()->get('security.context')->getToken()->getUser();
        $response = 1;
        if( $user ){
            $uid = $user->getId();      
            $role = $em->getRepository('SiteBackendBundle:User')->find($uid);
            $rrole = $role->getRoles();
            $role = $em->getRepository('SiteBackendBundle:UserRoles')->findBy(array('rolekey'=>$rrole[0]));
            $scheme = $em->getRepository('SiteBackendBundle:CssSchemes')->find($role[0]->getScheme());
            $response = $scheme->getKey();
        }
        else{
            return $this->redirect($this->generateUrl('admin_indexindex'));
        }
        return new Response($response);
    }

    public static function generateNameCabinet(){
        $kernel = $GLOBALS['kernel'];
        $em = $kernel->getContainer()->get('doctrine.orm.entity_manager');
        $uid = $kernel->getContainer()->get('security.context')->getToken()->getUser()->getId();
        $role = $em->getRepository('SiteBackendBundle:User')->find($uid);
        $rrole = $role->getRoles();
        $role = $em->getRepository('SiteBackendBundle:UserRoles')->findBy(array('rolekey'=>$rrole[0]));
        $name = $role[0]->getCabinetName();
        return new Response($name);
    }

    public static function getRegionsUser(){
        $kernel = $GLOBALS['kernel'];
        $em = $kernel->getContainer()->get('doctrine.orm.entity_manager');
        //$roles = $kernel->getContainer()->get('security.context')->getToken()->getRoles();
        $uid = $kernel->getContainer()->get('security.context')->getToken()->getUser()->getId();
        $role = $em->getRepository('SiteBackendBundle:User')->find($uid);
        $rrole = $role->getRoles();

        $role = $em->getRepository('SiteBackendBundle:UserRoles')->findBy(array('rolekey'=>$rrole[0]));
        $name = $role[0]->getRegions();
        if ( $name == 0 ){
            $rUser = $em->getRepository('SiteBackendBundle:RegionsUsers')->findBy(array('userId'=>$uid));
            if($rUser)
                $rid = $rUser[0]->getRegionId();
            else
                return false;
        }
        else{
            return true;
        }
        $regionssite = array();
        $site = $em->getRepository('SiteBackendBundle:Regions')->find($rid);
        if($site)
            $regionssite = array('all'=>$name,'alias'=>$site->getAlias(),'id'=>$rid);

        return $regionssite;
    }

    public static function generateAdminMenu(){
        $kernel = $GLOBALS['kernel'];
        $em = $kernel->getContainer()->get('doctrine.orm.entity_manager');
        $roles = $kernel->getContainer()->get('security.context')->getToken()->getRoles();
        $permissions = array();
        $levels = array();
        $levels[0] = null;
        $levels[1] = null;
        $levels[2] = null;
        $url = $_SERVER['REQUEST_URI'];
        $url = explode('/admin/',$url);
        if(count($url) > 1){
        $url = $url[1];
        $levels = explode('-', $url);
        }
        foreach ($roles as $key => $value) {
            $role = $value->getRole();
            $perms = $em->getRepository('SiteBackendBundle:AdalrimAdminMenu')->getPermissions($role);
            foreach ( $perms as $k => $v) {
                $permissions[$k] = $v;
                $x = 0;
                foreach ($v as $i => $j) { if( $j == 1 ){ $x++; } }
                if ( $x == 0 ){ unset($permissions[$k]); }
            }
        }

        $menu = $em->getRepository('SiteBackendBundle:AdalrimAdminMenu')->getMenuSideBar($permissions);
        $dataformenu = array();
        $dataformenu['menu'] = $menu['menu'];
        $dataformenu['level1'] = $levels[0];
        if( count($levels) > 1 ){
            $dataformenu['level2'] = $levels[0].'-'.$levels[1];
            if( count($levels) > 2 ){
                $dataformenu['level3'] = $levels[0].'-'.$levels[1].'-'.$levels[2];
            }
        }
        return $kernel->getContainer()->get('templating')->renderResponse('SiteBackendBundle:Home:menu.html.twig', $dataformenu);    
    }

    static function userSelectedRolePermissions ($id = null, $full = false)
    {
        $kernel = $GLOBALS['kernel'];
        $em = $kernel->getContainer()->get('doctrine.orm.entity_manager');
        $role = $em->getRepository('SiteBackendBundle:UserRoles')->find($id);
        if ($role){
            $permissions = unserialize($role->getPerms());
        }
        if ( $full == false ){
            return $permissions;
        }
        else{
            $perms = array();
            foreach ($permissions as $key => $value) {
                $item = null;
                //echo $key;
                $item = $em->getRepository('SiteBackendBundle:AdalrimAdminMenu')->find($key);
                // var_dump($item);
                // foreach ($item as $k => $v) {
                // }
                if( $item != null )
                    $perms[$item->getAction()] = $value;
            }
            return $perms;
        }
    }

    static function userRolesPermissions ()
    {
        $kernel = $GLOBALS['kernel'];
        $em = $kernel->getContainer()->get('doctrine.orm.entity_manager');
        
        return array(
            array('label' => 'Главная АЧС',
                  'routes' => array('admin_index'  => 'Просмотр')),
            array('label' => 'Пользователи',
                  'routes' => array('admin_users_index'  => 'Просмотр',
                                    'admin_users_add'    => 'Добавление',
                                    'admin_users_edit'   => 'Редактирование',
                                    'admin_users_delete' => 'Удаление')),
            array('label' => 'Роли',
                  'routes' => array('admin_roles_index'  => 'Просмотр',
                                    'admin_roles_edit'   => 'Редактирование',
                                    'admin_roles_add'    => 'Добавление'))
        );
    }

    static function settings($type)
    {
        $kernel = $GLOBALS['kernel'];
        $em = $kernel->getContainer()->get('doctrine.orm.entity_manager');
        $settings = array();
        if( $type == 'news' ){
            $modes = array();
            $settings = $em->getRepository('SiteBackendBundle:NewsSettings')->findAll();
            foreach ($settings as $key => $value) {
                $modes[$value->getKey()] = $value->getValue();
            }
            $settings = $modes;
        }
        return $settings;
    }

    static function minimalCarsPrices($carId = null){
        $kernel = $GLOBALS['kernel'];
        $em = $kernel->getContainer()->get('doctrine.orm.entity_manager');
        $minimalCars = array();
        $cars = $em->getRepository('SiteBackendBundle:BaseAuto')->findAll();
        if( $carId ){
            $cars = array();
            $cars[] = $em->getRepository('SiteBackendBundle:BaseAuto')->find($carId);
        }
        foreach ($cars as $i => $car) {
            if($car){
            $id = $car->getId();
            $cid = $id;

            $minimalCars[$cid] = array();
            $minimalCars[$cid]['name'] = preg_replace('#Lada#iu', 'LADA', $car->getName());
            
            $qb = $em->createQueryBuilder();
            $qb->select('b.id, b.name,b.file')
                    ->from('SiteBackendBundle:BaseAutoBodyTypes','b')
                    ->where("b.carId = '$cid'")->orderBy("b.pos");;
                
                $v = $qb->getQuery()->getArrayResult();
            foreach ($v as $key => $value) {
                $minimalCars[$cid]['details'][$value['name']] = $value['file'];
            }
        }
    }
        return $minimalCars;
    }

    static function minimalCarsPricesMain($carId = null){
        $kernel = $GLOBALS['kernel'];
        $em = $kernel->getContainer()->get('doctrine.orm.entity_manager');
        $minimalCars = array();
        $cars = $em->getRepository('SiteBackendBundle:BaseAuto')->findAll();
        if( $carId ){
            $cars = array();
            $cars[] = $em->getRepository('SiteBackendBundle:BaseAuto')->find($carId);
        }
        foreach ($cars as $i => $car) {
            if($car){
                $id = $car->getId();
                $cid = $id;

                $minimalCars[$cid] = array();
                $minimalCars[$cid]['name'] = preg_replace('#Lada#iu', 'LADA', $car->getName());
                
                $qb = $em->createQueryBuilder();
                $qb->select('b.id, b.name,b.file')
                        ->from('SiteBackendBundle:BaseAutoBodyTypes','b')
                        ->where("b.carId = '$cid'")->orderBy("b.pos");
                    
                    $v = $qb->getQuery()->getArrayResult();
                foreach ($v as $key => $value) {
                    $minimalCars[$cid]['details'][$value['name']] = array();
                    $minimalCars[$cid]['details'][$value['name']]['image'] = $value['file'];
                    $minimalCars[$cid]['details'][$value['name']]['id'] = $value['id'];
                }
            }
        }
        return $minimalCars;
    }

    static function minimalCarsCompetitors(&$labels, $carId = null, $bodyId = null){
        $kernel = $GLOBALS['kernel'];
        $em = $kernel->getContainer()->get('doctrine.orm.entity_manager');
        $minimalCars = array();
        $cars = $em->getRepository('SiteBackendBundle:BaseAuto')->findAll();
        if( $carId ){
            $cars = array();
            $cars[] = $em->getRepository('SiteBackendBundle:BaseAuto')->find($carId);
        }
        foreach ($cars as $i => $car) {
            if($car){
            $id = $car->getId();
            $cid = $id;

            $minimalCars[$cid] = array();
            $minimalCars[$cid]['name'] = $car->getName();
            
            $qb = $em->createQueryBuilder();
            $qb->select('b.id, b.name, b.file')
                    ->from('SiteBackendBundle:BaseAutoBodyTypes','b');
                    if( $bodyId == null )
                        $qb->where("b.carId = '$cid'");
                    else
                        $qb->where("b.id = '$bodyId'");
                    $qb->orderBy('b.pos');
            $v = $qb->getQuery()->getArrayResult();

            foreach ($v as $key => $value) {
                $cvalue = $value['id'];

                $qb = $em->createQueryBuilder();
                $qb->select(array('ci.id','ci.name','ci.file','a.carId'))
                    ->from('SiteBackendBundle:BaseAutoCompetitorsAutoAssigned','a')
                    ->leftJoin('SiteBackendBundle:BaseAutoCompetitorsAuto', 'ci', 'WITH', "ci.id = a.competitorId")
                    ->where("a.carId = '$cvalue' and a.isMain = 1")->setMaxResults(3);
                $vi = $qb->getQuery()->getArrayResult();
                $info = array();
                foreach ($vi as $k => $v) {
                    $coId = $v['id'];

                    if( $coId ){
                        $qb = $em->createQueryBuilder();
                        $qb->select(array('cc.id','cc.value','ci.id as iid','ci.name as iname','ci.maxValue as ivalue'))
                            ->from('SiteBackendBundle:BaseAutoCompetitorsAutoInfo','cc')
                            ->leftJoin('SiteBackendBundle:BaseAutoCompetitorsInfo', 'ci', 'WITH', "ci.id = cc.infoId")
                            ->where("cc.autoId = '$coId'")->orderBy('ci.pos');
                        $c = $qb->getQuery()->getArrayResult();
                        $info[$coId] = array();
                        $info[$coId]['name'] = $v['name'];
                        $info[$coId]['image'] = $v['file'];
                        $info[$coId]['info'] = $c;



                    if( count($c) > 0 && count($labels) < 5 ){
                        foreach ($c as $ki => $ci) {
                            $labels[$ki] = array();
                            $labels[$ki]['iname'] = $ci['iname'];
                            $labels[$ki]['ivalue'] = $ci['ivalue'];
                        }
                        
                    }
                }
                }
                $minimalCars[$cid]['details'][$value['name']] = array();
                $minimalCars[$cid]['details'][$value['name']]['image'] = $value['file'];
                $minimalCars[$cid]['details'][$value['name']]['competitors'] = $info;

                $name = $value['name'];
                $qb = $em->createQueryBuilder();
                $qb->select('MIN(ci.price)')
                        ->from('SiteBackendBundle:BaseAutoBodyTypes','b')
                        ->leftJoin('SiteBackendBundle:Cars', 'ci', 'WITH', "ci.bodyId = b.id")
                        ->where("b.carId = '$cid' and b.name='$name'");
                $m = $qb->getQuery()->getArrayResult();

                $minimalCars[$cid]['details'][$value['name']]['price'] = $m[0];


            }
        }
    }
        return $minimalCars;
    }
    static function getFinance($data, &$page, &$info, $price = true){
        $kernel = $GLOBALS['kernel'];
        $em = $kernel->getContainer()->get('doctrine.orm.entity_manager');

        $page['bodies'] = $page['transmissions'] = $page['powers'] = $page['capacity'] = array();
        foreach ($data as $c => $car) {
            foreach ($car['types'] as $t => $type) {
                $bodies = $transmissions = $capacity = $powers = array();
                $body = $em->getRepository('SiteBackendBundle:BaseAutoBodyTypes')->findBy(array('name'=>$t, 'carId' => $c));

                if( count($body) > 0 ){
                    $bodyId = $body[0]->getId();
                    $qb = $em->createQueryBuilder();
                    $qb->select(array('c.id','c.name as ename','c.price','bodyselect.valueId as body','box.valueId as transmission','run.valueId as capacity','powers.valueId as power'))
                    ->from('SiteBackendBundle:Cars','c')
                    ->leftJoin('SiteBackendBundle:CarsInfo', 'powers', 'WITH', "powers.carModelId = c.id and powers.infoId = 23")
                    ->leftJoin('SiteBackendBundle:CarsInfo', 'bodyselect', 'WITH', "bodyselect.carModelId = c.id and bodyselect.infoId = 7")
                    ->leftJoin('SiteBackendBundle:CarsInfo', 'run', 'WITH', "run.carModelId = c.id and run.infoId = 10")
                    ->leftJoin('SiteBackendBundle:CarsInfo', 'box', 'WITH', "box.carModelId = c.id and box.infoId = 11");
                    
                    if( $price == false )
                        $qb->where("c.bodyId = ".$bodyId);
                    if( $price == true )
                        $qb->where("c.price is not null and c.bodyId = ".$bodyId);
                    $equipments = $qb->getQuery()->getResult();

                    foreach ($equipments as $key => $value) {
                        $values = array('body'=>false,'transmission'=>false,'capacity'=>false,'power'=>false);
                        if( $value['body'] != '' && $em->getRepository('SiteBackendBundle:InfoValue')->find($value['body']) ){
                            $values['body'] = $em->getRepository('SiteBackendBundle:InfoValue')->find($value['body'])->getName();
                            $bodies[] =$values['body'];
                        }
                        if( $value['transmission'] != '' && $em->getRepository('SiteBackendBundle:InfoValue')->find($value['transmission']) ){
                            $values['transmission'] = $em->getRepository('SiteBackendBundle:InfoValue')->find($value['transmission'])->getName();
                            $transmissions[] = $values['transmission'];
                        }
                        if( $value['capacity'] != '' && $em->getRepository('SiteBackendBundle:InfoValue')->find($value['capacity']) ){
                            $values['capacity'] = $em->getRepository('SiteBackendBundle:InfoValue')->find($value['capacity'])->getName();
                            $capacity[] = $values['capacity'];
                        }
                        if( $value['power'] != '' && $em->getRepository('SiteBackendBundle:InfoValue')->find($value['power']) ){
                            $values['power'] = $em->getRepository('SiteBackendBundle:InfoValue')->find($value['power'])->getName();
                            $powers[] = $values['power'];
                        }
                    }
                    $page['bodies'] = array_unique(array_merge($page['bodies'], $bodies));			//Тип кузова
                    $page['transmissions'] = array_unique(array_merge($page['transmissions'], $transmissions));	//КПП
                    $page['capacity'] = array_unique(array_merge($page['capacity'], $capacity));	//Вместимость багажника
                    $page['powers'] = array_unique(array_merge($page['powers'], $powers));			//Расход топлива

                    $details = array(
						'bodies' => $bodies,
						'transmissions' => $transmissions,
						'capacity' => $capacity,
						'powers' => $powers
					);
                    if (!isset($info[$c])) $info[$c] = array();
                    $info[$c][$bodyId] = $details;
                }
            }
        }
        sort($page['powers']);
        sort($page['bodies']);
    }

	//Функция для сравнения машин
	//Получаем список комплектаций для указанного кузова, а так же список доступных двигателей, исполнений и моделей
    static function getComplectations($carBodyId)		//id кузова из base_auto_body_types
    {
        $kernel = $GLOBALS['kernel'];
        $em = $kernel->getContainer()->get('doctrine.orm.entity_manager');

        $qb = $em->createQueryBuilder();

		//Сперва выберем все комплектации этой машины
		$qb->select('c')
		->from('SiteBackendBundle:Cars', 'c')
		->where('c.bodyId = :body')->setParameter('body', $carBodyId)
		->orderBy('c.name');

        $_cmp = $qb->getQuery()->getResult();
        $complects = array();

		$n = 0;
		foreach ($_cmp as $cpl)
		{
			$complects[$n]['id'] = $cpl->getId();
			$complects[$n]['name'] = $cpl->getName();
			$complects[$n++]['price'] = (float) $cpl->getPrice();
		}

		//Ищем комплектацию с минимальной ценой
		$cheapest = 0;		//Номер самой дешёвой комплектации
		$cheapestPrice = $complects[$cheapest]['price'];	//Начальная цена для поиска самой дешёвой
		$it = sizeof($complects);
		while ($it)
			if ($complects[--$it]['price'] < $cheapestPrice)
				$cheapestPrice = $complects[$cheapest = $it]['price'];

//Заполняем характеристики комплектаций
		//Сюда будем выбирать списки всех имеющихся моделей, двигателей и исполнений, потом выберем уникальные
		$engines = $execs = $models = $execSTR = array();

		//Пройдём по комплектациям и соберём их характеристики
		foreach ($complects as &$compl)
		{
			$chars = array();
			$cid = $compl['id'];		//Идентификатор комплектации
			
			$qb = $em->createQueryBuilder();
			$qb->select(array('iv.name as value', 'ci.valueId as carinfovalue', 'i.groupId as groupid', 'i.id as infoid', 'i.name as infoname', 'i.seltype as valuetype',
								'i.description as descr', 'i.image as image', 'i.pages as showinpage', 'i.identifier as interid'))
			->from('SiteBackendBundle:CarsInfo', 'ci')
			->join('SiteBackendBundle:InfoValue', 'iv', 'WITH', 'ci.valueId = iv.id')
			->join('SiteBackendBundle:Info', 'i', 'WITH', 'ci.infoId = i.id')
			->where('ci.carModelId = :cid')->setParameter('cid', $cid)
			->orderBy('i.pos');
			$_grp = $qb->getQuery()->getResult();

			//Перебираем характеристики
			foreach ($_grp as $cv)
			{
				$iid = $cv['infoid'];
				$char = $cv['infoname'];
				$value = $cv['value'];
				$group = (int) $cv['groupid'];
				//Для yesno у нас другая интерпретация значений
				if ($cv['valuetype'] == 'yesno')
					$value = ($cv['carinfovalue'] == 2) ? 'да' : 'нет';
				$description = $cv['descr'];
				$image = $cv['image'];
				$ShowOnConf = unserialize($cv['showinpage']);		//Отображать в конфигураторе?
				$interid = $cv['interid'];

				$chars[$iid]['name'] = $char;
				$chars[$iid]['value'] = $value;
				$chars[$iid]['group'] = $group;
				$chars[$iid]['description'] = $description;
				$chars[$iid]['image'] = $image;
				$chars[$iid]['intercept_id'] = $interid;
				$chars[$iid]['onconfigurator'] = (int)$ShowOnConf[3];

				//Найдём нужные нам параметры, по которым потом будем фильтровать комплектации
				switch ($iid)
				{
					case 27:
						$compl['model'] = $value;
						$models[] = $value;
					break;

					case 29:
						$compl['engine'] = $value;
						$engines[] = $value;
					break;

					case 31:
						$compl['exec'] = $value;
						$execs[] = $value;
						$compl['name'] = $value.'-'.$compl['name'];
					break;
				}
			}

			$compl['chars'] = $chars;		//Запсиываем все параметры комплектации внутрь массива с комплектацией
		}
		$models = array_unique($models);
		$engines = array_unique($engines);
		$execs = array_unique($execs);
		sort($models);
		sort($engines);
		sort($execs);
//*************************************

		//Находим имеющиеся группы характеристик
		$groups = array();
		$_grp = $qb->select('g')->from('SiteBackendBundle:InfoGroups', 'g')->getQuery()->getResult();
		foreach ($_grp as $grp)
			$groups[$grp->getId()] = $grp->getName();

		$result_array['complects'] = $complects;	//Массив комплектация
		$result_array['cheapest'] = $cheapest;		//Номер самой дешёвой комплектации, начнём отображение с неё
		$result_array['groups'] = $groups;			//Имеющиеся группы характеристик
		$result_array['models'] = $models;			//модели
		$result_array['engines'] = $engines;		//Двигатели
		$result_array['execs'] = $execs;			//Исполнения

		return $result_array;
    }

    static function crossFeatures($eqs, &$details, &$special){
        $kernel = $GLOBALS['kernel'];
        $em = $kernel->getContainer()->get('doctrine.orm.entity_manager');
        $allinfo = array();
        foreach ($eqs as $key => $value) {
            $qb = $em->createQueryBuilder();
            $qb->select(array('i.id as fid ','i.name as fname','g.id as gid','g.name as gname','iv.id as vid','iv.name as vname','c.carModelId as eid'))
                ->from('SiteBackendBundle:CarsInfo','c')
                ->leftJoin('SiteBackendBundle:Info', 'i', 'WITH', "c.infoId = i.id")
                ->leftJoin('SiteBackendBundle:InfoValue', 'iv', 'WITH', "iv.infoId = i.id and iv.id = c.valueId")
                ->leftJoin('SiteBackendBundle:InfoGroups', 'g', 'WITH', "g.id = i.groupId");
            $qb->where("c.carModelId = ".$key);
            $allinfo[$key] = $qb->getQuery()->getResult();
        }
        $details = array();
        foreach ($allinfo as $key => $value) {
            foreach ($value as $k => $v) {
                if( $v['vname'] != 'нет' ){
                    if( !isset($details[$v['gid']]) ){
                        $details[$v['gid']] = array();
                        $details[$v['gid']]['name'] = $v['gname'];
                    }
                    if( !isset($details[$v['gid']][$v['fid'].'-'.$v['vid']]) ){
                        $details[$v['gid']][$v['fid'].'-'.$v['vid']] = array();
                    }
                    if( !isset($details[$v['gid']][$v['fid'].'-'.$v['vid']]['equipments']) )
                        $details[$v['gid']][$v['fid'].'-'.$v['vid']]['equipments'] = array();
                    $details[$v['gid']][$v['fid'].'-'.$v['vid']]['equipments'][$v['eid']] = $v['eid'];
                    if ( $v['vname'] == 'да' )
                        $details[$v['gid']][$v['fid'].'-'.$v['vid']]['value'] = $v['fname'];
                    if ( $v['vname'] != '' && $v['vname'] != 'да' )
                        $details[$v['gid']][$v['fid'].'-'.$v['vid']]['value'] = $v['fname'].'('.$v['vname'].')';
                    
                }
            }
        }
        foreach ($details as $key => $value) {
            foreach ($value as $k => $v) {
                if( $k != 'name' ){
                    if( count($v['equipments']) != count($eqs) || !isset($v['value'])){
                        
                        foreach ($eqs as $q => $w) {
                            $name = $em->getRepository('SiteBackendBundle:Cars')->findBy(array('id'=>$q));
                            $name = $name[0]->getName();
                            if( !in_array($q, $v['equipments']) ){
                                if( !isset($special[$name]['values']) && isset($v['value']) && $v['value'] != '' ){
                                    $special[$name]['price'] = $w;
                                    $special[$name]['values'] = array();
                                }
                                if( isset($v['value']) && $v['value'] != '' )
                                $special[$name]['values'][$k] = $v['value'];

                            }
                        }
                        unset($details[$key][$k]);
                        
                    }
                    
                }
                
            }
        }
        //$details = $allinfo;
        return $details;
    }

    static function getcarsTypesList(){
        $kernel = $GLOBALS['kernel'];
        $em = $kernel->getContainer()->get('doctrine.orm.entity_manager');
        $cars = $em->getRepository('SiteBackendBundle:BaseAuto')->findAll();
        $info = array();
        foreach ($cars as $key => $car) {
            $id = $car->getId();
            $data = array();

            $qb = $em->createQueryBuilder();
            $qb->select('c.id as qid','b.name as value','c.price as price','b.file','b.id as bid')
                ->from('SiteBackendBundle:BaseAutoBodyTypes','b')
                ->leftJoin('SiteBackendBundle:Cars', 'c', 'WITH', "c.bodyId = b.id")
                ->where("b.carId = '$id' and c.price is not null");
            $types = $qb->getQuery()->getArrayResult();
            $data['name'] = str_replace('Lada ', '', str_replace('LADA ', '', $car->getName()));
            $data['types'] = array();
            foreach ($types as $key => $value) {
				$kuzov = $value['value'];
                if( !isset($data[$kuzov]) ){
                    $data['types'][$kuzov] = array();
                    $data['types'][$kuzov]['minPrice'] = 0;
                }
                $data['types'][$kuzov][$value['qid']]['price'] = $value['price'];
                $data['types'][$kuzov]['image'] = $value['file'];
                $data['types'][$kuzov]['id'] = $value['bid'];
                if( !isset($data['types'][$kuzov]['minPrice']) || 
                    $data['types'][$kuzov]['minPrice'] > $value['price'] ||
                    $data['types'][$kuzov]['minPrice'] == 0 )
                {
                    $data['types'][$kuzov]['minPrice'] = $value['price'];

                }
            }
            $info[$id] = $data;
        }
        $qb = $em->createQueryBuilder();
        $qb->select('c')
                ->from('SiteBackendBundle:CreditOffers','c')
                ->where("c.active = 1");
        $offers = $qb->getQuery()->getArrayResult();
        foreach ($offers as $key => $value) {
            $offers[$key]['tax'] = unserialize($value['tax']);
            $offers[$key]['auto'] = unserialize($value['auto']);
            if ( $value['typeCredit'] == 'Классическая' ){
                $classic = true;
            }
            if ( $value['typeCredit'] == 'Экспресс' ){
                $express = true;
            }
        }
        $searchFp = 50;
        $serachPr = 36;
        $newOffer = array();
        foreach ($offers as $key => $value) {
            foreach ($value['tax'] as $k => $v) {
                if( $v['firstpay']['min'] < $searchFp && $v['firstpay']['max'] > $searchFp ){
                    $newOffer[$key] = $v['values'][0]['tvalue'];
                    if( !isset($minTax) || $minTax > $newOffer[$key] ){
                        $minTax = $newOffer[$key];
                    }
                }

            }
        }

        $send = array();
        $cars_repository = $em->getRepository('SiteBackendBundle:Cars');
        $send['data'] = $info;
        $send['minTax'] = $minTax;
        return $send;
    }

    public static function getPageContent(){
        $kernel = $GLOBALS['kernel'];
        $container = $kernel->getContainer();
        $url = $_SERVER['REQUEST_URI'];
        $data = array();
        $key = $url;
        $em = $kernel->getContainer()->get('doctrine.orm.entity_manager');
        $options = $em->getRepository('SiteBackendBundle:Pages')->findBy(array('route'=>$key));
        if( count($options) > 0 ){
            $data['options'] = unserialize($options[0]->getDefaultOptions());
            if( !empty($data['options']) )
                return $kernel->getContainer()->get('templating')->renderResponse('SiteFrontendBundle:Home:page.html.twig', $data); 
        }

        return new response('');
    }

    public static function addMassmail($data){
        $kernel = $GLOBALS['kernel'];
        $container = $kernel->getContainer();
        $em = $kernel->getContainer()->get('doctrine.orm.entity_manager');     
        $email = $em->getRepository('SiteBackendBundle:MassMail')->findBy(array('email'=>$data['email']));
        if( count($email)== 0 ){
            $massmail = new Massmail();
            $massmail->setEmail($data['email']);
            $massmail->setStatus(1);
            $em->persist($massmail);
            $em->flush();
            return 1;
        }
        else{
            return 0;
        }
    }

    public static function removeMassmail($data){
        $kernel = $GLOBALS['kernel'];
        $container = $kernel->getContainer();
        $em = $kernel->getContainer()->get('doctrine.orm.entity_manager');     

        $email = $em->getRepository('SiteBackendBundle:MassMail')->findBy(array('email'=>$data['email']));
        if( count($email) > 0 ){
            $massmail = new Massmail();
            $massmail->setEmail($data['email']);
            $massmail->setStatus(0);
            $em->persist($massmail);
            $em->flush();
            return 1;
        }
        else{
            return 0;
        }

    }

    public static function getInfoMessage( $type = null ){
        $kernel = $GLOBALS['kernel'];
        $container = $kernel->getContainer();
        $em = $kernel->getContainer()->get('doctrine.orm.entity_manager');
        if( $type == null ){
            $url = $_SERVER['REQUEST_URI'];
            $data = array();
            $key = $url;
            $key = explode('/', $key);
            $type = $key[2];
        }

        $options = $em->getRepository('SiteBackendBundle:FormsConstructor')->findBy(array('alias'=>'form-'.$type));
        if( count($options) > 0 ){
            $data['success'] = $options[0]->getSuccess();
            $data['fail'] = $options[0]->getFail();
            $data['thistype'] = $type;
            if( !empty($data) )
                return $kernel->getContainer()->get('templating')->renderResponse('SiteFrontendBundle:Home:message.html.twig', $data); 
        }

        return new response('');
    }
    public static function saveRequest($data, $region, $type, $dc){
        $kernel = $GLOBALS['kernel'];
        $em = $kernel->getContainer()->get('doctrine.orm.entity_manager');
        $request = new Requests();
        $request->setType($type);
        $request->setRequestData(serialize($data));
        $request->setRequestDate(time());
        $request->setReadEd(0);
        $request->setDoneEd(0);
        $request->setDc($dc);
        $request->setRegion($region->getId());

        $em->persist($request);
        $em->flush();
        return 1;
    }

    static function getComplectationsForDc($carId, $type, $price = false){
        $kernel = $GLOBALS['kernel'];
        $em = $kernel->getContainer()->get('doctrine.orm.entity_manager');
        //$value = $em->getRepository('SiteBackendBundle:InfoValue')->findBy(array('name'=>$type));
        $value = $em->getRepository('SiteBackendBundle:BaseAutoBodyTypes')->findBy(array('name'=>$type, 'carId' => $carId));
        
        $valueId = $value[0]->getId();
        
        $qb = $em->createQueryBuilder();
        $qb->select(array('c.id','c.name as miname','c.name as ename','c.price','engineselect.valueId as engine','box.valueId as transmission',
							'run.valueId as capacity','type.valueId as typecar', 'execnum_name.name as execn', 'mdl.name as model'))
            ->from('SiteBackendBundle:Cars','c')
          //  ->leftJoin('SiteBackendBundle:CarsInfo', 'ci', 'WITH', "ci.carModelId = c.id")
            ->leftJoin('SiteBackendBundle:CarsInfo', 'type', 'WITH', "type.carModelId = c.id and type.infoId = 30")
            ->leftJoin('SiteBackendBundle:CarsInfo', 'engineselect', 'WITH', "engineselect.carModelId = c.id and engineselect.infoId = 16")
            ->leftJoin('SiteBackendBundle:CarsInfo', 'run', 'WITH', "run.carModelId = c.id and run.infoId = 23")
            ->leftJoin('SiteBackendBundle:CarsInfo', 'execnum', 'WITH', "execnum.carModelId = c.id and execnum.infoId = 31")		//ID номера исполнения
            ->leftJoin('SiteBackendBundle:InfoValue', 'execnum_name', 'WITH', "execnum_name.id = execnum.valueId")		//Номер исполнения
            ->leftJoin('SiteBackendBundle:CarsInfo', 'mdlval', 'WITH', "mdlval.carModelId = c.id and mdlval.infoId = 27")		//ID модели
            ->leftJoin('SiteBackendBundle:InfoValue', 'mdl', 'WITH', "mdl.id = mdlval.valueId")				//Модель
            ->leftJoin('SiteBackendBundle:CarsInfo', 'box', 'WITH', "box.carModelId = c.id and box.infoId = 11");
         //   ->leftJoin('SiteBackendBundle:BaseAutoBodyTypes', 'bt', 'WITH', "bt.id = c.bodyId");
            if( $price == false )
                $qb->where("c.bodyId = ".$valueId);
            if( $price == true )
                $qb->where("c.price is not null and c.bodyId = ".$valueId);
        $equipments = $qb->getQuery()->getResult();

        $equipmentsTree = array();

        foreach ($equipments as $key => $value)
        {
            $values = array('engine'=>false,'transmission'=>false,'capacity'=>false,'typecar'=>false);

            if( $value['engine'] != '' && $em->getRepository('SiteBackendBundle:InfoValue')->find($value['engine']) )
                $values['engine'] = $em->getRepository('SiteBackendBundle:InfoValue')->find($value['engine'])->getName();

            if( $value['transmission'] != '' && $em->getRepository('SiteBackendBundle:InfoValue')->find($value['transmission']) )
                $values['transmission'] = $em->getRepository('SiteBackendBundle:InfoValue')->find($value['transmission'])->getName();

            if( $value['capacity'] != '' && $em->getRepository('SiteBackendBundle:InfoValue')->find($value['capacity']) )
                $values['capacity'] = $em->getRepository('SiteBackendBundle:InfoValue')->find($value['capacity'])->getName();

            if( $value['typecar'] != '' && $em->getRepository('SiteBackendBundle:InfoValue')->find($value['typecar']) )
                $values['typecar'] = $em->getRepository('SiteBackendBundle:InfoValue')->find($value['typecar'])->getName();

            $equipments[$key]['values'] = $values;
            $etKey = $value['typecar'];

            if ($etKey != '')
            {
				if (!isset($equipmentsTree[$etKey]))
				{
					$equipmentsTree[$etKey] = array();
					$equipmentsTree[$etKey]['value'] = array();
				}
				$equipmentsTree[$etKey]['name'] = $values['typecar'];
				$longid = $value['id'];
				$equipmentsTree[$etKey]['value'][$longid] = array();
				$equipmentsTree[$etKey]['value'][$longid]['engine'] = $values['engine'].'/'.$values['transmission'].'/'.$values['capacity'];
				$equipmentsTree[$etKey]['value'][$longid]['price'] = $value['price'];
				$equipmentsTree[$etKey]['value'][$longid]['name'] = $value['ename'];
				$equipmentsTree[$etKey]['value'][$longid]['exec'] = $value['execn'];
				$equipmentsTree[$etKey]['value'][$longid]['model'] = $value['model'];
            }
        }
        //     if( !isset($equipmentsTree[$etKey]['types']) ){
        //         $equipmentsTree[$etKey]['types']['values'] = array();
        //         //$equipmentsTree[$etKey]['types']['eqs'] = array();
        //     }
        //     if( !isset($equipmentsTree[$etKey]['types']['values'][$value['typecar']])){
        //         $equipmentsTree[$etKey]['types']['values'][$value['typecar']] = array();
        //     }
        //     $equipmentsTree[$etKey]['types']['values'][$value['typecar']]['value'] = $values['typecar'];
        //     $equipmentsTree[$etKey]['types']['values'][$value['typecar']]['price'] = $value['price'];
        //     if( !isset($equipmentsTree[$etKey]['types']['values'][$value['typecar']]['eqs']) )
        //         $equipmentsTree[$etKey]['types']['values'][$value['typecar']]['eqs'] = array();
        //     $equipmentsTree[$etKey]['types']['values'][$value['typecar']]['eqs'][$value['id']] = $value['price'];

        //     /* SPECIAL FOR COMPARE START*/

        //     if( !isset($equipmentsTree[$etKey]['types']['values'][$value['typecar']]['eqss']) )
        //         $equipmentsTree[$etKey]['types']['values'][$value['typecar']]['eqss'] = array();
        //     $equipmentsTree[$etKey]['types']['values'][$value['typecar']]['eqss'][$value['id']] = $value['ename'];

        //     if( !isset($equipmentsTree[$etKey]['types']['values'][$value['typecar']]['minames']) )
        //         $equipmentsTree[$etKey]['types']['values'][$value['typecar']]['minames'] = array();
        //     $equipmentsTree[$etKey]['types']['values'][$value['typecar']]['minames'][$value['id']] = $value['miname'];

        //     /* SPECIAL FOR COMPARE END */
        //     if( !isset($minprice) || $value['price'] < $minprice['value'] ){
        //         $minprice = array();
        //         $minprice['value'] = $value['price'];
        //         $minprice['key'] = $etKey;
        //         $minprice['engine'] = $values['engine'];
        //         $minprice['transmission'] = $values['transmission'];
        //         $minprice['ename'] = $value['ename'];
        //         $minprice['type'] = $value['typecar'];
        //     }
        //     if( $etKey == '00' )
        //         unset($equipmentsTree[$etKey]);
        // }
        // foreach ($equipmentsTree as $key => $value) {
        //         foreach ($value['types']['values'] as $x => $y) {
        //             $all = $y['eqs'];
        //             $special = array();
        //             Utils::crossFeatures($all, $details, $special);
        //             $equipmentsTree[$key]['types']['values'][$x]['info'] = $details;
        //             $equipmentsTree[$key]['types']['values'][$x]['special'] = $special;
        //         }
        //     }
        
        // if(isset($minprice))
        // {
        //     $minvalues = $minprice;
        // }
        // else
        // {
        //     $minvalues = array('value' => 0, 'key' => 0, 'engine' => 0, 'transmission' => 0, 'ename' => 0, 'type' => 0);
        // }
        return $equipmentsTree;
    }
}