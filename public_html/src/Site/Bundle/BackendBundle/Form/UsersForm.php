<?php

namespace Site\Bundle\BackendBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Site\Bundle\BackendBundle\Entity\User;

class UsersForm extends AbstractType
{
	public function __construct ()
    {
        
    }
    
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
         $builder
            ->add('fio',null,array('label' => 'Ф.И.О.', 'attr' => array('class' => 'form-control') ))
            ->add('roles', 'text', array('mapped' => false, 'label' => 'Роль'));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Site\Bundle\BackendBundle\Entity\User'
        ));
    }

    public function getName()
    {
        return 'user';
    }
}

