<?php

namespace Site\Bundle\BackendBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

class BaseAutoSliderSlideForm extends AbstractType
{
	public function __construct ($stype)
    {
        $this->stype = $stype;
    }
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('title',null,array('label' => 'Заголовок', 'required' => false, 'attr' => array('class' => 'form-control') ));
        $builder->add('image','elfinder', array('label' => 'Изображение', 'required' => false, 'instance'=>'form', 'enable'=>true, 'attr' => array('class' => 'form-control')));
        
        if($this->stype != 2)
        {
            $builder->add('image2','elfinder', array('label' => 'Изображение автомобиля', 'required' => false, 'instance'=>'form', 'enable'=>true, 'attr' => array('class' => 'form-control')));
            $builder->add('textTitle',null,array('label' => 'Текстовый заголовок', 'required' => false, 'attr' => array('class' => 'form-control') ));
            $builder->add('textSubtitle',null,array('label' => 'Текстовый подзаголовок', 'required' => false, 'attr' => array('class' => 'form-control') ));
            $builder->add('shortDescription',null,array('label' => 'Краткое описание', 'required' => false, 'attr' => array('class' => 'form-control') ));
            $builder->add('textMonth',null,array('label' => 'Текст в месяц', 'required' => false, 'attr' => array('class' => 'form-control') ));
            $builder->add('linkButton',null,array('label' => 'Ссылка на кредитный калькулятор', 'required' => false, 'attr' => array('class' => 'form-control') ));
        }
        $builder->add('pos',null,array('label' => 'Позиция', 'required' => false, 'attr' => array('class' => 'form-control') ));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Site\Bundle\BackendBundle\Entity\BaseAutoSliderSlides'
        ));
    }

    public function getName()
    {
        return 'slide';
    }
}

