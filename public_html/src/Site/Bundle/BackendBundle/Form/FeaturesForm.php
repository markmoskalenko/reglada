<?php

namespace Site\Bundle\BackendBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

class FeaturesForm extends AbstractType
{
	public function __construct ($groups)
    {
        $this->groups = $groups;
    }
    
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name',null,array('label' => 'Название', 'attr' => array('class' => 'form-control') ));
        $builder->add('keyName',null,array('label' => 'Ключ', 'required' => false, 'attr' => array('class' => 'form-control') ));
        $builder->add('groupId','choice',array('label' => 'Группа','choices' => $this->groups, 'attr' => array('class' => 'form-control')));
        $builder->add('description','textarea',array('label' => 'Описание', 'required' => false, 'attr' => array('class' => 'form-control') ));
        $builder->add('pos','text',array('label' => 'Позиция', 'attr' => array('class' => 'form-control') ));
        $builder->add('pos','text',array('label' => 'Позиция', 'attr' => array('class' => 'form-control') ));
        $builder->add('seltype', 'choice', array(
                'label' => 'Тип выбора', 'attr' => array('class' => 'form-control'),
                'choices'   => array(
                    'select'   => 'Список выбора',
                    'checkbox' => 'Множественные значения',
                    'yesno' => 'Да/нет',
                ),
                'multiple'  => false,
            ));
        $builder->add('image','elfinder', array('label' => 'Изображение', 'required' => false, 'instance'=>'form', 'enable'=>true, 'attr' => array('class' => 'form-control')));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Site\Bundle\BackendBundle\Entity\Info'
        ));
    }

    public function getName()
    {
        return 'info';
    }
}

