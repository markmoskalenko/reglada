<?php

namespace Site\Bundle\BackendBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

class FeaturesValuesForm extends AbstractType
{
	public function __construct ()
    {
        
    }
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name',null,array('label' => 'Название', 'attr' => array('class' => 'form-control') ));
        $builder->add('pos',null,array('label' => 'Позиция', 'required' => false, 'attr' => array('class' => 'form-control') ));
        $builder->add('file','elfinder', array('label' => 'Файл', 'required' => false, 'instance'=>'form', 'enable'=>true, 'attr' => array('class' => 'form-control')));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Site\Bundle\BackendBundle\Entity\InfoValue'
        ));
    }

    public function getName()
    {
        return 'info';
    }
}

