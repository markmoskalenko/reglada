<?php

namespace Site\Bundle\BackendBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

class BaseAutoDocumentsForm extends AbstractType
{
	public function __construct ()
    {
        
    }
    
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name',null,array('label' => 'Название', 'attr' => array('class' => 'form-control') ));
        $builder->add('file','elfinder', array('label' => 'Файл', 'required' => true, 'instance'=>'form', 'enable'=>true, 'attr' => array('class' => 'form-control')));
        $builder->add('fileSize','text',array('label' => 'Размер файла', 'required' => true, 'attr' => array('class' => 'form-control') ));
        $builder->add('status','checkbox',array('label' => 'Архивный', 'required' => false, 'attr' => array() ));
        $builder->add('isCatalog','checkbox',array('label' => 'Каталог', 'required' => false, 'attr' => array() ));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Site\Bundle\BackendBundle\Entity\BaseAutoDocuments'
        ));
    }

    public function getName()
    {
        return 'doc';
    }
}

