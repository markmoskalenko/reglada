<?php

namespace Site\Bundle\BackendBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Site\Bundle\BackendBundle\Entity\Role;

class RolesForm extends AbstractType
{
	public function __construct ($key = false)
    {
        $this->key = $key;
    }
    
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name',null,array('label' => 'Название', 'attr' => array('class' => 'form-control') ));
        $builder->add('cabinetName',null,array('label' => 'Заголовок кабинета', 'attr' => array('class' => 'form-control') ));
        //$builder->add('cabinetName',null,array('label' => 'Имя кабинета', 'attr' => array('class' => 'form-control') ));
        if( $this->key == false ){
            $builder->add('rolekey', null, array('label' => 'Ключ', 'attr' => array('class' => 'form-control')));
        }
        else{
            $builder->add('rolekey', null, array('label' => 'Ключ', 'attr' => array('disabled'=>true, 'class' => 'form-control')));
        }
            // ->add('perms', 'text', array('mapped' => false, 'label' => 'URL'));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Site\Bundle\BackendBundle\Entity\UserRoles'
        ));
    }

    public function getName()
    {
        return 'role';
    }
}

