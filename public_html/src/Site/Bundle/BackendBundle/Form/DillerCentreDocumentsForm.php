<?php

namespace Site\Bundle\BackendBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

class DillerCentreDocumentsForm extends AbstractType
{
	public function __construct ($dillers)
    {
        $this->dillers = $dillers;
    }
    
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name',null,array('label' => 'Название', 'attr' => array('class' => 'form-control') ));
        $builder->add('dillerId', 'choice', array(
            'label' => 'Диллер',
            'choices'   => $this->dillers,
            'multiple'  => false,
            'attr' => array('class' => 'form-control')
        ));
        $builder->add('dateAdd', 'date', array(
            'label' => 'Дата добавления',
            'input'  => 'datetime',
            'widget' => 'choice',
        ));
        $builder->add('type', 'choice', array(
            'label' => 'Тип',
            'choices'   => array('bill' => 'Счет',
                                 'close' => 'Закрывающие',
                                 'report' => 'Отчет',
                                 'no_type' => 'Без типа',
                                 'template' => 'Шаблон',
                                 'catalog' => 'Каталог запчастей',
                                 'template_card' => 'Шаблон карточка'),
            'multiple'  => false,
            'attr' => array('class' => 'form-control')
        ));
        $builder->add('descr','textarea',array('label' => 'Комментарий', 'attr' => array('class' => 'form-control') ));
        $builder->add('link','file',array('label' => 'Файл', 'mapped' => false, 'required' => false));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Site\Bundle\BackendBundle\Entity\DillerCentreDocuments'
        ));
    }

    public function getName()
    {
        return 'doc';
    }
}

