<?php

namespace Site\Bundle\BackendBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

class BaseAutoBlocksForm extends AbstractType
{
	public function __construct ()
    {
        
    }
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name',null,array('label' => 'Название', 'required' => true, 'attr' => array('class' => 'form-control') ));
        $builder->add('image','elfinder', array('label' => 'Изображение', 'required' => true, 'instance'=>'form', 'enable'=>true, 'attr' => array('class' => 'form-control')));
        $builder->add('descr',null,array('label' => 'Описание', 'required' => true, 'attr' => array('class' => 'form-control') ));
        $builder->add('pos',null,array('label' => 'Позиция', 'required' => false, 'attr' => array('class' => 'form-control') ));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Site\Bundle\BackendBundle\Entity\BaseAutoCarsBlocks'
        ));
    }

    public function getName()
    {
        return 'block';
    }
}

