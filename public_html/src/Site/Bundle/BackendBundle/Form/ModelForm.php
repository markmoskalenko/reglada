<?php

namespace Site\Bundle\BackendBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

class ModelForm extends AbstractType
{
	public function __construct ($bodies, $bodyid = false)
    {
        $this->bodies = array();
        $this->bodyid = $bodyid;
        foreach($bodies as $body)
        {
            $this->bodies[$body->getId()] = $body->getNameAdmin();
        }
    }
    
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name',null,array('label' => 'Название', 'attr' => array('class' => 'form-control') ));
        $builder->add('nameAdmin',null,array('label' => 'Название для админки', 'attr' => array('class' => 'form-control') ));
        $builder->add('file','elfinder', array('label' => 'Файл', 'required' => false, 'instance'=>'form', 'enable'=>true, 'attr' => array('class' => 'form-control')));
        $builder->add('pos','text',array('label' => 'Позиция', 'required' => false, 'attr' => array('class' => 'form-control') ));
        $builder->add('price','text',array('label' => 'Цена', 'required' => false, 'attr' => array('class' => 'form-control')));
        
        $arr_body = array(
                'label' => 'Кузов', 'attr' => array('class' => 'form-control'),
                'choices'   => $this->bodies,
                'multiple'  => false,
            );
        if($this->bodyid)
            $arr_body['data'] = $this->bodyid;
        
        $builder->add('bodyId', 'choice', $arr_body);
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Site\Bundle\BackendBundle\Entity\Cars'
        ));
    }

    public function getName()
    {
        return 'cars';
    }
}

