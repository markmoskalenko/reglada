<?php

namespace Site\Bundle\BackendBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

class FrontDillerSpecialForm extends AbstractType
{
	public function __construct ()
    {
    }
    
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('title',null,array('label' => 'Заголовок', 'attr' => array('class' => 'form-control') ));
        $builder->add('dateExpire',null,array('label' => 'Срок действия акции', 'attr' => array('class' => 'form-control') ));
        $builder->add('descr','textarea',array('label' => 'Текст акции', 'attr' => array('class' => 'form-control') ));
        $builder->add('image','file',array('label' => 'Иллюстрация к акции', 'required' => false, 'mapped' => false, 'attr' => array('onchange' => "$('.field_file .text').text($(this).val());") ));
        $builder->add('isInPage','checkbox',array('label' => 'Вывести на страницу Акции', 'required' => false, 'attr' => array() ));
        $builder->add('isFirst','checkbox',array('label' => 'Поставить первой в показ', 'required' => false, 'attr' => array() ));
        $builder->add('isArchive','checkbox',array('label' => 'Архивная', 'required' => false, 'attr' => array() ));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Site\Bundle\BackendBundle\Entity\DillerCentreSpecial'
        ));
    }

    public function getName()
    {
        return 'auto';
    }
}

