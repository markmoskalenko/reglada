<?php

namespace Site\Bundle\BackendBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

class FrontDillerCentreInfoForm extends AbstractType
{
	public function __construct ($phones, $blocks)
    {
        $ph = unserialize($phones);
        if(is_array($ph))
            $this->phones = implode(', ', $ph);
        else
            $this->phones = '';
            
        $bl = unserialize($blocks);
        
        if(is_array($bl))
            $this->blocks = $bl;
        else
            $this->blocks = array();
    }
    
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name',null,array('label' => 'Название', 'attr' => array('class' => 'form-control') ));
        $builder->add('legalAddress',null,array('label' => 'Адрес', 'attr' => array('class' => 'form-control') ));
        $builder->add('phone',null,array('label' => 'Телефоны (через запятую)', 'data' => $this->phones, 'attr' => array('class' => 'form-control') ));
        $builder->add('worktime',null,array('label' => 'Время работы', 'attr' => array('class' => 'form-control') ));
        $builder->add('block_title_1',null,array('label' => 'Заголовок', 'mapped' => false, 'data' => (isset($this->blocks['block_title_1'])) ? $this->blocks['block_title_1'] : '', 'attr' => array('class' => 'form-control') ));
        $builder->add('block_title_2',null,array('label' => 'Заголовок', 'mapped' => false, 'data' => (isset($this->blocks['block_title_2'])) ? $this->blocks['block_title_2'] : '', 'attr' => array('class' => 'form-control') ));
        $builder->add('block_title_3',null,array('label' => 'Заголовок', 'mapped' => false, 'data' => (isset($this->blocks['block_title_3'])) ? $this->blocks['block_title_3'] : '', 'attr' => array('class' => 'form-control') ));
        $builder->add('block_body_1','textarea',array('label' => 'Текст', 'mapped' => false, 'data' => (isset($this->blocks['block_body_1'])) ? $this->blocks['block_body_1'] : '', 'attr' => array('class' => 'form-control') ));
        $builder->add('block_body_2','textarea',array('label' => 'Текст', 'mapped' => false, 'data' => (isset($this->blocks['block_body_2'])) ? $this->blocks['block_body_2'] : '', 'attr' => array('class' => 'form-control') ));
        $builder->add('block_body_3','textarea',array('label' => 'Текст', 'mapped' => false, 'data' => (isset($this->blocks['block_body_3'])) ? $this->blocks['block_body_3'] : '', 'attr' => array('class' => 'form-control') ));
        $builder->add('pricelist','file',array('label' => 'Документ', 'mapped' => false, 'required' => false));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Site\Bundle\BackendBundle\Entity\DillerCentre'
        ));
    }

    public function getName()
    {
        return 'diller';
    }
}

