<?php

namespace Site\Bundle\BackendBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

class BaseAutoReviewsForm extends AbstractType
{
	public function __construct ()
    {
        
    }
    
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name',null,array('label' => 'Название', 'attr' => array('class' => 'form-control') ));
        $builder->add('file','elfinder', array('label' => 'Файл', 'required' => false, 'instance'=>'form', 'enable'=>true, 'attr' => array('class' => 'form-control')));
        $builder->add('description','textarea',array('label' => 'Описание', 'attr' => array('class' => 'form-control') ));
        $builder->add('authorName',null,array('label' => 'Имя автора', 'attr' => array('class' => 'form-control') ));
        $builder->add('authorStatus',null,array('label' => 'Статус автора', 'attr' => array('class' => 'form-control') ));
        $builder->add('isActive', 'checkbox', array('label' => 'Активен', 'required'  => false,));
        $builder->add('inHome', 'checkbox', array('label' => 'На главную', 'required'  => false,));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Site\Bundle\BackendBundle\Entity\BaseAutoReviews'
        ));
    }

    public function getName()
    {
        return 'review';
    }
}

