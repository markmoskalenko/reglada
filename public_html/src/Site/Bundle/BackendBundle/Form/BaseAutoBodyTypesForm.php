<?php

namespace Site\Bundle\BackendBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

class BaseAutoBodyTypesForm extends AbstractType
{
	public function __construct ($cars, $carid = false)
    {
        $this->cars = array();
        $this->carid = $carid;
        foreach($cars as $car)
        {
            $this->cars[$car->getId()] = $car->getName();
        }
    }
    
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name',null,array('label' => 'Название', 'attr' => array('class' => 'form-control') ));
        $builder->add('nameAdmin',null,array('label' => 'Название для админа', 'attr' => array('class' => 'form-control') ));
        $builder->add('file','elfinder', array('label' => 'Файл', 'required' => false, 'instance'=>'form', 'enable'=>true, 'attr' => array('class' => 'form-control')));
        $builder->add('banner','elfinder', array('label' => 'Банер', 'required' => false, 'instance'=>'form', 'enable'=>true, 'attr' => array('class' => 'form-control')));
        $builder->add('pos','text',array('label' => 'Позиция', 'required' => false, 'attr' => array('class' => 'form-control') ));
        
        $arr_car = array(
                'label' => 'Базовый автомобиль', 'attr' => array('class' => 'form-control'),
                'choices'   => $this->cars,
                'multiple'  => false,
            );
        if($this->carid)
            $arr_car['data'] = $this->carid;
        
        $builder->add('carId', 'choice', $arr_car);
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Site\Bundle\BackendBundle\Entity\BaseAutoBodyTypes'
        ));
    }

    public function getName()
    {
        return 'body';
    }
}

