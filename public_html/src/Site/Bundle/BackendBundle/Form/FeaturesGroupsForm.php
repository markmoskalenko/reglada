<?php

namespace Site\Bundle\BackendBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Site\Bundle\BackendBundle\Entity\Role;

class FeaturesGroupsForm extends AbstractType
{
	public function __construct ()
    {
        
    }
    
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name',null,array('label' => 'Название', 'attr' => array('class' => 'form-control') ));
        $builder->add('keyName',null,array('label' => 'Ключ', 'required' => false, 'attr' => array('class' => 'form-control') ));
        $builder->add('tabPlace', 'choice', array(
                'label' => 'Вкладка', 'attr' => array('class' => 'form-control'),
                'choices'   => array(1 => 'Технические характеристики', 2 => 'Комплектации и цены'),
                'multiple'  => false,
            ));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Site\Bundle\BackendBundle\Entity\InfoGroups'
        ));
    }

    public function getName()
    {
        return 'group';
    }
}

