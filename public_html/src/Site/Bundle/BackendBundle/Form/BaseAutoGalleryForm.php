<?php

namespace Site\Bundle\BackendBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

class BaseAutoGalleryForm extends AbstractType
{
	public function __construct ()
    {
        
    }
    
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('text',null,array('label' => 'Текст', 'attr' => array('class' => 'form-control') ));
        $builder->add('type', 'choice', array(
                'label' => 'Тип', 'attr' => array('class' => 'form-control'),
                'choices'   => array(
                    1 => 'Изображение',
                    2 => 'Видео',
                ),
                'multiple'  => false,
            ));
        $builder->add('file','elfinder', array('label' => 'Изображение', 'required' => false, 'instance'=>'form', 'enable'=>true, 'attr' => array('class' => 'form-control')));
        $builder->add('video','text',array('label' => 'Видео', 'required' => false, 'attr' => array('class' => 'form-control') ));
        $builder->add('pos',null,array('label' => 'Позиция', 'attr' => array('class' => 'form-control') ));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Site\Bundle\BackendBundle\Entity\BaseAutoGallery'
        ));
    }

    public function getName()
    {
        return 'doc';
    }
}

