<?php

namespace Site\Bundle\BackendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Groups
 *
 * @ORM\Table(name="groups")
 * @ORM\Entity(repositoryClass="Site\Bundle\BackendBundle\Repository\GroupsRepository")
 */
class FormFields
{

    private $id;
    private $alias;
    private $name;
    private $required;
    private $num;
    private $alpha;
    private $numAlpha;
    private $email;
    private $phone;
    private $type;
    private $options;
    private $pos;
    private $personal;

    public function getId(){
        return $this->id;
    }

    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }
    public function getName()
    {
        return $this->name;
    }

    public function setAlias($alias)
    {
        $this->alias = $alias;
        return $this;
    }
    public function getAlias()
    {
        return $this->alias;
    }

    public function setRequired($required)
    {
        $this->required = $required;
        return $this;
    }
    public function getRequired()
    {
        return $this->required;
    }

    public function setNum($num)
    {
        $this->num = $num;
        return $this;
    }
    public function getNum()
    {
        return $this->num;
    }

    public function setNumAlpha($numAlpha)
    {
        $this->numAlpha = $numAlpha;
        return $this;
    }
    public function getNumAlpha()
    {
        return $this->numAlpha;
    }

    public function setAlpha($alpha)
    {
        $this->alpha = $alpha;
        return $this;
    }
    public function getAlpha()
    {
        return $this->alpha;
    }

    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }
    public function getEmail()
    {
        return $this->email;
    }

    public function setPhone($phone)
    {
        $this->phone = $phone;
        return $this;
    }
    public function getPhone()
    {
        return $this->phone;
    }

    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }
    public function getType()
    {
        return $this->type;
    }

    public function setOptions($options)
    {
        $this->options = $options;
        return $this;
    }
    public function getOptions()
    {
        return unserialize($this->options);
    }

    public function setPos($pos)
    {
        $this->pos = $pos;
        return $this;
    }
    public function getPos()
    {
        return $this->pos;
    }

    public function setPersonal($personal)
    {
        $this->personal = $personal;
        return $this;
    }
    public function getPersonal()
    {
        return $this->personal;
    }
}
