<?php

namespace Site\Bundle\BackendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * BodyTypesColors
 */
class BodyTypesColors
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $bodyTypeId;

    /**
     * @var integer
     */
    private $colorId;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set bodyTypeId
     *
     * @param integer $bodyTypeId
     * @return BodyTypesColors
     */
    public function setBodyTypeId($bodyTypeId)
    {
        $this->bodyTypeId = $bodyTypeId;

        return $this;
    }

    /**
     * Get bodyTypeId
     *
     * @return integer 
     */
    public function getBodyTypeId()
    {
        return $this->bodyTypeId;
    }

    /**
     * Set colorId
     *
     * @param integer $colorId
     * @return BodyTypesColors
     */
    public function setColorId($colorId)
    {
        $this->colorId = $colorId;

        return $this;
    }

    /**
     * Get colorId
     *
     * @return integer 
     */
    public function getColorId()
    {
        return $this->colorId;
    }
}
