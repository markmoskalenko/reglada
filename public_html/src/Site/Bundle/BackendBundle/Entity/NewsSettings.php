<?php

namespace Site\Bundle\BackendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

class NewsSettings
{

    private $id;
    private $name;
    private $key;
    private $value;

    public function getId()
    {
        return $this->id;
    }



    /**
     * Get status
     *
     * @return string 
     */
    public function getKey()
    {
        return $this->key;
    }

    public function getName()
    {
        return $this->name;
    }
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }
    public function getValue()
    {
        return $this->value;
    }
}
