<?php

namespace Site\Bundle\BackendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * FeatureComplectations
 */
class FeatureComplectations
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $complectationId;

    /**
     * @var integer
     */
    private $featureId;

    /**
     * @var integer
     */
    private $featureValue;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set complectationId
     *
     * @param integer $complectationId
     * @return FeatureComplectations
     */
    public function setComplectationId($complectationId)
    {
        $this->complectationId = $complectationId;

        return $this;
    }

    /**
     * Get complectationId
     *
     * @return integer 
     */
    public function getComplectationId()
    {
        return $this->complectationId;
    }

    /**
     * Set featureId
     *
     * @param integer $featureId
     * @return FeatureComplectations
     */
    public function setFeatureId($featureId)
    {
        $this->featureId = $featureId;

        return $this;
    }

    /**
     * Get featureId
     *
     * @return integer 
     */
    public function getFeatureId()
    {
        return $this->featureId;
    }

    /**
     * Set featureValue
     *
     * @param integer $featureValue
     * @return FeatureComplectations
     */
    public function setFeatureValue($featureValue)
    {
        $this->featureValue = $featureValue;

        return $this;
    }

    /**
     * Get featureValue
     *
     * @return integer 
     */
    public function getFeatureValue()
    {
        return $this->featureValue;
    }
}
