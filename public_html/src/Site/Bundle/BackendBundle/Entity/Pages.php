<?php

namespace Site\Bundle\BackendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * FeatureGroups
 */
class Pages
{
    /**
     * @var integer
     */
    private $id;
    private $route;
    private $name;
    private $status;
    private $defaultOptions;
    private $regionOptions;

    public function getId()
    {
        return $this->id;
    }
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setRoute($route)
    {
        $this->route = $route;

        return $this;
    }

    public function getRoute()
    {
        return $this->route;
    }

    public function setDefaultOptions($defaultOptions)
    {
        $this->defaultOptions = $defaultOptions;

        return $this;
    }

    public function getDefaultOptions()
    {
        return $this->defaultOptions;
    }

    public function setRegionOptions($regionOptions)
    {
        $this->regionOptions = $regionOptions;

        return $this;
    }

    public function getRegionOptions()
    {
        return $this->regionOptions;
    }

    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    public function getStatus()
    {
        return $this->status;
    }

}
