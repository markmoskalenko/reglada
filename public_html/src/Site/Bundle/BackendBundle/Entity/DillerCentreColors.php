<?php

namespace Site\Bundle\BackendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DillerCentreColors
 */
class DillerCentreColors
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $dillerId;

    /**
     * @var integer
     */
    private $bodyId;

    /**
     * @var integer
     */
    private $colorId;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dillerId
     *
     * @param integer $dillerId
     * @return DillerCentreColors
     */
    public function setDillerId($dillerId)
    {
        $this->dillerId = $dillerId;

        return $this;
    }

    /**
     * Get dillerId
     *
     * @return integer 
     */
    public function getDillerId()
    {
        return $this->dillerId;
    }

    /**
     * Set bodyId
     *
     * @param integer $bodyId
     * @return DillerCentreColors
     */
    public function setBodyId($bodyId)
    {
        $this->bodyId = $bodyId;

        return $this;
    }

    /**
     * Get bodyId
     *
     * @return integer 
     */
    public function getBodyId()
    {
        return $this->bodyId;
    }

    /**
     * Set colorId
     *
     * @param integer $colorId
     * @return DillerCentreColors
     */
    public function setColorId($colorId)
    {
        $this->colorId = $colorId;

        return $this;
    }

    /**
     * Get colorId
     *
     * @return integer 
     */
    public function getColorId()
    {
        return $this->colorId;
    }
}
