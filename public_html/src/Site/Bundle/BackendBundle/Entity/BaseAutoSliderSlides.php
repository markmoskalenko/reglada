<?php

namespace Site\Bundle\BackendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * BaseAutoSliderSlides
 *
 * @ORM\Table(name="base_auto_slider_slides")
 * @ORM\Entity
 */
class BaseAutoSliderSlides
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var boolean
     *
     * @ORM\Column(name="slider_pos", type="boolean", nullable=false)
     */
    private $sliderPos;
    
    /**
     * @var boolean
     *
     * @ORM\Column(name="car_id", type="integer", nullable=false)
     */
    private $carId;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=false)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="image", type="string", length=255, nullable=false)
     */
    private $image;

    /**
     * @var string
     *
     * @ORM\Column(name="image2", type="string", length=255, nullable=false)
     */
    private $image2;

    /**
     * @var string
     *
     * @ORM\Column(name="text_title", type="string", length=255, nullable=false)
     */
    private $textTitle;

    /**
     * @var string
     *
     * @ORM\Column(name="text_subtitle", type="string", length=255, nullable=false)
     */
    private $textSubtitle;

    /**
     * @var string
     *
     * @ORM\Column(name="short_description", type="string", length=255, nullable=false)
     */
    private $shortDescription;

    /**
     * @var string
     *
     * @ORM\Column(name="text_month", type="string", length=255, nullable=false)
     */
    private $textMonth;

    /**
     * @var string
     *
     * @ORM\Column(name="link_button", type="string", length=255, nullable=false)
     */
    private $linkButton;

    /**
     * @var integer
     *
     * @ORM\Column(name="pos", type="integer", nullable=false)
     */
    private $pos;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set carId
     *
     * @param integer $carId
     * @return BaseAutoSliderSlides
     */
    public function setCarId($carId)
    {
        $this->carId = $carId;

        return $this;
    }

    /**
     * Get carId
     *
     * @return integer 
     */
    public function getCarId()
    {
        return $this->carId;
    }

    /**
     * Set sliderPos
     *
     * @param boolean $sliderPos
     * @return BaseAutoSliderSlides
     */
    public function setSliderPos($sliderPos)
    {
        $this->sliderPos = $sliderPos;

        return $this;
    }

    /**
     * Get sliderPos
     *
     * @return boolean 
     */
    public function getSliderPos()
    {
        return $this->sliderPos;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return BaseAutoSliderSlides
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set image
     *
     * @param string $image
     * @return BaseAutoSliderSlides
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return string 
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set image2
     *
     * @param string $image2
     * @return BaseAutoSliderSlides
     */
    public function setImage2($image2)
    {
        $this->image2 = $image2;

        return $this;
    }

    /**
     * Get image2
     *
     * @return string 
     */
    public function getImage2()
    {
        return $this->image2;
    }

    /**
     * Set textTitle
     *
     * @param string $textTitle
     * @return BaseAutoSliderSlides
     */
    public function setTextTitle($textTitle)
    {
        $this->textTitle = $textTitle;

        return $this;
    }

    /**
     * Get textTitle
     *
     * @return string 
     */
    public function getTextTitle()
    {
        return $this->textTitle;
    }

    /**
     * Set textSubtitle
     *
     * @param string $textSubtitle
     * @return BaseAutoSliderSlides
     */
    public function setTextSubtitle($textSubtitle)
    {
        $this->textSubtitle = $textSubtitle;

        return $this;
    }

    /**
     * Get textSubtitle
     *
     * @return string 
     */
    public function getTextSubtitle()
    {
        return $this->textSubtitle;
    }

    /**
     * Set shortDescription
     *
     * @param string $shortDescription
     * @return BaseAutoSliderSlides
     */
    public function setShortDescription($shortDescription)
    {
        $this->shortDescription = $shortDescription;

        return $this;
    }

    /**
     * Get shortDescription
     *
     * @return string 
     */
    public function getShortDescription()
    {
        return $this->shortDescription;
    }

    /**
     * Set textMonth
     *
     * @param string $textMonth
     * @return BaseAutoSliderSlides
     */
    public function setTextMonth($textMonth)
    {
        $this->textMonth = $textMonth;

        return $this;
    }

    /**
     * Get textMonth
     *
     * @return string 
     */
    public function getTextMonth()
    {
        return $this->textMonth;
    }

    /**
     * Set linkButton
     *
     * @param string $linkButton
     * @return BaseAutoSliderSlides
     */
    public function setLinkButton($linkButton)
    {
        $this->linkButton = $linkButton;

        return $this;
    }

    /**
     * Get linkButton
     *
     * @return string 
     */
    public function getLinkButton()
    {
        return $this->linkButton;
    }

    /**
     * Set pos
     *
     * @param integer $pos
     * @return BaseAutoSliderSlides
     */
    public function setPos($pos)
    {
        $this->pos = $pos;

        return $this;
    }

    /**
     * Get pos
     *
     * @return integer 
     */
    public function getPos()
    {
        return $this->pos;
    }
}
