<?php

namespace Site\Bundle\BackendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;


class Requests
{

    private $id;
    private $type;
    private $requestData;
    private $requestDate;
    private $readEd;
    private $doneEd;
    private $dc;
    private $region;

    public function getId()
    {
        return $this->id;
    }

    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    public function getType()
    {
        return $this->type;
    }
    public function setDc($dc)
    {
        $this->dc = $dc;
        return $this;
    }

    public function getDc()
    {
        return $this->dc;
    }

    public function setRegion($region)
    {
        $this->region = $region;
        return $this;
    }

    public function getRegion()
    {
        return $this->region;
    }
    public function setRequestData($requestData)
    {
        $this->requestData = $requestData;
        return $this;
    }

    public function getRequestData()
    {
        return $this->requestData;
    }
    public function setRequestDate($requestDate)
    {
        $this->requestDate = $requestDate;
        return $this;
    }

    public function getRequestDate()
    {
        return $this->requestDate;
    }
    public function setDoneEd($doneEd)
    {
        $this->doneEd = $doneEd;
        return $this;
    }

    public function getDoneEd()
    {
        return $this->doneEd;
    }
    public function setReadEd($readEd)
    {
        $this->readEd = $readEd;
        return $this;
    }

    public function getReadEd()
    {
        return $this->readEd;
    }
}
