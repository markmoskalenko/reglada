<?php

namespace Site\Bundle\BackendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InfoGroups
 */
class InfoGroups
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $keyName;

    /**
     * @var string
     */
    private $gtype;
    /**
     * @var integer
     */
    private $tabPlace;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return InfoGroups
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set keyName
     *
     * @param string $keyName
     * @return InfoGroups
     */
    public function setKeyName($keyName)
    {
        $this->keyName = $keyName;

        return $this;
    }

    /**
     * Get keyName
     *
     * @return string 
     */
    public function getKeyName()
    {
        return $this->keyName;
    }

    /**
     * Set gtype
     *
     * @param string $gtype
     * @return InfoGroups
     */
    public function setGtype($gtype)
    {
        $this->gtype = $gtype;

        return $this;
    }

    /**
     * Get gtype
     *
     * @return string 
     */
    public function getGtype()
    {
        return $this->gtype;
    }

    /**
     * Set tabPlace
     *
     * @param integer $tabPlace
     * @return InfoGroups
     */
    public function setTabPlace($tabPlace)
    {
        $this->tabPlace = $tabPlace;

        return $this;
    }

    /**
     * Get tabPlace
     *
     * @return integer 
     */
    public function getTabPlace()
    {
        return $this->tabPlace;
    }
}
