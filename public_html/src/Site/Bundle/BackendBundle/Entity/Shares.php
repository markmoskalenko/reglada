<?php

namespace Site\Bundle\BackendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

class Shares
{
    private $id;
    private $name;
    private $regions;
    private $author;
    private $regionParent;
    private $teaser;
    private $content;
    private $picture;
    private $status;
    private $dateStart;
    private $dateStop;
    private $top;
    private $general;
    private $dc;
    private $cars;
    private $equipments;

    public function getId()
    {
        return $this->id;
    }

    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setDc($dc)
    {
        $this->dc = $dc;
        return $this;
    }

    public function getDc()
    {
        return $this->dc;
    }

    public function setGeneral($general)
    {
        $this->general = $general;
        return $this;
    }

    public function getGeneral()
    {
        return $this->general;
    }

    public function setRegions($regions)
    {
        $this->regions = $regions;
        return $this;
    }

    public function getRegions()
    {
        return unserialize($this->regions);
    }

    public function setAuthor($author)
    {
        $this->author = $author;
        return $this;
    }

    public function getAuthor()
    {
        return unserialize($this->author);
    }

    public function setCars($cars)
    {
        $this->cars = $cars;
        return $this;
    }

    public function getCars()
    {
        return unserialize($this->cars);
    }

    public function setEquipments($equipments)
    {
        $this->equipments = $equipments;
        return $this;
    }

    public function getEquipments()
    {
        return unserialize($this->equipments);
    }

    public function setTeaser($teaser)
    {
        $this->teaser = $teaser;
        return $this;
    }

    public function getTeaser()
    {
        return $this->teaser;
    }
    public function setPicture($picture)
    {
        $this->picture = $picture;
        return $this;
    }

    public function getPicture()
    {
        return $this->picture;
    }
    public function setContent($content)
    {
        $this->content = $content;
        return $this;
    }

    public function getContent()
    {
        return $this->content;
    }

    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function setDateStart($dateStart)
    {
        $this->dateStart = $dateStart;
        return $this;
    }

    public function getDateStart()
    {
        return $this->dateStart;
    }

    public function setDateStop($dateStop)
    {
        $this->dateStop = $dateStop;
        return $this;
    }

    public function getDateStop()
    {
        return $this->dateStop;
    }

    public function setTop($top)
    {
        $this->top = $top;
        return $this;
    }

    public function getTop()
    {
        return $this->top;
    }

    public function setRegionParent($regionParent)
    {
        $this->regionParent = $regionParent;
        return $this;
    }

    public function getRegionParent()
    {
        return $this->regionParent;
    }
}
