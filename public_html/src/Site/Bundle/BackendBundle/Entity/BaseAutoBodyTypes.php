<?php

namespace Site\Bundle\BackendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * BaseAutoBodyTypes
 */
class BaseAutoBodyTypes
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $carId;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $nameAdmin;

    /**
     * @var string
     */
    private $file;

    /**
     * @var string
     */
    private $banner;

    /**
     * @var integer
     */
    private $pos;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set carId
     *
     * @param integer $carId
     * @return BaseAutoBodyTypes
     */
    public function setCarId($carId)
    {
        $this->carId = $carId;

        return $this;
    }

    /**
     * Get carId
     *
     * @return integer 
     */
    public function getCarId()
    {
        return $this->carId;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return BaseAutoBodyTypes
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set nameAdmin
     *
     * @param string $nameAdmin
     * @return BaseAutoBodyTypes
     */
    public function setNameAdmin($nameAdmin)
    {
        $this->nameAdmin = $nameAdmin;

        return $this;
    }

    /**
     * Get nameAdmin
     *
     * @return string 
     */
    public function getNameAdmin()
    {
        return $this->nameAdmin;
    }

    /**
     * Set file
     *
     * @param string $file
     * @return BaseAutoBodyTypes
     */
    public function setFile($file)
    {
        $this->file = $file;

        return $this;
    }

    /**
     * Get file
     *
     * @return string 
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * Set banner
     *
     * @param string $banner
     * @return BaseAutoBodyTypes
     */
    public function setBanner($banner)
    {
        $this->banner = $banner;

        return $this;
    }

    /**
     * Get banner
     *
     * @return string 
     */
    public function getBanner()
    {
        return $this->banner;
    }

    /**
     * Set pos
     *
     * @param integer $pos
     * @return BaseAutoBodyTypes
     */
    public function setPos($pos)
    {
        $this->pos = $pos;

        return $this;
    }

    /**
     * Get pos
     *
     * @return integer 
     */
    public function getPos()
    {
        return $this->pos;
    }
}
