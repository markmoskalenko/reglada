<?php

namespace Site\Bundle\BackendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

class CreditOffers
{

    private $id;
    private $bank;
    private $nameCredit;
    private $typeCredit;
    private $auto;
    private $tax;
    private $insurance;
    private $documents;
    private $requirements;
    private $dateCreated;
    private $dateUpdated;
    private $active;

    public function getId()
    {
        return $this->id;
    }

    public function setBank($bank)
    {
        $this->bank = $bank;

        return $this;
    }

    public function getBank()
    {
        return $this->bank;
    }

    public function setNameCredit($nameCredit)
    {
        $this->nameCredit = $nameCredit;

        return $this;
    }

    public function getNameCredit()
    {
        return $this->nameCredit;
    }

    public function setTypeCredit($typeCredit)
    {
        $this->typeCredit = $typeCredit;

        return $this;
    }

    public function getTypeCredit()
    {
        return $this->typeCredit;
    }

    public function setAuto($auto)
    {
        $this->auto = serialize($auto);

        return $this;
    }

    public function getAuto()
    {
        return unserialize($this->auto);
    }

    public function setTax($tax)
    {
        $this->tax = serialize($tax);

        return $this;
    }

    public function getTax()
    {
        return unserialize($this->tax);
    }

    public function setRequirements($requirements)
    {
        $this->requirements = $requirements;

        return $this;
    }

    public function getRequirements()
    {
        return $this->requirements;
    }


    public function setInsurance($insurance)
    {
        $this->insurance = $insurance;

        return $this;
    }

    public function getInsurance()
    {
        return $this->insurance;
    }

    public function setDocuments($documents)
    {
        $this->documents = $documents;

        return $this;
    }

    public function getDocuments()
    {
        return $this->documents;
    }
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;

        return $this;
    }

    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    public function setDateUpdated($dateUpdated)
    {
        $this->dateUpdated = $dateUpdated;

        return $this;
    }

    public function getDateUpdated()
    {
        return $this->dateUpdated;
    }

    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    public function getActive()
    {
        return $this->active;
    }
}
