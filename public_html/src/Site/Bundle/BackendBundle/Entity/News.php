<?php

namespace Site\Bundle\BackendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

class News
{
    private $id;
    private $name;
    private $regions;
    private $groupId;
    private $teaser;
    private $content;
    private $picture;
    private $tags;
    private $status;
    private $date;
    private $top;

    public function getId()
    {
        return $this->id;
    }

    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setRegions($regions)
    {
        $this->regions = $regions;
        return $this;
    }

    public function getRegions()
    {
        return unserialize($this->regions);
    }

    public function setGroupId($groupId)
    {
        $this->groupId = $groupId;
        return $this;
    }

    public function getGroupId()
    {
        return unserialize($this->groupId);
    }

    public function setTeaser($teaser)
    {
        $this->teaser = $teaser;
        return $this;
    }

    public function getTeaser()
    {
        return $this->teaser;
    }
    public function setPicture($picture)
    {
        $this->picture = $picture;
        return $this;
    }

    public function getPicture()
    {
        return $this->picture;
    }
    public function setContent($content)
    {
        $this->content = $content;
        return $this;
    }

    public function getContent()
    {
        return $this->content;
    }

    public function setTags($tags)
    {
        $this->tags = $tags;
        return $this;
    }

    public function getTags()
    {
        return $this->tags;
    }

    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function setDate($date)
    {
        $this->date = $date;
        return $this;
    }

    public function getDate()
    {
        return $this->date;
    }

    public function setTop($top)
    {
        $this->top = $top;
        return $this;
    }

    public function getTop()
    {
        return $this->top;
    }
}
