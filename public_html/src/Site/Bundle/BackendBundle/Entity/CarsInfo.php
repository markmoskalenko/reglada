<?php

namespace Site\Bundle\BackendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CarsInfo
 */
class CarsInfo
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $infoId;

    /**
     * @var integer
     */
    private $valueId;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set infoId
     *
     * @param integer $infoId
     * @return CarsInfo
     */
    public function setInfoId($infoId)
    {
        $this->infoId = $infoId;

        return $this;
    }

    /**
     * Get infoId
     *
     * @return integer 
     */
    public function getInfoId()
    {
        return $this->infoId;
    }

    /**
     * Set valueId
     *
     * @param integer $valueId
     * @return CarsInfo
     */
    public function setValueId($valueId)
    {
        $this->valueId = $valueId;

        return $this;
    }

    /**
     * Get valueId
     *
     * @return integer 
     */
    public function getValueId()
    {
        return $this->valueId;
    }
    /**
     * @var integer
     */
    private $carModelId;


    /**
     * Set carModelId
     *
     * @param integer $carModelId
     * @return CarsInfo
     */
    public function setCarModelId($carModelId)
    {
        $this->carModelId = $carModelId;

        return $this;
    }

    /**
     * Get carModelId
     *
     * @return integer 
     */
    public function getCarModelId()
    {
        return $this->carModelId;
    }

}
