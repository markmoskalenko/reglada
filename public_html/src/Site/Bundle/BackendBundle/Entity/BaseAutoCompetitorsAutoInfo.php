<?php

namespace Site\Bundle\BackendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * BaseAutoCompetitorsAutoInfo
 */
class BaseAutoCompetitorsAutoInfo
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $autoId;

    /**
     * @var integer
     */
    private $infoId;

    /**
     * @var integer
     */
    private $value;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set autoId
     *
     * @param integer $autoId
     * @return BaseAutoCompetitorsAutoInfo
     */
    public function setAutoId($autoId)
    {
        $this->autoId = $autoId;

        return $this;
    }

    /**
     * Get autoId
     *
     * @return integer 
     */
    public function getAutoId()
    {
        return $this->autoId;
    }

    /**
     * Set infoId
     *
     * @param integer $infoId
     * @return BaseAutoCompetitorsAutoInfo
     */
    public function setInfoId($infoId)
    {
        $this->infoId = $infoId;

        return $this;
    }

    /**
     * Get infoId
     *
     * @return integer 
     */
    public function getInfoId()
    {
        return $this->infoId;
    }

    /**
     * Set value
     *
     * @param integer $value
     * @return BaseAutoCompetitorsAutoInfo
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return integer 
     */
    public function getValue()
    {
        return $this->value;
    }
}
