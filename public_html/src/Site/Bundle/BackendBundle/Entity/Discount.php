<?php

namespace Site\Bundle\BackendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * FeatureGroups
 */
class Discount
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var integer
     */
    private $status;

    /**
     * @var string
     */
    private $period;
    /**
     * @var string
     */
    private $firstpay;

    /**
     * @var integer
     */
    private $auto;

    /**
     * @var integer
     */
    private $regions;

    private $persent;
    private $summ;
    private $datestart;
    private $dateend;
    private $content;
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return FeatureGroups
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    public function getContent()
    {
        return $this->content;
    }

    public function setDatestart($datestart)
    {
        $this->datestart = $datestart;

        return $this;
    }

    public function getDatestart()
    {
        return $this->datestart;
    }

    public function setDateend($dateend)
    {
        $this->dateend = $dateend;

        return $this;
    }

    public function getDateend()
    {
        return $this->dateend;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return FeatureGroups
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    public function setPersent($persent)
    {
        $this->persent = $persent;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getPersent()
    {
        return $this->persent;
    }

    public function setSumm($summ)
    {
        $this->summ = $summ;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getSumm()
    {
        return $this->summ;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    public function setAuto($auto)
    {
        $this->auto = serialize($auto);

        return $this;
    }



    public function getAuto()
    {
        return unserialize($this->auto);
    }

    public function setRegions($regions)
    {
        $this->regions = serialize($regions);

        return $this;
    }


    public function getRegions()
    {
        return unserialize($this->regions);
    }

    public function setFirstpay($firstpay)
    {
        $this->firstpay = serialize($firstpay);

        return $this;
    }


    public function getFirstpay()
    {
        return unserialize($this->firstpay);
    }

    public function setPeriod($period)
    {
        $this->period = serialize($period);

        return $this;
    }


    public function getPeriod()
    {
        return unserialize($this->period);
    }
}
