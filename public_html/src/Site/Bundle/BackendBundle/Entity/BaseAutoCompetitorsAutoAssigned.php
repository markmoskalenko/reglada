<?php

namespace Site\Bundle\BackendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * BaseAutoCompetitorsAutoAssigned
 */
class BaseAutoCompetitorsAutoAssigned
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $carId;

    /**
     * @var integer
     */
    private $competitorId;

    /**
     * @var boolean
     */
    private $isMain;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set carId
     *
     * @param integer $carId
     * @return BaseAutoCompetitorsAutoAssigned
     */
    public function setCarId($carId)
    {
        $this->carId = $carId;

        return $this;
    }

    /**
     * Get carId
     *
     * @return integer 
     */
    public function getCarId()
    {
        return $this->carId;
    }

    /**
     * Set competitorId
     *
     * @param integer $competitorId
     * @return BaseAutoCompetitorsAutoAssigned
     */
    public function setCompetitorId($competitorId)
    {
        $this->competitorId = $competitorId;

        return $this;
    }

    /**
     * Get competitorId
     *
     * @return integer 
     */
    public function getCompetitorId()
    {
        return $this->competitorId;
    }

    /**
     * Set isMain
     *
     * @param boolean $isMain
     * @return BaseAutoCompetitorsAutoAssigned
     */
    public function setIsMain($isMain)
    {
        $this->isMain = $isMain;

        return $this;
    }

    /**
     * Get isMain
     *
     * @return boolean 
     */
    public function getIsMain()
    {
        return $this->isMain;
    }
}
