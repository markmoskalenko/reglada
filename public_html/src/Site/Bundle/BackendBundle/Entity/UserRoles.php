<?php

namespace Site\Bundle\BackendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * UserRoles
 */
class UserRoles
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $rolekey;

    /**
     * @var string
     */
    private $perms;

    /**
     * @var integer
     */
    private $regions;
    /**
     * @var integer
     */
    // private $scheme;

    /**
     * @var string
     */
    // private $cabinetName;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return UserRoles
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set rolekey
     *
     * @param string $rolekey
     * @return UserRoles
     */
    public function setRolekey($rolekey)
    {
        $this->rolekey = $rolekey;

        return $this;
    }

    /**
     * Get rolekey
     *
     * @return string 
     */
    public function getRolekey()
    {
        return $this->rolekey;
    }

    /**
     * Set perms
     *
     * @param string $perms
     * @return UserRoles
     */
    public function setPerms($perms)
    {
        $this->perms = $perms;

        return $this;
    }

    /**
     * Get perms
     *
     * @return string 
     */
    public function getPerms()
    {
        return $this->perms;
    }
    /**
     * @var integer
     */
    private $scheme;

    /**
     * @var string
     */
    private $cabinetName;


    /**
     * Set scheme
     *
     * @param integer $scheme
     * @return UserRoles
     */
    public function setScheme($scheme)
    {
        $this->scheme = $scheme;

        return $this;
    }

    /**
     * Get scheme
     *
     * @return integer 
     */
    public function getScheme()
    {
        return $this->scheme;
    }

    /**
     * Set cabinetName
     *
     * @param string $cabinetName
     * @return UserRoles
     */
    public function setCabinetName($cabinetName)
    {
        $this->cabinetName = $cabinetName;

        return $this;
    }

    /**
     * Get cabinetName
     *
     * @return string 
     */
    public function getCabinetName()
    {
        return $this->cabinetName;
    }

    /**
     * Set regions
     *
     * @param string $cabinetName
     * @return UserRoles
     */
    public function setRegions($regions)
    {
        $this->regions = $regions;

        return $this;
    }

    /**
     * Get cabinetName
     *
     * @return string 
     */
    public function getRegions()
    {
        return $this->regions;
    }
}
