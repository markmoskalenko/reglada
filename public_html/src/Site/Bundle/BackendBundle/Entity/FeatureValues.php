<?php

namespace Site\Bundle\BackendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * FeatureValues
 */
class FeatureValues
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $featureId;

    /**
     * @var string
     */
    private $value;

    /**
     * @var string
     */
    private $description;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set featureId
     *
     * @param integer $featureId
     * @return FeatureValues
     */
    public function setFeatureId($featureId)
    {
        $this->featureId = $featureId;

        return $this;
    }

    /**
     * Get featureId
     *
     * @return integer 
     */
    public function getFeatureId()
    {
        return $this->featureId;
    }

    /**
     * Set value
     *
     * @param string $value
     * @return FeatureValues
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return string 
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return FeatureValues
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }
}
