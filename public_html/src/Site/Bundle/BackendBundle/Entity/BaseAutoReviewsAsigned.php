<?php

namespace Site\Bundle\BackendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * BaseAutoReviewsAsigned
 */
class BaseAutoReviewsAsigned
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $carId;

    /**
     * @var integer
     */
    private $reviewId;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set carId
     *
     * @param integer $carId
     * @return BaseAutoReviewsAsigned
     */
    public function setCarId($carId)
    {
        $this->carId = $carId;

        return $this;
    }

    /**
     * Get carId
     *
     * @return integer 
     */
    public function getCarId()
    {
        return $this->carId;
    }

    /**
     * Set reviewId
     *
     * @param integer $reviewId
     * @return BaseAutoReviewsAsigned
     */
    public function setReviewId($reviewId)
    {
        $this->reviewId = $reviewId;

        return $this;
    }

    /**
     * Get reviewId
     *
     * @return integer 
     */
    public function getReviewId()
    {
        return $this->reviewId;
    }
}
