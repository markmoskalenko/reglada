<?php

namespace Site\Bundle\BackendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * BlockSliderSettings
 */
class BlockSliderSettings
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $type;

    /**
     * @var string
     */
    private $directionShow;

    /**
     * @var integer
     */
    private $pauseShow;

    /**
     * @var integer
     */
    private $pauseNext;

    /**
     * @var integer
     */
    private $autoShow;

    /**
     * @var string
     */
    private $sliders;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return BlockSliderSettings
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set directionShow
     *
     * @param string $directionShow
     * @return BlockSliderSettings
     */
    public function setDirectionShow($directionShow)
    {
        $this->directionShow = $directionShow;

        return $this;
    }

    /**
     * Get directionShow
     *
     * @return string 
     */
    public function getDirectionShow()
    {
        return $this->directionShow;
    }

    /**
     * Set pauseShow
     *
     * @param integer $pauseShow
     * @return BlockSliderSettings
     */
    public function setPauseShow($pauseShow)
    {
        $this->pauseShow = $pauseShow;

        return $this;
    }

    /**
     * Get pauseShow
     *
     * @return integer 
     */
    public function getPauseShow()
    {
        return $this->pauseShow;
    }

    /**
     * Set pauseNext
     *
     * @param integer $pauseNext
     * @return BlockSliderSettings
     */
    public function setPauseNext($pauseNext)
    {
        $this->pauseNext = $pauseNext;

        return $this;
    }

    /**
     * Get pauseNext
     *
     * @return integer 
     */
    public function getPauseNext()
    {
        return $this->pauseNext;
    }

    /**
     * Set autoShow
     *
     * @param integer $autoShow
     * @return BlockSliderSettings
     */
    public function setAutoShow($autoShow)
    {
        $this->autoShow = $autoShow;

        return $this;
    }

    /**
     * Get autoShow
     *
     * @return integer 
     */
    public function getAutoShow()
    {
        return $this->autoShow;
    }

    /**
     * Set sliders
     *
     * @param string $sliders
     * @return BlockSliderSettings
     */
    public function setSliders($sliders)
    {
        $this->sliders = $sliders;

        return $this;
    }

    /**
     * Get sliders
     *
     * @return string 
     */
    public function getSliders()
    {
        return $this->sliders;
    }
}
