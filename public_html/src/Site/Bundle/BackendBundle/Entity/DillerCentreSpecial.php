<?php

namespace Site\Bundle\BackendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DillerCentreSpecial
 */
class DillerCentreSpecial
{
    /**
     * @var integer
     */
    private $id;
    /**
     * @var integer
     */
    private $dillerCentreId;

    /**
     * @var string
     */
    private $title;

    /**
     * @var string
     */
    private $dateExpire;

    /**
     * @var string
     */
    private $descr;

    /**
     * @var string
     */
    private $image;

    /**
     * @var boolean
     */
    private $isInPage;

    /**
     * @var boolean
     */
    private $isFirst;

    /**
     * @var boolean
     */
    private $isArchive;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dillerCentreId
     *
     * @param string $dillerCentreId
     * @return DillerCentreSpecial
     */
    public function setDillerCentreId($dillerCentreId)
    {
        $this->dillerCentreId = $dillerCentreId;

        return $this;
    }

    /**
     * Get dillerCentreId
     *
     * @return string 
     */
    public function getDillerCentreId()
    {
        return $this->dillerCentreId;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return DillerCentreSpecial
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set dateExpire
     *
     * @param string $dateExpire
     * @return DillerCentreSpecial
     */
    public function setDateExpire($dateExpire)
    {
        $this->dateExpire = $dateExpire;

        return $this;
    }

    /**
     * Get dateExpire
     *
     * @return string 
     */
    public function getDateExpire()
    {
        return $this->dateExpire;
    }

    /**
     * Set descr
     *
     * @param string $descr
     * @return DillerCentreSpecial
     */
    public function setDescr($descr)
    {
        $this->descr = $descr;

        return $this;
    }

    /**
     * Get descr
     *
     * @return string 
     */
    public function getDescr()
    {
        return $this->descr;
    }

    /**
     * Set image
     *
     * @param string $image
     * @return DillerCentreSpecial
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return string 
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set isInPage
     *
     * @param boolean $isInPage
     * @return DillerCentreSpecial
     */
    public function setIsInPage($isInPage)
    {
        $this->isInPage = $isInPage;

        return $this;
    }

    /**
     * Get isInPage
     *
     * @return boolean 
     */
    public function getIsInPage()
    {
        return $this->isInPage;
    }

    /**
     * Set isFirst
     *
     * @param boolean $isFirst
     * @return DillerCentreSpecial
     */
    public function setIsFirst($isFirst)
    {
        $this->isFirst = $isFirst;

        return $this;
    }

    /**
     * Get isFirst
     *
     * @return boolean 
     */
    public function getIsFirst()
    {
        return $this->isFirst;
    }

    /**
     * Set isArchive
     *
     * @param boolean $isArchive
     * @return DillerCentreSpecial
     */
    public function setIsArchive($isArchive)
    {
        $this->isArchive = $isArchive;

        return $this;
    }

    /**
     * Get isArchive
     *
     * @return boolean 
     */
    public function getIsArchive()
    {
        return $this->isArchive;
    }
}
