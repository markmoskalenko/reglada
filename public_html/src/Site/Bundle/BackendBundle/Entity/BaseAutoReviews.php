<?php

namespace Site\Bundle\BackendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * BaseAutoReviews
 */
class BaseAutoReviews
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $carId;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $description;

    /**
     * @var string
     */
    private $file;

    /**
     * @var string
     */
    private $authorName;

    /**
     * @var string
     */
    private $authorStatus;

    /**
     * @var \DateTime
     */
    private $dateAdd;

    /**
     * @var \DateTime
     */
    private $dateUpdate;

    /**
     * @var boolean
     */
    private $isActive;

    /**
     * @var boolean
     */
    private $inHome;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set carId
     *
     * @param integer $carId
     * @return BaseAutoReviews
     */
    public function setCarId($carId)
    {
        $this->carId = $carId;

        return $this;
    }

    /**
     * Get carId
     *
     * @return integer 
     */
    public function getCarId()
    {
        return $this->carId;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return BaseAutoReviews
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return BaseAutoReviews
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set file
     *
     * @param string $file
     * @return BaseAutoReviews
     */
    public function setFile($file)
    {
        $this->file = $file;

        return $this;
    }

    /**
     * Get file
     *
     * @return string 
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * Set authorName
     *
     * @param string $authorName
     * @return BaseAutoReviews
     */
    public function setAuthorName($authorName)
    {
        $this->authorName = $authorName;

        return $this;
    }

    /**
     * Get authorName
     *
     * @return string 
     */
    public function getAuthorName()
    {
        return $this->authorName;
    }

    /**
     * Set authorStatus
     *
     * @param string $authorStatus
     * @return BaseAutoReviews
     */
    public function setAuthorStatus($authorStatus)
    {
        $this->authorStatus = $authorStatus;

        return $this;
    }

    /**
     * Get authorStatus
     *
     * @return string 
     */
    public function getAuthorStatus()
    {
        return $this->authorStatus;
    }

    /**
     * Set dateAdd
     *
     * @param \DateTime $dateAdd
     * @return BaseAutoReviews
     */
    public function setDateAdd($dateAdd)
    {
        $this->dateAdd = $dateAdd;

        return $this;
    }

    /**
     * Get dateAdd
     *
     * @return \DateTime 
     */
    public function getDateAdd()
    {
        return $this->dateAdd;
    }

    /**
     * Set dateUpdate
     *
     * @param \DateTime $dateUpdate
     * @return BaseAutoReviews
     */
    public function setDateUpdate($dateUpdate)
    {
        $this->dateUpdate = $dateUpdate;

        return $this;
    }

    /**
     * Get dateUpdate
     *
     * @return \DateTime 
     */
    public function getDateUpdate()
    {
        return $this->dateUpdate;
    }

    /**
     * Set isActive
     *
     * @param boolean $isActive
     * @return BaseAutoReviews
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * Get isActive
     *
     * @return boolean 
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * Set inHome
     *
     * @param boolean $inHome
     * @return BaseAutoReviews
     */
    public function setInHome($inHome)
    {
        $this->inHome = $inHome;

        return $this;
    }

    /**
     * Get inHome
     *
     * @return boolean 
     */
    public function getInHome()
    {
        return $this->inHome;
    }
}
