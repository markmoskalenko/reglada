<?php

namespace Site\Bundle\BackendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Info
 */
class Info
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $description;

    /**
     * @var integer
     */
    private $pos;

    /**
     * @var string
     */
    private $seltype;

    /**
     * @var string
     */
    private $infoType;

    private $pages;
    private $identifier;
    private $image;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Info
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Info
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set pos
     *
     * @param integer $pos
     * @return Info
     */
    public function setPos($pos)
    {
        $this->pos = $pos;

        return $this;
    }

    /**
     * Get pos
     *
     * @return integer 
     */
    public function getPos()
    {
        return $this->pos;
    }
    /**
     * @var integer
     */
    private $groupId;

    /**
     * @var string
     */
    private $keyName;


    /**
     * Set groupId
     *
     * @param integer $groupId
     * @return Info
     */
    public function setGroupId($groupId)
    {
        $this->groupId = $groupId;

        return $this;
    }

    /**
     * Get groupId
     *
     * @return integer 
     */
    public function getGroupId()
    {
        return $this->groupId;
    }

    /**
     * Set keyName
     *
     * @param string $keyName
     * @return Info
     */
    public function setKeyName($keyName)
    {
        $this->keyName = $keyName;

        return $this;
    }

    /**
     * Get keyName
     *
     * @return string 
     */
    public function getKeyName()
    {
        return $this->keyName;
    }

    /**
     * Set seltype
     *
     * @param string $seltype
     * @return Info
     */
    public function setSeltype($seltype)
    {
        $this->seltype = $seltype;

        return $this;
    }

    /**
     * Get seltype
     *
     * @return string 
     */
    public function getSeltype()
    {
        return $this->seltype;
    }

    /**
     * Set infoType
     *
     * @param string $infoType
     * @return Info
     */
    public function setInfoType($infoType)
    {
        $this->infoType = $infoType;

        return $this;
    }

    /**
     * Get infoType
     *
     * @return string 
     */
    public function getInfoType()
    {
        return $this->infoType;
    }

    public function setIdentifier($identifier)
    {
        $this->identifier = $identifier;

        return $this;
    }

    /**
     * Get infoType
     *
     * @return string 
     */
    public function getIdentifier()
    {
        return $this->identifier;
    }

    public function setPages($pages)
    {
        $this->pages = $pages;

        return $this;
    }

    /**
     * Get infoType
     *
     * @return string 
     */
    public function getPages()
    {
        return unserialize($this->pages);
    }

    /**
     * Set image
     *
     * @param string $image
     * @return Info
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return string 
     */
    public function getImage()
    {
        return $this->image;
    }
}
