<?php

namespace Site\Bundle\BackendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * FeatureGroups
 */
class Colors
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var integer
     */
    private $status;

    /**
     * @var string
     */
    private $code;

    /**
     * @var integer
     */
    private $auto;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return FeatureGroups
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return FeatureGroups
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    public function setAuto($auto)
    {
        $this->auto = $auto;

        return $this;
    }


    public function getAuto()
    {
        return unserialize($this->auto);
    }

    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }


    public function getCode()
    {
        return $this->code;
    }
}
