<?php

namespace Site\Bundle\BackendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * BaseAutoCarsBlocks
 */
class BaseAutoCarsBlocks
{
    /**
     * @var integer
     */
    private $id;
    
    /**
     * @var boolean
     *
     * @ORM\Column(name="car_id", type="integer", nullable=false)
     */
    private $carId;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $descr;

    /**
     * @var string
     */
    private $image;

    /**
     * @var boolean
     */
    private $blockType;

    /**
     * @var integer
     */
    private $pos;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * Set carId
     *
     * @param integer $carId
     * @return BaseAutoSliderSlides
     */
    public function setCarId($carId)
    {
        $this->carId = $carId;

        return $this;
    }

    /**
     * Get carId
     *
     * @return integer 
     */
    public function getCarId()
    {
        return $this->carId;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return BaseAutoCarsBlocks
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set descr
     *
     * @param string $descr
     * @return BaseAutoCarsBlocks
     */
    public function setDescr($descr)
    {
        $this->descr = $descr;

        return $this;
    }

    /**
     * Get descr
     *
     * @return string 
     */
    public function getDescr()
    {
        return $this->descr;
    }

    /**
     * Set image
     *
     * @param string $image
     * @return BaseAutoCarsBlocks
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return string 
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set blockType
     *
     * @param boolean $blockType
     * @return BaseAutoCarsBlocks
     */
    public function setBlockType($blockType)
    {
        $this->blockType = $blockType;

        return $this;
    }

    /**
     * Get blockType
     *
     * @return boolean 
     */
    public function getBlockType()
    {
        return $this->blockType;
    }

    /**
     * Set pos
     *
     * @param integer $pos
     * @return Cars
     */
    public function setPos($pos)
    {
        $this->pos = $pos;

        return $this;
    }

    /**
     * Get pos
     *
     * @return integer 
     */
    public function getPos()
    {
        return $this->pos;
    }
}
