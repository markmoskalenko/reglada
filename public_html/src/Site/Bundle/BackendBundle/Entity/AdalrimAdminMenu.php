<?php

namespace Site\Bundle\BackendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AdalrimAdminMenu
 *
 * @ORM\Table(name="adalrim_admin_menu")
 * @ORM\Entity(repositoryClass="Site\Bundle\BackendBundle\Repository\AdalrimAdminMenuRepository")
 */
class AdalrimAdminMenu
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="action", type="string", length=256, nullable=false)
     */
    private $action;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=256, nullable=false)
     */
    private $title;

    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="integer", nullable=false)
     */
    private $status;

    /**
     * @var string
     *
     * @ORM\Column(name="allowed_regions", type="text", nullable=true)
     */
    private $allowedRegions;

    /**
     * @var integer
     *
     * @ORM\Column(name="parent", type="integer", nullable=false)
     */
    private $parent;

    /**
     * @var integer
     *
     * @ORM\Column(name="delta", type="integer", nullable=false)
     */
    private $delta;

    /**
     * @var integer
     *
     * @ORM\Column(name="read", type="integer", nullable=true)
     */
    private $read;

    /**
     * @var integer
     *
     * @ORM\Column(name="create", type="integer", nullable=true)
     */
    private $create;

    /**
     * @var integer
     *
     * @ORM\Column(name="edit", type="integer", nullable=true)
     */
    private $edit;

    /**
     * @var integer
     *
     * @ORM\Column(name="delete", type="integer", nullable=true)
     */
    private $delete;

    /**
     * @var integer
     *
     * @ORM\Column(name="target", type="integer", nullable=true)
     */
    private $target;

    private $showed;
    public function getAction()
    {
        return $this->action;
    }
    public function getTarget()
    {
        return $this->target;
    }
}
