<?php

namespace Site\Bundle\BackendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Cars
 */
class Cars
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;
    
    /**
     * @var string
     */
    private $nameAdmin;

    /**
     * @var integer
     */
    private $bodyId;

    /**
     * @var string
     */
    private $file;

    /**
     * @var decimal
     */
    private $price;

    /**
     * @var integer
     */
    private $pos;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Cars
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set nameAdmin
     *
     * @param string $nameAdmin
     * @return Cars
     */
    public function setNameAdmin($nameAdmin)
    {
        $this->nameAdmin = $nameAdmin;

        return $this;
    }

    /**
     * Get nameAdmin
     *
     * @return string 
     */
    public function getNameAdmin()
    {
        return $this->nameAdmin;
    }

    /**
     * Set pos
     *
     * @param integer $pos
     * @return Cars
     */
    public function setPos($pos)
    {
        $this->pos = $pos;

        return $this;
    }

    /**
     * Get pos
     *
     * @return integer 
     */
    public function getPos()
    {
        return $this->pos;
    }

    /**
     * Set bodyId
     *
     * @param integer $bodyId
     * @return Cars
     */
    public function setBodyId($bodyId)
    {
        $this->bodyId = $bodyId;

        return $this;
    }

    /**
     * Get bodyId
     *
     * @return integer 
     */
    public function getBodyId()
    {
        return $this->bodyId;
    }

    /**
     * Set file
     *
     * @param string $file
     * @return Cars
     */
    public function setFile($file)
    {
        $this->file = $file;

        return $this;
    }

    /**
     * Get file
     *
     * @return string 
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * Set price
     *
     * @param string $price
     * @return Cars
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return string 
     */
    public function getPrice()
    {
        return $this->price;
    }
}
