<?php

namespace Site\Bundle\BackendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SliderSlides
 */
class SliderSlides
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $title;

    /**
     * @var string
     */
    private $image;

    /**
     * @var string
     */
    private $image2;

    /**
     * @var string
     */
    private $textTitle;

    /**
     * @var string
     */
    private $textSubtitle;

    /**
     * @var string
     */
    private $shortDescription;

    /**
     * @var string
     */
    private $textMonth;

    /**
     * @var string
     */
    private $linkButton;

    /**
     * @var integer
     */
    private $pos;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return SliderSlides
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set image
     *
     * @param string $image
     * @return SliderSlides
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image2
     *
     * @return string 
     */
    public function getImage2()
    {
        return $this->image2;
    }

    /**
     * Set image2
     *
     * @param string $image2
     * @return SliderSlides
     */
    public function setImage2($image2)
    {
        $this->image2 = $image2;

        return $this;
    }

    /**
     * Get image
     *
     * @return string 
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set textTitle
     *
     * @param string $textTitle
     * @return SliderSlides
     */
    public function setTextTitle($textTitle)
    {
        $this->textTitle = $textTitle;

        return $this;
    }

    /**
     * Get textTitle
     *
     * @return string 
     */
    public function getTextTitle()
    {
        return $this->textTitle;
    }

    /**
     * Set textSubtitle
     *
     * @param string $textSubtitle
     * @return SliderSlides
     */
    public function setTextSubtitle($textSubtitle)
    {
        $this->textSubtitle = $textSubtitle;

        return $this;
    }

    /**
     * Get textSubtitle
     *
     * @return string 
     */
    public function getTextSubtitle()
    {
        return $this->textSubtitle;
    }

    /**
     * Set shortDescription
     *
     * @param string $shortDescription
     * @return SliderSlides
     */
    public function setShortDescription($shortDescription)
    {
        $this->shortDescription = $shortDescription;

        return $this;
    }

    /**
     * Get shortDescription
     *
     * @return string 
     */
    public function getShortDescription()
    {
        return $this->shortDescription;
    }

    /**
     * Set textMonth
     *
     * @param string $textMonth
     * @return SliderSlides
     */
    public function setTextMonth($textMonth)
    {
        $this->textMonth = $textMonth;

        return $this;
    }

    /**
     * Get textMonth
     *
     * @return string 
     */
    public function getTextMonth()
    {
        return $this->textMonth;
    }

    /**
     * Set linkButton
     *
     * @param string $linkButton
     * @return SliderSlides
     */
    public function setLinkButton($linkButton)
    {
        $this->linkButton = $linkButton;

        return $this;
    }

    /**
     * Get linkButton
     *
     * @return string 
     */
    public function getLinkButton()
    {
        return $this->linkButton;
    }

    /**
     * Set pos
     *
     * @param integer $pos
     * @return SliderSlides
     */
    public function setPos($pos)
    {
        $this->pos = $pos;

        return $this;
    }

    /**
     * Get pos
     *
     * @return integer 
     */
    public function getPos()
    {
        return $this->pos;
    }
}
