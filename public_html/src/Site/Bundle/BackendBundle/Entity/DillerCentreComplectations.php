<?php

namespace Site\Bundle\BackendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DillerCentreComplectations
 */
class DillerCentreComplectations
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $dillerId;

    /**
     * @var integer
     */
    private $bodyId;

    /**
     * @var integer
     */
    private $complectationId;

    /**
     * @var integer
     */
    private $variantId;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dillerId
     *
     * @param integer $dillerId
     * @return DillerCentreComplectations
     */
    public function setDillerId($dillerId)
    {
        $this->dillerId = $dillerId;

        return $this;
    }

    /**
     * Get dillerId
     *
     * @return integer 
     */
    public function getDillerId()
    {
        return $this->dillerId;
    }

    /**
     * Set bodyId
     *
     * @param integer $bodyId
     * @return DillerCentreComplectations
     */
    public function setBodyId($bodyId)
    {
        $this->bodyId = $bodyId;

        return $this;
    }

    /**
     * Get bodyId
     *
     * @return integer 
     */
    public function getBodyId()
    {
        return $this->bodyId;
    }

    /**
     * Set complectationId
     *
     * @param integer $complectationId
     * @return DillerCentreComplectations
     */
    public function setComplectationId($complectationId)
    {
        $this->complectationId = $complectationId;

        return $this;
    }

    /**
     * Get complectationId
     *
     * @return integer 
     */
    public function getComplectationId()
    {
        return $this->complectationId;
    }

    /**
     * Set variantId
     *
     * @param integer $variantId
     * @return DillerCentreComplectations
     */
    public function setVariantId($variantId)
    {
        $this->variantId = $variantId;

        return $this;
    }

    /**
     * Get variantId
     *
     * @return integer 
     */
    public function getVariantId()
    {
        return $this->variantId;
    }
}
