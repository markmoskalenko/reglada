<?php

namespace Site\Bundle\BackendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DillerCentreDocuments
 */
class DillerCentreDocuments
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $type;

    /**
     * @var string
     */
    private $link;

    /**
     * @var integer
     */
    private $dillerId;

    /**
     * @var string
     */
    private $descr;

    /**
     * @var \DateTime
     */
    private $dateAdd;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return DillerCentreDocuments
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return DillerCentreDocuments
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set link
     *
     * @param string $link
     * @return DillerCentreDocuments
     */
    public function setLink($link)
    {
        $this->link = $link;

        return $this;
    }

    /**
     * Get link
     *
     * @return string 
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * Set dillerId
     *
     * @param integer $dillerId
     * @return DillerCentreDocuments
     */
    public function setDillerId($dillerId)
    {
        $this->dillerId = $dillerId;

        return $this;
    }

    /**
     * Get dillerId
     *
     * @return integer 
     */
    public function getDillerId()
    {
        return $this->dillerId;
    }

    /**
     * Set descr
     *
     * @param string $descr
     * @return DillerCentreDocuments
     */
    public function setDescr($descr)
    {
        $this->descr = $descr;

        return $this;
    }

    /**
     * Get descr
     *
     * @return string 
     */
    public function getDescr()
    {
        return $this->descr;
    }

    /**
     * Set dateAdd
     *
     * @param \DateTime $dateAdd
     * @return DillerCentreDocuments
     */
    public function setDateAdd($dateAdd)
    {
        $this->dateAdd = $dateAdd;

        return $this;
    }

    /**
     * Get dateAdd
     *
     * @return \DateTime 
     */
    public function getDateAdd()
    {
        return $this->dateAdd;
    }
}
