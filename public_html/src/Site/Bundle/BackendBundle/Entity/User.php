<?php

namespace Site\Bundle\BackendBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Site\Bundle\BackendBundle\Repository\UserRepository")
 * @ORM\Table(name="fos_user")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="user_type", type="integer", nullable=false)
     */
    private $userType;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_reg", type="datetime", nullable=false)
     */
    private $dateReg;

    /**
     * @var string
     *
     * @ORM\Column(name="fio", type="string", length=255, nullable=false)
     */
    private $fio;

    /**
     * @var string
     *
     * @ORM\Column(name="diller_centre_ids", type="text", nullable=true)
     */
    private $dillerCentreIds;

    public function __construct()
    {
        parent::__construct();
        
        $this->roles = array('ROLE_USER');
    }

    /**
     * Set userType
     *
     * @param integer $userType
     * @return FosUser
     */
    public function setUserType($userType)
    {
        $this->userType = $userType;

        return $this;
    }

    /**
     * Get userType
     *
     * @return integer 
     */
    public function getUserType()
    {
        return $this->userType;
    }

    /**
     * Set dateReg
     *
     * @param \DateTime $dateReg
     * @return FosUser
     */
    public function setDateReg($dateReg)
    {
        $this->dateReg = $dateReg;

        return $this;
    }

    /**
     * Get dateReg
     *
     * @return \DateTime 
     */
    public function getDateReg()
    {
        return $this->dateReg;
    }


    /**
     * Set fio
     *
     * @param string $fio
     * @return FosUser
     */
    public function setFio($fio)
    {
        $this->fio = $fio;

        return $this;
    }

    /**
     * Get fio
     *
     * @return string 
     */
    public function getFio()
    {
        return $this->fio;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Set dillerCentreIds
     *
     * @param string $dillerCentreIds
     * @return FosUser
     */
    public function setDillerCentreIds($dillerCentreIds)
    {
        $this->dillerCentreIds = serialize($dillerCentreIds);

        return $this;
    }

    /**
     * Get dillerCentreIds
     *
     * @return string 
     */
    public function getDillerCentreIds()
    {
        return unserialize($this->dillerCentreIds);
    }
}
