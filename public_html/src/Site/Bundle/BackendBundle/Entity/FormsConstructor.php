<?php

namespace Site\Bundle\BackendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

class FormsConstructor
{

    private $id;
    private $name;
    private $alias;
    private $options;
    private $success;
    private $fail;
    private $status;

    public function getId(){
        return $this->id;
    }

    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }
    public function getName()
    {
        return $this->name;
    }

    public function setAlias($alias)
    {
        $this->alias = $alias;
        return $this;
    }
    public function getAlias()
    {
        return $this->alias;
    }
    public function setOptions($options)
    {
        $this->options = $options;
        return $this;
    }
    public function getOptions()
    {
        return unserialize($this->options);
    }
    public function setSuccess($success)
    {
        $this->success = $success;
        return $this;
    }
    public function getSuccess()
    {
        return $this->success;
    }
    public function setFail($fail)
    {
        $this->fail = $fail;
        return $this;
    }
    public function getFail()
    {
        return $this->fail;
    }
    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }
    public function getStatus()
    {
        return $this->status;
    }
}
