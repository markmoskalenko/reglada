<?php

namespace Site\Bundle\BackendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DillerAutoTestDrive
 */
class DillerAutoTestDrive
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $dillerId;

    /**
     * @var integer
     */
    private $carId;

    /**
     * @var boolean
     */
    private $mt;

    /**
     * @var boolean
     */
    private $at;

    /**
     * @var boolean
     */
    private $rb;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dillerId
     *
     * @param integer $dillerId
     * @return DillerAutoTestDrive
     */
    public function setDillerId($dillerId)
    {
        $this->dillerId = $dillerId;

        return $this;
    }

    /**
     * Get dillerId
     *
     * @return integer 
     */
    public function getDillerId()
    {
        return $this->dillerId;
    }

    /**
     * Set carId
     *
     * @param integer $carId
     * @return DillerAutoTestDrive
     */
    public function setCarId($carId)
    {
        $this->carId = $carId;

        return $this;
    }

    /**
     * Get carId
     *
     * @return integer 
     */
    public function getCarId()
    {
        return $this->carId;
    }

    /**
     * Set mt
     *
     * @param boolean $mt
     * @return DillerAutoTestDrive
     */
    public function setMt($mt)
    {
        $this->mt = $mt;

        return $this;
    }

    /**
     * Get mt
     *
     * @return boolean 
     */
    public function getMt()
    {
        return $this->mt;
    }

    /**
     * Set at
     *
     * @param boolean $at
     * @return DillerAutoTestDrive
     */
    public function setAt($at)
    {
        $this->at = $at;

        return $this;
    }

    /**
     * Get at
     *
     * @return boolean 
     */
    public function getAt()
    {
        return $this->at;
    }

    /**
     * Set rb
     *
     * @param boolean $rb
     * @return DillerAutoTestDrive
     */
    public function setRb($rb)
    {
        $this->rb = $rb;

        return $this;
    }

    /**
     * Get rb
     *
     * @return boolean 
     */
    public function getRb()
    {
        return $this->rb;
    }
}
