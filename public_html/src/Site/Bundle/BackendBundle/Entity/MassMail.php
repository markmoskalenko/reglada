<?php

namespace Site\Bundle\BackendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

class MassMail
{
    /**
     * @var integer
     */
    private $id;
    private $email;
    private $status;

    public function getId()
    {
        return $this->id;
    }


    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }
    public function getEmail()
    {
        return $this->email;
    }


    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    public function getStatus()
    {
        return $this->status;
    }



}
