<?php

namespace Site\Bundle\BackendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * BaseAutoDocuments
 */
class BaseAutoDocuments
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $carId;

    /**
     * @var boolean
     */
    private $isCatalog;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $file;

    /**
     * @var string
     */
    private $filesize;

    /**
     * @var boolean
     */
    private $status;

    /**
     * @var \DateTime
     */
    private $dateUpload;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set carId
     *
     * @param integer $carId
     * @return BaseAutoDocumentsAssigned
     */
    public function setCarId($carId)
    {
        $this->carId = $carId;

        return $this;
    }

    /**
     * Get carId
     *
     * @return integer 
     */
    public function getCarId()
    {
        return $this->carId;
    }

    /**
     * Set isCatalog
     *
     * @param boolean $isCatalog
     * @return BaseAutoDocumentsAssigned
     */
    public function setIsCatalog($isCatalog)
    {
        $this->isCatalog = $isCatalog;

        return $this;
    }

    /**
     * Get isCatalog
     *
     * @return boolean 
     */
    public function getIsCatalog()
    {
        return $this->isCatalog;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return BaseAutoDocuments
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set file
     *
     * @param string $file
     * @return BaseAutoDocuments
     */
    public function setFile($file)
    {
        $this->file = $file;

        return $this;
    }

    /**
     * Get file
     *
     * @return string 
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * Set filesize
     *
     * @param string $filesize
     * @return BaseAutoDocuments
     */
    public function setFilesize($filesize)
    {
        $this->filesize = $filesize;

        return $this;
    }

    /**
     * Get filesize
     *
     * @return string 
     */
    public function getFilesize()
    {
        return $this->filesize;
    }

    /**
     * Set status
     *
     * @param boolean $status
     * @return BaseAutoDocuments
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return boolean 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set dateUpload
     *
     * @param \DateTime $dateUpload
     * @return BaseAutoDocuments
     */
    public function setDateUpload($dateUpload)
    {
        $this->dateUpload = $dateUpload;

        return $this;
    }

    /**
     * Get dateUpload
     *
     * @return \DateTime 
     */
    public function getDateUpload()
    {
        return $this->dateUpload;
    }
}
