<?php

namespace Site\Bundle\BackendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DillerCentre
 */
class DillerCentre
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $alias;

    /**
     * @var string
     */
    private $name;

    /**
     * @var integer
     */
    private $regionId;

    /**
     * @var integer
     */
    private $status;

    /**
     * @var string
     */
    private $users;

    /**
     * @var string
     */
    private $phone;

    /**
     * @var string
     */
    private $email;

    /**
     * @var string
     */
    private $coordinates;

    /**
     * @var string
     */
    private $seo;

    /**
     * @var string
     */
    private $information;

    /**
     * @var string
     */
    private $inn;

    /**
     * @var string
     */
    private $city;
    /**
     * @var string
     */
    private $contract;

    /**
     * @var string
     */
    private $addContract;

    /**
     * @var string
     */
    private $bossName;

    /**
     * @var string
     */
    private $bossCode;
    /**
     * @var string
     */
    private $bossPhone;

    /**
     * @var string
     */
    private $bossFax;
    /**
     * @var string
     */
    private $bossMail;
    /**
     * @var string
     */
    private $legalName;
    /**
     * @var string
     */
    private $legalAddress;
    /**
     * @var string
     */
    private $image;
    /**
     * @var string
     */
    private $worktime;
    /**
     * @var string
     */
    private $textBlocks;

     private $cars;

     private $services;

     private $pricelist;
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set alias
     *
     * @param string $alias
     * @return DillerCentre
     */
    public function setAlias($alias)
    {
        $this->alias = $alias;

        return $this;
    }

    /**
     * Get alias
     *
     * @return string 
     */
    public function getAlias()
    {
        return $this->alias;
    }

    public function setCars($cars)
    {
        $this->cars = $cars;

        return $this;
    }

    /**
     * Get alias
     *
     * @return string 
     */
    public function getCars()
    {
        return unserialize($this->cars);
    }

    /**
     * Set name
     *
     * @param string $name
     * @return DillerCentre
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set regionId
     *
     * @param integer $regionId
     * @return DillerCentre
     */
    public function setRegionId($regionId)
    {
        $this->regionId = $regionId;

        return $this;
    }

    /**
     * Get regionId
     *
     * @return integer 
     */
    public function getRegionId()
    {
        return $this->regionId;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return DillerCentre
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set users
     *
     * @param string $users
     * @return DillerCentre
     */
    public function setUsers($users)
    {
        $this->users = $users;

        return $this;
    }

    /**
     * Get users
     *
     * @return string 
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * Set phone
     *
     * @param string $phone
     * @return DillerCentre
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string 
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return DillerCentre
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set coordinates
     *
     * @param string $coordinates
     * @return DillerCentre
     */
    public function setCoordinates($coordinates)
    {
        $this->coordinates = $coordinates;

        return $this;
    }

    /**
     * Get coordinates
     *
     * @return string 
     */
    public function getCoordinates()
    {
        return $this->coordinates;
    }

    /**
     * Set seo
     *
     * @param string $seo
     * @return DillerCentre
     */
    public function setSeo($seo)
    {
        $this->seo = $seo;

        return $this;
    }

    /**
     * Get seo
     *
     * @return string 
     */
    public function getSeo()
    {
        return $this->seo;
    }

    /**
     * Set information
     *
     * @param string $information
     * @return DillerCentre
     */
    public function setInformation($information)
    {
        $this->information = $information;

        return $this;
    }

    /**
     * Get information
     *
     * @return string 
     */
    public function getInformation()
    {
        return $this->information;
    }

    public function setInn($inn)
    {
        $this->inn = $inn;

        return $this;
    }


    public function getInn()
    {
        return $this->inn;
    }

    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }


    public function getCity()
    {
        return $this->city;
    }

    public function setContract($contract)
    {
        $this->contract = $contract;

        return $this;
    }


    public function getContract()
    {
        return $this->contract;
    }

    public function setAddContract($addContract)
    {
        $this->addContract = $addContract;

        return $this;
    }


    public function getAddContract()
    {
        return $this->addContract;
    }

    public function setBossName($bossName)
    {
        $this->bossName = $bossName;

        return $this;
    }


    public function getBossName()
    {
        return $this->bossName;
    }

    public function setBossCode($bossCode)
    {
        $this->bossCode = $bossCode;

        return $this;
    }


    public function getBossCode()
    {
        return $this->bossCode;
    }

    public function setBossPhone($bossPhone)
    {
        $this->bossPhone = $bossPhone;

        return $this;
    }


    public function getBossPhone()
    {
        return $this->bossPhone;
    }

    public function setBossFax($bossFax)
    {
        $this->bossFax = $bossFax;

        return $this;
    }


    public function getBossFax()
    {
        return $this->bossFax;
    }

    public function setBossMail($bossMail)
    {
        $this->bossMail = $bossMail;

        return $this;
    }


    public function getBossMail()
    {
        return $this->bossMail;
    }

    public function setLegalName($legalName)
    {
        $this->legalName = $legalName;

        return $this;
    }


    public function getLegalName()
    {
        return $this->legalName;
    }

    public function setLegalAddress($legalAddress)
    {
        $this->legalAddress = $legalAddress;

        return $this;
    }


    public function getLegalAddress()
    {
        return $this->legalAddress;
    }

    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }


    public function getImage()
    {
        return $this->image;
    }

    public function setWorktime($worktime)
    {
        $this->worktime = $worktime;

        return $this;
    }


    public function getWorktime()
    {
        return $this->worktime;
    }

    public function setTextBlocks($textBlocks)
    {
        $this->textBlocks = $textBlocks;

        return $this;
    }


    public function getTextBlocks()
    {
        return $this->textBlocks;
    }

    public function setServices($services)
    {
        $this->services = $services;

        return $this;
    }


    public function getServices()
    {
        return $this->services;
    }

    public function setPricelist($pricelist)
    {
        $this->pricelist = $pricelist;

        return $this;
    }


    public function getPricelist()
    {
        return $this->pricelist;
    }
}
