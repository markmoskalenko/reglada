<?php

// src/Acme/DemoBundle/Twig/AcmeExtension.php
namespace Site\Bundle\BackendBundle\Twig;

class LadaExtension extends \Twig_Extension
{
    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('jsonDecodeImplode', array($this, 'jsonDecodeImplode')),
        );
    }

    public function jsonDecodeImplode($str, $s = ', ')
    {
        $str = unserialize($str);
        
        if(is_array($str))
            return implode($s, $str);
        else
            return $str;
    }
    
    public function getName()
    {
        return 'lada_extension';
    }
}