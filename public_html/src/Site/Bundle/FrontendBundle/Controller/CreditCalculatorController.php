<?php

namespace Site\Bundle\FrontendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\Event\GetResponseUserEvent;
use FOS\UserBundle\Event\UserEvent;
use Site\Bundle\BackendBundle\Entity\CreditRequests;
use FOS\UserBundle\Event\FilterUserResponseEvent;
use Symfony\Component\DependencyInjection\ContainerAware;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use FOS\UserBundle\Model\UserInterface;
use Symfony\Component\Security\Core\SecurityContextInterface;
use Site\Bundle\BackendBundle\Utils\Utils;
use Symfony\Component\HttpFoundation\Response;

class CreditCalculatorController extends Controller
{
    public function indexAction($id, $bid=false, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $regions = $em->getRepository('SiteBackendBundle:Regions')->findAll();
        $region = Utils::getRegion();

        $car = $em->getRepository('SiteBackendBundle:BaseAuto')->find($id);

        $data = array();

        $qb = $em->createQueryBuilder();
        $qb->select('c.id as qid','b.name as value','c.price as price','b.file','b.id as bid')
                ->from('SiteBackendBundle:BaseAutoBodyTypes','b')
                ->leftJoin('SiteBackendBundle:Cars', 'c', 'WITH', "c.bodyId = b.id")
                ->where("b.carId = '$id' and c.price is not null")
                ->orderBy('b.pos','ASC');
        $types = $qb->getQuery()->getArrayResult();

        $car_bodies = array();
        foreach( $types as $item ){
            $car_bodies[$item['bid']]=$item;
        }
        $car_bodies = array_values($car_bodies);

        $temp = array();

        $nextBId = isset($car_bodies[1]) && $bid==false ? $car_bodies[1]['bid'] : $car_bodies[0]['bid'];
        $prevBId = end($car_bodies)['bid'];

        if( $bid != null ){
            foreach ($car_bodies as $key => $value) {
                if( $value['bid'] == $bid ){

                    if( isset($car_bodies[$key-1]) ){
                        $prevBId = $car_bodies[$key-1]['bid'];
                    }

                    if( isset($car_bodies[$key+1]) ){
                        $nextBId = $car_bodies[$key+1]['bid'];
                    }

                    $temp[] = $value;
                    unset($car_bodies[$key]);

                }
            }
        }

        $types = array_merge($temp, $types);


        foreach ($types as $key => $value) {
            if( !isset($data[$value['value']]) ){
                $data[$value['value']] = array();
                $data[$value['value']]['minPrice'] = 0;
                $data[$value['value']]['minPriceEngine'] = '';
                $data[$value['value']]['minPriceType'] = '';
                $data[$value['value']]['bid'] = $value['bid'];
                $data[$value['value']]['engines'] = array();
            }
            $data[$value['value']]['image'] = $value['file'];
            $data[$value['value']][$value['qid']]['price'] = $value['price'];
            $qid = $value['qid'];

            $qb = $em->createQueryBuilder();
            $qb->select('sv.id','iv.id','iv.name as vvalue')
                ->from('SiteBackendBundle:CarsInfo','sv')
                ->leftJoin('SiteBackendBundle:InfoValue', 'iv', 'WITH', "iv.id = sv.valueId")
                ->where("sv.carModelId = '$qid' and iv.infoId = '11'");
            $vinfo = $qb->getQuery()->getArrayResult();
            if( count($vinfo) == 0 ){ continue;}
            $qb = $em->createQueryBuilder();
            $qb->select('sv.id','iv.id','iv.name as cvalue')
                ->from('SiteBackendBundle:CarsInfo','sv')
                ->leftJoin('SiteBackendBundle:InfoValue', 'iv', 'WITH', "iv.id = sv.valueId")
                ->where("sv.carModelId = '$qid' and iv.infoId = '29'");
            $cinfo = $qb->getQuery()->getArrayResult();
            if( count($cinfo) == 0 ){ continue;}
            $qb = $em->createQueryBuilder();
            $qb->select('sv.id','iv.id','iv.name as uvalue')
                ->from('SiteBackendBundle:CarsInfo','sv')
                ->leftJoin('SiteBackendBundle:InfoValue', 'iv', 'WITH', "iv.id = sv.valueId")
                ->where("sv.carModelId = '$qid' and iv.infoId = '31'");
            $uinfo = $qb->getQuery()->getArrayResult();
            if( count($uinfo) == 0 ){ continue;}          
            $qb = $em->createQueryBuilder();
            $qb->select('sv.id','iv.id','iv.name as evalue')
                ->from('SiteBackendBundle:CarsInfo','sv')
                ->leftJoin('SiteBackendBundle:InfoValue', 'iv', 'WITH', "iv.id = sv.valueId")
                ->where("sv.carModelId = '$qid' and iv.infoId = '23'");
            $einfo = $qb->getQuery()->getArrayResult();
            if( count($uinfo) == 0 ){ continue;}
            $qb = $em->createQueryBuilder();
            $qb->select('sv.id','iv.id','iv.name as pvalue')
                ->from('SiteBackendBundle:CarsInfo','sv')
                ->leftJoin('SiteBackendBundle:InfoValue', 'iv', 'WITH', "iv.id = sv.valueId")
                ->where("sv.carModelId = '$qid' and iv.infoId = '23'");
            $pinfo = $qb->getQuery()->getArrayResult();
            if( count($pinfo) == 0 ){ continue;}
            if( isset($uinfo[0]['uvalue']))
                $data[$value['value']][$value['qid']]['cartype']['value'] = $uinfo[0]['uvalue'];

			/*if (!isset($data[$value['value']]['engines'][$cinfo[0]['cvalue'].' / '.$einfo[0]['evalue'].' л/100км / '.$vinfo[0]['vvalue']]['types'][$uinfo[0]['uvalue']]) ||
                $data[$value['value']]['engines'][$cinfo[0]['cvalue'].' / '.$einfo[0]['evalue'].' л/100км / '.$vinfo[0]['vvalue']]['types'][$uinfo[0]['uvalue']] > $value['price']){
                $data[$value['value']]['engines'][$cinfo[0]['cvalue'].' / '.$einfo[0]['evalue'].' л/100км / '.$vinfo[0]['vvalue']]['types'][$uinfo[0]['uvalue']] = $value['price'];
            }

            if( !isset($data[$value['value']]['engines'][  $cinfo[0]['cvalue'].' / '.$einfo[0]['evalue'].' л/100км / '.$vinfo[0]['vvalue']  ]['minprice']) || 
                $data[$value['value']]['engines'][  $cinfo[0]['cvalue'].' / '.$einfo[0]['evalue'].' л/100км / '.$vinfo[0]['vvalue']  ]['minprice'] > $value['price'] ){
                $data[$value['value']]['engines'][  $cinfo[0]['cvalue'].' / '.$einfo[0]['evalue'].' л/100км / '.$vinfo[0]['vvalue']  ]['minprice'] = $value['price'];
            }*/
            //Переделали, теперь нужен всего один параметр из базы
			if (!isset($data[$value['value']]['engines'][$cinfo[0]['cvalue']]['types'][$uinfo[0]['uvalue']])
				|| $data[$value['value']]['engines'][$cinfo[0]['cvalue']]['types'][$uinfo[0]['uvalue']] > $value['price'])
                $data[$value['value']]['engines'][$cinfo[0]['cvalue']]['types'][$uinfo[0]['uvalue']] = $value['price'];

            if( !isset($data[$value['value']]['engines'][$cinfo[0]['cvalue']]['minprice'])
				|| $data[$value['value']]['engines'][$cinfo[0]['cvalue']]['minprice'] > $value['price'])
                $data[$value['value']]['engines'][$cinfo[0]['cvalue']]['minprice'] = $value['price'];
           
            $discount = $em->getRepository('SiteBackendBundle:Discount')->findBy(array('status'=>1));
            $data[$value['value']]['discount'] = false;
            foreach ($discount as $d => $share) {
                $auto = $share->getAuto();
                $discountRegions = $share->getRegions();
                if (isset( $auto[$id][$value['value']]) && in_array($region->getId(), $discountRegions)) {
                    if ($data[$value['value']]['discount'] == false)
                        $data[$value['value']]['discount'] = array();
                    $data[$value['value']]['discount'][$d] = array();
                    $data[$value['value']]['discount'][$d]['name'] = $share->getName();
                    $data[$value['value']]['discount'][$d]['period'] = $share->getPeriod();
                    $data[$value['value']]['discount'][$d]['firstpay'] = $share->getFirstpay();
                    $data[$value['value']]['discount'][$d]['summ'] = $share->getSumm();
                    $data[$value['value']]['discount'][$d]['persent'] = $share->getPersent();
                }
            }
            //$data[$value['value']][$value['qid']]['engine']['value'] = $cinfo[0]['cvalue'].' / '.$einfo[0]['evalue'].' л/100км / '.$vinfo[0]['vvalue'];
            $data[$value['value']][$value['qid']]['engine']['value'] = $cinfo[0]['cvalue'];
            if( !isset($data[$value['value']]['minPrice']) || 
                $data[$value['value']]['minPrice'] > $value['price'] ||
                $data[$value['value']]['minPrice'] == 0 )
            {
                $data[$value['value']]['minPrice'] = $value['price'];
                //$data[$value['value']]['minPriceEngine'] = $cinfo[0]['cvalue'].' / '.$einfo[0]['evalue'].' л/100км / '.$vinfo[0]['vvalue'];
                $data[$value['value']]['minPriceEngine'] = $cinfo[0]['cvalue'];
                $data[$value['value']]['minPriceType'] = $uinfo[0]['uvalue'];
            }

        }



        $qb = $em->createQueryBuilder();
        $qb->select('c')
                ->from('SiteBackendBundle:CreditOffers','c')
                ->where("c.active = 1");
        $offers = $qb->getQuery()->getArrayResult();

        $classic = false;
        $express = false;
        $carsinfo = $em->getRepository('SiteBackendBundle:Cars')->getCarsInfo($id);
        foreach ($offers as $key => $value) {
            $offers[$key]['tax'] = unserialize($value['tax']);
            $offers[$key]['auto'] = unserialize($value['auto']);
            if ( $value['typeCredit'] == 'Классическая' ){
                $classic = true;
            }
            if ( $value['typeCredit'] == 'Экспресс' ){
                $express = true;
            }
        }
        $activeOffer = array();
        $activeTaxes = array();
        $current = array('id'=>'','tax'=>-1,'periodMin'=>'','periodMax'=>'','firstpay'=>'');

        foreach ($offers as $key => $value) {
            unset($periodMax);
            unset($periodMin);
            unset($firstPayMax);
            unset($firstPayMin);
            $tax = $value['tax'];
            foreach ($tax as $k => $v) {
                $activeTaxes[$key]['counttaxes'] = count($v['values']);
                foreach ($v['values'] as $i => $x) {
                    if( !isset($activeTaxes[$key]['tax']) || $activeTaxes[$key]['tax'] > $x['tvalue']){
                        $activeTaxes[$key]['tax'] = $x['tvalue'];
                        $activeTaxes[$key]['period'] = $x['perMin'];
                        $taxfix = $v['taxfix'];
                        $activeTaxes[$key]['firstpay'] = $v['firstpay']['min'];
                        $activeTaxes[$key]['taxfiction'] = $v['taxfiction']['min'];
                        if( $v['taxfiction']['min'] != $v['taxfiction']['max'] && isset($v['taxfiction']['max']) && $v['taxfiction']['max'] != '' ){
                            $activeTaxes[$key]['taxfiction'] .= ' - '.$v['taxfiction']['max'];
                        }
                    }
                    if( !isset($firstPayMax) || $firstPayMax < $v['firstpay']['max'] )
                        $firstPayMax = $v['firstpay']['max'];
                    if( !isset($firstPayMin) || $firstPayMin > $v['firstpay']['min'] )
                        $firstPayMin = $v['firstpay']['min'];
                    if( !isset($periodMin) || $periodMin > $x['perMin'] ){
                        $periodMin = $x['perMin'];
                    }
                    if( !isset($periodMax) || $periodMax < $x['perMax'] ){
                        $periodMax = $x['perMax'];
                    }

                }


            }

            $activeTaxes[$key]['firstpayMin'] = $firstPayMin;
            $activeTaxes[$key]['firstpayMax'] = $firstPayMax;

            $activeTaxes[$key]['periodMin'] = $periodMin;
            $activeTaxes[$key]['periodMax'] = $periodMax;
            $activeTaxes[$key]['fixed'] = $taxfix;
            if( $taxfix == 1 ){ $activeTaxes[$key]['step'] = ($periodMax - $periodMin)/($activeTaxes[$key]['counttaxes']-1);}
            else{ $activeTaxes[$key]['step'] = 1; }

            if ( $current['tax'] > $activeTaxes[$key]['tax'] || $current['tax'] < 0 ){

                $current['tax'] = $activeTaxes[$key]['tax'];
                $current['firstpay'] = $activeTaxes[$key]['firstpay'];
                $current['firstpayMin'] = $firstPayMin;
                $current['firstpayMax'] = $firstPayMax;
                $current['period'] = $activeTaxes[$key]['period'];
                $current['periodMin'] = $periodMin;
                $current['periodMax'] = $periodMax;
                $current['id'] = $value['id'];
                $current['fixed'] = $taxfix;
                if( $taxfix == 1 ){
                    $current['step'] = ($periodMax - $periodMin)/($activeTaxes[$key]['counttaxes']-1);
                }
                else{
                    $current['step'] = 1;
                }
                
            }
        }
        $qb = $em->createQueryBuilder();
        $qb->select('dc')
                ->from('SiteBackendBundle:Discount','dc')
                ->where("dc.status = 1");
        $discountTables = $qb->getQuery()->getResult();
        return $this->render('SiteFrontendBundle:CreditCalculator:index.html.twig', array(
                'regions' => $regions,
                'region' => $region,
                'data' => $data,
                'alias' => $region->getAlias(),
                'car' => $car,
                // 'carsinfo' => $carsinfo['info_data'],
                // 'modelsinfo' => $carsinfo['model_info'],
                'offers' => $offers,
                'activeOffer' => $activeOffer,
                'classic' => $classic,
                'express' => $express,
                'activeTaxes' => $activeTaxes,
                'current' => $current,
                'request' => true,
                'discountTables' => $discountTables,
                'nextBId'=>$nextBId,
                'prevBId'=>$prevBId,
                'acar'=> $id,

        ));
    }

    public function taxAction(Request $request)
    {
        //return 1;
        $answer = array();
        // return new response(json_encode(array()));
        $em = $this->getDoctrine()->getManager();
        $post = $request->request->all();
        $searchFirstpay = $post['firstpay'];
        $searchPeriod = $post['period'];
        $qb = $em->createQueryBuilder();
        $qb->select('c')
                ->from('SiteBackendBundle:CreditOffers','c')
                ->where("c.id = ".$post['id']);
        $offers = $qb->getQuery()->getArrayResult();
        $offer = $offers[0];
        $offer['tax'] = unserialize($offer['tax']);
        foreach ($offer['tax'] as $key => $value) {
            if( $value['firstpay']['min'] <= $searchFirstpay && $value['firstpay']['max'] >= $searchFirstpay ){
                //$answer = $value['values']['tvalue'];
                $per = array();
                foreach ($value['values'] as $k => $v) {
                    if( !isset($per['min']) || $per['min'] > $v['perMin']){ $per['min'] = $v['perMin']; }
                    if( !isset($per['max']) || $per['max'] > $v['perMax']){ $per['max'] = $v['perMax']; $saveTax = $v['tvalue']; 
                        if( $value['taxfiction']['max'] == '' || !isset($value['taxfiction']['max']) || ( $value['taxfiction']['min'] == $value['taxfiction']['max']))
                            $saveTaxfiction = $value['taxfiction']['min'];
                        else
                            $saveTaxfiction = $value['taxfiction']['min'].'-'.$value['taxfiction']['max'];
                    }
                    if( $v['perMin'] <= $searchPeriod && $v['perMax'] >= $searchPeriod ){
                        $answer['value'] = $v['tvalue'];
                        $answer['periodset'] = $v['perMax'];
                        if( $value['taxfiction']['max'] == '' || !isset($value['taxfiction']['max']) || ( $value['taxfiction']['min'] == $value['taxfiction']['max']))
                            $answer['taxfiction'] = $value['taxfiction']['min'];
                        else
                            $answer['taxfiction'] = $value['taxfiction']['min'].'-'.$value['taxfiction']['max'];
                        
                    }
                    $answer['step'] = 1;
                    if( $value['taxfix'] == 1 ){
                        $answer['step'] = ($per['max'] - $per['min'])/(count($value['values'])-1);
                    }
                }
                if( !isset($answer['periodset']) ){ 
                    foreach ($value['values'] as $k => $v) {
                        if( $v['perMin'] <= $searchPeriod && $v['perMax'] >= $searchPeriod ){
                            $answer['value'] = $v['tvalue'];
                            $answer['periodset'] = $v['perMax'];
                            $answer['firstpay'] = $value['firstpay'];
                            if( $value['taxfiction']['max'] == '' || !isset($value['taxfiction']['max']) || ( $value['taxfiction']['min'] == $value['taxfiction']['max']))
                                $answer['taxfiction'] = $value['taxfiction']['min'];
                            else
                                $answer['taxfiction'] = $value['taxfiction']['min'].'-'.$value['taxfiction']['max'];
                            $answer['step'] = 1;
                            if( $value['taxfix'] == 1 ){
                                $answer['step'] = ($per['max'] - $per['min'])/(count($value['values'])-1);
                            }

                        }
                    }
                }
                $answer['periods'] = array('min'=>$per['min'],'max'=>$per['max']);
            }
        }
        return new response(json_encode($answer));

    }

    public function taxperiodAction(Request $request)
    {
        $answer = array();
        $em = $this->getDoctrine()->getManager();
        $post = $request->request->all();
        $searchFirstpay = $post['firstpay'];
        $searchPeriod = $post['period'];
        $qb = $em->createQueryBuilder();
        $qb->select('c')
                ->from('SiteBackendBundle:CreditOffers','c')
                ->where("c.id = ".$post['id']);
        $offers = $qb->getQuery()->getArrayResult();
        $offer = $offers[0];
        $offer['tax'] = unserialize($offer['tax']);
        foreach ($offer['tax'] as $key => $value) {
            if( $value['firstpay']['min'] <= $searchFirstpay && $value['firstpay']['max'] >= $searchFirstpay ){
                $per = array();
                foreach ($value['values'] as $k => $v) {
                    if( !isset($per['min']) || $per['min'] > $v['perMin']){ $per['min'] = $v['perMin']; }
                    if( !isset($per['max']) || $per['max'] > $v['perMax']){ $per['max'] = $v['perMax']; $saveTax = $v['tvalue']; 
                        if( $value['taxfiction']['max'] == '' || !isset($value['taxfiction']['max']) || ( $value['taxfiction']['min'] == $value['taxfiction']['max']))
                            $saveTaxfiction = $value['taxfiction']['min'];
                        else
                            $saveTaxfiction = $value['taxfiction']['min'].'-'.$value['taxfiction']['max'];
                    }
                    if( $v['perMin'] <= $searchPeriod && $v['perMax'] >= $searchPeriod ){
                        $answer['value'] = $v['tvalue'];
                        $answer['firstpay'] = $value['firstpay'];
                        $answer['periodset'] = $v['perMax'];
                        if( $value['taxfiction']['max'] == '' || !isset($value['taxfiction']['max']) || ( $value['taxfiction']['min'] == $value['taxfiction']['max']))
                            $answer['taxfiction'] = $value['taxfiction']['min'];
                        else
                            $answer['taxfiction'] = $value['taxfiction']['min'].'-'.$value['taxfiction']['max'];

                        $answer['step'] = 1;
                        if( $value['taxfix'] == 1 ){
                            $answer['step'] = ($per['max'] - $per['min'])/(count($value['values'])-1);
                        }
                        
                    }
                }
                if( !isset($answer['periodset']) ){ 
                    foreach ($value['values'] as $k => $v) {
                        if( $v['perMin'] <= $searchPeriod && $v['perMax'] >= $searchPeriod ){
                            $answer['value'] = $v['tvalue'];
                            $answer['periodset'] = $v['perMax'];
                            $answer['firstpay'] = $value['firstpay'];
                            if( $value['taxfiction']['max'] == '' || !isset($value['taxfiction']['max']) || ( $value['taxfiction']['min'] == $value['taxfiction']['max']))
                                $answer['taxfiction'] = $value['taxfiction']['min'];
                            else
                                $answer['taxfiction'] = $value['taxfiction']['min'].'-'.$value['taxfiction']['max'];
                            $answer['step'] = 1;
                            if( $value['taxfix'] == 1 ){
                                $answer['step'] = ($per['max'] - $per['min'])/(count($value['values'])-1);
                            }
                            
                        }
                    }
                }
                $answer['periods'] = array('min'=>$per['min'],'max'=>$per['max']);
            }
        }
        return new response(json_encode($answer));

    }
}