<?php

namespace Site\Bundle\FrontendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\Event\GetResponseUserEvent;
use FOS\UserBundle\Event\UserEvent;
use FOS\UserBundle\Event\FilterUserResponseEvent;
use Symfony\Component\DependencyInjection\ContainerAware;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use FOS\UserBundle\Model\UserInterface;
use Symfony\Component\Security\Core\SecurityContextInterface;
use Site\Bundle\BackendBundle\Utils\Utils;
use Symfony\Component\HttpFoundation\Response;
class DillersController extends Controller
{
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $regions = $em->getRepository('SiteBackendBundle:Regions')->findAll();
        $region = Utils::getRegion();
        $offices = $em->getRepository('SiteBackendBundle:DillerCentre')->getDcsFront($region->getId());
        return $this->render('SiteFrontendBundle:Dillers:dealers-page.html.twig', array(
                'regions' => $regions,
                'region' => $region,
                'alias' => $region->getAlias(),
                'pin_menu' => true,
                'offices' => $offices,
                'map' => false,
            ));    
    }

    public function ajaxNearestAction(Request $request){
        $post = $request->request->all();
        $data = array();

        $lt = $data['latitude'] = $post['latitude'];
        $ln = $data['longtitude'] = $post['longtitude'];

        $region = Utils::getRegion();
        $em = $this->getDoctrine()->getManager();
        $offices = $em->getRepository('SiteBackendBundle:DillerCentre')->findBy(array('status'=>1));
        
        $array = array();
        foreach ($offices as $key => $value) {
            $crs = $value->getCoordinates();
            $crs = explode('::', $crs);
            $array[$value->getId()] = array();
            $latitudeDiff = $lt - $crs[0];
            $longitudeDiff = $ln - $crs[1];
            $latitudeDiff < 0 ? $latitudeDiff *= -1 : $latitudeDiff = $latitudeDiff;
            $longitudeDiff < 0 ? $longitudeDiff *= -1 : $longitudeDiff = $longitudeDiff;
            $summ = $latitudeDiff + $longitudeDiff;
            if( !isset($minDiff) || $minDiff > $summ ){
                $minDiff = $summ;
                $thisregion = $value->getRegionId();
                $data['minKey'] = $value->getId();
                $data['name'] = $value->getName();
                $data['dcId'] = $value->getId();

                $data['latitudeWin'] = $latitudeDiff;
                $data['longtitudeWin'] = $longitudeDiff;
               // return new Response(json_encode($send));
            }
        }
        $data['thisRegion'] = true;
        if( $thisregion != $region->getId() ){
            $region = $em->getRepository('SiteBackendBundle:Regions')->find($thisregion);
            $data['thisRegion'] = false;
            $data['regionName'] = $region->getName();
            $data['regionId'] = $region->getId();
            $data['regionAlias'] = $region->getAlias();
            $data['regionFor'] = $region->getFor();
        }
        else{
            $data['thisRegion'] = true;
            
        }
        return new Response(json_encode($data));
    }

    public function viewAction($alias, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $regions = $em->getRepository('SiteBackendBundle:Regions')->findAll();
        $region = Utils::getRegion();
        $offices = $em->getRepository('SiteBackendBundle:DillerCentre')->getDcsFront($region->getId());
        $ifexists = $em->getRepository('SiteBackendBundle:DillerCentre')->findBy(array('regionId'=>$region->getId(),'alias'=>$alias));
        if(count($ifexists) == 0)
            return $this->redirect($this->generateUrl('front_dillers'));
            
            
        $dc = $em->getRepository('SiteBackendBundle:DillerCentre')->getDcsOne($ifexists[0]->getId());
        
        if(strlen($dc['textBlocks']) > 2)
            $dc['textBlocks'] = unserialize($dc['textBlocks']);
        else
            $dc['textBlocks'] = false;
        
        $cars = $em->getRepository('SiteBackendBundle:BaseAuto')->findAll();
        $shares = $em->getRepository('SiteBackendBundle:Shares')->findBy(array('dc'=>$dc['id']));
        $basecars = array();
        foreach ($cars as $key => $value) {
            $basecars[$value->getId()] = $value->getName();
        }
        $details = $minvalues = array();

        $params = false;
        $eq = $request->query->get('eq');
        $type = $request->query->get('type');
        $color = $request->query->get('color');
        $cid = $request->query->get('cid');
        $bid = $request->query->get('bid');
        if( isset($eq) && isset($type) && isset($color) ){
            $params = array('eq'=>$eq,'type'=>$type,'color'=>$color,'bid'=>$bid,'cid'=>$cid);
        }
        
        return $this->render('SiteFrontendBundle:Dillers:dealer-page.html.twig', array(
                'params' => $params,
                'regions' => $regions,
                'region' => $region,
                'alias' => $region->getAlias(),
                'pin_menu' => true,
                'offices' => $offices,
                'dc' => $dc,
                'cars' => $basecars,
                'shares' => $shares,
                'dcid' => $ifexists[0]->getId(),
            ));    
    }


    public function ajaxGetCdcAction(Request $request){
        $em = $this->getDoctrine()->getManager();
        $post = $request->request->all();
        $carId = $post['carId'];
        $type = $post['type'];
        $dcid = $post['dcid'];

        $data = array();
        $data['activecar'] = $post['carId'].'0'.$post['type'];
        $data['activetype'] = false;
        $data['activecolor'] = false;
        $data['activeeq'] = false;
        if( isset($post['cartype']) )
            $data['activetype'] = $post['cartype'];
        if( isset($post['color']) )
            $data['activecolor'] = $post['color'];
        if( isset($post['eq']) )
            $data['activeeq'] = $post['eq'];
        $cars = $em->getRepository('SiteBackendBundle:BaseAuto')->findAll();
        $basecars = array();
        foreach ($cars as $key => $value) {
            $basecars[$value->getId()] = $value->getName();
        }
        $data['cdc'] = Utils::getComplectationsForDc($carId, $type);
        $data['cars'] = $basecars;
        $data['dc'] = $em->getRepository('SiteBackendBundle:DillerCentre')->getDcsOne($dcid);

        /*$colors = $em->getRepository('SiteBackendBundle:Colors')->findAll();
        foreach ($colors as $key => $value) {
            $auto = $value->getAuto();
            if( isset($auto[$post['carId']][$post['type']])){
                if( $value->getStatus() == 1 )
                    $data['colors'][$key] = $value;
            }
        }
        */
        $qb = $em->createQueryBuilder();
        $qb->select('c.id, c.name, c.code, dcc.bodyId')
            ->from('SiteBackendBundle:DillerCentreColors','dcc')
            ->leftJoin('SiteBackendBundle:Colors',   'c', 'WITH','dcc.colorId = c.id')
            ->leftJoin('SiteBackendBundle:BaseAutoBodyTypes',   'babt', 'WITH','dcc.bodyId = babt.id')
            ->leftJoin('SiteBackendBundle:Cars',   'cr', 'WITH','cr.bodyId = babt.id')
            ->where('babt.id = '.$carId.' AND c.status = 1 AND dcc.dillerId = '.$dcid);
        $colors_data = $qb->getQuery()->getResult();
        print_r($data);
        print_r($colors_data);
        print_r($dcid);
        $colors = array();
        foreach ($colors_data as $key => $value) {
            $colors[$value['id']] = array('name' => $value['name'], 'code' => $value['code']);
        }
        $data['colors'] = $colors;
        
        $template = $this->renderView('SiteFrontendBundle:Cars:request-car.html.twig', $data); 
        return new Response($template, 200);
    }
}
