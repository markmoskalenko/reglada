<?php

namespace Site\Bundle\FrontendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\Event\GetResponseUserEvent;
use FOS\UserBundle\Event\UserEvent;
use FOS\UserBundle\Event\FilterUserResponseEvent;
use Symfony\Component\DependencyInjection\ContainerAware;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use FOS\UserBundle\Model\UserInterface;
use Symfony\Component\Security\Core\SecurityContextInterface;
use Site\Bundle\BackendBundle\Utils\Utils;

class OwnersController extends Controller
{
    public function indexAction($menu, $sq = false, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $regions = $em->getRepository('SiteBackendBundle:Regions')->findAll();
        $region = Utils::getRegion();
        $content = '';
        $senddata = array();
        $func = 'get'.ucfirst($menu);
        if( method_exists($this, $func) )
            $content = $this->{$func}($senddata);

        $switchdc = $request->query->get('dc');
        if( isset( $switchdc ) )
            $senddata['switchdc'] = $switchdc;
        $senddata['regions'] = $regions;
        $senddata['region'] = $region;
        $senddata['main_menu_owners'] = true;
        $senddata['abortcars'] = true;
        $senddata['active'] = $menu;
        $senddata['alias'] = $region->getAlias();
        $senddata['content'] = $content;
        $senddata['show_question'] = $sq ? true : false;
        return $this->render('SiteFrontendBundle:Owners:owners.html.twig', $senddata);
    }

    private function getInsurance(&$senddata){
        $senddata['request'] = true;
        return $this->renderView('SiteFrontendBundle:Owners:inner-insurance.html.twig', array());
    }

    private function getService(&$senddata){
        $senddata['request'] = true;
        return $this->renderView('SiteFrontendBundle:Owners:inner-service.html.twig', array());
    }

    private function getTradein(&$senddata){
        return $this->renderView('SiteFrontendBundle:Owners:inner-tradein.html.twig', array());
    }

    private function getCatalog(&$senddata){
        return $this->renderView('SiteFrontendBundle:Owners:inner-catalog.html.twig', array());
    }

    private function getTradeinquery(&$senddata){
        $senddata['request'] = true;
        return $this->renderView('SiteFrontendBundle:Owners:inner-tradeinquery.html.twig', array());
    }
}
