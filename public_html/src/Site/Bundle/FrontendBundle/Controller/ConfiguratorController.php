<?php

namespace Site\Bundle\FrontendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\Event\GetResponseUserEvent;
use FOS\UserBundle\Event\UserEvent;
use FOS\UserBundle\Event\FilterUserResponseEvent;
use Symfony\Component\DependencyInjection\ContainerAware;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use FOS\UserBundle\Model\UserInterface;
use Symfony\Component\Security\Core\SecurityContextInterface;
use Site\Bundle\BackendBundle\Utils\Utils;
use Symfony\Component\HttpFoundation\Response;

class ConfiguratorController extends Controller
{
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $regions = $em->getRepository('SiteBackendBundle:Regions')->findAll();
        $region = Utils::getRegion();
        $cars = Utils::minimalCarsPricesMain();
        
        
        /*$data['minvalues'] = array();
        $data['details'] = array();
        $data['equipments'] = Utils::getComplectations($data['details'], $data['minvalues'], 1, 'седан', true);
        
        print_r($data);*/
        
        return $this->render('SiteFrontendBundle:Configurator:index.html.twig', array(
                'cars' => $cars,
                'regions' => $regions,
                'region' => $region,
                'main_menu_buy' => true,
                //'abortcars'=> true,
                'active' => 'config',
                'alias' => $region->getAlias(),
                'car_menu' => true,
            ));  
    }

    public function ajaxDetailsAction(Request $request){
        $data = array();
        $post = $request->request->all();
        $em = $this->getDoctrine()->getManager();
        $cars = Utils::minimalCarsPrices();

        if (isset($post['bodyId']))
            $post['type'] = $em->getRepository('SiteBackendBundle:BaseAutoBodyTypes')->find($post['bodyId'])->getName();
        $data['minvalues'] = array();
        $data['details'] = array();
        $data['equipments'] = Utils::getComplectations($post['bodyId']);

        $data['colors'] = array();
        $colors = $em->getRepository('SiteBackendBundle:Colors')->findAll();
        foreach ($colors as $key => $value)
        {
            $auto = $value->getAuto();
            if (isset($auto[$post['carId']][$post['type']]) && $value->getStatus() == 1)
				$data['colors'][$key] = $value;
        }
        $data['code'] = $post['carId'].'0'.$post['bodyId'];

		//Параметры комплектации с минимальной ценой
        $mincost_comp = $data['equipments']['complects'][$data['equipments']['cheapest']]['chars'];
        $mincost_array = array();
        foreach ($mincost_comp as $charact)
			$mincost_array[$charact['name']] = $charact['value'];
		$data['minvalues'] = $mincost_array;

		//Отображаемая комплектация
		$data['current_complect'] = $data['equipments']['cheapest'];

		//TMP TODO:Убрать из шаблона и здесь
        $data['minvalues']['KPP'] = $data['equipments']['complects'][$data['current_complect']]['chars'][11]['value'];		//Выбранная коробка передач
        $data['minvalues']['value'] = number_format($data['equipments']['complects'][$data['current_complect']]['price'], 0, '.', ' ');			//Стоимость комплектации
        $data['minvalues']['engine'] = $data['equipments']['complects'][$data['current_complect']]['chars'][29]['value'];	//Двигатель
        $data['minvalues']['complect'] = $data['equipments']['complects'][$data['current_complect']]['model'].'-'.$data['equipments']['complects'][$data['current_complect']]['name'];

		//Связи идентификаторов групп с переменными, передаваемыми в шаблон
		$groupIDs = array(
			1 => 'korob',	//Коробка передач
			2 => 'dvig',	//Двигатель
			0 => 'dvig',	//Приходится так делать, потому что нужная характеристика двигателя в 0-й группе
			6 => 'safe',	//Безопасность
			8 => 'komfort',	//Комфорт
			9 => 'ext',		//Экстерьер
			7 => 'int'		//Интерьер
        );

		foreach ($groupIDs as $id => $var)
			$data[$var] = array();		//Заводим массивы под все характеристики

		$complects = $data['equipments']['complects'];
		for ($p = 0; $p < sizeof($complects); $p++)		//Проходимся ко комплектациям
		{
			//Для быстрого поиска значений характеристик привяжем их к именам
			$data['equipments']['complects'][$p]['invert_chars'] = array();
				
			$CharsList = $complects[$p]['chars'];		//Массив с характеристиками
			foreach ($CharsList as $c => $cmpl)
				switch ($cmpl['group'])
				{
					case 1: case 2: case 6: case 7: case 8: case 9:
					case 0:		//Специально для двигателя
						if ($cmpl['group'] == 0 && $c != 29) break;		//Из 0-й группы нужен только двигатель

						
						if (!$cmpl['onconfigurator'])			//Пропускаем те, у кого не стоит флаг отображения в конфигураторе
							break;

						$data['equipments']['complects'][$p]['invert_chars'][$cmpl['group']][$cmpl['name']] = $cmpl['value'];

						$inarray = $data[$groupIDs[$cmpl['group']]];		//Служебная переменная с уже встреченными характеристиками

						//Поищем в массиве, не заведена ли уже такая характеристика
						$found = 0;
						foreach ($inarray as $index => $value)
							if ($value['name'] == $cmpl['name'])
							{
								$found = 1;
								if (!in_array($cmpl['value'], $value['values']))		//Нам нужны только уникальные значения
									$data[$groupIDs[$cmpl['group']]][$index]['values'][] = $cmpl['value'];
							}

						//Первое добавление характеристики
						if (!$found)
						{
							$procdata = array(
								'name' => $cmpl['name'],
								'group' => $cmpl['group'],
								'description' => $cmpl['description'],
								'image' => $cmpl['image'],
								'values' => array($cmpl['value']),
								'textname' => $groupIDs[$cmpl['group']]
							);
							if ($cmpl['intercept_id'] != 0)
								$procdata['intercept'] = $cmpl['intercept_id'];
							$data[$groupIDs[$cmpl['group']]][] = $procdata;
						}

						//Добавим ещё несколько массивов для быстрого поиска характеристик типа да/нет
						$data['equipments']['complects'][$p]['invert_chars'][$cmpl['name']] = $cmpl['value'];
					break;
				}
		}


		$data['jseq'] = json_encode($data['equipments']);

        $template = $this->renderView('SiteFrontendBundle:Configurator:conf-block.html.twig', $data); 
        return new Response($template, 200);
    }

}