<?php

namespace Site\Bundle\FrontendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\Event\GetResponseUserEvent;
use FOS\UserBundle\Event\UserEvent;
use FOS\UserBundle\Event\FilterUserResponseEvent;
use Symfony\Component\DependencyInjection\ContainerAware;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use FOS\UserBundle\Model\UserInterface;
use Symfony\Component\Security\Core\SecurityContextInterface;
use Site\Bundle\BackendBundle\Utils\Utils;

class NewsController extends Controller
{
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $regions = $em->getRepository('SiteBackendBundle:Regions')->findAll();
        $region = Utils::getRegion();
        $news = $em->getRepository('SiteBackendBundle:News')->getNews($region->getId(), true);
        return $this->render('SiteFrontendBundle:News:news.html.twig', array(
                'regions' => $regions,
                'region' => $region,
                'main_menu_news' => true,
                'alias' => $region->getAlias(),
                'news' => $news,
                'map' => false,
            ));    
    }

    public function pageAction($page, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $regions = $em->getRepository('SiteBackendBundle:Regions')->findAll();
        $region = Utils::getRegion();
        $settings = Utils::settings('news');
        $news = $em->getRepository('SiteBackendBundle:News')->getNewsByPage($page, $region->getId());
        $count = $em->getRepository('SiteBackendBundle:News')->countActive($region->getId());
        $diff = floor($count/$limit = $settings['items_per_page_news_previous']);
        if ($count > $diff)
            $diff++;

        return $this->render('SiteFrontendBundle:News:previous.html.twig', array(
                'regions' => $regions,
                'region' => $region,
                'main_menu_news' => true,
                'alias' => $region->getAlias(),
                'news' => $news,
                'map' => false,
                'count' => $diff,
                'page' => $page,
            ));    
    }

	//Обработчик кнопки "показать ещё" на странице новостей
    public function nextpageAction($page)
    {
        $em = $this->getDoctrine()->getManager();
        $settings = Utils::settings('news');
        $count = $em->getRepository('SiteBackendBundle:News')->countActive(null);				//Количество новостей в базе
        $maxpage = ceil($count/$settings['items_per_page_news_previous']);
        $news = $em->getRepository('SiteBackendBundle:News')->getNewsByPage($page, null); 		//Список новостей, начиная с нужной страницы

        if ($page > $maxpage)
			$page = $maxpage;
        return $this->render('SiteFrontendBundle:News:nextpage.html.twig', array(
                'news' => $news,
                'count' => $maxpage,
                'page' => $page,
            ));    
	}

    public function viewAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $regions = $em->getRepository('SiteBackendBundle:Regions')->findAll();
        $region = Utils::getRegion();
        $news = $em->getRepository('SiteBackendBundle:News')->getNews($region->getId());
        $top = $em->getRepository('SiteBackendBundle:News')->find($id);
        $news['top'] = $top;
        $settings = Utils::settings('news');
        // echo $top->getId();
        // echo '<pre>';
        // var_dump($news);
        // echo '</pre>';
        $limit = $settings['items_per_page_news_one'];
        if ( isset($news[$top->getId()] ) ){
            unset($news[$top->getId()]);
        }
        else{
            $i = 1;
            foreach ($news as $key => $value) {
                $newarray[] = $value;
                $i++;
                if( $i > $limit )
                    break;
            }
        }
        return $this->render('SiteFrontendBundle:News:view.html.twig', array(
                'regions' => $regions,
                'region' => $region,
                'main_menu_news' => true,
                'alias' => $region->getAlias(),
                'news' => $news,
                'map' => false,
            ));    
    }
}
