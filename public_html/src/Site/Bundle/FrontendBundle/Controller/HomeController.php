<?php

namespace Site\Bundle\FrontendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\Event\GetResponseUserEvent;
use FOS\UserBundle\Event\UserEvent;
use FOS\UserBundle\Event\FilterUserResponseEvent;
use Symfony\Component\DependencyInjection\ContainerAware;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use FOS\UserBundle\Model\UserInterface;
use Symfony\Component\Security\Core\SecurityContextInterface;
use Site\Bundle\BackendBundle\Utils\Utils;
use Symfony\Component\HttpFoundation\Response;

class HomeController extends Controller
{
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $competitors = array();
        $cars = Utils::minimalCarsPricesMain();
        $labels = array();
        $competitors = Utils::minimalCarsCompetitors($labels);
        $regions = $em->getRepository('SiteBackendBundle:Regions')->findAll();
        $alias = Utils::getRegionAlias();
        $region = Utils::getRegion();
        $news = Utils::getNewsForMain();
        
        
        $qb = $em->createQueryBuilder()->distinct()
            ->select('r.id, r.name, r.description, r.file, r.authorName, r.authorStatus')
            ->from('SiteBackendBundle:BaseAutoReviews', 'r')
            ->where("r.inHome = 1 AND r.isActive = 1")
            ->setMaxResults(3);
        $reviews = $qb->getQuery()->getResult();
        

        $offices = $em->getRepository('SiteBackendBundle:DillerCentre')->getDcsFront($region->getId());
        $share = $em->getRepository('SiteBackendBundle:Shares')->getSharesFront();

        return $this->render('SiteFrontendBundle:Home:index.html.twig', array(
                'regions' => $regions,
                'region' => $region,
                'alias' => $alias,
                'cars' => $cars,
                // 'offices' => $offices,
                'competitors' => $competitors,
                'labels' => $labels,
                'news' => $news,
                'share' => $share,
                'reviews' => $reviews,
            ));    
    }

    public function mapAction(){
        $em = $this->getDoctrine()->getManager();
        $region = Utils::getRegion();
        $offices = $em->getRepository('SiteBackendBundle:DillerCentre')->getDcsFront($region->getId());
        return $this->render('SiteFrontendBundle:Home:map.html.twig', array(
                'offices' => $offices,
                'region' => $region,
            ));   
    }

    public function rulesAction(){
        $cars = Utils::minimalCarsPrices();
        $data = array();
        $data['cars'] = $cars;
        return $this->render('SiteFrontendBundle:Home:rules.html.twig', $data);   
    }

    public function carsSubMenuAction($active = null){
        $cars = Utils::minimalCarsPrices();
        $data = array();
        $data['cars'] = $cars;
        $data['active'] = $active;
        return $this->render('SiteFrontendBundle:Home:cars-sub-menu.html.twig', $data);   
    }

    public function footerAction($active = null){
        $data = array();
        $data['active'] = $active;
        $cars = Utils::minimalCarsPrices();
        $data['cars'] = $cars;
        return $this->render('SiteFrontendBundle:Home:footer.html.twig', $data);   
    }

    public function getCarsAction(Request $request){
        $cars = Utils::minimalCarsPricesMain();
        $data = array();
        $data['cars'] = $cars;
        return $this->render('SiteFrontendBundle:Home:main-slider-mini.html.twig', $data);   
    }

    public function getLeftFormAction($alias, Request $request){
        return Utils::generateFormGeneral($alias, 1); 
    }

    public function getRightFormAction($alias, Request $request){
        return Utils::generateFormGeneral($alias, 0); 
    }

    public function getPageSectionAction(Request $request){
        return Utils::getPageContent(); 
    }

    public function getInfoMessageAction($type = null, Request $request){
        return Utils::getInfoMessage($type); 
    }

    public function getFieldsAttributesAction($type, Request $request){
       // return new response($type);
        return new response(json_encode(Utils::getFieldsAttributes($type))); 
    }

    public function ajaxAddMassmailAction(Request $request){
        $data = $request->request->all();
        $answer = Utils::addMassmail($data); 
        return new response($answer);
    }
    public function loginAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        
        $regions = $em->getRepository('SiteBackendBundle:Regions')->findAll();
        $region = Utils::getRegion();
        /** @var $session \Symfony\Component\HttpFoundation\Session\Session */
        $session = $request->getSession();

        // get the error if any (works with forward and redirect -- see below)
        if ($request->attributes->has(SecurityContextInterface::AUTHENTICATION_ERROR)) {
            $error = $request->attributes->get(SecurityContextInterface::AUTHENTICATION_ERROR);
        } elseif (null !== $session && $session->has(SecurityContextInterface::AUTHENTICATION_ERROR)) {
            $error = $session->get(SecurityContextInterface::AUTHENTICATION_ERROR);
            $session->remove(SecurityContextInterface::AUTHENTICATION_ERROR);
        } else {
            $error = '';
        }

        if ($error) {
            // TODO: this is a potential security risk (see http://trac.symfony-project.org/ticket/9523)
            $error = $error->getMessage();
        }
        // last username entered by the user
        $lastUsername = (null === $session) ? '' : $session->get(SecurityContextInterface::LAST_USERNAME);

        $csrfToken = $this->container->has('form.csrf_provider')
            ? $this->container->get('form.csrf_provider')->generateCsrfToken('authenticate')
            : null;
            
        return $this->container->get('templating')->renderResponse('SiteFrontendBundle:Home:login.html.twig', array(
                'regions' => $regions,
                'region' => $region,
                'alias' => $region->getAlias(),
                'last_username' => $lastUsername,
                'error'         => $error,
                'csrf_token' => $csrfToken,
                'abortcars' => true,
                'active' => false,
                'abortfooter' => true,
                'abortheader'=> true,
                'map' => false,
            ));
    }
    
    public function registerAction(Request $request)
    {
        /** @var $formFactory \FOS\UserBundle\Form\Factory\FactoryInterface */
        $formFactory = $this->container->get('fos_user.registration.form.factory');
        /** @var $userManager \FOS\UserBundle\Model\UserManagerInterface */
        $userManager = $this->container->get('fos_user.user_manager');
        /** @var $dispatcher \Symfony\Component\EventDispatcher\EventDispatcherInterface */
        $dispatcher = $this->container->get('event_dispatcher');

        $user = $userManager->createUser();
        $user->setEnabled(true);

        $event = new GetResponseUserEvent($user, $request);
        $dispatcher->dispatch(FOSUserEvents::REGISTRATION_INITIALIZE, $event);

        if (null !== $event->getResponse()) {
            return $event->getResponse();
        }

        $form = $formFactory->createForm();
        $form->setData($user);

        if ('POST' === $request->getMethod()) {
            
            $data = $request->request->get('fos_user_registration_form');
            $addrole = 'ROLE_USER';
            $user_type = 0;
            
            $form->bind($request);

            if ($form->isValid()) {
                $event = new FormEvent($form, $request);
                $dispatcher->dispatch(FOSUserEvents::REGISTRATION_SUCCESS, $event);
                
                $user->addRole($addrole);
                $user->setUserType($user_type);
                $user->setDateReg(new \DateTime());
                $user->setFio($data['fio']);
                $userManager->updateUser($user);

                if (null === $response = $event->getResponse()) {
                    $url = $this->container->get('router')->generate('fos_user_registration_confirmed');
                    $response = new RedirectResponse($url);
                }

                $dispatcher->dispatch(FOSUserEvents::REGISTRATION_COMPLETED, new FilterUserResponseEvent($user, $request, $response));

                return $response;
            }
        }

        return $this->container->get('templating')->renderResponse('SiteFrontendBundle:Home:register.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    public function sliderAction()
    {
        $em = $this->getDoctrine()->getManager();
        
        $qb = $em->createQueryBuilder();
        $qb->select('s')
            ->from('SiteBackendBundle:SliderSlides','s')
            ->orderBy('s.pos', 'ASC');
        $slides = $qb->getQuery()->getResult();
        
        return $this->render('SiteFrontendBundle:Home:slider.html.twig', array('slides' => $slides));
    }

    public function ajaxRequestSaveAction(Request $request){
        $data = $request->request->all();
        $type = $data['type'];
        $dc = null;
        if( isset($data['dc']) )
            $dc = $data['dc'];
        unset($data['type']);
        unset($data['dc']);
        $region = Utils::getRegion();
        $answer = Utils::saveRequest($data, $region, $type, $dc);
        return new response($answer);
    }

    /**
     * Форма задать вопрос
     *
     * @param Request $request
     *
     * @return Response
     */
    public function getQuestionFormAction(Request $request){
        return $this->render('SiteFrontendBundle:Home:question-form.html.twig');
    }
}
