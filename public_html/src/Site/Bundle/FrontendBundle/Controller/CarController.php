<?php

namespace Site\Bundle\FrontendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\Event\GetResponseUserEvent;
use FOS\UserBundle\Event\UserEvent;
use FOS\UserBundle\Event\FilterUserResponseEvent;
use Symfony\Component\DependencyInjection\ContainerAware;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use FOS\UserBundle\Model\UserInterface;
use Symfony\Component\Security\Core\SecurityContextInterface;
use Site\Bundle\BackendBundle\Utils\Utils;

class CarController extends Controller
{
    public function indexAction($id,$bid=null,Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $regions = $em->getRepository('SiteBackendBundle:Regions')->findAll();
        $region = Utils::getRegion();
        
        $car = $em->getRepository('SiteBackendBundle:BaseAuto')->find($id);
        
        //$car_bodies = $em->getRepository('SiteBackendBundle:BaseAutoBodyTypes')->findByCarId($id);

        $qb = $em->createQueryBuilder();
        $qb->select('b')
                ->from('SiteBackendBundle:BaseAutoBodyTypes','b')
                ->where("b.carId = '$id'")->orderBy("b.pos");;
        $car_bodies = $qb->getQuery()->getResult();
        //code to open the exact type car the is requested for base auto START

        $temp = array();

        $nextBId = isset($car_bodies[1]) && $bid==null ? $car_bodies[1]->getId() : $car_bodies[0]->getId();
        $prevBId = end($car_bodies)->getId();


        if( $bid != null ){
            foreach ($car_bodies as $key => $value) {
                if( $value->getId() == $bid ){

                    if( isset($car_bodies[$key-1]) ){
                        $prevBId = $car_bodies[$key-1]->getId();
                    }

                    if( isset($car_bodies[$key+1]) ){
                        $nextBId = $car_bodies[$key+1]->getId();
                    }

                    $temp[] = $value;
                    unset($car_bodies[$key]);

                }
            }
        }

        $car_bodies = array_merge($temp, $car_bodies);

        //code to open the exact type car the is requested for base auto END
        $body_id = $car_bodies[0]->getId();
        
        $cars_repository = $em->getRepository('SiteBackendBundle:Cars');
        
        $car_gallery = $cars_repository->getCarsGallery($body_id);

        $slider1 = $cars_repository->getCarsSlider($body_id, 1);
        $slider2 = $cars_repository->getCarsSlider($body_id, 2);    
        
        $blocks1 = $cars_repository->getCarsBlocks($body_id, 1);
        $blocks2 = $cars_repository->getCarsBlocks($body_id, 2);
        
        $carsinfo = $cars_repository->getCarsInfo($id, $body_id, 1);
        $carsinfo2 = $cars_repository->getCarsInfo($id, $body_id, 2);
        $carsinfo3 = $cars_repository->getCarsInfo($id, $body_id, false, array(1, 27, 28, 29, 32));
        $topinfo = $cars_repository->getTopInfo($id, $body_id);
        
        $competitorsinfo = $cars_repository->getCompetitorsInfo($id);
        
        $reviews = $cars_repository->getCarsReviews($id, 3);
        
        $documents = $cars_repository->getCarsDocuments($body_id);
        
        $labels = array();
        $competitors = Utils::minimalCarsCompetitors($labels, $id, $body_id);
        return $this->render('SiteFrontendBundle:Cars:carpage.html.twig', array(
                'regions' => $regions,
                'region' => $region,
                // 'main_menu_car' => true,
                'alias' => $region->getAlias(),
                'car' => $car,
                'slider1' => $slider1,
                'slider2' => $slider2,
                'blocks1' => $blocks1,
                'blocks2' => $blocks2,
                'carsinfo' => $carsinfo,
                'carsinfo2' => $carsinfo2,
                'carsinfo3' => $carsinfo3,
                'topinfo' => $topinfo,
                'gallery' => $car_gallery,
                'competitorsinfo' => $competitorsinfo,
                'labels' => $labels,
                'competitors' => $competitors,
                'reviews' => $reviews,
                'documents' => $documents,
                'car_bodies' => $car_bodies,
                'competitors' => $competitors,
                'labels' => $labels,
                'car_body' => $car_bodies[0],
                'acar'=> $id,
                'bid'=>$bid,
                'nextBId' => $nextBId,
                'prevBId' => $prevBId
            ));
    }
    
    public function ajaxBodyAction($id, $bodyid, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        
        $car = $em->getRepository('SiteBackendBundle:BaseAuto')->find($id);
        
        $body_id = $bodyid;
        
        $car_body = $em->getRepository('SiteBackendBundle:BaseAutoBodyTypes')->find($body_id);
        
        $cars_repository = $em->getRepository('SiteBackendBundle:Cars');
        
        $car_gallery = $cars_repository->getCarsGallery($body_id);

        $slider1 = $cars_repository->getCarsSlider($body_id, 1);
        $slider2 = $cars_repository->getCarsSlider($body_id, 2);
        
        $blocks1 = $cars_repository->getCarsBlocks($body_id, 1);
        $blocks2 = $cars_repository->getCarsBlocks($body_id, 2);
        
        
        $carsinfo = $cars_repository->getCarsInfo($id, $body_id, 1);
        $carsinfo2 = $cars_repository->getCarsInfo($id, $body_id, 2);
        $carsinfo3 = $cars_repository->getCarsInfo($id, $body_id, false, array(1, 27, 28, 29, 32));
        
        $topinfo = $cars_repository->getTopInfo($id, $body_id);
        
        $competitorsinfo = $cars_repository->getCompetitorsInfo($id);
        
        $reviews = $cars_repository->getCarsReviews($id, 3);
        
        $documents = $cars_repository->getCarsDocuments($body_id);
        
        $labels = array();
        $competitors = Utils::minimalCarsCompetitors($labels, $id, $body_id);

        $template1 = $this->renderView('SiteFrontendBundle:Cars:topslider.html.twig', array('slider1' => $slider1)); 
        $template2 = $this->renderView('SiteFrontendBundle:Cars:carbottom.html.twig', array(
                'car' => $car,
                'slider2' => $slider2,
                'blocks1' => $blocks1,
                'blocks2' => $blocks2,
                'carsinfo' => $carsinfo,
                'carsinfo2' => $carsinfo2,
                'carsinfo3' => $carsinfo3,
                'topinfo' => $topinfo,
                'gallery' => $car_gallery,
                'competitorsinfo' => $competitorsinfo,
                'reviews' => $reviews,
                'documents' => $documents,
                'competitors' => $competitors,
                'labels' => $labels,
                'car_body' => $car_body));
        
        return new JsonResponse(array('tpl1' => $template1, 'tpl2' => $template2));
    }
}
