<?php

namespace Site\Bundle\FrontendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\Event\GetResponseUserEvent;
use FOS\UserBundle\Event\UserEvent;
use FOS\UserBundle\Event\FilterUserResponseEvent;
use Symfony\Component\DependencyInjection\ContainerAware;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use FOS\UserBundle\Model\UserInterface;
use Symfony\Component\Security\Core\SecurityContextInterface;
use Site\Bundle\BackendBundle\Utils\Utils;
use Symfony\Component\HttpFoundation\Response;

class BuyController extends Controller
{
    public function indexAction($menu, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $regions = $em->getRepository('SiteBackendBundle:Regions')->findAll();
        $region = Utils::getRegion();
        $senddata = array();
        $content = '';
        $func = 'get'.ucfirst($menu);
        if( $menu == 'testdrive' ){
            //return $this->testDriveCarAction( 1, $request, true );
        }
        $bid = $request->query->get('bid');
        $cid = $request->query->get('cid');
        $dcid = $request->query->get('dcid');
        $byid = $request->query->get('byid');

        if( isset($bid) && isset($cid) ){
            if( isset($byid) && $byid == 1 )
                $bid = $em->getRepository('SiteBackendBundle:BaseAutoBodyTypes')->find($bid)->getName();
            $senddata['bid'] = $bid;
            $senddata['cid'] = $cid;
            $senddata['truebid'] = $request->query->get('bid');
            $car = $em->getRepository('SiteBackendBundle:BaseAuto')->find($cid);
            $senddata['carname'] = $car->getName();

            $senddata['cartype'] = $bid;
            $senddata['request'] = true;
            if ( isset($dcid) ){
                $senddata['switchdcd'] = $dcid;
                $senddata['alias'] = $em->getRepository('SiteBackendBundle:DillerCentre')->find($dcid)->getAlias();
            }
            $senddata['cdc'] = Utils::getComplectationsForDc($request->query->get('cid'), $bid);

			//********Дополнительные параметры
            if (isset($_GET['exec']))
				$senddata['selected_type'] = $request->query->get('exec');
			if (isset($_GET['complect']))
			{
				preg_match('#^(.+)-(.+)-(.+)$#Uu', $request->query->get('complect'), $parts);
				$senddata['selected_model'] = $parts[1];
				$senddata['selected_execnum'] = $parts[2];
				$senddata['selected_complect'] = $parts[3];
			}
			if (isset($_GET['compID']))
				$senddata['compID'] = $request->query->get('compID');
			//********************************

            $qb = $em->createQueryBuilder();
            $qb->select('c')
                ->from('SiteBackendBundle:BodyTypesColors','bc')
                ->leftJoin('SiteBackendBundle:Colors',   'c', 'WITH','bc.colorId = c.id')
                ->where('bc.bodyTypeId = '.(int)$request->query->get('bid').' AND c.status = 1');
            $colors = $qb->getQuery()->getResult();

            $senddata['colors'] = $colors;
        }
        if( method_exists($this, $func) ){
            $content = $this->{$func}($senddata);
        }
        if ( isset($dcid) )
            $switchdc = $request->query->get('dcid');

        if( isset( $switchdc ) )
            $senddata['switchdc'] = $switchdc;

        $senddata['regions'] = $regions;
        $senddata['region'] = $region;
        $senddata['main_menu_buy'] = true;
        $senddata['abortcars'] = true;
        $senddata['active'] = $menu;
        $senddata['alias'] = $region->getAlias();
        $senddata['content'] = $content;

        return $this->render('SiteFrontendBundle:Buy:buy.html.twig', $senddata);
    }

    private function getCalculator(&$senddata){
        $send = Utils::getcarsTypesList();
        return $this->renderView('SiteFrontendBundle:Buy:inner-calculator.html.twig', $send);
    }

    private function getCorporate(&$senddata){
        return $this->renderView('SiteFrontendBundle:Buy:inner-corporate.html.twig', array());
    }

    private function getCardetails(&$senddata){
        return $this->renderView('SiteFrontendBundle:Buy:inner-cardetails.html.twig', $senddata);
    }
    private function getTestdrive(&$senddata){
        $em = $this->getDoctrine()->getManager();
        $data = array();
        $r = Utils::getRegion();
        $data['region'] = Utils::getRegion();
        $data['regions'] = $em->getRepository('SiteBackendBundle:Regions')->findAll();
        $data['alias'] = $r->getAlias();
        $data['general'] = true;
        $data['main_menu_buy'] = true;
        $data['abortcars'] = true;
        $data['active'] = 'testdrive';
        $data['request'] = true;
        $senddata['request'] = true;
        return $this->renderView('SiteFrontendBundle:Buy:inner-testdrive.html.twig', $data);
    }
    private function getAutocredit(&$senddata){
        $send = array();
        $em = $this->getDoctrine()->getManager();
        $offers = $em->getRepository('SiteBackendBundle:CreditOffers')->findAll();
        foreach ($offers as $key => $value) {
            # code...
        }
        return $this->renderView('SiteFrontendBundle:Buy:inner-autocredit.html.twig', $send);
    }

    public function ajaxCreditOffersAction($carId, $bodyId){
        $send = $arrayOffers = array();
        $em = $this->getDoctrine()->getManager();
        $offers = $em->getRepository('SiteBackendBundle:CreditOffers')->findAll();
        foreach ($offers as $key => $value) {
            $tax = $period = array();
            $nameBody = $em->getRepository('SiteBackendBundle:BaseAutoBodyTypes')->find($bodyId);
            $allow = $value->getAuto();
            if( !isset( $allow[$carId][$nameBody->getName()]) )
                unset($offers[$key]);
            else{
                if ( !isset($arrayOffers[$value->getBank()]) ){
                    $arrayOffers[$value->getBank()] = array();
                }
                $arrayOffers[$value->getBank()][$key]['commision'] = 0;
                $arrayOffers[$value->getBank()][$key]['nameCredit'] = $value->getNameCredit();
                $arrayOffers[$value->getBank()][$key]['insurance'] = $value->getInsurance();
                $arrayOffers[$value->getBank()][$key]['firstpay'] = $value->getTax()[0]['firstpay']['min'];
                foreach ($value->getTax() as $x => $z) {
                    if( $arrayOffers[$value->getBank()][$key]['commision'] < $z['commision'] )
                        $arrayOffers[$value->getBank()][$key]['commision'] = $z['commision'];
                    foreach ($z['values'] as $c => $p) {
                        $tax[] = $p['tvalue'];
                        if( $p['perMin'] != $p['perMax'] )
                            $period[] = $p['perMin'].' - '.$p['perMax'];
                        else
                            $period[] = $p['perMin'];
                    }

                }
                $tax = array_unique($tax); $period = array_unique($period);
                asort($tax);
                $tax = implode(' / ', $tax); $period = implode(' / ', $period);
                $arrayOffers[$value->getBank()][$key]['tax'] = $tax;
                $arrayOffers[$value->getBank()][$key]['period'] = $period;
                $arrayOffers[$value->getBank()][$key]['nameCredit'] = $value->getNameCredit();
                $arrayOffers[$value->getBank()][$key]['requirements'] = $value->getRequirements();
                //$arrayOffers[$value->getBank()][$key]['commision'] = $value->getCommision();

            }
        }

        $send['offers'] = $arrayOffers;
        return $this->render('SiteFrontendBundle:Buy:inner-offers.html.twig', $send);
    }

    private function getNewlada(&$senddata) {
        $send = Utils::getcarsTypesList();
        $page = $info = $cap = array();
        Utils::getFinance($send['data'], $page, $info);
        $capa = $page['capacity'];
        //Убираем значения через слэш, оставляем только последнее число
        foreach ($capa as $cpa)
        {
			if (preg_match('#/(.+)$#', $cpa, $lastpart))
				$cap[] = $lastpart[1];
			else
				$cap[] = $cpa;
		}
		$cap = array_unique($cap);
		sort($cap, SORT_NUMERIC);
		$page['capacity'] = $cap;
		//********************
        $send['page'] = $page;
        $send['info'] = $info;
		//TODO подкорректировать capacity: убрать первое значение и сделать unique
        return $this->renderView('SiteFrontendBundle:Buy:inner-chooselada.html.twig', $send);
    }

    public function testDriveCarAction($id, $bid=false, Request $request, $general = null){

        $em = $this->getDoctrine()->getManager();
        $regions = $em->getRepository('SiteBackendBundle:Regions')->findAll();
        $region = Utils::getRegion();

        // Если есть модель и кузов
        if( $bid ){

            $qb = $em->createQueryBuilder();

            $qb->select('b')
                ->from('SiteBackendBundle:BaseAutoBodyTypes','b')
                ->where("b.carId = '$id'")->orderBy("b.pos");

            $cars = $qb->getQuery()->getResult();

            $car = $em->getRepository('SiteBackendBundle:BaseAuto')->find($id);

            $temp = array();

            if( $bid != null ){
                foreach ($cars as $key => $value) {
                    if( $value->getId() == $bid ){
                        $temp[] = $value;
                        unset($cars[$key]);

                    }
                }
            }

            $cars = array_merge($temp, $cars);

        }else{
            $cars = Utils::minimalCarsPrices();
            $car = array('id'=>$id);
        }

        return $this->render('SiteFrontendBundle:Cars:test-drive-car.html.twig', array(
                'regions' => $regions,
                'region' => $region,
                'main_menu_buy' => true,
                'cars' => $cars,
                'active' => 'testdrive',
                'activex' => '11111111',
                'alias' => $region->getAlias(),
                'car' => $car,
                'general' => $general,
                'request' => true,
                'car'=>$car,
                'bid'=>$bid
            ));
    }

	//Получение информации о машинах для страницы сравнения
    public function ajaxGetInfoCarsAction( Request $request){
        $em = $this->getDoctrine()->getManager();
        $post = $request->request->all();
        $recieved = $post['data'];
        $cars = array();

		//Тут в $value два параметра: car — идентификатор в base_auto, body — идентификатор в таблице кузовов
        foreach ($recieved as $value) {
			$minvalues = $details = array();
			$info = Utils::getComplectations($value['body']);
			$cars[$value['body']] = $info;
        }
        return new response(json_encode($cars));
    }
}