<?php

namespace Site\Bundle\FrontendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\Event\GetResponseUserEvent;
use FOS\UserBundle\Event\UserEvent;
use FOS\UserBundle\Event\FilterUserResponseEvent;
use Symfony\Component\DependencyInjection\ContainerAware;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use FOS\UserBundle\Model\UserInterface;
use Symfony\Component\Security\Core\SecurityContextInterface;
use Site\Bundle\BackendBundle\Utils\Utils;

class ShareController extends Controller
{
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $regions = $em->getRepository('SiteBackendBundle:Regions')->findAll();
        $region = Utils::getRegion();
        $shares = $em->getRepository('SiteBackendBundle:Shares')->getSharesDc($region->getId());
        $sharesTop = $em->getRepository('SiteBackendBundle:Shares')->getSharesTop();
        $cars = $em->getRepository('SiteBackendBundle:BaseAuto')->findAll();
        //$content = $this->renderView('SiteFrontendBundle:Buy:inner-calculator.html.twig', array());

        return $this->render('SiteFrontendBundle:Shares:index.html.twig', array(
                'regions' => $regions,
                'region' => $region,
                'main_menu_buy' => true,
                'abortcars'=> true,
                'active' => '',
                'alias' => $region->getAlias(),
                'percent_menu' => true,
                'shares' => $shares,
                'sharesTop' => $sharesTop,
                'cars' => $cars,
            ));  
    }
}
