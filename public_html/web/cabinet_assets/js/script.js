$(document).ready(function(){
	

	//placeholder ie7
	$('[placeholder]').placeholder();


	//select
	$('select').coreUISelect();


	//diller select
	$('.diller_select .select_menu .btn_menu').on('click touchstart', function() {
		if ($('.select_menu ul').css('display')=='block') {
			$('.select_menu ul').slideUp(200);
		} else {
			$('.select_menu ul').slideDown(200);
		}
		return  false;
	})
	/*$('.diller_select .select_menu li a').on('click touchstart', function() {
		var newD=$(this).text();
		$('.diller_select .name').text(newD);
		$('.diller_select .select_menu li a').removeClass('active');
		$(this).addClass('active');
		$(this).parents('ul').slideUp(200);
		return false;
	})*/


	//overlay
	$('.overlay').on('click touchstart', function(){
		$(this).removeClass('active');
		$('.select_menu ul').slideUp(200);
	})


	// message popup
	$('#popup_message').dialog({
		autoOpen: false,
		modal: true,
		resizable: false,
		draggable: false,
		width: 980,
		show: { effect: "fade", duration: 400 },
		hide: { effect: "fade", duration: 400 },
		dialogClass: '',
		title: ''
	})
	$('.btn_message').on('click touchstart', function() {
		$('#popup_message').dialog('open');
		return false;
	})


	//in stock
	$('.tbl_car a').on('click touchstart', function(){
		$('.tbl_car a').removeClass('active');
		$(this).addClass('active');
		$('.car_block').slideUp(0);
		var carNum=$(this).attr('data-car');
		$('.car_block#car'+carNum).slideDown(0);
		return false;
	})
	//diller instock
	var carNum=$('.diller_instock .tbl_car a.active').attr('data-car');
	$('.car_block#car'+carNum).slideDown(0);


	//title search
	$('.main_title .search .i-search').on('click touchstart', function(){
		$(this).parent('.search').addClass('active');
	})


	//tabs
	$('.tabs').tabs();
	$('.tabs_filter').tabs({active:3});




	
});