jQuery(function () {
    // $('.main-menu').on('click',function(){
    // 	if ( $(this).hasClass('empty')){
	   //  	$('.currentpage').removeClass('currentpage');
	   //  	$(this).parents('.first-level').addClass('currentpage');
    // 	}
    // 	$(this).find('.glyphicon').toggleClass('glyphicon-chevron-right').toggleClass('glyphicon-chevron-down');
    // 	$(this).parents('.first-level').find('.ul-sub-menu').toggle(300);
    // 	$('.activemenu').removeClass('activemenu');
    // 	$(this).parents('li').addClass('activemenu');
    // })
    // $('.sub-menu').on('click',function(){
    // 	if ( $(this).hasClass('empty')){
	   //  	$('.currentpage').removeClass('currentpage');
	   //  	$(this).parents('.second-level').addClass('currentpage');
    // 	}
    // 	$(this).parents('.second-level').find('.ul-sub-sub-menu').toggle(300);
    // 	$(this).find('.glyphicon').toggleClass('glyphicon-chevron-right').toggleClass('glyphicon-chevron-down');
    // })
    // $('.sub-sub-menu').on('click',function(){
    // 	$('.currentpage').removeClass('currentpage');
    // 	$(this).parents('.third-level').addClass('currentpage');
    // })
    // $('#show-all').on('click',function(){
    // 	$(this).hide();
    // 	$('#hide-all').show();
    // 	$('.ul-sub-menu').show(100);
    // 	$('.ul-sub-sub-menu').show(100);
    // 	$('.glyphicon').removeClass('glyphicon-chevron-right').addClass('glyphicon-chevron-down');
    // })
    // $('#hide-all').on('click',function(){
    // 	$(this).hide();
    // 	$('#show-all').show();
    // 	$('.ul-sub-menu').hide(100);
    // 	$('.ul-sub-sub-menu').hide(100);
    // 	$('.glyphicon').addClass('glyphicon-chevron-right').removeClass('glyphicon-chevron-down');
    // })

    $('input').on('focus',function(){
        $(this).parents('.form-group').removeClass('has-error');
    })
});

var admin = {};
admin.errors = {};
errorsDestroy();

function checkTrim(array){
    var keys = {};
    $.each(array,function(k,v){
        if( $.trim(v) == ''){
            keys[k] = 1;
        }
    });
    console.log(keys);
    return keys;
}

function errorsDestroy(){
    admin.errors = {};
    $('.has-error').removeClass('has-error');
}

function showErrorInput(el){
    el.parents('.form-group').addClass('has-error')
}

function enableCover(){
    $('#cover').fadeIn(300);
}

function disableCover(){
    setTimeout(function(){
        $('#cover').fadeOut(300);
    },1000)
}

function showSuccessMessage(){
    $(window).scrollTop();
    $('.alert').hide();
    setTimeout(function(){
    $('.alert-success').show();
    },1000);
}
function showErrorMessage(){
    $('.alert .error-details').remove();
    $('.alert').hide();
    $('.alert-danger').show();
    if ( Object.keys(admin.errors).length > 0 ){
        var ul = $('<ul/>').addClass('error-details');
        $.each(admin.errors,function(k,v){
            var li = $('<li/>');
            li.html(v);
            ul.append(li);
        })
    }
    $('.alert-danger').append(ul);
}

function ajaxResponseHandler(data){
    if( data == 0 ){
    disableCover();
    showSuccessMessage();
    }
    if( data == 1 ){
    disableCover();
    showErrorMessage();
    }
    if( data != 1 && data != 0 ){
    showErrorMessage();
    disableCover();

    }
}