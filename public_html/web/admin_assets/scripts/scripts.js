jQuery(function () {
    $('.sidebar .main-menu').on('click',function(){
    	if ( $(this).hasClass('empty')){
	    	$('.currentpage').removeClass('currentpage');
	    	$(this).parents('.first-level').addClass('currentpage');
    	}
    	$(this).find('.glyphicon').toggleClass('glyphicon-chevron-right').toggleClass('glyphicon-chevron-down');
    	$(this).parents('.first-level').find('.ul-sub-menu').toggle(300);
    	$('.activemenu').removeClass('activemenu');
    	$(this).parents('li').addClass('activemenu');
    })
    $('.sidebar .sub-menu').on('click',function(){
    	if ( $(this).hasClass('empty')){
	    	$('.currentpage').removeClass('currentpage');
	    	$(this).parents('.second-level').addClass('currentpage');
    	}
    	$(this).parents('.second-level').find('.ul-sub-sub-menu').toggle(300);
    	$(this).find('.sidebar .glyphicon').toggleClass('glyphicon-chevron-right').toggleClass('glyphicon-chevron-down');
    })
    $('.sidebar .sub-sub-menu').on('click',function(){
    	$('.currentpage').removeClass('currentpage');
    	$(this).parents('.third-level').addClass('currentpage');
    })
    $('.sidebar #show-all').on('click',function(){
    	$(this).hide();
    	$('.sidebar #hide-all').show();
    	$('.sidebar .ul-sub-menu').show(100);
    	$('.sidebar .ul-sub-sub-menu').show(100);
    	$('.sidebar .glyphicon').removeClass('glyphicon-chevron-right').addClass('glyphicon-chevron-down');
    })
    $('.sidebar #hide-all').on('click',function(){
    	$(this).hide();
    	$('.sidebar #show-all').show();
    	$('.sidebar .ul-sub-menu').hide(100);
    	$('.sidebar .ul-sub-sub-menu').hide(100);
    	$('.sidebar .glyphicon').addClass('glyphicon-chevron-right').removeClass('glyphicon-chevron-down');
    })
});

     $.fn.blink = function (speed, blink) {
    var options = {
    xSpeed: speed ? speed : 500, // Set the blink speed
    xBlink: blink ? blink : 5 // Set how many times the element should blink
    };
    for (var i = 0; i < options.xBlink; i++) {
    this.animate({opacity: 0},options.xSpeed);
    this.animate({opacity: 1},options.xSpeed);
    }
    return this; // To support jQuery chain-ability
    };
    function enableInputsCstm(){
      $("input[type='checkbox'], input[type='radio']").iCheck({
          checkboxClass: 'icheckbox_minimal',
          radioClass: 'iradio_minimal'
      });
    }
    
function getTimestamp(date){
  var array = date.split("/");
  var date = array[2]+'-'+array[0]+'-'+array[1];
  var timestamp = (new Date(date).getTime())/1000;
  return timestamp;
}
function activateImageLibrary(id){
    $('[data-type="elfinder-input-field"][id="'+id+'"]').on("click",function() {
    var childWin = window.open("/elfinder/form?id="+id, "popupWindow", "height=450, width=900");
    });
}
function setValue(value, element_id) {
    $('[data-type="elfinder-input-field"]' + (element_id ? '[id="'+ element_id +'"]': '')).val(value);
}

function activateEditore(id){
var editor = new TINY.editor.edit('editor1', {
    id: id,
    width: 700,
    height: 200,
    cssclass: 'tinyeditor',
    controlclass: 'tinyeditor-control',
    rowclass: 'tinyeditor-header',
    dividerclass: 'tinyeditor-divider',
    controls: ['bold', 'italic', 'underline', 'strikethrough', '|', 'subscript', 'superscript', '|',
        'orderedlist', 'unorderedlist', '|', 'outdent', 'indent', '|', 'leftalign',
        'centeralign', 'rightalign', 'blockjustify', '|', 'unformat', '|', 'undo', 'redo', 'n',
        'font', 'size', 'style', '|', 'image', 'hr', 'link', 'unlink', '|', 'print'],
    footer: true,
    fonts: ['Verdana','Arial','Georgia','Trebuchet MS'],
    xhtml: true,
    cssfile: 'custom.css',
    bodyid: 'editor1',
    footerclass: 'tinyeditor-footer',
    toggle: {text: 'source', activetext: 'wysiwyg', cssclass: 'toggle'},
    resize: {cssclass: 'resize'}
});
}

function activateEditore2(id){
var editor = new TINY.editor.edit('editor2', {
    id: id,
    width: 700,
    height: 200,
    cssclass: 'tinyeditor',
    controlclass: 'tinyeditor-control',
    rowclass: 'tinyeditor-header',
    dividerclass: 'tinyeditor-divider',
    controls: ['bold', 'italic', 'underline', 'strikethrough', '|', 'subscript', 'superscript', '|',
        'orderedlist', 'unorderedlist', '|', 'outdent', 'indent', '|', 'leftalign',
        'centeralign', 'rightalign', 'blockjustify', '|', 'unformat', '|', 'undo', 'redo', 'n',
        'font', 'size', 'style', '|', 'image', 'hr', 'link', 'unlink', '|', 'print'],
    footer: true,
    fonts: ['Verdana','Arial','Georgia','Trebuchet MS'],
    xhtml: true,
    cssfile: 'custom.css',
    bodyid: 'editor2',
    footerclass: 'tinyeditor-footer',
    toggle: {text: 'source', activetext: 'wysiwyg', cssclass: 'toggle'},
    resize: {cssclass: 'resize'}
});
}