$(document).ready(function(){
    $(document).on('click',function(e){
    $('#myModal2 .close').on('click',function(){
        $('#myModal2').addClass('hidden');
        var h = location.href;
        h = h.split('?');
        location.href = h[0];
    });



    $('#color label.customcolor').click(function(e) {
      var colorcode = $(this).data('content');
      $('.car360 img').css('backgroundColor', colorcode);
      
      if( $('.main-thumbnail').length > 0 ) {
        $('.main-thumbnail img').css('backgroundColor', colorcode);
      }
    });


    //enableMassmail()
    $('.questions').on('click',function(){
        if( $('#callback').hasClass('show') == false )
            $('#callback').addClass('show');
        else
            $('#callback').removeClass('show');
    })
    $('#callback .close').on('click',function(){
        $('#callback').removeClass('show');
    })
     setTimeout(function(){
    $('.map-labels-inner').hide()
    $('.c-map-labels').hover(function(){
      var cel = $(this).attr('class');
      var cc = $.trim(cel.replace('c-map-labels',''));             
      $('.map-labels-inner.of'+cc).show();
     
      
    },function(){
      var cel = $(this).attr('class');
      var cc = $.trim(cel.replace('c-map-labels',''));             
        $('.map-labels-inner.of'+cc).hide(); 
    }
    );
  },1000)


    var el = $(e.target);
    if( el.attr('class') == 'ui-menu-item' && $('#map').length > 0 && el.parents('#ui-id-1').length > 0 ){
        $('#dealers-single').html('Выбрать');
        $('#dealer-centre-name').val(el.html());
    }
    });

    $('body').bind('click',function(e){
        if( $('.dealers-list').is(':visible') == true && $(e.target).attr('id') != 'dealers-all')
            $('.dealers-list').toggle();
    })
    $('#dealers-all').on('click',function(e){
        e.preventDefault();
        setDefaultPosition();
        //$('.infobox').hide();
        $('.dealers-list').toggle();
    })
    $('#dealers-single').on('click',function(e){
        $('#dealers-single').html('Найти');
        e.preventDefault();
        var name = $('#dealer-centre-name').val();
        $.each(autolist,function(k,v){
            if ( v == name ){
                simulatePlacemark(k);
            }
        })
    })
     $('.dealer-centre').on('click',function(){
        $('#dealers-single').html('Выбрать');
        $('#dealer-centre-name').val($(this).find('a').html());
        // var id = $(this).find('a').attr('data-id');
        // simulatePlacemark(id);
        // $('.dealers-list').toggle();
     })

     $('#offering').on('click',function(e){
        if( $(e.target).hasClass('inner') == false && $(e.target).parents('.inner').length == 0 ){
            $('#offering').addClass('hidden');
        }
     })

     $('#offering #cancel').on('click',function(e){
            $('#offering').addClass('hidden');
     })

     setTimeout(function(){
        $('.main-menu li').on('mouseover',function(){
          $(this).find('a').addClass('hover');
          
        })
        $('.main-menu li').on('mouseout',function(){
          $(this).find('a').removeClass('hover');
        })
        $('.sub-menu a').on('mouseover',function(){
          $(this).addClass('hover');
          
        })
        $('.sub-menu a').on('mouseout',function(){
          $(this).removeClass('hover');
        })
    },100)

     enableMassmail()
})

function setDefaultPosition(){
    map.setCenter(new google.maps.LatLng(generalL, generalW));
    map.setZoom(9);
}
function simulatePlacemark(id){
    google.maps.event.trigger(simulates['msc'+id], 'click', {
      latLng: new google.maps.LatLng(0, 0)
    });
}
function getNearest(data){
  $.ajax({
      url: '/dealers/nearest',
      data: data,
      dataType : 'html',
      type: 'post',
      success: function (response) {
        response = JSON.parse(response);
        if( response.thisRegion == true ){
            //simulatePlacemark(response.dcId);
            var zoom = 13;
            if( response.latitude > 2 || response.longtitude > 2){ zoom = 7 }
            if( response.latitude < 2 || response.longtitude < 2){ zoom = 9 }
            if( response.latitude < 1 || response.longtitude < 1){ zoom = 10 }
            map.setCenter(new google.maps.LatLng(response.latitude, response.longtitude));
            map.setZoom(zoom);
        }
        else{
            var region = response.regionAlias;
            //console.log(region);
            $('#offering').removeClass('hidden');
            $('#goto').attr('href','http://'+region+'.reglada.ru/dealers')
            $('#offering #dc').html(response.name);
            $('#offering #forregion').html(response.regionFor);
        }
      },
      error: function(e){

      }});
}

function showErrorFront(){
  $('#forming').removeClass('hidden');
}
function showSuccessFront(){
  $('#forming #text-message').html('Заявка успешно отправлена региональному дилеру');
  $('#forming').removeClass('hidden');
}

function showSuccessCST(text){
  $('#forming #text-message').html(text);
  $('#forming').removeClass('hidden');
}

function newThreeDModel(bodyid){
  var container = $('<div/>').addClass('container');
  var inner = $('<div/>').addClass('car360 have-tooltip').attr({'data-tipped-options':"target: 'mouse'",'title':'Кликните и потяните курсором в нужном вам направлении'});
  var img = $('<img/>').addClass('rollerblade-img img-responsive').attr({'src':'/uploads/3d/'+bodyid+'/1.png'});
  inner.html(img);
  var icon = $('<div/>').addClass('icon360').html($('<img/>').attr({'src':'/front_assets/img/icon-360.png'}).html('<span class="hidden-xs">Кликните и потяните</span>'));
  container.html(inner).append(icon);
  $('.block-360').html(container);
	objImg = new Image();
	objImg.src = '/uploads/3d/'+bodyid+'/1.png';
	objImg.onload = function() {
							App.Car360(bodyid)
							$('.grid-load').hide();
							}
}

function newThreeDModelCalc(bodyid) {
	//var container = $('<div/>').addClass('container');
	var inner = $('<div/>').addClass('car360 col-xs-8');
	var img = $('<img/>').addClass('rollerblade-img img-responsive').attr({'src':'/uploads/3d/'+bodyid+'/1.png'});
	inner.html(img);
	// var icon = $('<div/>').addClass('icon360').html($('<img/>').attr({'src':'/front_assets/img/icon-360.png'}).html('<span class="hidden-xs">Кликните и потяните</span>'));
	//container.html(inner);//.append(icon);
	$('.car-preview .car360.col-xs-8').remove();
	inner.insertBefore('.info-car');
	objImg = new Image();
	objImg.src = '/uploads/3d/'+bodyid+'/1.png';
	objImg.onload = function() {
							App.Car360(bodyid)
							$('.grid-load').hide();
							}
}


function newThreeDModelConf(bodyid){
  //console.log(bodyid)
  var container = $('<div/>').addClass('container');
  var inner = $('<div/>').addClass('car360 have-tooltip').attr({'data-tipped-options':"target: 'mouse'",'title':'Кликните и потяните курсором в нужном вам направлении'});
  var img = $('<img/>').addClass('rollerblade-img img-responsive').attr({'src':'/uploads/3d/'+bodyid+'/1.jpg'});
  inner.html(img);
  var icon = $('<div/>').addClass('icon360').html($('<img/>').attr({'src':'/front_assets/img/icon-360.png'}).html('<span class="hidden-xs">Кликните и потяните</span>'));
  container.html(inner).append(icon);

  $('.block-360').html(container);
  App.Car360(bodyid)

}

function saveDataRequest(data, type){
  $.ajax({
      url: '/request/save',
      data: data,
      dataType : 'json',
      type: 'post',
      success: function (response, textStatus) {
        if( JSON.parse(response) == 1 ){
            //showSuccessFront();
            $('body').removeClass('modal-open');
            $('.modal').removeClass('show');
            console.log(type);
            $('.infomsg.this-'+type).removeClass('hidden').addClass('show');
        }
      }
    });
}

function enableMassmail(){
      $('.subscrube_button').on('click',function(){
        var email = $('#subscribe_email').val();
        $.ajax({
            url: '/add/massmail',
            data: {'email':email},
            dataType : 'json',
            type: 'post',
            success: function (response, textStatus) {
              if( JSON.parse(response) == 1 ){
                  showSuccessCST('Вы успешно подписались на рассылку');
              }
              else{
                  showSuccessCST('Этот адрес уже состоит в рассылке');
              }
            }
          });
    })
}