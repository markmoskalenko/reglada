var offices = [
    {
        value: 'Москва, Остоженка 10',
        data: {
            latLng: [55.742604, 37.599881],
            button: '#msc'
        }
    },
    {
        value: '1st Floor, Plot № 14, Institutional Area, Sector 44, Gurgaon 122002',
        data: {
            latLng: [28.453192, 77.068356],
            button: '#daily'
        }
    },
    {
        value: 'Киев, Ольгинская 6',
        data: {
            latLng: [50.447296, 30.528473],
            button: '#kiev'
        }
    },
    {
        value: 'New-York, 526 7th Avenue, Floor 7',
        data: {
            latLng: [18.431389, -64.6230559],
            button: '#new-york'
        }
    },
    {
        value: 'Road Town, Trident Chambers. P.O. Box 146',
        data: {
            latLng: [18.431389, -64.6230559],
            button: '#road-town'
        }
    },
];
