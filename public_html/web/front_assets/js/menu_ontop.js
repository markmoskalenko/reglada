$(function() {
	if( $('menu').length > 0 ){
		menu_initial_position = $('menu').offset().top;

		$(window).on('scroll', function() {
			if (window.scrollY > menu_initial_position)
				$('menu .floating-block').css('position', 'fixed');
			else
				$('menu .floating-block').css('position', 'static');
		});
	}
});