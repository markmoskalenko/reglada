var map;
var simulates = [];
var defaultmap = {};
var offs = [];
defaultmap.regionOptions = {};
var calculatorOn = false;
var configurator = false;
var competitors = false;
var gallery_reset = false;
var carpagesingle = false;
var allowload = true;
var App;
var threedmodel = false;
var switchdc = false;
$(function() {
    window.mobileDetection = {
        Android:function () {
            return navigator.userAgent.match(/Android/i);
        },
        BlackBerry:function () {
            return navigator.userAgent.match(/BlackBerry/i);
        },
        iOS:function () {
            return navigator.userAgent.match(/iPhone|iPad|iPod/i);
        },
        Opera:function () {
            return navigator.userAgent.match(/Opera Mini/i);
        },
        Windows:function () {
            return navigator.userAgent.match(/IEMobile/i);
        },
        any:function () {
            return (this.Android() || this.BlackBerry() || this.iOS() || this.Opera() || this.Windows());
        }
    };
    window.isMobile = mobileDetection.any();
    //console.log(threedmodel+'_____--');

    App = {

        init: function() {
            
            App.Menu();
            App.SubMenu();
            App.Custom_elements();
            App.Tooltip();
            App.InputForms();
            App.LoadMap();
            App.MainSlider();
            App.Comparison(false);
            
            App.Gallary();
            App.Tabs();
            App.ProgressBar();
            App.CreditCalculator();
            App.Car360(threedmodel);


        },
        Menu: function(){
            $('a.menu_but, .header-inner .additional-menu .-menu').click(function(){ //мобильное меню
                $('header').toggleClass('-opened');
            });
            $('.region-menu-top .dealer-name, .region-menu-top .arrow-wrap').click(function(){ //выбор диллера
                $('.region-menu-top').toggleClass('active');
                $('body').toggleClass('modal-open');

                var toggle_var = 1;

                if($('.region-menu-top').hasClass('active')){
                    $("header .ul-regions-wrap").css({'height':'auto'});
                    //var win_height =$ (window).height();
                    //console.log($("header .ul-regions-wrap").height());
                    var win_height = $("header .ul-regions-wrap").height()+100;
                    var dealer_name_height = $('.region-menu-top .dealer-name').height()
                    var arrow_pos = -win_height + dealer_name_height;
                    var dealer_list_height = win_height - dealer_name_height;
                }else{
                    $('.region-menu-top .arrow-wrap').css({
                        bottom: ''
                    });
                    $('.region-menu-top .dealer-list-wrap').css({
                        height: ''
                    });
                }
                $('.region-menu-top .arrow-wrap').css({
                    bottom: arrow_pos
                });
                $('.region-menu-top .dealer-list-wrap').css({
                    height: dealer_list_height
                });
                $("header .ul-regions-wrap").css({
                    height: win_height-55
                })

            })
            $('header .main-menu li').eq(-2).addClass('last-child');
            $("header .ul-regions-wrap").mCustomScrollbar({
                axis:"y",
                theme:"light-3"

            });
        },
        SubMenu: function(){
            $('section.sub-menu').each(function(){
                var items_length = $(this).find('.container a').length;
                $(this).find('.container a').css({
                    width: 100/items_length + '%'
                });
            });

        },
        Gallary: function(){
            $('.block-gallery a').iLightBox(
                {
                    skin: 'dark',
                    path: 'horizontal',

                    /*social: {
                        start: true,
                        buttons: {
                            vk: {
                                source: "http://vkontakte.ru/share.php?url={URL}",
                                text: "Share on Custom Button",
                                width: 720,
                                height: 420
                            },
                            facebook: true,
                            twitter: true,
                            googleplus: true
                        }
                    }*/
                }
            );
        },
        Car360: function(bodyid){
            var imgList = [];
            if( bodyid == false ){
                bodyid = '102';
            }
            for (var i = 1; i <= 18; i++) {
                //if (i < 10)  y = '0'+i; else  y = i;
                imgList.push('/uploads/3d/'+bodyid+'/'+i+'.png');      

            };
            var car360 = $(".car360").rollerblade({
                imageArray: imgList,
                sensitivity: 10,
                drag: true,
                auto: false
            });

        },
        Comparison: function(reload){
            if( false == configurator && reload == false){
            var slider = $('.slider-comparison-car');
            var slider_comparison_car = $('.slider-comparison-car').bxSlider({
                controls: false,
                pager: false,
                autoHover: true,
                adaptiveHeight: true,
                mode: 'horizontal',
                touchEnabled: false,
                onSlideBefore: function (currentSlideNumber, totalSlideQty, currentSlideHtmlObject) {
                    $('.active-slide').removeClass('active-slide');
                    $('.grid-load').show()
                    slider.find('li.item-slide').eq(currentSlideHtmlObject + 1).addClass('active-slide');
                    if( gallery_reset == true  && reload == false)
                        SetCarBodyData();
                },
                onSliderLoad: function () {
                    slider.find('li.item-slide').eq(1).addClass('active-slide');
                    if( gallery_reset == true && reload == false && allowload == true ){
                        SetCarBodyData();
                    }
                },
            });
            if ( $('.slider-price-car').length > 0 ){
                var slider_price_car = $('.slider-price-car').bxSlider({
                    controls: false,
                    pager: false,
                    autoHover: true,
                    adaptiveHeight: true,
                    mode: 'horizontal',
                    touchEnabled: false,
                });
            }
            if ( $('.slider-competitors').length > 0 ){
                var slider_competitors = $('.slider-competitors').bxSlider({
                    controls: false,
                    pager: false,
                    autoHover: true,
                    adaptiveHeight: false,
                    mode: 'fade',
                    touchEnabled: false
                });
            }
            if ( $('.slider-features-car').length > 0 ){
                var slider_features_car = $('.slider-features-car').bxSlider({
                    controls: false,
                    pager: false,
                    autoHover: true,
                    adaptiveHeight: true,
                    mode: 'fade',
                    touchEnabled: false
                });
            }
            if ( $('.slider-offers-car').length > 0 ){
            var slider_offers_car = $('.slider-offers-car').bxSlider({
                controls: false,
                pager: false,
                autoHover: true,
                adaptiveHeight: true,
                mode: 'fade',
                touchEnabled: false
            });
            }

            $('.control-next-slide').click(function(e){

                if( $(this).hasClass('main-slider') == false ){

                    if( $(this).hasClass('cnfg') == false ){
                        e.preventDefault();
                        slider_comparison_car.goToNextSlide();
                        if( slider_price_car != undefined )
                            slider_price_car.goToNextSlide();
                        if( slider_features_car != undefined )
                            slider_features_car.goToNextSlide();
                        if( slider_offers_car != undefined )
                            slider_offers_car.goToNextSlide();
                        if( slider_competitors != undefined )
                            slider_competitors.goToNextSlide();
                    }
                }
            });
            $('.control-prev-slide').click(function(e){
                if( $(this).hasClass('main-slider') == false ){
                    // if( gallery_reset == true )
                    //     SetCarBodyData();
                    if( $(this).hasClass('cnfg') == false ){
                        e.preventDefault();
                        slider_comparison_car.goToPrevSlide();
                        if( slider_price_car != undefined )
                            slider_price_car.goToPrevSlide();
                        if( slider_features_car != undefined )
                            slider_features_car.goToPrevSlide();
                        if( slider_offers_car != undefined )
                            slider_offers_car.goToPrevSlide();
                        if( slider_competitors != undefined )
                            slider_competitors.goToPrevSlide();
                    }
                }
            });
            $("section.block-comparison .features-block-inner").mCustomScrollbar({
                axis:"x",
                theme:"light-3"

            });
            }
            $(window).keydown(function (e) {
                //console.log('_'+e);
                if (!e.ctrlKey || e.keyCode > 40 || e.keyCode < 37) return;
                switch (e.keyCode){
                    case 37:
                        slider_comparison_car.goToPrevSlide();
                        slider_features_car.goToPrevSlide();
                        break;
                    case 39:
                        slider_comparison_car.goToNextSlide();
                        slider_features_car.goToNextSlide();
                        break;
                }
                e.preventDefault();
            });

        },
        MainSlider: function(){
            if( $('.slider-main').length > 0 ){
            var sliders_main;
            $('.slider-main').each(function(i)
            {
                var indexSlider = i;
                sliders_main = $(this).bxSlider({
                    controls: false,
                    auto: true,
                    autoHover: true,
                    adaptiveHeight: true,
                    mode: 'fade',
                    pager: false
                });
                
                $('.control-next-slide').click(function(e){
                    if( $(this).hasClass('main-slider') == true ){
                        e.preventDefault();
                        sliders_main.goToNextSlide();
                        sliders_main.stopAuto();
                    }
                });
                $('.control-prev-slide').click(function(e){
                    if( $(this).hasClass('main-slider') == true ){
                        e.preventDefault();
                        sliders_main.goToPrevSlide();
                        sliders_main.stopAuto();
                    }
                })
            })
            /*var slider_main = $('.slider-main').bxSlider({
                controls: false,
                auto: true,
                autoHover: true,
                adaptiveHeight: true,
                mode: 'fade'

            });

            $('.control-next-slide').click(function(e){
                e.preventDefault();
                slider_main.goToNextSlide();
                slider_main.stopAuto();
            });
            $('.control-prev-slide').click(function(e){
                e.preventDefault();
                slider_main.goToPrevSlide();
                slider_main.stopAuto();
            })*/
        }

        },
        Custom_elements: function(){
            $('.custom-checkbox input[type="checkbox"], .custom-select select').styler();
            $('.custom-radio input[type="radio"]').styler();
        },
        Tooltip: function(){
            /*$('.have-tooltip').tooltip();*/
            /*$('.have-tooltip').tooltip({
                animated : 'fade',
                placement : 'mouse',
                container: 'body'
            });*/

            Tipped.create('.have-tooltip',
                {
                    position: 'bottom'
                }
            );

            setTimeout(function(){
                Tipped.create('.bx-pager-item .bx-pager-link', 'Показать слайд', {
                    position: 'bottom'
                });
            }, 3000)

        },
        InputForms: function(){
            $('.material-input input, .material-input textarea').blur(function() {
                if ($(this).val())
                    $(this).addClass('used');
                else
                    $(this).removeClass('used');
            });
        },
        LoadMap: function(){
            var Map = {
                style: [{"featureType":"all","stylers":[{"saturation":0},{"hue":"#e7ecf0"}]},{"featureType":"road","stylers":[{"saturation":-70}]},{"featureType":"transit","stylers":[{"visibility":"off"}]},{"featureType":"poi","stylers":[{"visibility":"off"}]},{"featureType":"water","stylers":[{"visibility":"simplified"},{"saturation":-60}]}],

                offices: offs,

                init: function () {
                    var mapOptions = {
                        center: new google.maps.LatLng(generalL, generalW),
                        zoom: 9,
                        // mapTypeId: google.maps.MapTypeId.ROADMAP,

                        scrollwheel: false,
                        zoomControl: true,
                        panControl: false,
                        mapTypeControl: true, //пока что так
                        scaleControl: false,
                        streetViewControl: false,
                        overviewMapControl: false,
                        zoomControlOptions: {
                            style: google.maps.ZoomControlStyle.SMALL,
                            position: google.maps.ControlPosition.RIGHT_BOTTOM
                        }

                    };
                    defaultmap.regionOptions = mapOptions;
                    map = new google.maps.Map(document.getElementById('map-offices'), mapOptions);

                    var usRoadMapType = new google.maps.StyledMapType(Map.style);
                    map.mapTypes.set('usroadatlas', usRoadMapType);
                    map.setMapTypeId('usroadatlas');
                    var infoboxes= {};

                    
                    $.each(Map.offices, function (i, o) {
                        var marker = new MarkerWithLabel({
                            position: new google.maps.LatLng(o.latLng[0], o.latLng[1]),
                            map: map,
                            title: o.title,
                            labelContent: "<div data-id='"+$(this)[0].button+"' class='map-labels-inner of"+i+"'>"+o.title+"</div>",
                            labelAnchor: new google.maps.Point(0, 5),
                            labelClass: "c-map-labels "+i, // the CSS class for the label
                            labelStyle: {opacity: 0.75},
                            icon: '.'
                        });
                        var aid = $(this)[0].button;
                        infoboxes[aid] = new InfoBox({
                            content: document.getElementById(aid),
                            alignBottom: true,
                            disableAutoPan: false,
                            maxWidth: 0,
                            pixelOffset: new google.maps.Size(-190, -35),
                            zIndex: null,
                            closeBoxMargin: "7px 7px 2px 2px",
                            closeBoxURL: "/front_assets/img/modal-close.png",
                            infoBoxClearance: new google.maps.Size(1, 1)
                        });

                        google.maps.event.addListener(marker, 'click', function () {
                            map.setZoom(15);
                            map.setCenter(marker.getPosition());
                            infoboxes[aid].open(map, this);
                            $('.sendinfo').removeClass('disabled');
                            $('.sendinfo').removeClass('disabled');
                            $('.submitrequest').removeClass('disabled');
                            Tipped.remove('.ttp');
                            $('.sendinfo').parent().removeAttr('title');
                            $('.submitrequest').parent().removeAttr('title');
                        });
                        google.maps.event.addListenerOnce(map, 'idle', function(){
                            $('.map-labels-inner').hide()
                            $('.c-map-labels').hover(function(){
                              var cel = $(this).attr('class');
                              var cc = $.trim(cel.replace('c-map-labels',''));             
                              $('.map-labels-inner.of'+cc).show();
                             
                              
                            },function(){
                              var cel = $(this).attr('class');
                              var cc = $.trim(cel.replace('c-map-labels',''));             
                                $('.map-labels-inner.of'+cc).hide(); 
                            }
                            );
                        });
                        simulates[aid] = marker;

                        $(o.button).click(function () {
                            map.setZoom(15);
                            map.setCenter(marker.getPosition());
                            $('.sendinfo').addClass('disabled');
                            $('.sendinfo').parent().addClass('have-tooltip').attr('title','Выберите дилерский центр');
                            $('.submitrequest').addClass('disabled');
                            $('.submitrequest').parent().addClass('have-tooltip').attr('title','Выберите дилерский центр');
                            Tipped.create('.ttp');
                        });
                    });


                        $('body').on('modal-open', function(){
                            setTimeout(function(){
                              google.maps.event.trigger(map, 'resize');
                            }, 100)
                        });
                        $(document).on('click',function(e){
                            var el = $(e.target);
                            if( el.attr('class') == 'ui-menu-item' && $('#map').length > 0 && el.parents('#ui-id-1').length > 0 ){
                                $.each(infoboxes,function(k,ib){
                                    ib.close();
                                    $('.sendinfo').addClass('disabled');
                                    $('.sendinfo').parent().addClass('have-tooltip').attr('title','Выберите дилерский центр');
                                    Tipped.create('.ttp');
                                    
                                })
                            }
                        })
                        $('#dealers-all').on('click',function(){
                            $.each(infoboxes,function(k,ib){
                                ib.close();
                                $('.sendinfo').addClass('disabled');
                                $('.sendinfo').parent().addClass('have-tooltip').attr('title','Выберите дилерский центр');
                                Tipped.create('.ttp');

                            })
                        })
                    /*< Geolocation >*/
                    $('a.btn_geoposition').click(function(e){
                        e.preventDefault();
                        getMyPosition();
                    });
                    function getMyPosition() {
                        if(navigator.geolocation) {
                            navigator.geolocation.getCurrentPosition(function(position) {
                                var pos = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
                                var dataAjax = { 'latitude':position.coords.latitude,'longtitude':position.coords.longitude };
                                getNearest(dataAjax);
                                var infowindow = new google.maps.InfoWindow({
                                    map: map,
                                    position: pos,
                                    content: '<div style="white-space: nowrap;">Вы тут</div>',
                                    maxWidth: 320,
                                 });

                                map.setCenter(pos);
                                map.setZoom(13);
                            }, function() {
                                handleNoGeolocation(true);
                            });
                        } else {
                            handleNoGeolocation(false);
                        }
                    }

                    function handleNoGeolocation(errorFlag) {
                        if (errorFlag) {
                            var content = '<div style="white-space: nowrap;">Error: The Geolocation service failed.</div>';
                        } else {
                            var content = '<div style="white-space: nowrap;">Error: Your browser doesn\'t support geolocation.</div>';
                        }

                        var options = {
                            map: map,
                            position: new google.maps.LatLng(54.185958, 45.232782),
                            content: content,
                        };

                        var infowindow = new google.maps.InfoWindow(options);
                        map.setCenter(options.position);
                    }
                    /*</ Geolocation >*/
                        $('.map-labels-inner').hide()

                }
            };
            // Map
            if ($('#map-offices').length) {
                google.maps.event.addDomListener(window, 'load', Map.init);
            }
            $( "#dealer-centre-name" ).keyup(function(event){
                if(event.keyCode == 13){
                    $("#dealers-single").click();
                }
            });
                setTimeout(function(){
                if( switchdc != false ){
                    simulatePlacemark(switchdc);
                }
                },1000)
        },
        Tabs: function(){
            $('.accordion').on('show', function (e) {
                $(e.target).prev('.accordion-heading').find('.accordion-toggle').addClass('active');
            });

            $('.accordion').on('hide', function (e) {
                $(this).find('.accordion-toggle').not($(e.target)).removeClass('active');
            });

            $('.panel-group').on('hide.bs.collapse', function (e) {
                $(e.target).parent().find('.panel-heading').toggleClass('active');
            })
            $('.panel-group').on('show.bs.collapse', function (e) {
                $(e.target).parent().find('.panel-heading').toggleClass('active');
            });

            $('.collapse-heading').click(function(){
                $(this).toggleClass('active').next().slideToggle().toggleClass('opened');

                return false;
            });
        },
        
        ProgressBar: function(){
            $('.progress .progress-bar').progressbar(
                {
                    transition_delay: 1000,
                    use_percentage: false,
                    amount_format: function(amount_part, amount_total) { return amount_part; },
                    display_text: 'fill'
                }
            );
            $('.progress .progress-bar').progressbar({
                display_text: 'center',
                use_percentage: false,
                amount_format: function (p, max, min) {
                    return 'min: ' + min + ' current: ' + p + ' max: ' + max;
                }
            });
        },
        CreditCalculator: function(){
            if( calculatorOn != false ){
                //console.log(fpayMin,fpayMax)
            $('#pre-pay').noUiSlider({
                connect: "lower",
                start: [ fpay ],
                step: 1,
                range: {
                    'min': [  fpayMin ],
                    'max': [ fpayMax ]
                }
            });
            $('#pre-pay').Link('lower').to($('#pre-pay-val'), null, wNumb({
                mark: '.',
                thousand: ' ',
                prefix: '',
                postfix: '.-',
                decimals: 0
            }));
            $('#pre-pay').on('slide', function(){
                changeDatainOffers(true)
            });
            $('#month-pay').on('slide', function(){
                changeDatainOffers(false)
            });

            $('#month-pay').noUiSlider({
                connect: "lower",
                start: [ fperiod ],
                step: fstep,
                range: {
                    'min': [  fperiodMin ],
                    'max': [ fperiodMax ]
                }
            });
            $('#month-pay').Link('lower').to($('#month-pay-val'), null, wNumb({
                decimals: 0
            }));
        }
        }

    }

    $(function() {
        App.init();
    });

	confy = new Configurator();
});

//Объект для работы конфигуратора
function Configurator() {
	//Список групп, чтобы по активной группе накладывать фильтр только по предыдущим группам
	this.groupList = {
		0: {
			'code': 'korob',
			'name': 'Коробка передач',
			'prev': null,
			'next': 1
		},
		1: {
			'code': 'dvig',
			'name': 'Двигатель',
			'prev': 0,
			'next': 2
		},
		2: {
			'code': 'safe',
			'name': 'Безопасность',
			'prev': 1,
			'next': 3
		},
		3: {
			'code': 'komfort',
			'name': 'Комфорт',
			'prev': 2,
			'next': 4
		},
		4: {
			'code': 'ext',
			'name': 'Экстерьер',
			'prev': 3,
			'next': 5
		},
		5: {
			'code': 'int',
			'name': 'Интерьер',
			'prev': 4,
			'next': null
		}		
	}
	
	//Привязка событий к инпутам
	this.bind = function() {
		
	}

	//Поиск комплектации по заданным параметрам. Выдаёт имя комплектации в виде модель-исполнение-комплектация
	this.findcomplect = function(params) {
		
	}

	this.interceptProcecssing = function() {
		//Проходим все взаимоисключающие чекбоксы и группируем ихtab-pane
		var objects = $('#all .jq-radio[data-intercept]');
		$.each(objects, function(n, obj) {
			var groupid = $(obj).data('intercept');
			var recipient = $('#all .jq-radio[data-intercept='+groupid+']:first').closest('.option', $('#all'));
			var oldparent = $(obj).closest('.option', $('#all'));
			var donor = $(obj).closest('.custom-radio');
			donor.appendTo(recipient);
		});

		//Убираем опустевшие блоки
		$.each($('#all .option'), function(n, obj) {
			if (!$(obj).find('.jq-checkbox, .jq-radio').length)
				$(obj).remove();
		});
	}

	//Функция заполнения правого списка из выбранных характеристик слева
	//Вызывается при смене характеристик кроме коробки передач и двигателя
	this.fillOverallList = function() {
		var shown = [];		//Отображённые группы, чтобы второй раз заголовок не писать
		$('.configurator-infotable table tr').remove();
		var list = $('#all :radio[data-intercept]:checked, #all :checkbox:checked');
		for (var np = 0; np < list.length; np++)
		{
			if (shown.indexOf($(list[np]).closest('.tab-pane')[0].id) == -1)
			{
				$('.configurator-infotable table').append('<tr class="caption"><td>' + this.CodeToName($(list[np]).closest('.tab-pane')[0].id) + '</td></tr>');
				shown.push($(list[np]).closest('.tab-pane')[0].id);
			}
			$('.configurator-infotable table').append('<tr><td>' + $(list[np]).data('name') + '</td></tr>');
		}
	}

	//Преобразование кода раздела в читаемое имя
	this.CodeToName = function(code) {
		for (var _ in this.groupList)
			if (this.groupList[_].code == code)
				return this.groupList[_].name;
	}
}