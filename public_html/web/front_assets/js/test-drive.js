//С помощью line_calendat.getValue() можно получить строковое значнеие выбранной даты.
//В параметр функции init передаётся идентификатор DOM-объекта с календарём, с которым должен работать конкретный объект

function Cline_calendar() {
	this.firstWeekDate = null;		//Первый день отображаемой недели (объект Date)

	this.selectedDate = null;		//Выбранный день
	this.selectedMonth = null;		//Месяц
	this.selectedYear = null;		//Год
	this.selectedDaypart = null;	//Выбранное время суток

	this.todayDate = null;
	this.todayMonth = null;
	this.todayYear = null;
	this.todayDOW = null;

	this.page = 0;			//Текущая страница календаря
	this.max_page = 0;		//Количество страниц (максимальный номер страницы), если page достигнет этого значения, то листалка добавит ещё 7 элементов к календарю

	this._weekdays = ['пн', 'вт', 'ср', 'чт', 'пт', 'сб', 'вс'];
	this._monthes = ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'];
	this._dayparts = ['Утро', 'День', 'Вечер'];

	//Выдача выбранного пользователем значения (или сегодняшнего дня, если он ничего не выбрал)
	this.getValue = function() {return this.selectedDate + '.' + (1 + parseInt(this.selectedMonth)) + '.' + this.selectedYear + ', ' + this._dayparts[this.selectedDaypart-1]};

	//Начальные настройки
	this.init = function(blockClass)
	{
		//Класс или идентификатор, определяющий календарь, с которым работать каждый экземпляр этого объекта
		this.mainblock = blockClass;
		
		var d = new Date();
		if (d.getHours() >= 18)
			d.setTime(d.getTime() + 6000*3600);
		this.todayYear	= this.selectedYear		= d.getFullYear();
		this.todayMonth	= this.selectedMonth	= d.getMonth();
		this.todayDate	= this.selectedDate		= d.getDate();
		this.todayDOW = d.getDay();		//Сегодняшний день недели. 0 - воскресенье
		if (!this.todayDOW) this.todayDOW = 7;

		this.selectedDaypart = 1;	//По умолчанию поставим утро;
		//************
		
		this.firstWeekDate = new Date(this.todayYear, this.todayMonth, this.todayDate);
		this.firstWeekDate.setTime(this.firstWeekDate.getTime() - (this.todayDOW-1)*3600*24000);		//Инициализируем дату первого дня отображаемой денели

		//Создаём блок с днями недели
		var dwks = '<div class="cell day-of-week"></div>';
		for (var dwk = 0; dwk < 7; dwk++)
			dwks += '<div class="cell day-of-week">'+this._weekdays[dwk]+'</div>';
		dwks += '<div class="cell day-of-week"></div>';
		$(dwks).insertAfter($('.calendar_month-year', this.mainblock));
		
		//Создаём блоки для первой недели
		$('.calendar_slider', this.mainblock).append('<div class="cell day"></div><div class="cell day"></div><div class="cell day"></div><div class="cell day"></div><div class="cell day"></div><div class="cell day"></div><div class="cell day"></div>');


		this.fillDates();	//Формируем первую отображаемую неделю
		this.showCurrentMonth();
		this.bindHover('.cell.day');

		var parent = this;
		$('.arrow.lar', this.mainblock).on('click', function() {if (!$(this).hasClass('disabled')) parent.slideLeft()});
		$('.arrow.rar', this.mainblock).on('click', function() {if (!$(this).hasClass('disabled')) parent.slideRight()});
	};

	//Заполнение всех имеющихся полей с датами
	this.fillDates = function()
	{
		var fromYear = this.todayYear;		//Эта дата представляет собой первую неделю, от коротой строится календарь
		var fromMonth = this.todayMonth;
		var toDay = this.todayDate;
		var dw = this.todayDOW;

		//Посчитаем день, с которого будем рисовать наш календарь
		var tmpdate = new Date(fromYear, fromMonth, toDay);
		tmpdate.setTime(tmpdate.getTime() - (dw-1)*3600*24000);

		var day_cells = $('.day', this.mainblock);
		var parent = this;
		day_cells.each(function() {
			var fromDay = tmpdate.getDate();
			fromMonth = tmpdate.getMonth();
			fromYear = tmpdate.getFullYear();
			tmpdate.setTime(tmpdate.getTime() + 1000*3600*24);		//Прибавляем сутки для следующей итерации			
			//-------------------------------
			
			if (fromYear < parent.todayYear || (fromMonth < parent.todayMonth && fromYear == parent.todayYear))
				$(this).addClass('disabled');	//Всё, что в прошлом, деактивируем

			else if (fromYear == parent.todayYear && fromMonth == parent.todayMonth && fromDay < parent.todayDate)
				$(this).addClass('disabled');	//Повторяем процедуру для текущего месяца, но меньше сегодняшнего дня
				
			if (fromDay == parent.selectedDate && fromMonth == parent.selectedMonth && fromYear == parent.selectedYear)
				$(this).addClass('active');		//Сегодняшний день
	
			$(this).text(fromDay);
			this.setAttribute('data-month', fromMonth);
			this.setAttribute('data-year', fromYear);
			if (!$(this).hasClass('disabled'))
			{
				$(this).on('click', function() {
					parent.selectedDate = $(this).text();
					parent.selectedMonth = this.getAttribute('data-month');
					parent.selectedYear = this.getAttribute('data-year');
					$('.day.active', this.mainblock).removeClass('active').css({'background-color' : '', 'color' : ''});
					$(this).addClass('active');
				});
			}
		});
	};

	//Подстановка в заголовок календаря месяца и года отображаемой недели
	this.showCurrentMonth = function () {$('.calendar_month-year', this.mainblock).text(this._monthes[this.firstWeekDate.getMonth()] + ' ' + this.firstWeekDate.getFullYear())};

	//События при наведении мыши и щелчке
	this.bindHover = function(styles)
	{
		var parent = this;
		$(styles, this.mainblock).off('mouseenter mouseleave').css({'background': '', 'color': ''});
		$(styles + ':not(.disabled)', this.mainblock).hover(
		function() {
			//Выводим месяц и год выбранной даты
			if (!$(this).hasClass('arrow'))
			{
				if (parent.headerTimer) clearTimeout(parent.headerTimer);
				$('.calendar_month-year', this.mainblock).text(parent._monthes[this.getAttribute('data-month')] + ' ' + this.getAttribute('data-year'));
			}

			if (!$(this).hasClass('active'))
				$(this).css({'background-color': '#0051A1', 'color': 'white'});
		},
		//----------
		function() {
			if (!$(this).hasClass('arrow'))
				parent.headerTimer = setTimeout(function() {parent.showCurrentMonth();}, 200);
				
			if (!$(this).hasClass('active'))
				$(this).css({'background-color': 'white', 'color': '#0051A1'});
		});
	};

	//Выбор времени суток
	this.setDaypart = function(value) {this.selectedDaypart = value};

	//Листалка влево
	this.slideLeft = function()
	{
		var slider = $('.calendar_slider', this.mainblock);
		var width = slider.parent().width();
		if (this.page > 0)
		{
			slider.css('left', -width * (this.page - 1) + 'px');
			this.firstWeekDate.setTime(this.firstWeekDate.getTime() - 3600*24*7000);
			this.showCurrentMonth();
		}
		if (--this.page == 0)
			$('.arrow.lar', this.mainblock).addClass('disabled');
		this.bindHover('.arrow.lar');
	};

	//Листалка вправо
	this.slideRight = function()
	{
		var slider = $('.calendar_slider', this.mainblock);
		$('.arrow.lar', this.mainblock).removeClass('disabled');
		var width = slider.parent().width();

		if (++this.page > this.max_page)
		{
			//Добавим ещё одну неделю в список
			$('.day', slider).off('click');
			slider.append('<div class="cell day"></div><div class="cell day"></div><div class="cell day"></div><div class="cell day"></div><div class="cell day"></div><div class="cell day"></div><div class="cell day"></div>');
			this.fillDates();
			this.bindHover('.cell.day');
			this.max_page = this.page;
		}
		slider.css('left', -width * this.page + 'px');
		this.firstWeekDate.setTime(this.firstWeekDate.getTime() + 3600*24*7000);
		this.showCurrentMonth();
	};
}

line_calendar = new Cline_calendar();

$(function() {
	line_calendar.init('.line_calendar');
})