$(function() {
	$('#tech').scroll(function() {
		$('.panel-group .panel .panel-collapse .panel-body').each(function() {
			this.scrollLeft = $('#tech')[0].scrollLeft;
		});
	});

	$('.panel-group .panel .panel-collapse .panel-body').scroll(function() {
		var pos = this.scrollLeft;
		$('#tech').each(function() {this.scrollLeft = pos;});
	});

	complectCheck();
});

$(window).load(function(){
    EqualHeight();



	$('body').on('car-ajax-response', function(){
	    EqualHeight();
	});
});

function EqualHeight() {
	var maxHeight = 0;
	$('.equalheight').each(function(){  
		var $th = $(this);

		$th.attr('style', '')
		var itemHeight = $th.height();

		if( itemHeight > maxHeight ) {
			maxHeight = itemHeight;
		}
	});
	$('.equalheight').parents('.equalheightWrap').find('.equalheight').css({'height':maxHeight});
}
function complectCheck () {
	if ($('.complect-select .colmplect-block').length > 0) {
		$('.complect-select').each(function() {
			var select = this;
			$.each($('.colmplect-block', select), function(n, blk) {
				var inp = $('input[type=radio]', blk).get(0);
				if ($(inp).attr('checked') == 'checked')
				{
					$(blk).addClass('active');
					return false;
				}
			});
		});

		$('.complect-select .colmplect-block input[type=radio]').on('change', function() {
			$(this).parents('.complect-select').find('.colmplect-block').removeClass('active');
			$(this).parent().addClass('active');
		});

		$('.complect-select .colmplect-block').click(function() {
			$(this).find('input').prop('checked', true).trigger('change');
			return false;
		});

		var lis = $('.nav > li.typecar');
		if (!lis.filter('.active').length)
			$(lis[0]).addClass('active');
	}
}